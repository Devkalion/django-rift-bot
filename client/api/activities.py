from httpx import Response

from .base import APIClient, API_URL


class ActivityAPIClient(APIClient):
    url = f'{API_URL}/activities'

    async def start(self, codename: str, user_id):
        response: Response = await self.post(f'{self.url}/{codename}/start/', data={
            'user_id': user_id,
        })
        return response.json()
