from typing import Mapping

from httpx import Response, AsyncClient, codes, Timeout

from conf import API_URL


class APIException(Exception):
    def __init__(self, response: Response):
        self.status = response.status_code
        try:
            self.data: Mapping = response.json()
        except:
            self.data: str = response.text
        super().__init__(self.data)


class CriticalException(Exception):
    def __init__(self, response: Response):
        self.status = 500
        super().__init__(response.request.method, response.request.url)


class APIClient(AsyncClient):
    url = API_URL

    def __init__(self, **kwargs):
        if 'timeout' not in kwargs:
            kwargs['timeout'] = Timeout(None, connect=5.0)
        if 'follow_redirects' not in kwargs:
            kwargs['follow_redirects'] = True
        super().__init__(**kwargs)

    async def request(self, *args, **kwargs):
        response: Response = await super().request(*args, **kwargs)
        if codes.is_server_error(response.status_code):
            raise CriticalException(response)
        elif codes.is_client_error(response.status_code):
            raise APIException(response)
        return response
