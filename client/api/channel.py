import logging
from typing import Optional

from .base import APIClient, APIException, API_URL


class PersonalChannelAPIClient(APIClient):
    url = f'{API_URL}/personal_channels'

    async def set_channel_id(self, channel_id: int, discord_channel_id: int):
        data = {
            'channel_id': discord_channel_id
        }
        await self.patch(f'{self.url}/{channel_id}/', data=data)

    async def get_channels(self, user_id: Optional[int] = None):
        params = {}
        if user_id:
            params['user_id'] = user_id
        response = await self.get(f'{self.url}/', params=params)
        return response.json()

    async def create_channel_member(self, channel_id: int, user_id: int, inviter_id: int):
        payload = {
            'channel': channel_id,
            'user': user_id,
            'inviter': inviter_id,
        }
        try:
            await self.post(f'{self.url}/members/', data=payload)
        except APIException as e:
            logging.error('Channel member creation error', exc_info=e)

    async def delete_channel_member(self, channel_id: int, user_id: int):
        payload = {
            'channel_id': channel_id,
            'user_id': user_id,
        }
        await self.delete(f'{self.url}/members/', params=payload)
