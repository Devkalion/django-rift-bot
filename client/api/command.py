from httpx import Response

from .base import APIClient, API_URL, APIException


class CommandsAPIClient(APIClient):
    url = f'{API_URL}/commands'

    async def get_command(self, command: str):
        try:
            response: Response = await self.get(f'{self.url}/', params={
                'command': command
            })
            return response.json()
        except APIException as e:
            if e.status == 404:
                return None
            raise


class SlashCommandsAPIClient(APIClient):
    url = f'{API_URL}/slash_commands/'

    async def get_commands(self):
        response: Response = await self.get(self.url)
        return response.json()
