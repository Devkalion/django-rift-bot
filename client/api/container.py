from httpx import Response

from .base import APIClient, API_URL


class ContainersAPIClient(APIClient):
    url = f'{API_URL}/containers'

    async def find(self, form, value):
        response: Response = await self.get(f'{self.url}/', params={
            'form': form,
            'value': value
        })
        data: list = response.json()
        if data:
            return data[0]
        else:
            return None

    async def retrieve(self, container_id: str) -> dict:
        response: Response = await self.get(f'{self.url}/{container_id}/')
        return response.json()

    async def open(self, user_id, container_id, count):
        req_data = {
            'user_id': user_id,
            'count': count
        }
        response: Response = await self.post(f'{self.url}/{container_id}/open/', json=req_data)
        return response.json()
