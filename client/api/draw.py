from .base import APIClient, API_URL, APIException


class DrawAPIClient(APIClient):
    url = f'{API_URL}/draws'

    async def set_msg_id(self, draw_id, msg_id):
        response = await self.patch(f'{self.url}/{draw_id}/', json={
            'msg_id': msg_id,
        })
        return response.json()

    async def participate(self, channel_id, msg_id, user_id):
        try:
            await self.post(f'{self.url}/participate/', params={
                'channel_id': channel_id,
                'msg_id': msg_id,
            }, json={
                'user_id': user_id,
            })
        except APIException as e:
            if e.status == 404:
                return
            raise
