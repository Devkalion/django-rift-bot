import asyncio
from io import BytesIO
from typing import Iterable, Sequence
from urllib.parse import urlparse

import httpx
from discord import ButtonStyle, ComponentType, Embed, SelectOption, File
from discord.ui import Button, Select, View
from httpx import Response

from .base import APIClient, API_URL

button_styles = {
    ButtonStyle.primary.value: ButtonStyle.primary,
    ButtonStyle.secondary.value: ButtonStyle.secondary,
    ButtonStyle.success.value: ButtonStyle.success,
    ButtonStyle.danger.value: ButtonStyle.danger,
    ButtonStyle.link.value: ButtonStyle.link,
}


def get_components(components: list):
    component: dict
    for component in components:
        component_type = component.pop('type')
        if component_type == ComponentType.action_row.value:
            yield from get_components(component['components'])
        elif component_type == ComponentType.button.value:
            style = component.pop('style')
            yield Button(**component, style=button_styles.get(style, ButtonStyle.secondary))
        elif component_type == ComponentType.select.value:
            options = component.pop('options')
            yield Select(**component, options=[
                SelectOption(**option)
                for option in options
            ])


async def get_file(url: str) -> File:
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
    filename = urlparse(url).path.split('/')[-1]
    return File(BytesIO(response.content), filename=filename)


async def get_files(file_urls: Iterable[str]) -> Sequence[File]:
    return await asyncio.gather(*[
        get_file(url)
        for url in file_urls
    ])


class EmbedAPIClient(APIClient):
    url = f'{API_URL}/embeds'

    @classmethod
    async def class_render_embed(cls, name, data, allow_ephemeral=False):
        async with cls() as api_client:
            return await api_client.render_embed(name, data, allow_ephemeral)

    async def render_embed(self, name, data, allow_ephemeral=False):
        req_data = {
            'user_data': data
        }
        response: Response = await self.post(f'{self.url}/{name}/render/', json=req_data)
        msg_data = response.json()
        if 'embed' in msg_data:
            msg_data['embed'] = Embed.from_dict(msg_data['embed'])
        if 'components' in msg_data:
            view = View()
            for component in get_components(msg_data.pop('components')):
                view.add_item(component)
            msg_data['view'] = view
        if not allow_ephemeral:
            msg_data.pop('ephemeral', None)
        if 'file_urls' in msg_data:
            msg_data['files'] = await get_files(msg_data.pop('file_urls'))
        return msg_data
