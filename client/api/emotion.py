from httpx import Response, codes

from .base import APIClient, API_URL, APIException


class EmotionAPIClient(APIClient):
    url = f'{API_URL}/emotions'

    async def get_emotion(self, emotion_id: int, user_id: int):
        try:
            response: Response = await self.get(f'{self.url}/{emotion_id}/', params={
                'user_id': user_id
            })
        except APIException as e:
            if e.status == codes.NOT_FOUND:
                return None
            raise
        return response.json()
