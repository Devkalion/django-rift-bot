from httpx import Response

from data_classes import Event
from .base import APIClient, API_URL


class EventAPIClient(APIClient):
    url = f'{API_URL}/events'

    async def get_events(self):
        response: Response = await self.get(f'{self.url}/')
        data: dict = response.json()
        return [event['id'] for event in data]

    async def update_status(self, event_id, new_status):
        data = {'status': new_status}
        return await self.patch(f'{self.url}/{event_id}/', data=data)
