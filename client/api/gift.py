from httpx import Response

from .base import APIClient, API_URL


class GiftAPIClient(APIClient):
    url = f'{API_URL}/gifts'

    async def gift(self, sender_id, recipient_id, gift_id):
        response: Response = await self.post(f'{self.url}/{gift_id}/gift/', data={
            'user_id': sender_id,
            'to_user_id': recipient_id
        })
        return response.json()['reward']
