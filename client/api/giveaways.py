from .base import APIClient, API_URL


class GiveawayAPIClient(APIClient):
    url = f'{API_URL}/giveaways'

    async def claim(self, user_id):
        response = await self.post(f'{self.url}/claim/', json={
            'user_id': user_id,
        })
        return response.json()


class EncouragementAPIClient(APIClient):
    url = f'{API_URL}/encouragement'

    async def get_encouragement(self, encouragement_id):
        response = await self.get(f'{self.url}/{encouragement_id}/')
        return response.json()
