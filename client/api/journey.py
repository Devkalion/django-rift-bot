from httpx import Response

from .base import APIClient, API_URL


class JourneyAPIClient(APIClient):
    url = f'{API_URL}/journeys'

    async def start(self, index_number, user_id):
        response: Response = await self.post(f'{self.url}/{index_number}/start/', data={
            'user_id': user_id,
        })
        return response.json()['to_date']
