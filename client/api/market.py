from httpx import Response

from .base import APIClient, API_URL


class MarketAPIClient(APIClient):
    url = f'{API_URL}/market'

    async def list_sales(self):
        response: Response = await self.get(f'{self.url}/sales/')
        return response.json()

    async def list_exchanges(self):
        response: Response = await self.get(f'{self.url}/exchanges/')
        return response.json()

    async def sale(self, index_number, user_id):
        response: Response = await self.post(f'{self.url}/sales/{index_number}/sale/', data={
            'user_id': user_id,
        })
        return response.json()['reward']

    async def exchange(self, index_number, user_id):
        response: Response = await self.post(f'{self.url}/exchanges/{index_number}/exchange/', data={
            'user_id': user_id,
        })
        return response.json()['reward']
