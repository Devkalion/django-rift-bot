from .base import APIClient, API_URL


class UserMessageAPIClient(APIClient):
    url = f'{API_URL}/user_messages'

    async def create_message(self, user_id, recipient_id, text):
        data = {
            'user': user_id,
            'recipient': recipient_id,
            'text': text,
        }
        response = await self.post(f'{self.url}/', data=data)
        return response.json()
