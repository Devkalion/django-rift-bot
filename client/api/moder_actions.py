from datetime import datetime, timedelta
from typing import Optional

from conf import LOCAL_TZ
from .base import APIClient, API_URL


class ModerActionAPIClient(APIClient):
    url = f'{API_URL}/moder_actions'

    async def get_actions(self, user_id, _type='mute'):
        response = await self.get(f'{self.url}/', params={
            'type': _type,
            'user': user_id,
        })
        return response.json()['results']

    async def create_action(self, user_id, moder_id, reason, _type, duration: Optional[timedelta] = None):
        data = {
            'user': user_id,
            'moderator': moder_id,
            'reason': reason,
            'type': _type,
        }

        if duration:
            finish_time: datetime = datetime.now(LOCAL_TZ) + duration
            data['finish_time'] = finish_time.isoformat()

        response = await self.post(f'{self.url}/', data=data)
        return response.json()


class AppealAPIClient(APIClient):
    url = f'{API_URL}/appeals'

    async def create_appeal(self, user_id, author_id, reason):
        data = {
            'user': user_id,
            'author': author_id,
            'text': reason,
        }

        response = await self.post(f'{self.url}/', data=data)
        return response.json()
