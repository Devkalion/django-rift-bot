from httpx import Response

from .base import APIClient, API_URL, APIException


class RoleMessageAPIClient(APIClient):
    url = f'{API_URL}/channel'

    async def get_role_message(self, channel_id, message_id):
        try:
            response: Response = await self.get(f'{self.url}/{channel_id}/message/{message_id}/')
            return response.json()
        except APIException as e:
            if e.status == 404:
                return None
            raise

    async def give_reward(self, channel_id, message_id, user_id):
        response: Response = await self.post(f'{self.url}/{channel_id}/message/{message_id}/reward/', json={
            'user_id': user_id
        })
        return response.json()['reward']
