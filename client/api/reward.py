from httpx import Response

from .base import APIClient, API_URL


class RewardsAPIClient(APIClient):
    url = f'{API_URL}/reward'

    async def give_level_reward(self, user_id, level):
        req_data = {
            'user_id': user_id,
            'level': level
        }
        response: Response = await self.post(f'{self.url}/give_level/', json=req_data)
        return response.json()
