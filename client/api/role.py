from httpx import Response, codes

from .base import APIClient, API_URL, APIException


class RoleAPIClient(APIClient):
    url = f'{API_URL}/roles'

    async def get_roles(self):
        response: Response = await self.get(f'{self.url}/')
        return response.json()

    async def get_role(self, role_id: int):
        try:
            response: Response = await self.get(f'{self.url}/{role_id}/')
        except APIException as e:
            if e.status == codes.NOT_FOUND:
                return None
            raise
        return response.json()

    async def get_user_roles(self, user_id: int):
        response: Response = await self.get(f'{API_URL}/user/{user_id}/roles/')
        return response.json()

    async def get_user_role(self, user_id: int, role_id: int):
        try:
            response: Response = await self.get(f'{API_URL}/user/{user_id}/roles/{role_id}/')
        except APIException as e:
            if e.status == codes.NOT_FOUND:
                return None
            raise
        return response.json()

    async def create_role(self, role_id: int, name: str, position: int):
        data = {
            'discord_id': role_id,
            'name': name,
            'description': name,
            'position': position,
        }

        response = await self.post(f'{self.url}/', data=data)
        return response.json()

    async def delete_role(self, role_id):
        await self.delete(f'{self.url}/{role_id}/')

    async def update_role(self, role_id: int, name: str, position: int):
        data = {
            'name': name,
            'position': position,
        }
        await self.patch(f'{self.url}/{role_id}/', data=data)


class PersonalRoleAPIClient(APIClient):
    url = f'{API_URL}/personal_roles'

    async def update_role(self, role_id: int, user_id: int, name: str, color: int):
        data = {}
        if name is not None:
            data['name'] = name
        if color is not None:
            data['color'] = color
        await self.patch(f'{self.url}/{role_id}/', params={
            'user_id': user_id
        }, data=data)

    async def set_discord_role(self, role_id: int, discord_role_id: int):
        await self.patch(f'{self.url}/{role_id}/', data={
            'role': discord_role_id,
        })
