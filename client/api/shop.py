from httpx import Response, codes

from .base import APIClient, API_URL, APIException


class ShopAPIClient(APIClient):
    url = f'{API_URL}/shops'

    async def get_shop(self, command_name=None, buy_name=None):
        params = {}
        if command_name is not None:
            params['command_name'] = command_name
        elif buy_name is not None:
            params['buy_name'] = buy_name
        else:
            raise AssertionError('invalid method call')

        try:
            response: Response = await self.get(f'{self.url}/', params=params)
        except APIException as e:
            if e.status == codes.NOT_FOUND:
                return None
            raise
        data = response.json()
        if not len(data):
            return None
        return data[0]

    async def retrieve(self, shop_id):
        response: Response = await self.get(f'{self.url}/{shop_id}/')
        return response.json()

    async def buy(self, shop_id, shop_item_index_number, user_id):
        response: Response = await self.post(f'{API_URL}/shop_items/{shop_item_index_number}/buy/', data={
            'user_id': user_id,
        }, params={
            'shop_id': shop_id
        })
        return response.json()['reward']

    async def list_items(self, shop_id):
        response: Response = await self.get(f'{API_URL}/shop_items/', params={
            'shop_id': shop_id
        })
        return response.json()
