from httpx import Response

from .base import APIClient, API_URL


class TransformationsAPIClient(APIClient):
    url = f'{API_URL}/transformations'

    async def start_transform(self, transformation_id, user_id):
        response: Response = await self.post(f'{self.url}/{transformation_id}/start/', data={
            'user_id': user_id,
        })
        return response.json()['to_date']
