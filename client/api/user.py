from datetime import datetime
from typing import Iterable, Optional, List, Tuple

from httpx import Response, codes

from data_classes import User, SmallUser
from .base import APIClient, API_URL, APIException


class UserAPIClient(APIClient):
    url = f'{API_URL}/users'

    async def get_user(self, user_id: int) -> Optional[User]:
        try:
            response: Response = await self.get(f'{self.url}/{user_id}/')
        except APIException as e:
            if e.status == codes.NOT_FOUND:
                return None
            raise
        data: dict = response.json()
        return User(**data)

    async def get_users(self, offset=0) -> Tuple[int, List[SmallUser]]:
        response: Response = await self.get(f'{self.url}/', params={'offset': offset})
        json_data = response.json()
        return json_data['count'], [SmallUser(**data) for data in json_data['results']]

    async def add_experience(self, user_id: int, amount: int):
        data = {'amount': amount}
        await self.post(f'{self.url}/{user_id}/add_experience/', data=data)

    async def touch(self, user_id: int):
        await self.post(f'{self.url}/{user_id}/touch/')

    async def add_message(self, user_id: int):
        await self.post(f'{self.url}/{user_id}/add_message/')

    async def create_user(self, user_id: int, name: str, created_at: datetime, joined_at: datetime):
        data = {
            'discord_id': user_id,
            'name': name,
            'discord_registration_date': created_at.isoformat(),
            'registration_date': joined_at.isoformat(),
        }

        await self.post(f'{self.url}/', data=data)

    async def add_roles(self, user_id: int, roles: Iterable[str]):
        await self.post(f'{self.url}/{user_id}/add_roles/', json=roles)

    async def update_name(self, user_id, name):
        data = {'name': name}
        await self.patch(f'{self.url}/{user_id}/', data=data)

    async def set_background(self, user_id, background_id):
        data = {'active_background': background_id}
        await self.patch(f'{self.url}/{user_id}/', data=data)

    async def set_title(self, user_id, title_id):
        data = {'active_title': title_id}
        await self.patch(f'{self.url}/{user_id}/', data=data)

    async def set_left_state(self, user_id: int, left: bool):
        data = {'left': left}
        await self.patch(f'{self.url}/{user_id}/', data=data)
