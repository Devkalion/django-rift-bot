import logging
import sys

from aiohttp import BasicAuth
from discord import Client, Intents, Message, Game, Status

from api import CommandsAPIClient
from commands import CommandRegister, SimpleCommand, NoSuchCommand
from conf import conf
from data_classes import ProcessMessage
from utils import error_handling

logger = logging.getLogger(sys.argv[1])


class BaseClient(Client):
    prefix = '!'
    intents = Intents()

    def __init__(self, *, loop=None, **options):
        intents = options.pop('intents', self.intents)
        if conf.DISCORD_PROXY_URL:
            options['proxy'] = conf.DISCORD_PROXY_URL
            options['proxy_auth'] = BasicAuth(conf.DISCORD_PROXY_LOGIN, conf.DISCORD_PROXY_PASSWORD)
        super().__init__(loop=loop, intents=intents, **options)

    @error_handling
    async def on_ready(self):
        logger.info(f'Logged on as {self.user}!')

    def message_is_command(self, message: Message):
        channel_id = message.channel.id
        if channel_id in conf.IGNORE_COMMAND_CHANNELS:
            return False
        text: str = message.content.strip()
        return text.startswith(self.prefix) and len(text) > len(self.prefix)

    @classmethod
    def launch(cls, **kwargs):
        client = cls(**kwargs)
        client.run(conf.TOKEN)


class BaseCommandClient(BaseClient):
    intents = Intents(guilds=True, messages=True)

    async def handle_command(self, message: Message):
        raise NotImplementedError

    async def prepare_data(self, message: Message):
        command: str = message.content[len(self.prefix):]
        async with CommandsAPIClient() as api_client:
            api_command = await api_client.get_command(command.lower())

        if api_command:
            if 'handler' in api_command:
                handler = CommandRegister.get_registered_handler(api_command['handler'])
                command = command[len(api_command['prefix']):].strip()
            else:
                handler = SimpleCommand
        else:
            handler = NoSuchCommand

        return handler, ProcessMessage(
            command=command,
            channel_id=message.channel.id,
            guild_id=getattr(message.guild, 'id', None),
            message_id=message.id,
            handler=handler.__name__,
            api_command=api_command,
        )

    async def on_message(self, message: Message):
        if message.author.bot:
            return
        if self.message_is_command(message):
            await self.handle_command(message)

    @error_handling
    async def on_ready(self):
        if conf.BOT_ACTIVITY_TEXT:
            await self.change_presence(status=Status.idle, activity=Game(name=conf.BOT_ACTIVITY_TEXT))
        await super().on_ready()
