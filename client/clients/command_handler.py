import asyncio
import json
import logging
import sys
from typing import Optional

from discord import TextChannel, Message, Client, DMChannel, Intents

from commands import CommandRegister
from conf import conf
from data_classes import ProcessMessage

logger = logging.getLogger(sys.argv[1])


async def stdio(loop):
    reader = asyncio.StreamReader(loop=loop)
    await loop.connect_read_pipe(
        lambda: asyncio.StreamReaderProtocol(reader, loop=loop),
        sys.stdin
    )
    return reader


async def get_data(reader):
    bytes_line = await reader.readline()
    line = bytes_line.decode('utf-8').strip()
    return ProcessMessage(**json.loads(line))


async def get_message(client: Client, data: ProcessMessage) -> Message:
    guild = client.get_guild(data.guild_id)
    if guild:
        channel: TextChannel = guild.get_channel(data.channel_id)
    else:
        channel: Optional[DMChannel] = client.get_channel(data.channel_id)
        if channel is None:
            channel: DMChannel = await client.fetch_channel(data.channel_id)
    return await channel.fetch_message(data.message_id)


async def async_reader(client, loop):
    reader = await stdio(loop)
    await client.wait_until_ready()

    while True:
        data: ProcessMessage = await get_data(reader)
        try:
            message: Message = await get_message(client, data)
        except Exception as e:
            logger.error(f'Unable to get message. Error "{e}"')
            continue
        asyncio.create_task(
            CommandRegister.handle_command(data, message, client)
        )


async def main():
    loop = asyncio.get_event_loop()
    client = Client(loop=loop, intents=Intents(
        guilds=True,
        members=True,
        voice_states=True,
    ))
    try:
        await asyncio.gather(
            async_reader(client, loop),
            client.start(conf.TOKEN, bot=True, reconnect=True),
        )
    except KeyboardInterrupt:
        pass
    finally:
        await client.close()
