import logging
import sys

from discord import Message, Intents

from commands import CommandRegister
from .base import BaseCommandClient

logger = logging.getLogger(sys.argv[1])


class MyClient(BaseCommandClient):
    intents = Intents(
        guilds=True,
        members=True,
        messages=True,
        message_content=True,
        voice_states=True,
    )

    async def handle_command(self, message: Message):
        handler, data = await self.prepare_data(message)
        await CommandRegister.handle_command(data, message, self)
