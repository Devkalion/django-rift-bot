import asyncio
import json
import logging
import sys

from discord import Message

from commands import CommandRegister, BaseCommand
from utils import error_handling
from .base import BaseCommandClient

logger = logging.getLogger(sys.argv[1])


class MyClient(BaseCommandClient):
    processes = None

    async def handle_command(self, message: Message):
        handler, data = await self.prepare_data(message)
        subprocess = getattr(handler, 'subprocess', BaseCommand.subprocess).value
        while self.processes is None:
            await asyncio.sleep(0)
        self.processes[subprocess].stdin.write(json.dumps(data.__dict__).encode('utf-8') + b'\n')

    async def init_processes(self):
        processes_names = CommandRegister.processes()
        processes = await asyncio.gather(*[
            asyncio.create_subprocess_exec(
                sys.executable, 'run.py', 'handler', name,
                stdin=asyncio.subprocess.PIPE
            )
            for name in processes_names
        ])
        self.processes = {
            name: subprocess
            for name, subprocess in zip(processes_names, processes)
        }

    # Callbacks

    @error_handling
    async def on_ready(self):
        if self.processes is None:
            await self.init_processes()
        await super().on_ready()
