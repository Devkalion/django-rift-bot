import asyncio
import logging
import sys

from discord import Intents

from clients.base import BaseClient
from conf import conf
from tasks.events import event_listener

logger = logging.getLogger(sys.argv[1])


async def main():
    client = BaseClient(guild_subscriptions=False, intents=Intents(
        guilds=True,
        members=True,
        bans=True
    ), loop=asyncio.get_running_loop())
    try:
        await client.login(conf.TOKEN)
        await asyncio.gather(
            event_listener(client),
            client.connect(reconnect=True),
        )
    except:
        logger.exception("Listener error")
    finally:
        await client.close()
