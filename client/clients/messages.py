import logging
import re
import sys

from discord import Message, Member, Intents, Guild, TextChannel
from redis import Redis

from api import UserAPIClient, EmbedAPIClient
from conf import conf, REDIS_CONNECTION_SETTINGS
from timeouts import DailyMessages, get_msg_cd
from utils import error_handling
from .base import BaseClient

logger = logging.getLogger(sys.argv[1])


class MyClient(BaseClient):
    intents = Intents(guilds=True, messages=True, message_content=True)
    processes = {}
    _log_channel = None

    @error_handling
    async def handle_text(self, message: Message):
        # Delete emotions
        text = re.sub(r'<:\w+:\d+>', '', message.content).strip()
        if not text:
            return
        member: Member = message.author
        async with UserAPIClient() as api_client:
            await api_client.add_message(member.id)
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            messages = int(DailyMessages.get(member.id, redis=redis, default=0))
            blocker = get_msg_cd(messages)
            ttl = blocker.ttl(member.id, redis=redis)
            if not ttl or ttl < 0:
                if conf.MESSAGE_EXPERIENCE:
                    async with UserAPIClient() as api_client:
                        await api_client.add_experience(member.id, conf.MESSAGE_EXPERIENCE)
                blocker.set(member.id, '', redis=redis)
                DailyMessages.set(member.id, messages + 1)

        async with UserAPIClient() as api_client:
            await api_client.touch(message.author.id)

    @property
    def log_channel(self) -> TextChannel:
        if self._log_channel is None:
            guild: Guild = self.get_guild(conf.MAIN_GUILD_ID)
            self._log_channel = guild.get_channel(conf.MESSAGE_LOG_CHANNEL_ID)
        return self._log_channel

    # Callbacks

    async def on_ready(self):
        await super().on_ready()

    async def on_message(self, message: Message):
        if message.author.bot or self.message_is_command(message):
            return

        if message.channel.id in conf.MESSAGE_CHANNELS:
            await self.handle_text(message)

        async with UserAPIClient() as api_client:
            await api_client.touch(message.author.id)

    @error_handling
    async def on_message_delete(self, message: Message):
        if message.channel.id not in conf.LOGGABLE_MESSAGE_CHANNELS:
            return
        if message.author.bot or message.author.id == self.user.id:
            return
        data = {
            'text': message.content,
            'channel_id': message.channel.id,
            'user_id': message.author.id,
        }
        msg_data = await EmbedAPIClient.class_render_embed('on_delete', data)
        await self.log_channel.send(**msg_data)

    @error_handling
    async def on_message_edit(self, before: Message, after: Message):
        if after.channel.id not in conf.LOGGABLE_MESSAGE_CHANNELS:
            return
        if after.author.bot or after.author.id == self.user.id:
            return
        data = {
            'text': after.content,
            'old_text': before.content,
            'channel_id': after.channel.id,
            'user_id': after.author.id,
            'message_url': after.jump_url,
        }
        msg_data = await EmbedAPIClient.class_render_embed('on_edit', data)
        await self.log_channel.send(**msg_data)
