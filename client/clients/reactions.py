import asyncio
import logging
import sys
from typing import Callable

from discord import Member, Guild, RawReactionActionEvent, Intents, TextChannel, Message

from api import RoleMessageAPIClient, APIException, RoleAPIClient, DrawAPIClient
from clients.base import BaseClient
from utils import error_handling, revoke_role, grant_role

logger = logging.getLogger(sys.argv[1])


class MyClient(BaseClient):
    intents = Intents(guilds=True, reactions=True)

    @staticmethod
    async def get_reaction(message_id, channel_id):
        async with RoleMessageAPIClient() as api_client:
            return await api_client.get_role_message(channel_id, message_id)

    async def grant_reaction_reward(self, member: Member, payload: RawReactionActionEvent):
        try:
            async with RoleMessageAPIClient() as api_client:
                reward = await api_client.give_reward(payload.channel_id, payload.message_id, member.id)
        except APIException:
            return
        roles = {_id for _id in reward['roles']}
        async with RoleAPIClient() as api_client:
            api_roles = await asyncio.gather(*[
                api_client.get_user_role(member.id, role_id)
                for role_id in roles
            ])
        await asyncio.gather(*[
            grant_role(api_role, member, self)
            for api_role in api_roles
        ])

    async def operate_role_action(self, payload: RawReactionActionEvent, role_func: Callable):
        guild: Guild = self.get_guild(payload.guild_id)
        api_reaction_msg: dict = await self.get_reaction(payload.message_id, payload.channel_id)
        if api_reaction_msg is None:
            return
        for api_reaction in api_reaction_msg['reactions']:
            if api_reaction['emoji'] == payload.emoji.name:
                if payload.member:
                    member = payload.member
                else:
                    member: Member = await guild.fetch_member(payload.user_id)
                if payload.event_type == 'REACTION_ADD':
                    if api_reaction_msg['single_role']:
                        channel: TextChannel = guild.get_channel(payload.channel_id)
                        message: Message = await channel.fetch_message(payload.message_id)
                        await asyncio.wait([
                            message.remove_reaction(_reaction, member)
                            for _reaction in message.reactions
                            if _reaction.emoji.name != api_reaction['emoji']
                        ])
                    if api_reaction_msg['reward']:
                        return await asyncio.gather(
                            role_func(api_reaction['role'], member, self),
                            self.grant_reaction_reward(member, payload)
                        )
                return await role_func(api_reaction['role'], member, self)

    async def participate_draw(self, payload: RawReactionActionEvent):
        user_id = payload.user_id
        if user_id == self.user.id:
            return
        async with DrawAPIClient() as api_client:
            await api_client.participate(payload.channel_id, payload.message_id, user_id)

    # Callbacks

    @error_handling
    async def on_raw_reaction_add(self, payload: RawReactionActionEvent):
        await self.operate_role_action(payload, grant_role)
        await self.participate_draw(payload)

    @error_handling
    async def on_raw_reaction_remove(self, payload: RawReactionActionEvent):
        return await self.operate_role_action(payload, revoke_role)
