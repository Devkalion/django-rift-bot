from discord import Intents

from conf import conf
from utils import report_error, error_handling
from .base import BaseCommandClient


class MyClient(BaseCommandClient):
    intents = Intents(
        guilds=True,
        members=True,
        voice_states=True,
    )

    @error_handling
    async def on_ready(self):
        from slash_commands import refresh_commands

        await super().on_ready()
        await refresh_commands()

    async def on_error(self, event_method: str, /, *args, **kwargs) -> None:
        report_error(self, Exception(event_method), event_method, *args, **kwargs)


client = MyClient()

if __name__ == '__main__':
    client.run(conf.TOKEN)
