import asyncio
import itertools
import logging
import sys
from typing import List, Tuple, Optional, Dict

from discord import Member, Role, Guild, Intents, User as DUser, Streaming

from api import UserAPIClient, RoleAPIClient, EmbedAPIClient, ModerActionAPIClient, APIException
from clients.base import BaseClient
from conf import conf
from data_classes import SmallUser, User
from utils import error_handling, send_dm

logger = logging.getLogger(sys.argv[1])


async def create_role(role: Role):
    async with RoleAPIClient() as api_client:
        await api_client.create_role(role.id, role.name, role.position)


async def update_role(role: Role, _role: dict):
    if role.name != _role['name'] or role.position != _role['position']:
        async with RoleAPIClient() as api_client:
            await api_client.update_role(role.id, role.name, role.position)


async def delete_role(role_id):
    async with RoleAPIClient() as api_client:
        await api_client.delete_role(role_id)


async def update_name(member: Member):
    async with UserAPIClient() as api_client:
        await api_client.update_name(member.id, member.display_name)


async def create_user(member: Member):
    async with UserAPIClient() as api_client:
        try:
            await api_client.create_user(member.id, member.display_name, member.created_at, member.joined_at)
        except APIException as e:
            if not len(e.data) or 'discord_id' not in e.data:
                raise


async def leaving_user(user_id: int):
    async with UserAPIClient() as api_client:
        await api_client.set_left_state(user_id, True)


async def returning_user(user_id: int):
    async with UserAPIClient() as api_client:
        await api_client.set_left_state(user_id, False)


async def streaming_user(
        member: Member, guild: Guild, channel, api_user: Optional[SmallUser] = None, is_initial: bool = False,
):
    role: Role = guild.get_role(conf.ON_AIR_ROLE_ID)

    if api_user is None:
        async with UserAPIClient() as api_client:
            api_user: User = await api_client.get_user(member.id)

    # user is not created
    if api_user is None:
        return

    role_required = api_user.is_streamer or api_user.is_partner
    message_required = api_user.is_partner and not is_initial
    user_has_role = role in member.roles

    stream_is_active = any((
        a for a in member.activities
        if type(a) == Streaming
    ))
    if stream_is_active and not user_has_role:
        if role_required:
            await member.add_roles(role)
        if message_required:
            async with EmbedAPIClient() as api_client:
                msg_data = await api_client.render_embed('user_stream_started', {
                    'user_id': member.id,
                    'avatar': str((member.avatar or member.default_avatar).url),
                })
            asyncio.create_task(channel.send(**msg_data))
    elif not stream_is_active and user_has_role:
        await member.remove_roles(role)


coroutines_limit = 10


def chunks(iterable, length):
    return (
        iterable[i:i + length]
        for i in range(0, len(iterable), length)
    )


class MyClient(BaseClient):
    queue = None
    intents = Intents(guilds=True, members=True, presences=True)

    async def sync_roles(self):
        async with RoleAPIClient() as api_client:
            roles = await api_client.get_roles()
        roles = {role['discord_id']: role for role in roles}
        guild: Guild = self.get_guild(conf.MAIN_GUILD_ID)
        coroutines = []
        guild_roles: List[Role] = await guild.fetch_roles()
        for role in guild_roles:
            server_role = roles.get(role.id)
            if server_role is None:
                coroutines.append(create_role(role))
            else:
                self.queue.put_nowait(update_role(role, server_role))

        guild_roles_ids = {role.id for role in guild_roles}

        for role_id in roles:
            if role_id not in guild_roles_ids:
                self.queue.put_nowait(delete_role(role_id))

        if len(coroutines):
            await asyncio.gather(*coroutines)

    @staticmethod
    def split_users(members, api_users) -> Tuple[List[Member], List[Member]]:
        registered_users, unregistered_users = [], []
        for member in members:
            array = registered_users if member.id in api_users else unregistered_users
            array.append(member)
        return registered_users, unregistered_users

    @staticmethod
    async def get_users() -> Dict[int, SmallUser]:
        async with UserAPIClient() as api_client:
            count, _users = await api_client.get_users()
            coroutines = [
                api_client.get_users(offset)
                for offset in range(conf.USER_PAGE_SIZE, count, conf.USER_PAGE_SIZE)
            ]
            responses = []
            for coroutine_chunk in chunks(coroutines, conf.USERS_THREADS):
                _responses = await asyncio.gather(*coroutine_chunk)
                responses += [resp[1] for resp in _responses]
        return {user.discord_id: user for user in itertools.chain(_users, *responses)}

    @error_handling
    async def sync_users(self):
        users = await self.get_users()
        guild: Guild = self.get_guild(conf.MAIN_GUILD_ID)
        stream_channel = self.get_channel(conf.ON_AIR_MESSAGE_CHANNEL_ID)
        registered_users, unregistered_users = self.split_users(guild.members, users)

        for members_chunk in chunks(unregistered_users, 20):
            await asyncio.gather(*[
                create_user(member)
                for member in members_chunk
            ])

        logger.info('Synchronization half completed')

        for member in registered_users:
            user: SmallUser = users[member.id]
            if user.name != member.display_name:
                self.queue.put_nowait(update_name(member))

        banned_users = {
            be.user.id
            async for be in guild.bans()
        }
        members = {
            member.id
            for member in guild.members
        }

        # sync streaming activity on client init in case of
        # various client shutdowns
        for member in guild.members:
            if member.id in users:
                await streaming_user(member, guild, stream_channel, api_user=users[member.id], is_initial=True)

        for _id, user in users.items():
            actually_left = _id in banned_users or _id not in members
            if actually_left == user.left:
                continue
            if actually_left:
                self.queue.put_nowait(leaving_user(_id))
            else:
                self.queue.put_nowait(returning_user(_id))

        logger.info('All init tasks scheduled')

    async def operate_queue(self):
        while True:
            coroutine = await self.queue.get()
            await coroutine
            self.queue.task_done()

    # Callbacks

    @error_handling
    async def on_ready(self):
        await super().on_ready()
        if self.queue is None:
            self.queue = asyncio.Queue()
            asyncio.create_task(self.operate_queue())
        await self.sync_roles()
        await self.sync_users()

    @error_handling
    async def on_member_remove(self, member: Member):
        msg_data = await EmbedAPIClient.class_render_embed('user_connection', {
            'user_id': member.id,
            'username': str(member),
            'type': 'leave',
            'avatar': str((member.avatar or member.default_avatar).url),
        })
        log_channel = self.get_channel(conf.LOG_CHANNEL_ID)
        await asyncio.gather(
            log_channel.send(**msg_data),
            leaving_user(member.id)
        )

    @error_handling
    async def on_member_join(self, member: Member):
        async with UserAPIClient() as api_client:
            user: User = await api_client.get_user(member.id)
            new_user = user is None
            if new_user:
                await api_client.create_user(member.id, member.display_name, member.created_at, member.joined_at)
            elif user.name != member.display_name:
                await api_client.update_name(member.id, member.display_name)

        if conf.WELCOME:
            msg_data = await EmbedAPIClient.class_render_embed('welcome_dm', {'user_id': member.id})
            asyncio.create_task(send_dm(member, msg_data))

        async with EmbedAPIClient() as api_client:
            msg_data, log_data = await asyncio.gather(
                api_client.render_embed('welcome', {'user_id': member.id}),
                api_client.render_embed('user_connection', {
                    'user_id': member.id,
                    'username': str(member),
                    'type': 'join',
                    'avatar': str((member.avatar or member.default_avatar).url),
                })
            )
        channel = self.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID)
        log_channel = self.get_channel(conf.LOG_CHANNEL_ID)
        asyncio.create_task(channel.send(**msg_data))
        asyncio.create_task(log_channel.send(**log_data))

        if not new_user:
            async with ModerActionAPIClient() as api_client:
                active_mute_actions = await api_client.get_actions(member.id)
            if active_mute_actions:
                guild: Guild = self.get_guild(conf.MAIN_GUILD_ID)
                role: Role = guild.get_role(conf.MUTE_ROLE_ID)
                await member.add_roles(role)

        async with UserAPIClient() as api_client:
            await asyncio.gather(
                api_client.set_left_state(member.id, False),
                api_client.touch(member.id)
            )

    @error_handling
    async def on_member_update(self, before_member: Member, after_member: Member):
        if before_member.display_name != after_member.display_name:
            await update_name(after_member)

    @error_handling
    async def on_presence_update(self, before_member: Member, after_member: Member):
        old_stream_activity = bool([
            a for a in before_member.activities
            if type(a) == Streaming
        ])

        new_stream_activity = bool([
            a for a in after_member.activities
            if type(a) == Streaming
        ])

        if old_stream_activity != new_stream_activity:
            channel = self.get_channel(conf.ON_AIR_MESSAGE_CHANNEL_ID)
            guild: Guild = before_member.guild
            await streaming_user(after_member, guild, channel)

    @error_handling
    async def on_user_update(self, before: DUser, after: DUser):
        guild: Guild = self.get_guild(conf.MAIN_GUILD_ID)
        member: Member = guild.get_member(after.id)
        async with UserAPIClient() as api_client:
            api_user: User = await api_client.get_user(member.id)
            if api_user.name != member.display_name:
                await api_client.update_name(member.id, member.display_name)

    @error_handling
    async def on_guild_role_create(self, role: Role):
        await create_role(role)

    @error_handling
    async def on_guild_role_delete(self, role: Role):
        await delete_role(role.id)

    @error_handling
    async def on_guild_role_update(self, before: Role, after: Role):
        if before.name != after.name or before.position != after.position:
            await self.queue.put(update_role(after, {
                'name': before.name,
                'position': before.position
            }))
