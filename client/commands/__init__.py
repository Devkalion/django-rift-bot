import sys
import traceback
from typing import Dict, Type

from discord import Client, Message
from discord.abc import Messageable

from conf import conf
from data_classes import ProcessMessage
from .base import BaseCommand, NoSuchCommand, SubprocessName, DisabledCommand
from .simple import SimpleCommand

is_parent_process = len(sys.argv) == 2


class CommandRegister:
    _register: Dict[str, Type[BaseCommand]] = {}

    @classmethod
    def get_registered_handler(cls, class_name: str) -> Type[BaseCommand]:
        return cls._register.get(class_name)

    @classmethod
    async def handle_command(cls, data: ProcessMessage, message: Message, client: Client):
        command = data.command
        if data.handler == SimpleCommand.__name__:
            handler = SimpleCommand(**data.api_command)
        elif data.handler == NoSuchCommand.__name__:
            handler = NoSuchCommand()
        elif data.api_command['disabled']:
            handler = DisabledCommand()
        else:
            handler: BaseCommand = cls.get_registered_handler(data.handler)(
                cooldown=data.api_command['cooldown'],
                roles=data.api_command.get('roles')
            )
            command = f'{data.api_command["prefix"]} {data.command}'

        try:
            await handler(data.command, message, client)
        except Exception as e:
            if conf.ERROR_CHANNEL_ID:
                exc_type, exc_value, exc_tb = sys.exc_info()
                channel: Messageable = client.get_channel(conf.ERROR_CHANNEL_ID)
                await channel.send('\n'.join([
                    '@here',
                    f'Пользователь: {message.author.display_name}',
                    f'Сообщение: {message.jump_url}',
                    f'Команда: `{command}`',
                    f'Обработчик: `{handler}`',
                    f'Ошибка: `{e}`',
                    'Стектрейс: ```',
                    *(traceback.format_exception(exc_type, exc_value, exc_tb)),
                    '```'
                ]))
            raise

    @classmethod
    def register_command(cls, command_class: Type[BaseCommand]):
        if is_parent_process or command_class.subprocess.value == sys.argv[2]:
            cls._register[command_class.__name__] = command_class

    @classmethod
    def processes(cls):
        return [
            subprocess.value
            for subprocess in SubprocessName.__members__.values()
        ]


def register(cls):
    CommandRegister.register_command(cls)
    return cls


from .backgrounds import BackgroundsCommand
from .emotion import EmotionCommand, EmotionsCommand
from .gift import GiftCommand, GiftsCommand
from .inventory import InventoryCommand
from .market import MarketCommand
from .open_container import OpenCommand
from .profile import ProfileCommand
from .roles import RoleCommand
from .shop import ShopCommand, BuyCommand
from .transformations import TransformationsCommand, TransformCommand
from .moder_actions import OffensesCommand
from .giveaways import ClaimCommand
from .titles import TitlesCommand
from .voice import VoiceKickCommand
from .journey import JourneysCommand
from .rating import ActiveUsersCommand
