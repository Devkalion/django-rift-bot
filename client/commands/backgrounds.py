from discord import Client, Message
from discord.abc import Messageable

from api import UserAPIClient, APIException, EmbedAPIClient
from conf import conf
from data_classes import User
from . import register
from .base import PrepareDataCommand, UserMentionCommand, BaseCommand


@register
class BackgroundsCommand(UserMentionCommand):
    embed_name = 'backgrounds'


@register
class SetBackgroundCommand(PrepareDataCommand):
    success_embed_name: str = 'set_background'
    error_embed_name: str = 'set_background_error'

    @staticmethod
    async def populate_data(bg_id, user_id, client: Client):
        try:
            bg_id = int(bg_id)
        except ValueError:
            return {
                'reason': 'no_bg',
                'background_id': bg_id
            }

        async with UserAPIClient() as api_client:
            _user: User = await api_client.get_user(user_id)
            if _user.active_background != bg_id:
                try:
                    await api_client.set_background(user_id, bg_id)
                except APIException:
                    return {
                        'reason': 'no_bg',
                        'background_id': bg_id
                    }

        if _user.active_background == bg_id:
            return {
                'reason': 'already_set',
                'background_id': bg_id
            }

        return {'background_id': bg_id}


@register
class ClearBackgroundCommand(BaseCommand):
    @staticmethod
    async def clear_bg_error(user_id, reason, client):
        msg_data = await EmbedAPIClient.class_render_embed('clear_background_error', {
            'user_id': user_id,
            'reason': reason
        })
        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)

    async def execute(self, command_tail: str, message: Message, client: Client):
        user_id = message.author.id
        try:
            async with UserAPIClient() as api_client:
                _user: User = await api_client.get_user(user_id)
                if _user.active_background:
                    await api_client.set_background(user_id, '')
        except APIException:
            return await self.clear_bg_error(user_id, 'no bg', client)

        if not _user.active_background:
            return await self.clear_bg_error(user_id, 'already_cleared', client)

        msg_data = await EmbedAPIClient.class_render_embed('clear_background', {
            'user_id': user_id,
        })
        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)
