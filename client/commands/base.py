import logging
import sys
from abc import ABC
from enum import Enum
from typing import Optional, Iterable

from discord import Member, DMChannel, Message, Client, TextChannel, Guild
from discord.abc import Messageable
from redis import Redis

from api import EmbedAPIClient, APIException
from conf import conf, REDIS_CONNECTION_SETTINGS
from utils import report_error

logger = logging.getLogger(sys.argv[1])


class SubprocessName(Enum):
    DEFAULT = 'default'
    NONE = 'none'


class BaseCommand:
    allow_dm: bool = conf.ALLOW_DM_COMMANDS
    channels: Optional[Iterable] = [conf.COMMANDS_CHANNEL_ID]
    roles: Optional[set] = None
    subprocess: SubprocessName = SubprocessName.DEFAULT
    cooldown_error_embed_name: Optional[str] = None
    cooldown_error_embed_channel_id = conf.COMMANDS_CHANNEL_ID

    def __init__(self, cooldown=None, roles=None):
        if roles:
            self.roles = set(roles)
        self.cooldown = cooldown

    def validate_dm(self, channel):
        if not self.allow_dm:
            raise AssertionError('dm')

    def validate_channel(self, channel):
        if self.channels is not None and channel.id not in self.channels:
            raise AssertionError('channel')

    def validate_roles(self, user: Member):
        if self.roles is not None and self.roles.isdisjoint(user.roles):
            raise AssertionError('role')

    def validate_cooldown(self, user: Member):
        redis_key = f'command-{self.__class__.__name__}-{user.id}'
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            ttl = redis.ttl(redis_key)
            if not ttl or ttl <= 0:
                redis.set(redis_key, 1, ex=self.cooldown)
                return 0
            else:
                return ttl

    def get_cooldown_error_embed_data(self, message, command_tail, ttl):
        if self.cooldown:
            return {
                'user_id': message.author.id,
                'reason': 'cooldown',
                'cooldown': ttl,
            }

    def drop_cd_block(self, user_id):
        if self.cooldown is not None:
            redis_key = f'command-{self.__class__.__name__}-{user_id}'
            with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
                redis.delete(redis_key)

    async def __call__(self, command_tail: str, message: Message, client: Client):
        channel = message.channel
        try:
            if isinstance(channel, DMChannel):
                self.validate_dm(channel)
            else:
                self.validate_channel(channel)
            self.validate_roles(message.author)
        except AssertionError as e:
            channel: TextChannel = message.channel
            msg_data = await EmbedAPIClient.class_render_embed(
                'no_permissions',
                {
                    'user_id': message.author.id,
                    'handler': self.__class__.__name__,
                    'reason': str(e.args[0])
                }
            )
            await channel.send(**msg_data)
            return

        if self.cooldown is not None:
            ttl = self.validate_cooldown(message.author)
            if ttl > 0:
                msg_data = await EmbedAPIClient.class_render_embed(
                    self.cooldown_error_embed_name,
                    self.get_cooldown_error_embed_data(message, command_tail, ttl)
                )
                guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
                channel: TextChannel = guild.get_channel(self.cooldown_error_embed_channel_id)
                return await channel.send(**msg_data)

        await self.execute(command_tail, message, client)

    async def execute(self, command_tail: str, message: Message, client: Client):
        raise NotImplementedError


class RenderCommand(BaseCommand, ABC):
    embed_name: str = None
    channel_id = conf.COMMANDS_CHANNEL_ID

    @staticmethod
    def get_embed_data(message):
        user = message.author
        return {
            'user_id': user.id,
            'avatar': str((user.avatar or user.default_avatar).url),
        }

    async def execute(self, command_tail: str, message: Message, client: Client):
        msg_data = await EmbedAPIClient.class_render_embed(self.embed_name, self.get_embed_data(message))
        channel: Messageable = client.get_channel(self.channel_id)
        await channel.send(**msg_data)


class PrepareDataCommand(BaseCommand, ABC):
    """
    Command of type "!<command> <item_id>" with no extra discord actions
    """
    success_embed_name: str = None
    error_embed_name: str = None

    @staticmethod
    async def populate_data(item_id, user_id, client: Client):
        raise NotImplementedError

    async def execute(self, command_tail: str, message: Message, client: Client):
        item_id, *_ = command_tail.strip().split(' ', 1)

        try:
            data = await self.populate_data(item_id, message.author.id, client)
        except Exception as e:
            report_error(client, e, self.execute, command_tail)
            logger.exception(f'Exception while {self.__class__} operated')
            data = {'reason': 'unknown'}
        data['user_id'] = message.author.id

        if 'reason' in data:
            embed_name = self.error_embed_name
            self.drop_cd_block(message.author.id)
        else:
            embed_name = self.success_embed_name

        msg_data = await EmbedAPIClient.class_render_embed(embed_name, data)
        channel = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)


class UserMentionCommand(BaseCommand, ABC):
    """
    Base class for this embeds type
    !<command>
    !<command> <user_id>
    !<command> <@user_id>  (user mention)
    """

    embed_name: str = None

    @staticmethod
    async def prepare_data(user: Member):
        return {
            'user_id': user.id,
            'avatar': str((user.avatar or user.default_avatar).url)
        }

    async def execute(self, command_tail: str, message: Message, client: Client):
        mentions = message.mentions
        if mentions:
            user = mentions[0]
        else:
            try:
                user_id = int(command_tail)
                guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
                user = guild.get_member(user_id)
                if user is None:
                    user = message.author
            except ValueError:
                user = message.author

        embed_data = await self.prepare_data(user)
        msg_data = await EmbedAPIClient.class_render_embed(self.embed_name, embed_data)
        await message.reply(**msg_data)


class BaseTransformationCommand(PrepareDataCommand, ABC):
    async def api_call(self, item_id, user_id):
        raise NotImplementedError

    async def populate_data(self, item_id, user_id, client: Client):
        try:
            item_id = int(item_id)
        except ValueError:
            return {'reason': 'invalid item_id'}

        try:
            return await self.api_call(item_id, user_id)
        except APIException as e:
            if e.status == 404:
                return {'reason': 'no such item'}
            if e.status == 403:
                return {'reason': e.data['detail']}
            if e.status == 405:
                return {
                    'reason': 'limit',
                    'cooldown': e.data['cooldown'],
                }
            if e.status == 400:
                return {'reason': 'not enough', 'missing_inventory': e.data}
            raise


class NoSuchCommand(RenderCommand):
    embed_name = 'no_command'
    subprocess = SubprocessName.NONE


class DisabledCommand(RenderCommand):
    embed_name = 'command_disabled'
    subprocess = SubprocessName.NONE
