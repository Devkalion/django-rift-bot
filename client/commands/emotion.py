from discord import Message, Client
from discord.abc import Messageable

from api import EmbedAPIClient, EmotionAPIClient
from conf import conf
from . import register
from .base import BaseCommand, UserMentionCommand


@register
class EmotionsCommand(UserMentionCommand):
    embed_name = 'emotions'


@register
class EmotionCommand(BaseCommand):
    cooldown_error_embed_name = 'emotion_error'
    cooldown_error_embed_channel_id = conf.COMMANDS_CHANNEL_ID

    def get_cooldown_error_embed_data(self, message, command_tail, ttl):
        emotion_id: int = command_tail
        return {
            'user_id': message.author.id,
            'emotion_id': emotion_id,
            'reason': 'cooldown',
            'cooldown': ttl,
        }

    async def execute(self, command_tail: str, message: Message, client: Client):
        emotion_id: int = command_tail
        async with EmotionAPIClient() as api_client:
            emotion = await api_client.get_emotion(emotion_id, message.author.id)

        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)

        async with EmbedAPIClient() as api_client:
            if emotion is None:
                msg_data = await api_client.render_embed('emotion_error', {
                    'user_id': message.author.id,
                    'emotion_id': emotion_id,
                    'reason': 'emotion'
                })
                self.drop_cd_block(message.author.id)
            else:
                message_channel: Messageable = client.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID)
                msg_data = await api_client.render_embed('emotion_message', {
                    'user_id': message.author.id,
                    'emotion_id': emotion_id
                })
                emotion_message: Message = await message_channel.send(**msg_data)

                msg_data = await api_client.render_embed('emotion', {
                    'user_id': message.author.id,
                    'message_url': emotion_message.jump_url,
                    'emotion_id': emotion_id
                })

        await channel.send(**msg_data)
