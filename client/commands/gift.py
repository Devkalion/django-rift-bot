import asyncio

from discord import Message, Guild, Client

from api import GiftAPIClient, APIException, EmbedAPIClient
from conf import conf
from utils import send_dm
from . import register
from .base import BaseCommand, RenderCommand


@register
class GiftsCommand(RenderCommand):
    embed_name = 'gifts'


@register
class GiftCommand(BaseCommand):
    allow_dm: bool = False
    error_reasons = (
        'no recipient',
        'self gift',
        'no or invalid gift id',
        'bot',
        'no such gift',
        'not enough',
        'one-off',
    )
    cooldown_error_embed_name = 'gift_error'
    cooldown_error_embed_channel_id = conf.COMMANDS_CHANNEL_ID

    @staticmethod
    async def get_reward(sender_id, recipient_id, gift_id, **kwargs):
        async with GiftAPIClient() as api_client:
            return await api_client.gift(sender_id, recipient_id, gift_id)

    def get_cooldown_error_embed_data(self, message, command_tail, ttl):
        gift_id, *_ = command_tail.split(' ', 1)
        return {
            'sender_id': message.author.id,
            'gift_id': gift_id,
            'reason': 'cooldown',
            'cooldown': ttl
        }

    async def populate_data(self, data, command_tail, mentions, client: Client):
        # <gift_id> <user> <comment?>
        gift_id, *tail = command_tail.split(' ', 2)
        data['gift_id'] = gift_id

        if mentions:
            recipient = mentions[0]
            data['recipient_id'] = recipient.id
        elif len(tail) and tail[0]:
            try:
                recipient_id = int(tail[0])
            except ValueError:
                raise Exception(self.error_reasons[0])

            guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
            recipient = guild.get_member(recipient_id)
            if recipient is None:
                raise Exception(self.error_reasons[0])
            data['recipient_id'] = recipient.id
        else:
            raise Exception(self.error_reasons[0])

        if data['recipient_id'] == data['sender_id']:
            raise Exception(self.error_reasons[1])

        try:
            data['gift_id'] = int(gift_id)
        except ValueError:
            raise Exception(self.error_reasons[2])

        if recipient.bot:
            raise Exception(self.error_reasons[3])

        try:
            data['reward_dict'] = await self.get_reward(**data)
        except APIException as error:
            if error.status == 404:
                raise Exception(self.error_reasons[4])
            elif error.status == 403:
                raise Exception(error.data['detail'])
            elif error.status == 400:
                data['missing_inventory'] = error.data
                raise Exception(self.error_reasons[5])
            else:
                raise Exception('uknown')

        if len(tail) == 2 and tail[1]:
            data['text'] = tail[1]

    async def execute(self, command_tail: str, message: Message, client: Client):
        data = {'sender_id': message.author.id}

        try:
            await self.populate_data(data, command_tail, message.mentions, client)
        except Exception as e:
            self.drop_cd_block(message.author.id)
            data['reason'] = e.args[0]

        async with EmbedAPIClient() as api_client:
            if 'reason' in data:
                msg_data = await api_client.render_embed('gift_error', data)
                channel = client.get_channel(conf.COMMANDS_CHANNEL_ID)
                return await channel.send(**msg_data)
            else:
                guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
                recipient = guild.get_member(data['recipient_id'])

                channel = client.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID)
                channel_msg = await api_client.render_embed('gift_channel', data)
                channel_message: Message = await channel.send(**channel_msg)
                data['message_url'] = channel_message.jump_url
                sender_channel = client.get_channel(conf.COMMANDS_CHANNEL_ID)
                msg_data, recipient_msg = await asyncio.gather(
                    api_client.render_embed('gift_sender', data),
                    api_client.render_embed('gift_recipient_dm', data)
                )

        await asyncio.gather(
            sender_channel.send(**msg_data),
            send_dm(recipient, recipient_msg),
        )
