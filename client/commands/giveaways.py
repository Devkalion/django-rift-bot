from discord import Client, Message

from api import GiveawayAPIClient, APIException, EmbedAPIClient
from conf import conf
from utils import report_error
from . import register
from .base import RenderCommand, BaseCommand


@register
class GiveawaysCommand(RenderCommand):
    embed_name = 'giveaways'


@register
class ClaimCommand(BaseCommand):
    async def execute(self, command_tail: str, message: Message, client: Client):
        reason = None
        try:
            async with GiveawayAPIClient() as api_client:
                result: dict = await api_client.claim(message.author.id)
                result['reward_dict'] = result.pop('reward')
        except APIException as e:
            if e.status == 404:
                reason = 'no active giveaways'
            elif e.status == 403:
                reason = 'blocked'
            elif e.status == 405:
                reason = 'ended'
            else:
                reason = 'unknown'
                report_error(client, e, self.execute, command_tail)

        if reason:
            embed_name = 'giveaway_error'
            data = {'reason': reason}
        else:
            embed_name = 'giveaway'
            data = result
        data['user_id'] = message.author.id

        msg_data = await EmbedAPIClient.class_render_embed(embed_name, data)
        channel = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)
