from . import register
from .base import UserMentionCommand


@register
class InventoryCommand(UserMentionCommand):
    embed_name: str = 'inventory'
