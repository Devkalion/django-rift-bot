from api import JourneyAPIClient
from . import register
from .base import RenderCommand, BaseTransformationCommand


@register
class JourneysCommand(RenderCommand):
    embed_name = 'journeys'


@register
class JourneyCommand(BaseTransformationCommand):
    success_embed_name: str = 'journey_started'
    error_embed_name: str = 'journey_error'

    async def api_call(self, item_id, user_id):
        async with JourneyAPIClient() as api_client:
            return {
                'to_date': await api_client.start(item_id, user_id),
                'journey_id': item_id,
            }
