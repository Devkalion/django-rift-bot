from api import MarketAPIClient
from conf import conf
from . import register
from .base import RenderCommand, BaseTransformationCommand


@register
class MarketCommand(RenderCommand):
    embed_name = 'market'


@register
class SaleCommand(BaseTransformationCommand):
    success_embed_name: str = 'market_sale'
    error_embed_name: str = 'market_sale_error'

    cooldown_error_embed_name = 'market_sale_error'
    cooldown_error_embed_channel_id = conf.COMMANDS_CHANNEL_ID

    def get_cooldown_error_embed_data(self, message, command_tail, ttl):
        return {
            'user_id': message.author.id,
            'reason': 'cooldown',
            'cooldown': ttl,
        }

    async def api_call(self, item_id, user_id):
        async with MarketAPIClient() as api_client:
            return {
                'reward_dict': await api_client.sale(item_id, user_id),
                'item_id': item_id,
            }


@register
class ExchangeCommand(BaseTransformationCommand):
    success_embed_name: str = 'market_exchange'
    error_embed_name: str = 'market_exchange_error'

    cooldown_error_embed_name = 'market_exchange_error'
    cooldown_error_embed_channel_id = conf.COMMANDS_CHANNEL_ID

    def get_cooldown_error_embed_data(self, message, command_tail, ttl):
        return {
            'user_id': message.author.id,
            'reason': 'cooldown',
            'cooldown': ttl,
        }

    async def api_call(self, item_id, user_id):
        async with MarketAPIClient() as api_client:
            return {
                'reward_dict': await api_client.exchange(item_id, user_id),
                'item_id': item_id,
            }
