import asyncio
import re
from datetime import timedelta
from typing import Optional

from discord import Message, Client, Guild, Member, Role, NotFound, TextChannel, ClientUser
from discord.abc import Messageable, User

from api import EmbedAPIClient, ModerActionAPIClient, APIException
from api.moder_actions import AppealAPIClient
from conf import conf
from utils import send_dm
from . import register
from .base import BaseCommand, UserMentionCommand


@register
class OffensesCommand(UserMentionCommand):
    embed_name: str = 'offenses'


class ReasonAction(BaseCommand):
    channels = [conf.MODER_CHANNEL_ID]
    _type = None

    @staticmethod
    def parse_command_args(command_tail: str):
        user_id, reason = command_tail.split(' ', 1)
        return int(user_id), reason

    async def handle_error(self, error, client: Client):
        moder_channel: Messageable = client.get_channel(conf.MODER_CHANNEL_ID)
        channel_msg_data = await EmbedAPIClient.class_render_embed(f'{self._type}_error', {
            'reason': error
        })
        await moder_channel.send(**channel_msg_data)

    async def handle_success(self, user: User, reason, action_id, client: Client):
        message_channel: Messageable = client.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID)
        data = {
            'action_id': action_id
        }

        async with EmbedAPIClient() as api_client:
            channel_msg_data, user_msg_data = await asyncio.gather(
                api_client.render_embed(self._type, data),
                api_client.render_embed(f'{self._type}_dm', data)
            )
        await asyncio.gather(
            message_channel.send(**channel_msg_data),
            send_dm(user, user_msg_data),
        )
        await self.post_action(user, reason, client)

    async def get_user(self, client: Client, user_id):
        try:
            return await client.fetch_user(user_id)
        except NotFound:
            return

    async def execute(self, command_tail: str, message: Message, client: Client):
        try:
            user_id, reason = self.parse_command_args(command_tail)
        except Exception:
            return await self.handle_error('format', client)

        user: Optional[User] = await self.get_user(client, user_id)
        if user is None:
            return await self.handle_error('no such user', client)

        try:
            async with ModerActionAPIClient() as api_client:
                action = await api_client.create_action(user.id, message.author.id, reason, _type=self._type)
        except APIException as e:
            if e.status == 403:
                return await self.handle_error('forbidden', client)
            else:
                raise

        await self.handle_success(user, reason, action['id'], client)

    async def post_action(self, user: User, reason: str, client: Client):
        pass


class TimeReasonAction(ReasonAction):
    unit_mapping = {
        'д': 'days',
        'ч': 'hours',
        'м': 'minutes',
        'с': 'seconds',
    }

    def parse_command_args(self, command_tail: str):
        user_id, duration, reason = command_tail.split(' ', 2)
        duration, unit = int(duration[:-1]), duration[-1]
        duration = timedelta(**{self.unit_mapping[unit]: duration})
        return int(user_id), duration, reason

    async def execute(self, command_tail: str, message: Message, client: Client):
        try:
            user_id, duration, reason = self.parse_command_args(command_tail)
        except Exception:
            return await self.handle_error('format', client)

        user: Optional[User] = await self.get_user(client, user_id)
        if user is None:
            return await self.handle_error('no such user', client)

        try:
            async with ModerActionAPIClient() as api_client:
                action = await api_client.create_action(user.id, message.author.id, reason, self._type, duration)
        except APIException as e:
            if e.status == 403:
                return await self.handle_error('forbidden', client)
            else:
                raise

        await self.handle_success(user, reason, action['id'], client)


@register
class WarnCommand(ReasonAction):
    _type = 'warn'


@register
class KickCommand(ReasonAction):
    _type = 'kick'

    async def post_action(self, user: User, reason: str, client: Client):
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        await guild.kick(user, reason=reason)


@register
class MuteCommand(TimeReasonAction):
    _type = 'mute'

    async def post_action(self, user: User, reason: str, client: Client):
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        member: Member = await guild.fetch_member(user.id)
        role: Role = guild.get_role(conf.MUTE_ROLE_ID)
        await member.add_roles(role, reason=reason)


@register
class UnmuteCommand(ReasonAction):
    _type = 'unmute'

    async def post_action(self, user: User, reason: str, client: Client):
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        member: Member = await guild.fetch_member(user.id)
        role: Role = guild.get_role(conf.MUTE_ROLE_ID)
        await member.remove_roles(role, reason=reason)


@register
class BanCommand(TimeReasonAction):
    _type = 'ban'

    async def post_action(self, user: User, reason: str, client: Client):
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        await guild.ban(user, reason=reason)


@register
class UnbanCommand(ReasonAction):
    _type = 'unban'

    async def get_user(self, client: Client, user_id):
        user = await super().get_user(client, user_id)
        if user:
            return user
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        for ban in await guild.bans():
            if ban.user.id == user_id:
                return ban.user

    async def post_action(self, user: User, reason: str, client: Client):
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        await guild.unban(user, reason=reason)


@register
class AppealCommand(BaseCommand):
    cooldown_error_embed_name: Optional[str] = 'appeal_error'
    cooldown_error_embed_channel_id = conf.COMMANDS_CHANNEL_ID

    async def get_user(self, user_id: str, message: Message, client: Client) -> Optional[Member]:
        if message.mentions:
            return message.mentions[0]
        try:
            user_id = int(user_id)
        except ValueError:
            return None
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        user = guild.get_member(user_id)
        if user is not None:
            return user

        try:
            return await guild.fetch_member(user_id)
        except NotFound:
            return None

    def get_cooldown_error_embed_data(self, message, command_tail, ttl):
        if self.cooldown:
            return {
                'author_id': message.author.id,
                'reason': 'cooldown',
                'cooldown': ttl,
            }

    @staticmethod
    async def post_result(data, embed_name, member: Member):
        msg_data = await EmbedAPIClient.class_render_embed(embed_name, data)
        await send_dm(member, msg_data)

    async def execute(self, command_tail: str, message: Message, client: Client):
        await message.delete()
        command_tail = re.sub(' +', ' ', command_tail.strip())
        try:
            user_id, reason = command_tail.split(' ', 1)
        except ValueError:
            await self.post_result({
                'user_id': message.author.id,
                'reason': 'command',
            }, 'appeal_error', message.author)
            return
        user = await self.get_user(user_id, message, client)

        if not user:
            await self.post_result({
                'user_id': message.author.id,
                'reason': 'user',
            }, 'appeal_error', message.author)
            return

        if user.id == message.author.id:
            await self.post_result({
                'user_id': message.author.id,
                'reason': 'self appeal',
            }, 'appeal_error', message.author)
            return

        for role_id in conf.STAFF_DISCORD_ROLES:
            for role in user.roles:
                if role_id == role.id:
                    await self.post_result({
                        'user_id': message.author.id,
                        'reason': 'staff',
                    }, 'appeal_error', message.author)
                    return

        async with AppealAPIClient() as api_client:
            appeal = await api_client.create_appeal(user.id, message.author.id, reason)

        await self.post_result({
            'appeal_id': appeal['id'],
        }, 'appeal', message.author)
