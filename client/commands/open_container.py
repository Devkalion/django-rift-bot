from discord import Message, Client
from discord.abc import Messageable

from api import EmbedAPIClient, ContainersAPIClient, APIException
from conf import conf
from . import register
from .base import BaseCommand


@register
class OpenCommand(BaseCommand):
    @staticmethod
    async def send_success(reward, user_id, container_name, container_id, count, client):
        msg_data = await EmbedAPIClient.class_render_embed('open_container', {
            'user_id': user_id,
            'container_name': container_name,
            'container_id': container_id,
            'count': count,
            'reward_dict': reward
        })

        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)

    @staticmethod
    async def send_error(reason: str, user, container_name, client: Client, container_id=-1):
        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        msg_data = await EmbedAPIClient.class_render_embed('open_container_error', {
            'user_id': user.id,
            'container_name': container_name,
            'container_id': container_id,
            'reason': reason
        })
        await channel.send(**msg_data)

    @staticmethod
    def get_form(count):
        if count % 10 == 1 and count % 100 != 11:
            return 'form1'
        elif count % 10 in (2, 3, 4) and count % 100 // 10 != 1:
            return 'form2'
        else:
            return 'form3'

    async def execute(self, command_tail: str, message: Message, client: Client):
        words = [
            word
            for word in command_tail.strip().split(' ')
            if word
        ]

        if not words:
            return await self.send_error('container', message.author, '', client, -1)

        if words[0] == 'все':
            form = 'form4'
            count = 'все'
            container_name = ' '.join(words[1:])
        else:
            try:
                count = int(words[0])
                container_name = ' '.join(words[1:])
                form = self.get_form(count)

            except ValueError:
                form = 'form1'
                count = 1
                container_name = ' '.join(words[:])

            if count < 1:
                return await self.send_error('non natural', message.author, container_name, client)

        if not container_name.strip():
            return await self.send_error('container', message.author, '-', client)

        async with ContainersAPIClient() as api_client:
            container = await api_client.find(form, container_name)
            if container is None:
                await self.send_error('container', message.author, container_name, client)
                return

            try:
                response = await api_client.open(message.author.id, container['id'], count)
                reward = response['reward']
                if count == 'все':
                    count = response['count']
                    container_name = container[self.get_form(count)]

            except APIException as error:
                reason = error.data.get('reason', 'container')
                await self.send_error(reason, message.author, container['name'], client, container['id'])
                return

        await self.send_success(reward, message.author.id, container_name, container["id"], count, client)
