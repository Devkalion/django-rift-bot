from . import register
from .base import UserMentionCommand


@register
class ProfileCommand(UserMentionCommand):
    embed_name: str = 'profile'
