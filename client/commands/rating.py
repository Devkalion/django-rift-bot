from . import register
from .base import RenderCommand


@register
class ActiveUsersCommand(RenderCommand):
    embed_name = 'active_users'
