import logging
import re
import sys

from discord import Member, Client, Guild, Message

from api import RoleAPIClient, PersonalRoleAPIClient, APIException, EmbedAPIClient
from conf import conf
from utils import grant_role, is_role_granted, revoke_role, report_error
from . import register
from .base import PrepareDataCommand, UserMentionCommand, BaseCommand

logger = logging.getLogger(sys.argv[1])


@register
class RoleCommand(UserMentionCommand):
    embed_name: str = 'roles'
    allow_dm = False

    @staticmethod
    async def prepare_data(user: Member):
        return {
            'user_id': user.id,
            'active_role_ids': [role.id for role in user.roles],
            'avatar': str((user.avatar or user.default_avatar).url),
        }


async def get_role(user_id, role_id):
    try:
        role_id = int(role_id)
    except ValueError:
        return None
    async with RoleAPIClient() as api_client:
        return await api_client.get_user_role(user_id, role_id)


@register
class SetRoleCommand(PrepareDataCommand):
    success_embed_name: str = 'set_role'
    error_embed_name: str = 'set_role_error'

    @staticmethod
    async def populate_data(role_id, user_id, client: Client):
        role = await get_role(user_id, role_id)
        if role is None:
            return {
                'role_id': role_id,
                'reason': 'user_role'
            }
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        user: Member = guild.get_member(user_id)
        if is_role_granted(user, role['discord_id']):
            return {
                'role_id': role_id,
                'reason': 'already_set'
            }
        await grant_role(role, user, client)
        return {'role_id': role_id}


@register
class ClearRoleCommand(PrepareDataCommand):
    success_embed_name: str = 'clear_role'
    error_embed_name: str = 'clear_role_error'

    @staticmethod
    async def populate_data(role_id, user_id, client: Client):
        role = await get_role(user_id, role_id)
        if role is None:
            return {
                'role_id': role_id,
                'reason': 'user_role'
            }
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        user: Member = guild.get_member(user_id)
        if not is_role_granted(user, role['discord_id']):
            return {
                'role_id': role_id,
                'reason': 'already_cleared'
            }
        await revoke_role(role, user, client)
        return {
            'role_id': role_id
        }


@register
class EditPersonalRoleCommand(BaseCommand):
    cooldown_error_embed_name = 'edit_personal_role_error'
    validators = [
        (
            lambda command_tail: command_tail.split(' ', 2),
            'invalid command'
        ),
        (
            lambda role_id, *args: (int(role_id), *args),
            'invalid role_id'
        ),
        (
            lambda role_id, color, name: (role_id, int(color.replace('#', ''), 16), name),
            'invalid color'
        ),
        (
            lambda role_id, color, name: (role_id, color, name) if 0 <= color <= 0xFFFFFF else None,
            'invalid color'
        ),
        (
            lambda role_id, color, name: (role_id, color, name) if len(name) <= 40 else None,
            'name length'
        ),
        (
            lambda role_id, color, name: (role_id, color, name) if name > 0 else None,
            'empty name'
        ),
    ]

    def parse_input(self, command_tail: str):
        error_reason = None
        role_id, color, name = [None] * 3
        args = [re.sub(r'\s+', ' ', command_tail)]
        for validator, error in self.validators:
            try:
                role_id, color, name = validator(*args)
            except ValueError:
                error_reason = error
                break
        return error_reason, role_id, color, name

    async def execute(self, command_tail: str, message: Message, client: Client):
        user_id = message.author.id
        error_reason, role_id, color, name = self.parse_input(command_tail)
        if not error_reason:
            try:
                async with PersonalRoleAPIClient() as api_client:
                    await api_client.update_role(role_id, user_id, name, color)
            except APIException as e:
                if e.status == 404:
                    error_reason = 'no role'
                elif e.status == 400:
                    error_reason = 'duplicated name'
                else:
                    error_reason = 'unknown'
            except Exception as e:
                logger.exception("Exception occurred")
                report_error(
                    client, e, PersonalRoleAPIClient.update_role,
                    user_id=user_id, role_id=role_id, name=name, color=color
                )
                error_reason = 'unknown'

        guild = client.get_guild(conf.MAIN_GUILD_ID)
        channel = guild.get_channel(conf.COMMANDS_CHANNEL_ID)

        if error_reason:
            self.drop_cd_block(user_id)
            embed_name = 'edit_personal_role_error'
            embed_data = {
                'user_id': user_id,
                'reason': error_reason,
            }
        else:
            embed_name = 'edit_personal_role'
            embed_data = {
                'user_id': user_id,
                'personal_role_id': role_id,
            }

        msg_data = await EmbedAPIClient.class_render_embed(embed_name, embed_data)
        await channel.send(**msg_data)


@register
class EditPersonalRoleNameCommand(EditPersonalRoleCommand):
    validators = [
        (
            lambda command_tail: (command_tail.split(' ', 1)[0], None, command_tail.split(' ', 1)[1]),
            'invalid command'
        ),
        (
            lambda role_id, *args: (int(role_id), *args),
            'invalid role_id'
        ),
        (
            lambda role_id, color, name: (role_id, color, name) if len(name) <= 40 else None,
            'name length'
        ),
        (
            lambda role_id, color, name: (role_id, color, name) if name > 0 else None,
            'empty name'
        ),
    ]


@register
class EditPersonalRoleColorCommand(EditPersonalRoleCommand):
    validators = [
        (
            lambda command_tail: (*command_tail.split(' '), None),
            'invalid command'
        ),
        (
            lambda role_id, *args: (int(role_id), *args),
            'invalid role_id'
        ),
        (
            lambda role_id, color, name: (role_id, int(color.replace('#', ''), 16), name),
            'invalid color'
        ),
        (
            lambda role_id, color, name: (role_id, color, name) if 0 <= color <= 0xFFFFFF else None,
            'invalid color'
        )
    ]
