from discord import Message, Client
from discord.abc import Messageable
from redis import Redis

from api import ShopAPIClient, APIException, EmbedAPIClient
from conf import conf, REDIS_CONNECTION_SETTINGS
from utils import report_error
from . import register
from .base import BaseCommand


@register
class ShopCommand(BaseCommand):
    async def execute(self, command_tail: str, message: Message, client: Client):
        async with ShopAPIClient() as api_client:
            shop = await api_client.get_shop(command_tail)

        async with EmbedAPIClient() as api_client:
            if shop is None:
                msg_data = await api_client.render_embed('shop_error', {'user_id': message.author.id})
            else:
                msg_data = await api_client.render_embed('shop', {'shop_id': shop['id']})

        await message.reply(**msg_data)


@register
class BuyCommand(BaseCommand):
    @staticmethod
    async def populate_data(command_args, user_id):
        if len(command_args) == 0:
            return {'reason': 'invalid command'}

        if len(command_args) == 1:
            buy_name = ''
            item_id = command_args[0]
        else:
            buy_name = command_args[0]
            item_id = command_args[1]

        try:
            item_id = int(item_id)
        except ValueError:
            return {'reason': 'invalid item_id'}

        async with ShopAPIClient() as api_client:
            shop = await api_client.get_shop(buy_name=buy_name)
            if shop is None:
                return {'reason': 'no such shop'}

        redis_key = f'command-buy-{shop["id"]}-{user_id}'
        if shop['cooldown_seconds']:
            with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
                ttl = redis.ttl(redis_key)
                if ttl and ttl > 0:
                    return {'reason': 'cooldown', 'cooldown': ttl}

        try:
            async with ShopAPIClient() as api_client:
                reward = await api_client.buy(shop['id'], item_id, user_id)
        except APIException as e:
            if e.status == 404:
                return {'reason': 'no such item'}
            if e.status == 403:
                return {'reason': e.data['detail']}
            if e.status == 400:
                return {'reason': 'not enough', 'missing_inventory': e.data}
            return {'reason': 'unknown', 'error': e}

        if shop['cooldown_seconds']:
            with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
                redis.set(redis_key, 1, ex=shop['cooldown_seconds'])

        return {
            'reward_dict': reward,
            'shop_id': shop['id'],
            'item_id': item_id,
        }

    async def execute(self, command_tail: str, message: Message, client: Client):
        args = filter(None, command_tail.split(' '))
        data = await self.populate_data(list(args), message.author.id)
        data['user_id'] = message.author.id

        if data.get('reason') == 'unknown':
            report_error(client, data.pop('error'), self.execute, command_tail)

        embed_name = 'buy_error' if 'reason' in data else 'buy'
        msg_data = await EmbedAPIClient.class_render_embed(embed_name, data)
        channel = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)
