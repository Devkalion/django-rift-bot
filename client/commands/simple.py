from contextlib import suppress

from discord import Message, Client, errors

from api import EmbedAPIClient
from .base import BaseCommand, SubprocessName


class SimpleCommand(BaseCommand):
    subprocess = SubprocessName.NONE

    def __init__(self, input_channel_ids, result_channel_id, embed, **kwargs):
        super().__init__()
        if not input_channel_ids:
            input_channel_ids = ''

        if result_channel_id:
            self.result_channel_id = int(result_channel_id)
        else:
            self.result_channel_id = None

        input_channel_ids = list(map(str.strip, input_channel_ids.split(',')))
        self.embed_name = embed
        self.allow_dm = '' in input_channel_ids
        self.channels = [
            int(channel_id)
            for channel_id in input_channel_ids
            if channel_id
        ]

    async def execute(self, command_tail: str, message: Message, client: Client):
        user = message.author
        msg_data = await EmbedAPIClient.class_render_embed(
            self.embed_name,
            {'user_id': user.id, 'avatar': str((user.avatar or user.default_avatar).url)}
        )
        if self.result_channel_id:
            channel = client.get_channel(self.result_channel_id)
        else:
            channel = await user.create_dm()
        with suppress(errors.Forbidden):
            await channel.send(**msg_data)
