from discord import Client, Message
from discord.abc import Messageable

from api import UserAPIClient, APIException, EmbedAPIClient
from conf import conf
from data_classes import User
from . import register
from .base import PrepareDataCommand, UserMentionCommand, BaseCommand


@register
class TitlesCommand(UserMentionCommand):
    embed_name = 'titles'


@register
class SetTitleCommand(PrepareDataCommand):
    success_embed_name: str = 'set_title'
    error_embed_name: str = 'set_title_error'

    @staticmethod
    async def populate_data(title_id, user_id, client: Client):
        try:
            title_id = int(title_id)
        except ValueError:
            return {
                'reason': 'no_title',
                'title_id': title_id,
            }

        async with UserAPIClient() as api_client:
            _user: User = await api_client.get_user(user_id)
            if _user.active_title != title_id:
                try:
                    await api_client.set_title(user_id, title_id)
                except APIException:
                    return {
                        'reason': 'no_title',
                        'title_id': title_id,
                    }
            else:
                return {
                    'reason': 'already_set',
                    'title_id': title_id,
                }

        return {'title_id': title_id}


@register
class ClearTitleCommand(BaseCommand):
    @staticmethod
    async def clear_title_error(user_id, reason, client):
        msg_data = await EmbedAPIClient.class_render_embed('clear_title_error', {
            'user_id': user_id,
            'reason': reason
        })
        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)

    async def execute(self, command_tail: str, message: Message, client: Client):
        user_id = message.author.id
        try:
            async with UserAPIClient() as api_client:
                _user: User = await api_client.get_user(user_id)
                if _user.active_title:
                    await api_client.set_title(user_id, '')
        except APIException:
            return await self.clear_title_error(user_id, 'no title', client)

        if not _user.active_title:
            return await self.clear_title_error(user_id, 'already_cleared', client)

        msg_data = await EmbedAPIClient.class_render_embed('clear_title', {
            'user_id': user_id,
        })
        channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)
        await channel.send(**msg_data)
