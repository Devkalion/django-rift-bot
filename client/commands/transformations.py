from api import TransformationsAPIClient
from . import register
from .base import RenderCommand, BaseTransformationCommand


@register
class TransformationsCommand(RenderCommand):
    embed_name = 'transformations'


@register
class TransformCommand(BaseTransformationCommand):
    success_embed_name: str = 'transform_started'
    error_embed_name: str = 'transform_error'

    async def api_call(self, transformation_id, user_id):
        async with TransformationsAPIClient() as api_client:
            return {
                'to_date': await api_client.start_transform(transformation_id, user_id),
                'transformation_id': transformation_id,
            }
