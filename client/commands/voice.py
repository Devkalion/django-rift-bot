import asyncio

from discord import Message, Client, Guild, VoiceChannel, PermissionOverwrite, Member
from discord.abc import Messageable

from api import EmbedAPIClient
from conf import conf
from . import register
from .base import BaseCommand


@register
class VoiceKickCommand(BaseCommand):
    @staticmethod
    async def handle_error(channel: Messageable, reason, admin_id, user_id=None):
        msg_data = await EmbedAPIClient.class_render_embed('voice_kick_error', {
            'reason': reason,
            'user_id': user_id,
            'admin_id': admin_id,
        })
        await channel.send(**msg_data)

    async def execute(self, command_tail: str, message: Message, client: Client):
        guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
        message_channel: Messageable = client.get_channel(conf.COMMANDS_CHANNEL_ID)

        channel: VoiceChannel
        for channel in guild.voice_channels:
            if message.author not in channel.members:
                continue
            permissions: PermissionOverwrite = channel.overwrites_for(message.author)
            if permissions.manage_channels:
                break
        else:
            return await self.handle_error(message_channel, 'no channel', message.author.id)

        mentions = message.mentions
        if mentions:
            user = mentions[0]
        else:
            try:
                user_id = int(command_tail)
                user = client.get_user(user_id)
                if user is None:
                    user = message.author
            except ValueError:
                return await self.handle_error(message_channel, 'no user', message.author.id)

        if user not in channel.members:
            return await self.handle_error(message_channel, 'not member', message.author.id, user.id)

        if user.id == message.author.id:
            return await self.handle_error(message_channel, 'self kick', message.author.id, user.id)

        member: Member = guild.get_member(user.id)
        _, _, msg_data = await asyncio.gather(
            member.move_to(None),
            channel.set_permissions(member, connect=False),
            EmbedAPIClient.class_render_embed('voice_kick', {
                'user_id': user.id,
                'admin_id': message.author.id,
            })
        )
        await message_channel.send(**msg_data)
