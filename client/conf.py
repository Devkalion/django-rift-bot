import os as _os
import sys
from datetime import datetime
from logging.config import dictConfig
from time import sleep
from typing import Iterable, Optional

import redis.exceptions
from dotenv import load_dotenv
from redis import Redis

load_dotenv('../.env')

LOCAL_TZ = datetime.now().astimezone().tzinfo

REDIS_CONNECTION_SETTINGS = {
    'host': _os.environ.get('REDIS_HOST') or 'localhost',
    'port': int(_os.environ.get('REDIS_PORT', 0)) or 6379,
    'db': int(_os.environ.get('REDIS_DB', 0)),
    'password': _os.environ.get('REDIS_PASSWORD'),
}

RABBIT_MQ_CONNECTION_SETTINGS = {
    'host': _os.environ.get('RABBIT_MQ_HOST') or 'localhost',
    'port': int(_os.environ.get('RABBIT_MQ_PORT', 0)) or 5672,
    'login': _os.environ.get('RABBIT_MQ_LOGIN') or 'guest',
    'password': _os.environ.get('RABBIT_MQ_PASSWORD') or 'guest',
}

API_URL = _os.environ.get('API_URL', 'http://localhost:8000/api')

BOOLEAN_SETTINGS = {
    'ALLOW_DM_COMMANDS', 'WELCOME', 'LEVEL_UP_CHANNEL_MSG',

}

INT_SETTINGS = {
    'COMMANDS_CHANNEL_ID', 'MODER_CHANNEL_ID', 'MAIN_MESSAGE_CHANNEL_ID', 'MAIN_GUILD_ID', 'USER_MESSAGE_CHANNEL_ID',
    'ERROR_CHANNEL_ID', 'LOG_CHANNEL_ID', 'MESSAGE_LOG_CHANNEL_ID', 'MUTE_ROLE_ID', 'MESSAGE_EXPERIENCE',
    'MESSAGE_COOLDOWN', 'STRICT_MESSAGE_COOLDOWN', 'MESSAGES_BEFORE_COOLDOWN', 'PRIVATE_VOICE_CHANNELS_CATEGORY_ID',
    'ON_AIR_ROLE_ID', 'ON_AIR_MESSAGE_CHANNEL_ID', 'PERSONAL_ROLE_POSITION'
}

LIST_SETTINGS = {
    'MESSAGE_CHANNELS', 'LOGGABLE_MESSAGE_CHANNELS', 'STAFF_DISCORD_ROLES', 'IGNORE_COMMAND_CHANNELS'

}


class CONF:
    TOKEN = _os.environ['TOKEN']
    USER_PAGE_SIZE = int(_os.environ['USER_PAGE_SIZE'])
    USERS_THREADS = int(_os.environ.get('USERS_THREADS', 1))
    DISCORD_PROXY_URL: Optional[str] = _os.environ.get('DISCORD_PROXY_URL')
    DISCORD_PROXY_LOGIN: Optional[str] = _os.environ.get('DISCORD_PROXY_LOGIN')
    DISCORD_PROXY_PASSWORD: Optional[str] = _os.environ.get('DISCORD_PROXY_PASSWORD')

    def __init__(self):
        self._redis = Redis(**REDIS_CONNECTION_SETTINGS)

    def get_value(self, item):
        while True:
            try:
                value = self._redis.get(f'settings-{item}')
            except redis.exceptions.ConnectionError:
                sleep(0.5)
            else:
                break
        if value is not None:
            return value.decode()

    def __getattribute__(self, item):
        if item in (
            'TOKEN', 'USER_PAGE_SIZE', 'USERS_THREADS',
            'DISCORD_PROXY_URL', 'DISCORD_PROXY_LOGIN', 'DISCORD_PROXY_PASSWORD',
            '_redis', 'get_value',
        ):
            return super().__getattribute__(item)
        value = self.get_value(item)
        if item in BOOLEAN_SETTINGS:
            return value and value.lower() not in ('0', 'f', 'false', '')
        if item in INT_SETTINGS:
            return int(value)
        if item in LIST_SETTINGS:
            if value is None:
                return []
            return [
                int(channel.strip())
                for channel in value.split(',')
                if channel.strip() != ''
            ]
        return value

    ALLOW_DM_COMMANDS: bool = None
    COMMANDS_CHANNEL_ID: int = None
    MODER_CHANNEL_ID: int = None
    MAIN_MESSAGE_CHANNEL_ID: int = None
    MAIN_GUILD_ID: int = None
    MESSAGE_CHANNELS: Iterable[int] = None
    IGNORE_COMMAND_CHANNELS: Iterable[int] = None
    ERROR_CHANNEL_ID: int = None
    LOG_CHANNEL_ID: int = None
    MESSAGE_LOG_CHANNEL_ID: int = None
    LOGGABLE_MESSAGE_CHANNELS: Iterable[int] = None
    STAFF_DISCORD_ROLES: Iterable[int] = None
    MUTE_ROLE_ID: int = None
    MESSAGE_EXPERIENCE: int = None
    MESSAGE_COOLDOWN: int = None
    PRIVATE_VOICE_CHANNELS_CATEGORY_ID: int = None
    STRICT_MESSAGE_COOLDOWN: int = None
    MESSAGES_BEFORE_COOLDOWN: int = None
    WELCOME: bool = None
    LEVEL_UP_CHANNEL_MSG: bool = None
    USER_MESSAGE_CHANNEL_ID: int = None
    BOT_ACTIVITY_TEXT: str = None
    ON_AIR_ROLE_ID: int = None
    ON_AIR_MESSAGE_CHANNEL_ID: int = None
    PERSONAL_ROLE_POSITION: int = None


conf = CONF()

LOGGING = {
    'version': 1,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': f'../logs/client_{sys.argv[1]}.log',
            'formatter': 'simple',
            'maxBytes': 50 * 1024 * 1024,  # 50MB
            'backupCount': 1,
        },
        'complex_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '../logs/client.log',
            'formatter': 'mode',
            'maxBytes': 50 * 1024 * 1024,  # 50MB
            'backupCount': 1,
        }
    },
    'formatters': {
        'simple': {
            'format': '%(asctime)s [%(levelname)s] %(message)s',
        },
        'mode': {
            'format': f'%(asctime)s [%(name)s] [%(levelname)s] %(message)s',
        },
    },
    'root': {
        'handlers': ['console', 'file', 'complex_file'],
        'level': 'DEBUG'
    },
}

dictConfig(LOGGING)
