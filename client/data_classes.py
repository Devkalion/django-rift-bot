from typing import Optional

from attr import dataclass


@dataclass
class SmallUser:
    discord_id: int
    name: str
    roles: list
    left: bool
    is_streamer: bool
    is_partner: bool

@dataclass
class User:
    discord_id: int
    name: str
    level: int
    experience: int
    is_admin: bool
    is_developer: bool
    is_moder: bool
    is_streamer: bool
    is_partner: bool
    active_background: int
    active_title: int
    roles: list
    left: bool


@dataclass
class Event:
    id: int
    user_id: Optional[int]
    status: str
    type: str
    data: str


@dataclass
class ActiveTransform:
    id: int
    user_id: int
    transformation_id: int


@dataclass
class ProcessMessage:
    command: str
    handler: Optional[str]
    guild_id: Optional[int]
    channel_id: int
    message_id: int
    api_command: Optional[dict]
