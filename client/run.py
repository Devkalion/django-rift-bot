import asyncio
import sys

from conf import conf

if __name__ == '__main__':
    if sys.argv[1] == 'msg':
        from clients.messages import MyClient

        MyClient.launch(max_messages=5000)
    elif sys.argv[1] == 'cmd':
        from clients.commands import MyClient

        MyClient.launch()
    elif sys.argv[1] == 'slash':
        from clients.slash import client

        client.run(conf.TOKEN)
    elif sys.argv[1] == 'split_cmd':
        from clients.commands_splitted import MyClient

        MyClient.launch()
    elif sys.argv[1] == 'handler':
        from clients.command_handler import main as handler

        asyncio.run(handler())
    elif sys.argv[1] == 'listeners':
        from clients.listeners import main as listeners

        asyncio.run(listeners())
    elif sys.argv[1] == 'sync':
        from clients.sync import MyClient

        MyClient.launch()
    elif sys.argv[1] == 'reactions':
        from clients.reactions import MyClient

        MyClient.launch()
    elif sys.argv[1] == 'check':
        print('ok')
    else:
        raise Exception('unknown mode')
