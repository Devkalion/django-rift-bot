import asyncio
import importlib
import logging
import sys

if __name__ == '__main__':
    script_name = (sys.argv[1].split("/")[-1]).split(".")[0]
    sys.argv[1] = script_name
    script_file = importlib.import_module(f'scripts.{script_name}')
    logging.debug(f"Running {script_name}")
    operation = script_file.main
    loop = asyncio.get_event_loop()
    loop.run_until_complete(operation())
