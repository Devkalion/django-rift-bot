import logging

from discord import Intents, Guild
from discord.errors import NotFound

from clients.base import BaseClient
from conf import conf

user_ids = [
]


async def ban_users(client):
    guild: Guild = await client.fetch_guild(conf.MAIN_GUILD_ID)
    for user_id in user_ids:
        logging.debug(f"Banning {user_id}")
        try:
            member = await guild.fetch_member(user_id)
            if member is None:
                continue
            await guild.ban(member, reason="Possible spam")
        except NotFound:
            pass


async def main():
    client = BaseClient(intents=Intents(guilds=True))
    await client.login(conf.TOKEN)
    try:
        await ban_users(client)
    except Exception as e:
        logging.error(e, exc_info=e)
    finally:
        await client.close()
