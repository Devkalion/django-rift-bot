import asyncio
import logging
from datetime import datetime, timedelta, timezone
from typing import List

from discord import Client, Intents, Message

from clients.base import BaseClient
from conf import conf


async def f(client: Client):
    error_channel = await client.fetch_channel(conf.ERROR_CHANNEL_ID)
    if error_channel is None:
        raise Exception("no such channel")
    messages: List[Message] = [m async for m in error_channel.history(limit=50)]
    while len(messages):
        if messages[-1].created_at > datetime.now(tz=timezone.utc) - timedelta(days=14):
            await error_channel.delete_messages(messages)
            await asyncio.sleep(0.5)
            messages: List[Message] = [m async for m in error_channel.history(limit=50)]
        else:
            async for message in error_channel.history(limit=200):
                await message.delete()
                await asyncio.sleep(0.5)
            messages: List[Message] = [m async for m in error_channel.history(limit=2)]


async def main():
    client = BaseClient(intents=Intents(guilds=True))
    await client.login(conf.TOKEN)
    try:
        await f(client)
    except Exception as e:
        logging.error(e, exc_info=e)
    finally:
        await client.close()
