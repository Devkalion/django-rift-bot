import asyncio
import logging

from discord import Guild, Intents

from api import RoleAPIClient
from clients.base import BaseClient
from clients.sync import create_role, delete_role, update_role
from conf import conf
from utils import get_role


async def sync_roles(client):
    async with RoleAPIClient() as api_client:
        roles = await api_client.get_roles()
    guild: Guild = await client.fetch_guild(conf.MAIN_GUILD_ID)
    roles = {role['discord_id']: role for role in roles}
    coroutines = []
    for role in await guild.fetch_roles():
        server_role = roles.get(role.id)
        if server_role is None:
            coroutines.append(create_role(role))
        else:
            coroutines.append(update_role(role, server_role))

    for role_id in roles:
        if not await get_role(role_id, guild):
            coroutines.append(delete_role(role_id))

    if len(coroutines):
        await asyncio.gather(*coroutines)


async def main():
    client = BaseClient(intents=Intents(guilds=True))
    await client.login(conf.TOKEN)
    try:
        await sync_roles(client)
    except Exception as e:
        logging.error(e, exc_info=e)
    finally:
        await client.close()
