import asyncio
import itertools
import logging
from typing import List, Tuple

from discord import Client, Guild, Intents, Member

from api import UserAPIClient
from clients.base import BaseClient
from clients.sync import chunks, create_user, leaving_user, returning_user, update_name
from conf import conf
from data_classes import SmallUser

coroutines_limit = 10


async def get_users():
    async with UserAPIClient() as api_client:
        count, _users = await api_client.get_users()
        coroutines = [
            api_client.get_users(offset)
            for offset in range(conf.USER_PAGE_SIZE, count, conf.USER_PAGE_SIZE)
        ]
        responses = []
        for coroutine_chunk in chunks(coroutines, conf.USERS_THREADS):
            _responses = await asyncio.gather(*coroutine_chunk)
            responses += [resp[1] for resp in _responses]
    return {user.discord_id: user for user in itertools.chain(_users, *responses)}


def split_users(members, api_users) -> Tuple[List[Member], List[Member]]:
    registered_users, unregistered_users = [], []
    for member in members:
        array = registered_users if member.id in api_users else unregistered_users
        array.append(member)
    return registered_users, unregistered_users


queue = asyncio.Queue()


async def sync_users(client: Client):
    users = await get_users()
    guild: Guild = await client.fetch_guild(conf.MAIN_GUILD_ID)
    members = [member async for member in guild.fetch_members(limit=None)]
    registered_users, unregistered_users = split_users(members, users)
    for members_chunk in chunks(unregistered_users, 20):
        await asyncio.gather(*[
            create_user(member)
            for member in members_chunk
        ])

    logging.info('All missing users created')

    for member in registered_users:
        user: SmallUser = users[member.id]
        if user.name != member.display_name:
            await queue.put(update_name(member))

    banned_users = {
        be.user.id
        for be in await guild.bans()
    }
    members = {
        member.id
        for member in members
    }

    for _id, user in users.items():
        actually_left = _id in banned_users or _id not in members
        if actually_left == user.left:
            continue
        if actually_left:
            await queue.put(leaving_user(_id))
        else:
            await queue.put(returning_user(_id))

    logging.info('All tasks scheduled')
    while not queue.empty():
        coroutine = await queue.get()
        await coroutine
        queue.task_done()
    logging.info('All tasks completed')


async def main():
    client = BaseClient(intents=Intents(guilds=True, members=True, presences=True))
    await client.login(conf.TOKEN)
    try:
        await sync_users(client)
    except Exception as e:
        logging.error(e, exc_info=e)
    finally:
        await client.close()
