import logging

import discord
from discord import app_commands, Permissions
from discord.app_commands import CommandTree, Command

from api import SlashCommandsAPIClient
from clients import slash

command_tree = CommandTree(client=slash.client)


async def refresh_commands():
    logging.info('Refreshing commands')
    command_tree.clear_commands(guild=None)
    async with SlashCommandsAPIClient() as api_client:
        commands = await api_client.get_commands()
    for command in commands:
        print('~', end=' ')
        parsed_command = parse_command(command)
        if parsed_command is None:
            print('asda')
        print(parsed_command.name, end=' ')
        command_tree.add_command(parsed_command)
        print('~')
    await command_tree.sync()
    logging.info('Commands refreshed')


def parse_command(command_dict):
    if command_dict['type'] in handlers:
        parameters = command_dict['parameters']
        parameters_names = {
            pname: pdict["name"]
            for pname, pdict in parameters.items()
        }
        parameters_descriptions = {
            pname: pdict["description"]
            for pname, pdict in parameters.items()
        }
        handler = handlers[command_dict['type']]
        if parameters_descriptions:
            handler = app_commands.describe(**parameters_descriptions)(handler)
        if parameters_names:
            handler = app_commands.rename(**parameters_names)(handler)
        cmd = Command(
            name=command_dict['name'],
            description=command_dict['description'],
            callback=handler,
            extras=command_dict.get('extras'),
        )
        cmd.guild_only = True
        cmd.on_error = on_error
        cmd.default_permissions = Permissions(use_application_commands=True)
        return cmd
    if command_dict['type'] == 'group':
        group = app_commands.Group(name=command_dict["name"], description=command_dict["description"], guild_only=True)
        for inner_command_dict in command_dict["commands"]:
            inner_command = parse_command(inner_command_dict)
            group.add_command(inner_command)
        return group


async def on_error(interaction: discord.Interaction, error: app_commands.AppCommandError):
    if isinstance(error, app_commands.CommandOnCooldown):
        await interaction.response.send_message(
            f'{error}\n'
            f'command name: {interaction.command.name}\n'
            f'per: {error.cooldown.per}\n'
            f'rate: {error.cooldown.rate}\n'
            f'retry after {error.retry_after}',
            ephemeral=True
        )
        return
    logging.error(f'Some error: {error}', exc_info=error)


from .activities import start_activity_command
from .claim import claim_command
from .containers import open_container_command
from .market import exchange_command, sale_command
from .message import user_message_command
from .moder_actions import (
    appeal_command,
    ban_command,
    kick_command,
    mute_command,
    unban_command,
    unmute_command,
    warn_command,
)
from .private_channel import channel_add_command, channel_kick_command
from .refresh import refresh_command
from .shop import buy_command, shop_command
from .simple import render_simple_embed_command
from .user_info import user_info_command
from .voice_kick import voice_kick_command


handlers = {
    "appeal": appeal_command,
    "ban": ban_command,
    "buy": buy_command,
    "channel_add": channel_add_command,
    "channel_kick": channel_kick_command,
    "claim": claim_command,
    "exchange": exchange_command,
    "kick": kick_command,
    "message": user_message_command,
    "mute": mute_command,
    "open_container": open_container_command,
    "refresh_commands": refresh_command,
    "sale": sale_command,
    "shop": shop_command,
    "simple": render_simple_embed_command,
    "start_activity": start_activity_command,
    "unban": unban_command,
    "unmute": unmute_command,
    "user_info": user_info_command,
    "voice_kick": voice_kick_command,
    "warn": warn_command,
}
