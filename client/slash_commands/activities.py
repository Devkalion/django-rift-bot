import discord

from api import APIException
from api.activities import ActivityAPIClient
from .base import CommandError, command


@command
async def start_activity_command(interaction: discord.Interaction):
    activity_codename = interaction.command.extras['parameters']['activity_codename']

    async with ActivityAPIClient() as api_client:
        try:
            response = await api_client.start(activity_codename, interaction.user.id)
        except APIException as e:
            if e.status == 429:
                raise CommandError('cooldown', cooldown=e.data['cooldown'])
            if e.status == 422:
                raise CommandError('no result')
            if e.status == 400:
                raise CommandError('not enough', missing_inventory=e.data)
            raise

    return {
        'user_id': interaction.user.id,
        'reward_dict': response['reward'],
        'activity_result_id': response['activity_result_id'],
        'avatar': str((interaction.user.avatar or interaction.user.default_avatar).url),
    }
