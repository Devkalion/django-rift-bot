from functools import wraps
from typing import Callable

import discord
from discord.utils import MISSING
from redis import Redis

from api import EmbedAPIClient
from conf import REDIS_CONNECTION_SETTINGS
from utils import report_error


class CommandError(RuntimeError):
    def __init__(self, reason: str, **kwargs):
        super().__init__("")
        self.data = {"reason": reason, **kwargs}


def get_command_cooldown(user: discord.Member, app_command: discord.app_commands.Command) -> int:
    cooldown = app_command.extras['cooldown']
    redis_key = f'slash-{app_command.name}-{user.id}'
    with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
        ttl = redis.ttl(redis_key)
        if not ttl or ttl <= 0:
            redis.set(redis_key, 1, ex=cooldown)
            return 0
        else:
            return ttl


async def get_response_data(interaction: discord.Interaction, handler: Callable, *handler_args,
                            **handler_kwargs) -> dict:
    extras = interaction.command.extras

    if extras.get('cooldown'):
        cooldown = get_command_cooldown(interaction.user, interaction.command)
        if cooldown:
            return await EmbedAPIClient.class_render_embed(
                extras['error_embed_name'] or 'unknown error',
                {
                    'user_id': interaction.user.id,
                    'reason': 'cooldown',
                    'cooldown': cooldown,
                }
            )

    try:
        embed_data = await handler(interaction, *handler_args, **handler_kwargs)
        return await EmbedAPIClient.class_render_embed(extras['embed_name'], embed_data)
    except CommandError as e:
        return await EmbedAPIClient.class_render_embed(
            extras['error_embed_name'] or 'unknown error',
            {"user_id": interaction.user.id, **e.data}
        )
    except Exception as e:
        report_error(interaction.client, e, handler, *handler_args, **handler_kwargs)
        return await EmbedAPIClient.class_render_embed(
            'unknown error',
            {"user_id": interaction.user.id},
        )


def command(f):
    @wraps(f)
    async def func(interaction: discord.Interaction, *args, **kwargs):
        extras = interaction.command.extras
        await interaction.response.defer(ephemeral=extras.get('ephemeral'), thinking=True)
        msg_data = await get_response_data(interaction, f, *args, **kwargs)
        msg_data = {key: value for key, value in msg_data.items() if key != 'delete_after'}
        msg_data['attachments'] = msg_data.pop('files', MISSING)
        await interaction.edit_original_response(**msg_data)

    return func
