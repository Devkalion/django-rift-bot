import discord

from api import GiveawayAPIClient, APIException
from .base import command, CommandError


@command
async def claim_command(interaction: discord.Interaction):
    try:
        async with GiveawayAPIClient() as api_client:
            response: dict = await api_client.claim(interaction.user.id)
        return {
            "user_id": interaction.user.id,
            "giveaway_id": response["giveaway_id"],
            "reward_dict": response['reward'],
        }

    except APIException as e:
        if e.status == 404:
            raise CommandError('no active giveaways')
        elif e.status == 403:
            raise CommandError('blocked')
        elif e.status == 405:
            raise CommandError('ended')
        raise
