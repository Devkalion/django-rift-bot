from typing import Optional

import discord
from discord.app_commands import Range

from api import ContainersAPIClient, APIException
from .base import command, CommandError


def get_form(count):
    if count % 10 == 1 and count % 100 != 11:
        return 'form1'
    elif count % 10 in (2, 3, 4) and count % 100 // 10 != 1:
        return 'form2'
    else:
        return 'form3'


@command
async def open_container_command(interaction: discord.Interaction, amount: Optional[Range[int, 1]] = None):
    if amount is None:
        amount = 1
    container_id = interaction.command.extras['parameters']['container_id']

    async with ContainersAPIClient() as api_client:
        container = await api_client.retrieve(container_id)
        container_name = container[get_form(amount)]

        try:
            response = await api_client.open(interaction.user.id, container_id, amount)
            reward = response['reward']
        except APIException as error:
            raise CommandError(
                error.data.get('reason', 'container'),
                user_id=interaction.user.id,
                container_name=container_name,
                container_id=container_id,
            )

    return {
        'user_id': interaction.user.id,
        'container_name': container_name,
        'container_id': container_id,
        'count': amount,
        'reward_dict': reward
    }
