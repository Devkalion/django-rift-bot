from typing import List

import discord
from discord import app_commands
from discord.app_commands import Range

from api import APIException, MarketAPIClient
from .base import command, CommandError


async def get_sale_items():
    async with MarketAPIClient() as api_client:
        items = await api_client.list_sales()

    result = [
        (item['index_number'], f'{item["index_number"]}. {item["display_name"]}')
        for item in sorted(items, key=lambda x: x['index_number'])
        if not item["closed"] and not item["hidden"]
    ]
    return result


async def get_exchange_items():
    async with MarketAPIClient() as api_client:
        items = await api_client.list_exchanges()

    result = [
        (item['index_number'], f'{item["index_number"]}. {item["display_name"]}')
        for item in sorted(items, key=lambda x: x['index_number'])
        if not item["closed"] and not item["hidden"]
    ]
    return result


async def sale_item_autocomplete(
        interaction: discord.Interaction,
        current: str,
) -> List[app_commands.Choice[int]]:
    result = await get_sale_items()
    current = current.lower()

    return [
        app_commands.Choice(name=item, value=index_number)
        for index_number, item in result
        if current in item.lower()
    ]


async def exchange_item_autocomplete(
        interaction: discord.Interaction,
        current: str,
) -> List[app_commands.Choice[int]]:
    result = await get_exchange_items()
    current = current.lower()

    return [
        app_commands.Choice(name=item, value=index_number)
        for index_number, item in result
        if current in item.lower()
    ]


@app_commands.autocomplete(item_id=sale_item_autocomplete)
@command
async def sale_command(interaction: discord.Interaction, item_id: Range[int, 1]):
    try:
        async with MarketAPIClient() as api_client:
            reward = await api_client.sale(item_id, interaction.user.id)
    except APIException as e:
        if e.status == 404:
            raise CommandError('no such item')
        if e.status == 403:
            raise CommandError(e.data['detail'])
        if e.status == 405:
            raise CommandError('limit', cooldown=e.data['cooldown'])
        if e.status == 400:
            raise CommandError('not enough', missing_inventory=e.data)
        raise CommandError('unknown', error=e)

    return {
        'user_id': interaction.user.id,
        'reward_dict': reward,
        'item_id': item_id,
    }


@app_commands.autocomplete(item_id=exchange_item_autocomplete)
@command
async def exchange_command(interaction: discord.Interaction, item_id: Range[int, 1]):
    try:
        async with MarketAPIClient() as api_client:
            reward = await api_client.exchange(item_id, interaction.user.id)
    except APIException as e:
        if e.status == 404:
            raise CommandError('no such item')
        if e.status == 403:
            raise CommandError(e.data['detail'])
        if e.status == 405:
            raise CommandError('limit', cooldown=e.data['cooldown'])
        if e.status == 400:
            raise CommandError('not enough', missing_inventory=e.data)
        raise CommandError('unknown', error=e)

    return {
        'user_id': interaction.user.id,
        'reward_dict': reward,
        'item_id': item_id,
    }
