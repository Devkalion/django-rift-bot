import re

from discord import Interaction, ui, TextStyle, Member
from discord.utils import MISSING

from api import EmbedAPIClient
from api.message import UserMessageAPIClient
from slash_commands.base import get_command_cooldown
from utils import report_error


class MessageModal(ui.Modal, title='Анонимная валентинка'):
    text = ui.TextInput(
        label='Текст вашей валентинки',
        placeholder='Напишите свое признание или пожелание для указанного пользователя.',
        style=TextStyle.paragraph,
        max_length=800,
    )

    def __init__(self, user: Member, command_extras: dict):
        super().__init__()
        self.member = user
        self.command_extras = command_extras

    async def operate(self, interaction: Interaction):
        text = re.sub(' +', ' ', self.text.value.strip())
        async with UserMessageAPIClient() as api_client:
            message = await api_client.create_message(interaction.user.id, self.member.id, text)
        return {
            'user_message_id': message['id'],
        }

    async def get_msg_data(self, interaction: Interaction):
        handler = self.operate
        try:
            embed_data = await handler(interaction)
            return await EmbedAPIClient.class_render_embed(
                self.command_extras['embed_name'],
                embed_data,
            )
        except Exception as e:
            report_error(interaction.client, e, handler)
            return await EmbedAPIClient.class_render_embed(
                'unknown error',
                {"user_id": interaction.user.id},
            )

    async def on_submit(self, interaction: Interaction):
        await interaction.response.defer(ephemeral=self.command_extras.get('ephemeral'), thinking=True)
        msg_data = await self.get_msg_data(interaction)
        msg_data = {key: value for key, value in msg_data.items()}
        msg_data['attachments'] = msg_data.pop('files', MISSING)
        await interaction.edit_original_response(**msg_data)


async def user_message_command(
        interaction: Interaction,
        user: Member,
):
    extras = interaction.command.extras

    if user == interaction.user:
        msg_data = await EmbedAPIClient.class_render_embed(
            extras['error_embed_name'] or 'unknown error',
            {
                'user_id': interaction.user.id,
                'reason': 'self',
            }
        )
        await interaction.response.send_message(**msg_data, ephemeral=extras.get('ephemeral'))
        return

    if user.bot:
        msg_data = await EmbedAPIClient.class_render_embed(
            extras['error_embed_name'] or 'unknown error',
            {
                'user_id': interaction.user.id,
                'reason': 'bot',
            }
        )
        await interaction.response.send_message(**msg_data, ephemeral=extras.get('ephemeral'))
        return

    if extras.get('cooldown'):
        cooldown = get_command_cooldown(interaction.user, interaction.command)
        if cooldown:
            msg_data = await EmbedAPIClient.class_render_embed(
                extras['error_embed_name'] or 'unknown error',
                {
                    'user_id': interaction.user.id,
                    'reason': 'cooldown',
                    'cooldown': cooldown,
                }
            )
            await interaction.response.send_message(**msg_data, ephemeral=extras.get('ephemeral'))
            return

    modal = MessageModal(user, interaction.command.extras)
    try:
        await interaction.response.send_modal(modal)
    except Exception as e:
        report_error(interaction.client, e, user_message_command, interaction)
