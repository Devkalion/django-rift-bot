import re

from discord import Interaction, ui, TextStyle, Member
from discord.utils import MISSING

from api import APIException, EmbedAPIClient, AppealAPIClient
from slash_commands.base import CommandError
from utils import report_error


class AppealModal(ui.Modal, title='Жалоба на пользователя'):
    text = ui.TextInput(
        label='Описание жалобы',
        placeholder='Опишите ситуацию и приложите ссылки на доказательства нарушений',
        style=TextStyle.paragraph,
    )

    def __init__(self, user: Member, command_extras: dict):
        super().__init__()
        self.member = user
        self.command_extras = command_extras

    async def operate(self, interaction: Interaction):
        text = re.sub(' +', ' ', self.text.value.strip())
        try:
            async with AppealAPIClient() as api_client:
                appeal = await api_client.create_appeal(self.member.id, interaction.user.id, text)
        except APIException as e:
            if e.status == 403:
                raise CommandError('forbidden')
            raise
        return {
            'appeal_id': appeal['id'],
        }

    async def get_msg_data(self, interaction: Interaction):
        handler = self.operate
        try:
            embed_data = await handler(interaction)
            return await EmbedAPIClient.class_render_embed(
                self.command_extras['embed_name'],
                embed_data,
            )
        except CommandError as e:
            return await EmbedAPIClient.class_render_embed(
                self.command_extras['error_embed_name'] or 'unknown error',
                {"user_id": interaction.user.id, **e.data},
            )
        except Exception as e:
            report_error(interaction.client, e, handler)
            return await EmbedAPIClient.class_render_embed(
                'unknown error',
                {"user_id": interaction.user.id},
            )

    async def on_submit(self, interaction: Interaction):
        await interaction.response.defer(ephemeral=self.command_extras.get('ephemeral'), thinking=True)
        msg_data = await self.get_msg_data(interaction)
        msg_data = {key: value for key, value in msg_data.items()}
        msg_data['attachments'] = msg_data.pop('files', MISSING)
        await interaction.edit_original_response(**msg_data)


async def appeal_command(
        interaction: Interaction,
        user: Member,
):
    modal = AppealModal(user, interaction.command.extras)
    try:
        await interaction.response.send_modal(modal)
    except Exception as e:
        report_error(interaction.client, e, appeal_command, interaction)
