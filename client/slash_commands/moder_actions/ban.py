import asyncio
from datetime import timedelta
from typing import Optional

from discord import Member, app_commands, Interaction, User

from api import ModerActionAPIClient, APIException
from conf import conf
from .utils import render_dm, unit_mapping, render_channel
from ..base import command, CommandError


@command
@app_commands.choices(delete_message_seconds=[
    app_commands.Choice(name=f'Ничего не удалять', value=0),
    app_commands.Choice(name=f'За последний час', value=3600),
    app_commands.Choice(name=f'За последние 6 часов', value=3600 * 6),
    app_commands.Choice(name=f'За последние 12 часов', value=3600 * 12),
    app_commands.Choice(name=f'За последние 24 часа', value=3600 * 24),
    app_commands.Choice(name=f'За последние 3 дня', value=3600 * 24 * 3),
    app_commands.Choice(name=f'За последние 7 дней', value=3600 * 24 * 7),
])
@app_commands.default_permissions(ban_members=True)
async def ban_command(
        interaction: Interaction,
        user: Member,
        reason: str,
        duration: Optional[str],
        delete_message_seconds: app_commands.Choice[int],
):
    if duration:
        try:
            duration, unit = int(duration[:-1]), duration[-1]
            duration = timedelta(**{unit_mapping[unit]: duration})
        except Exception:
            raise CommandError('format')

    try:
        async with ModerActionAPIClient() as api_client:
            action = await api_client.create_action(user.id, interaction.user.id, reason, 'ban', duration)
    except APIException as e:
        if e.status == 403:
            raise CommandError('forbidden')
        elif e.status == 400:
            if 'user' in e.data:
                raise CommandError(next(e.data['user']))
            elif 'moderator' in e.data:
                raise CommandError('by_user')
        raise

    data = {
        'action_id': action['id'],
    }

    await asyncio.gather(
        render_dm('ban_dm', data, user),
        render_channel(
            interaction.guild.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID),
            'ban',
            data
        ),
    )
    await interaction.guild.ban(user, reason=reason, delete_message_seconds=delete_message_seconds.value)

    return data


@command
@app_commands.default_permissions(ban_members=True)
async def unban_command(
        interaction: Interaction,
        user: User,
        reason: str,
):
    try:
        async with ModerActionAPIClient() as api_client:
            action = await api_client.create_action(user.id, interaction.user.id, reason, 'unban')
    except APIException as e:
        if e.status == 403:
            raise CommandError('forbidden')
        elif e.status == 400:
            if 'user' in e.data:
                raise CommandError(next(e.data['user']))
            elif 'moderator' in e.data:
                raise CommandError('by_user')
        raise

    data = {
        'action_id': action['id'],
    }

    await interaction.guild.unban(user, reason=reason)
    await asyncio.gather(
        render_dm('unban_dm', data, user),
        render_channel(
            interaction.guild.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID),
            'unban',
            data
        ),
    )

    return data
