import asyncio
from datetime import timedelta
from typing import Optional

from discord import Member, app_commands, Interaction, Role

from api import ModerActionAPIClient, APIException
from conf import conf
from .utils import render_dm, unit_mapping, render_channel
from ..base import command, CommandError


@command
@app_commands.default_permissions(ban_members=True)
async def mute_command(
        interaction: Interaction,
        user: Member,
        reason: str,
        duration: Optional[str],
):
    if duration:
        try:
            duration, unit = int(duration[:-1]), duration[-1]
            duration = timedelta(**{unit_mapping[unit]: duration})
        except Exception:
            raise CommandError('format')

    try:
        async with ModerActionAPIClient() as api_client:
            action = await api_client.create_action(user.id, interaction.user.id, reason, 'mute', duration)
    except APIException as e:
        if e.status == 403:
            raise CommandError('forbidden')
        elif e.status == 400:
            if 'user' in e.data:
                raise CommandError(next(e.data['user']))
            elif 'moderator' in e.data:
                raise CommandError('by_user')
        raise

    data = {
        'action_id': action['id'],
    }

    role: Role = interaction.guild.get_role(conf.MUTE_ROLE_ID)
    await asyncio.gather(
        user.add_roles(role, reason=reason),
        render_dm('mute_dm', data, user),
        render_channel(
            interaction.guild.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID),
            'mute',
            data
        ),
    )

    return data


@command
@app_commands.default_permissions(ban_members=True)
async def unmute_command(
        interaction: Interaction,
        user: Member,
        reason: str,
):
    try:
        async with ModerActionAPIClient() as api_client:
            action = await api_client.create_action(user.id, interaction.user.id, reason, 'unmute')
    except APIException as e:
        if e.status == 403:
            raise CommandError('forbidden')
        elif e.status == 400:
            if 'user' in e.data:
                raise CommandError(next(e.data['user']))
            elif 'moderator' in e.data:
                raise CommandError('by_user')
        raise

    data = {
        'action_id': action['id'],
    }

    role: Role = interaction.guild.get_role(conf.MUTE_ROLE_ID)
    await asyncio.gather(
        user.remove_roles(role, reason=reason),
        render_dm('unmute_dm', data, user),
        render_channel(
            interaction.guild.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID),
            'unmute',
            data
        ),
    )

    return data
