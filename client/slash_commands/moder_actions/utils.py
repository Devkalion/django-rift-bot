import discord

from api import EmbedAPIClient
from utils import send_dm

unit_mapping = {
    'д': 'days',
    'ч': 'hours',
    'м': 'minutes',
    'с': 'seconds',
}


async def render_dm(
        embed_name: str,
        embed_data: dict,
        member: discord.Member,
):
    msg_data = await EmbedAPIClient.class_render_embed(embed_name, embed_data)
    await send_dm(member, msg_data)


async def render_channel(
        channel: discord.TextChannel,
        embed_name: str,
        embed_data: dict,
):
    return
    # msg_data = await EmbedAPIClient.class_render_embed(embed_name, embed_data)
    # await channel.send(**msg_data)
