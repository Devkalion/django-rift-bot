import asyncio

from discord import Member, app_commands, Interaction

from api import ModerActionAPIClient, APIException
from conf import conf
from .utils import render_dm, render_channel
from ..base import command, CommandError


@command
@app_commands.default_permissions(kick_members=True)
async def warn_command(
        interaction: Interaction,
        user: Member,
        reason: str,
):
    try:
        async with ModerActionAPIClient() as api_client:
            action = await api_client.create_action(user.id, interaction.user.id, reason, 'warn')
    except APIException as e:
        if e.status == 403:
            raise CommandError('forbidden')
        elif e.status == 400:
            if 'user' in e.data:
                raise CommandError(next(e.data['user']))
            elif 'moderator' in e.data:
                raise CommandError('by_user')
        raise

    data = {
        'action_id': action['id'],
    }

    await asyncio.gather(
        render_dm('warn_dm', data, user),
        render_channel(
            interaction.guild.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID),
            'warn',
            data
        ),
    )

    return data
