import asyncio
from typing import List

from discord import Interaction, Member, VoiceChannel, app_commands

from api import PersonalChannelAPIClient
from .base import CommandError, command


async def get_channel_dicts(user_id):
    async with PersonalChannelAPIClient() as api_client:
        channels_dicts = await api_client.get_channels(user_id)

    return channels_dicts


async def get_channel(guild, channel_id) -> VoiceChannel:
    channel = guild.get_channel(channel_id)
    if channel is None:
        channel = await guild.fetch_channel(channel_id)
    return channel


@command
async def channel_kick_command(interaction: Interaction, user: Member):
    user_id = int(user.id)
    if user_id == interaction.user.id:
        raise CommandError('self', user_id=user_id, admin_id=interaction.user.id)

    channels_dicts = await get_channel_dicts(interaction.user.id)
    if not channels_dicts:
        raise CommandError('no channel', user_id=user_id, admin_id=interaction.user.id)

    coroutines = []
    async with PersonalChannelAPIClient() as api_client:
        for channel_dict in channels_dicts:
            channel = await get_channel(interaction.guild, channel_dict['channel_id'])
            if user in channel.members:
                coroutines.append(user.move_to(None))
            coroutines.append(channel.set_permissions(user, connect=False, view_channel=None))
            coroutines.append(
                api_client.delete_channel_member(channel_dict['id'], user.id)
            )

        if coroutines:
            await asyncio.gather(*coroutines)

    return {
        'user_id': user.id,
        'admin_id': interaction.user.id,
    }


@command
async def channel_add_command(interaction: Interaction, user: Member):
    if user.id == interaction.user.id:
        raise CommandError('self', user_id=user.id, admin_id=interaction.user.id)

    channels_dicts = await get_channel_dicts(interaction.user.id)
    if not channels_dicts:
        raise CommandError('no channel', user_id=user.id, admin_id=interaction.user.id)

    async with PersonalChannelAPIClient() as api_client:
        coroutines = []
        for channel_dict in channels_dicts:
            channel = await get_channel(interaction.guild, channel_dict['channel_id'])
            coroutines.append(channel.set_permissions(user, connect=True, view_channel=True))
            coroutines.append(
                api_client.create_channel_member(channel_dict['id'], user.id, interaction.user.id)
            )

        if coroutines:
            await asyncio.gather(*coroutines)

    return {
        'user_id': user.id,
        'admin_id': interaction.user.id,
    }
