import discord
from discord import app_commands

from . import refresh_commands
from .base import command


@command
@app_commands.default_permissions(administrator=True)
async def refresh_command(interaction: discord.Interaction):
    await refresh_commands()
    return {
        'user_id': interaction.user.id,
        'avatar': str((interaction.user.avatar or interaction.user.default_avatar).url),
    }
