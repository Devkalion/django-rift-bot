from functools import lru_cache
from typing import List

import discord
from discord import app_commands
from discord.app_commands import Range
from redis import Redis

from api import APIException, ShopAPIClient
from conf import REDIS_CONNECTION_SETTINGS
from .base import command, CommandError


async def get_shop_items(shop_id: int):
    async with ShopAPIClient() as api_client:
        items = await api_client.list_items(shop_id)

    result = [
        (item['index_number'], f'{item["index_number"]}. {item["display_name"]}')
        for item in sorted(items, key=lambda x: x['index_number'])
        if not item["closed"] and not item["hidden"]
    ]
    return result


async def shop_item_autocomplete(
        interaction: discord.Interaction,
        current: str,
) -> List[app_commands.Choice[int]]:
    shop_id = interaction.command.extras['parameters']['shop_id']
    result = await get_shop_items(shop_id)
    current = current.lower()

    return [
        app_commands.Choice(name=item, value=index_number)
        for index_number, item in result
        if current in item.lower()
    ]


@app_commands.autocomplete(item_id=shop_item_autocomplete)
@command
async def buy_command(interaction: discord.Interaction, item_id: Range[int, 1]):
    shop_id = interaction.command.extras['parameters']['shop_id']

    async with ShopAPIClient() as api_client:
        shop = await api_client.retrieve(shop_id)

    redis_key = f'command-buy-{shop["id"]}-{interaction.user.id}'
    if shop['cooldown_seconds']:
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            ttl = redis.ttl(redis_key)
            if ttl and ttl > 0:
                raise CommandError('cooldown', cooldown=ttl)

    try:
        async with ShopAPIClient() as api_client:
            reward = await api_client.buy(shop['id'], item_id, interaction.user.id)
    except APIException as e:
        if e.status == 404:
            raise CommandError('no such item')
        if e.status == 403:
            raise CommandError(e.data['detail'])
        if e.status == 400:
            raise CommandError('not enough', missing_inventory=e.data)
        raise CommandError('unknown', error=e)

    if shop['cooldown_seconds']:
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            redis.set(redis_key, 1, ex=shop['cooldown_seconds'])

    return {
        'user_id': interaction.user.id,
        'reward_dict': reward,
        'shop_id': shop['id'],
        'item_id': item_id,
    }


@command
async def shop_command(interaction: discord.Interaction):
    shop_id = interaction.command.extras['parameters']['shop_id']

    return {
        'user_id': interaction.user.id,
        'shop_id': shop_id,
    }
