import discord

from .user_info import user_info_command


async def render_simple_embed_command(interaction: discord.Interaction):
    await user_info_command(interaction)
