from typing import Optional

import discord
from discord import app_commands, Member

from .base import command


@command
async def user_info_command(interaction: discord.Interaction, user: Optional[Member] = None):
    if user is None:
        user = interaction.user
    return {
        'user_id': user.id,
        'avatar': str((user.avatar or user.default_avatar).url),
    }
