import asyncio

from discord import Guild, VoiceChannel, PermissionOverwrite, Member, Interaction

from .base import command, CommandError


def handle_error(reason, admin_id, user_id=None):
    raise CommandError(reason, user_id=user_id, admin_id=admin_id)


@command
async def voice_kick_command(interaction: Interaction, user: Member):
    guild: Guild = interaction.guild

    if user.id == interaction.user.id:
        return handle_error('self kick', interaction.user.id, user.id)

    channel: VoiceChannel
    for channel in guild.voice_channels:
        if interaction.user not in channel.members:
            continue
        permissions: PermissionOverwrite = channel.overwrites_for(interaction.user)
        if permissions.manage_channels:
            break
    else:
        return handle_error('no channel', interaction.user.id)

    if user not in channel.members:
        return handle_error('not member', interaction.user.id, user.id)

    await asyncio.gather(
        user.move_to(None),
        channel.set_permissions(user, connect=False)
    )

    return {
        'user_id': user.id,
        'admin_id': interaction.user.id,
    }
