import asyncio

from discord import Guild, VoiceChannel, Client

from api import PersonalChannelAPIClient
from conf import conf
from .events import register


@register('dpc', json_data=False)
async def delete_personal_channel(client: Client, _, channel_id):
    channel: VoiceChannel = await client.fetch_channel(int(channel_id))
    await channel.delete(reason='expired')


@register('cpc', json_data=True)
async def create_personal_channel(client: Client, user_id, event_data):
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    category_channel_id = conf.PRIVATE_VOICE_CHANNELS_CATEGORY_ID
    if category_channel_id:
        category_channel = await client.fetch_channel(conf.PRIVATE_VOICE_CHANNELS_CATEGORY_ID)
    else:
        category_channel = None
    member, channel = await asyncio.gather(
        guild.fetch_member(user_id),
        guild.create_voice_channel(
            event_data['name'],
            category=category_channel
        )
    )
    await channel.set_permissions(
        member,
        view_channel=True,
        connect=True,
        mute_members=True,
        deafen_members=True,
    )
    async with PersonalChannelAPIClient() as api_client:
        await api_client.set_channel_id(event_data['channel_id'], channel.id)
