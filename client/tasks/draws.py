import logging
import sys

from discord import Message, TextChannel, Client

from api import EmbedAPIClient, DrawAPIClient
from utils import send_dm
from .events import register

logger = logging.getLogger(sys.argv[1])


async def send_channel_msg(embed_name, client: Client, event_data):
    async with EmbedAPIClient() as api_client:
        msg_data = await api_client.render_embed(embed_name, {'draw_id': event_data['draw_id']})

    channel: TextChannel = client.get_channel(int(event_data['channel_id']))
    return await channel.send(**msg_data)


@register('draw')
async def start_draw(client: Client, _, event_data):
    msg: Message = await send_channel_msg('start_draw', client, event_data)
    async with DrawAPIClient() as api_client:
        await api_client.set_msg_id(event_data['draw_id'], msg.id)
    await msg.add_reaction(event_data['emoji'])


@register('draw_end')
async def end_draw(client: Client, _, event_data):
    await send_channel_msg('finish_draw', client, event_data)
    for draw_reward_id, user_ids in event_data['rewards'].items():
        async with EmbedAPIClient() as api_client:
            dm_msg_data = await api_client.render_embed('finish_draw_dm', {
                'draw_id': event_data['draw_id'],
                'draw_reward_id': draw_reward_id
            })

        for user_id in user_ids:
            user = await client.fetch_user(int(user_id))
            await send_dm(user, dm_msg_data)
