import asyncio
import logging
import sys

from discord import Embed, Client
from discord.ui import View

from api.embed import get_components, EmbedAPIClient
from conf import conf
from utils import send_dm
from .events import register

logger = logging.getLogger(sys.argv[1])


@register('publish')
async def publish_embeds(client: Client, user_id: int, event_data):
    channel = None
    user = None
    if user_id:
        user = client.get_user(user_id)
    else:
        channel = client.get_channel(event_data['channel_id'])

    for msg_data in event_data['messages']:
        if 'embed' in msg_data:
            msg_data['embed'] = Embed.from_dict(msg_data['embed'])
        if 'components' in msg_data:
            view = View()
            for component in get_components(msg_data.pop('components')):
                view.add_item(component)
            msg_data['view'] = view
        if user_id:
            await send_dm(user, msg_data)
        else:
            await channel.send(**msg_data)


@register("approve_message")
async def publish_embeds(client: Client, user_id, event_data):
    user, recipient, channel = await asyncio.gather(
        client.fetch_user(event_data["user_id"]),
        client.fetch_user(event_data["recipient_id"]),
        client.fetch_channel(conf.USER_MESSAGE_CHANNEL_ID),
    )

    data = {"user_message_id": event_data["user_message_id"]}
    async with EmbedAPIClient() as api_client:
        msg_data = await api_client.render_embed("user_message_channel", data)
        msg = await channel.send(**msg_data)
        data["message_url"] = msg.jump_url
        user_msg_data, recipient_msg_data = await asyncio.gather(
            api_client.render_embed("user_message_sender_dm", data),
            api_client.render_embed("user_message_recipient_dm", data),
        )
    await asyncio.gather(
        send_dm(user, user_msg_data),
        send_dm(recipient, recipient_msg_data),
    )
