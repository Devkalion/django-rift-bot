import asyncio
import json
import logging
import sys
from functools import wraps
from typing import Optional, Callable, Dict

import aio_pika
from discord import Client

from api import EventAPIClient
from conf import RABBIT_MQ_CONNECTION_SETTINGS
from data_classes import Event
from utils import report_error

client: Optional[Client] = None
logger = logging.getLogger(sys.argv[1])
HANDLER_REGISTRY: Dict[str, Callable] = {}

WORKERS_COUNT = 8
QUEUE_MAX_SIZE = WORKERS_COUNT * 2


def register(event_type: str, no_data=False, json_data=True):
    logger.debug(f'event {event_type} registered')

    def decorator(func):
        @wraps(func)
        def f(event: Event):
            if no_data:
                return func(client, event.user_id)
            if json_data:
                return func(client, event.user_id, json.loads(event.data))

            return func(client, event.user_id, event.data)

        HANDLER_REGISTRY[event_type] = f
        return func

    return decorator


def await_client(f):
    @wraps(f)
    async def func(_client: Client, *args, **kwargs):
        global client
        client = _client

        while not client.is_ready():
            await asyncio.sleep(5)

        return await f(*args, **kwargs)

    return func


async def operate_event(event_id: str):
    try:
        logger.debug(f'operate event {event_id}')
        async with EventAPIClient() as api_client:
            response = await api_client.update_status(event_id, 'work')

        event = Event(**response.json())
        await HANDLER_REGISTRY[event.type](event)

        async with EventAPIClient() as api_client:
            await api_client.update_status(event_id, 'ready')
    except Exception as e:
        logger.exception(f'Error on operating event {event_id}')
        report_error(client, e, event_listener)


async def worker(name, queue):
    logger.info(f"launched {name}")
    while True:
        event = await queue.get()
        await operate_event(event)
        queue.task_done()


async def operate_existing_events(queue: asyncio.Queue):
    logger.debug('get events')
    async with EventAPIClient() as api_client:
        events = await api_client.get_events()

    if not events:
        return

    logger.debug(f'found {len(events)} events')

    for event in events:
        await queue.put(event)

    await queue.join()


async def operate_message_queue(queue: asyncio.Queue):
    queue_name = 'events'
    connection = await aio_pika.connect_robust(
        **RABBIT_MQ_CONNECTION_SETTINGS,
        loop=asyncio.get_running_loop()
    )
    logger.debug('connect to mq')
    async with connection:
        channel: aio_pika.RobustChannel = await connection.channel()
        msg_queue = await channel.declare_queue(queue_name)
        async with msg_queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    event_id = message.body.decode()
                    await queue.put(event_id)


@await_client
async def event_listener():
    logger.info('event listener started')
    queue = asyncio.Queue(maxsize=QUEUE_MAX_SIZE)
    workers = [
        asyncio.create_task(worker(f'worker-{i}', queue))
        for i in range(WORKERS_COUNT)
    ]
    await operate_existing_events(queue)
    try:
        await operate_message_queue(queue)
    except:
        for task in workers:
            task.cancel()
        loop = asyncio.get_running_loop()
        loop.close()
        raise
