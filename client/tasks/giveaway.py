import asyncio
import logging
import sys

from api import EmbedAPIClient, EncouragementAPIClient
from conf import conf
from utils import send_dm
from .events import register

logger = logging.getLogger(sys.argv[1])


@register('giveaway')
async def auto_giveaway(client, _, event_data):
    async with EmbedAPIClient() as api_client:
        msg_data = await api_client.render_embed('auto_giveaway', event_data)
    channel = client.get_channel(conf.COMMANDS_CHANNEL_ID)
    await channel.send(**msg_data)


@register('encouragement_end', json_data=False)
async def finish_encouragement(client, _, encouragement_id):
    async with EncouragementAPIClient() as api_client:
        encouragement = await api_client.get_encouragement(encouragement_id)
    payload = {
        'encouragement_id': encouragement_id
    }
    async with EmbedAPIClient() as api_client:
        channel_msg_data, dm_msg_data = await asyncio.gather(
            api_client.render_embed('encouragement_channel', payload),
            api_client.render_embed('encouragement_dm', payload)
        )

    msg_channel = client.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID)
    await msg_channel.send(**channel_msg_data)
    users = [
        await client.fetch_user(user_id)
        for user_id in encouragement['users']
    ]
    await asyncio.gather(*(
        send_dm(user, dm_msg_data) for user in users
    ))
