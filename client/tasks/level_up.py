import asyncio
import logging
import sys

from api import EmbedAPIClient, RewardsAPIClient
from conf import conf
from utils import send_dm
from .events import register

logger = logging.getLogger(sys.argv[1])


@register('lvl_up', json_data=False)
async def levelup(client, user_id, event_data):
    level = int(event_data)
    user = client.get_user(user_id)
    if user is None:
        return

    async with RewardsAPIClient() as api_client:
        reward = await api_client.give_level_reward(user_id, level)

    embed_data = {
        'user_id': user_id,
        'level': level,
        'reward_dict': reward
    }

    embeds = ['lvl_up_dm']
    if conf.LEVEL_UP_CHANNEL_MSG:
        embeds.append('lvl_up')

    async with EmbedAPIClient() as api_client:
        msgs = await asyncio.gather(*[
            api_client.render_embed(embed_name, embed_data)
            for embed_name in embeds
        ])

    coroutines = [
        send_dm(user, msgs[0])
    ]

    if conf.LEVEL_UP_CHANNEL_MSG:
        channel = client.get_channel(conf.MAIN_MESSAGE_CHANNEL_ID)
        coroutines.append(channel.send(**msgs[1]))

    await asyncio.gather(*coroutines)
