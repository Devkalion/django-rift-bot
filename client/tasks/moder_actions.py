import logging
import sys

from discord import Guild, Role, Client
from httpx import Timeout

from api import EmbedAPIClient, ModerActionAPIClient
from conf import conf
from utils import send_dm
from .events import register

logger = logging.getLogger(sys.argv[1])


@register('unmute', no_data=True)
async def check_mute(client, user_id):
    async with ModerActionAPIClient(timeout=Timeout(30)) as api_client:
        active_mutes = await api_client.get_actions(user_id)

    if active_mutes:
        return

    async with ModerActionAPIClient() as api_client:
        await api_client.create_action(user_id, client.user.id, 'auto', 'unmute')

    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    role: Role = guild.get_role(conf.MUTE_ROLE_ID)
    user = guild.get_member(user_id)
    if user is None:
        return
    await user.remove_roles(role)

    msg_data = await EmbedAPIClient.class_render_embed('auto_unmute', {})
    await send_dm(user, msg_data)


@register('unban', no_data=True)
async def check_ban(client, user_id):
    async with ModerActionAPIClient(timeout=Timeout(30)) as api_client:
        active_bans = await api_client.get_actions(user_id, _type='ban')

    if active_bans:
        return

    async with ModerActionAPIClient() as api_client:
        await api_client.create_action(user_id, client.user.id, 'auto', 'unban')

    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    user = client.get_user(user_id)
    if user is None:
        return
    await guild.unban(user)
    # async with EmbedAPIClient() as api_client:
    # msg_data = await api_client.render_embed('auto_unban', {})
    # dm_channel = await user.create_dm()
    # await dm_channel.send(**msg_data)


@register('appeal_reviewed', json_data=False)
async def levelup(client: Client, user_id, appeal_id):
    embed_data = {
        'user_id': user_id,
        'appeal_id': appeal_id,
    }
    msg = await EmbedAPIClient.class_render_embed('appeal_reviewed', embed_data)
    user = client.get_user(user_id)
    await send_dm(user, msg)
