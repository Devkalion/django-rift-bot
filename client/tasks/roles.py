from typing import Optional, Tuple

from api import APIException, EmbedAPIClient, PersonalRoleAPIClient, RoleAPIClient
from conf import conf
from discord import Client, Guild, Member, Permissions, Role
from utils import get_member, get_role

from .events import register


async def get_member_and_role(client: Client, user_id: int, role_id: int) -> Tuple[Optional[Member], Optional[Role]]:
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    member = await get_member(guild, user_id)
    role = await get_role(role_id, guild)
    return member, role


async def _add_user_role(client, user_id, role_id: int, reason: str = None):
    member, role = await get_member_and_role(client, user_id, role_id)
    if member is None or role is None:
        return
    await member.add_roles(role, reason=reason)


async def _delete_user_role(client, user_id, role_id: int, reason: str = None):
    member, role = await get_member_and_role(client, user_id, role_id)
    if member is None or role is None:
        return
    await member.remove_roles(role, reason=reason)


@register("grant_user_role")
async def grant_user_role(client, user_id, event_data: dict):
    await _add_user_role(client, user_id, event_data["role_id"], event_data.get("reason"))


@register("revoke_user_role")
async def revoke_user_role(client, user_id, event_data: dict):
    await _delete_user_role(client, user_id, event_data["role_id"], event_data.get("reason"))


@register("etr", json_data=False)
async def delete_temp_role(client, user_id, event_data):
    await _delete_user_role(client, user_id, role_id=int(event_data), reason="Temp role expired")


@register("epr", json_data=False)
async def expire_personal_role(client: Client, user_id, event_data):
    await _delete_user_role(client, user_id, role_id=int(event_data), reason="Personal role expired")
    result_embed = await EmbedAPIClient.class_render_embed('personal_role_expired', {
        'user_id': user_id,
    })
    user = client.get_user(user_id)
    await user.send(**result_embed)


@register("dpr", json_data=False)
async def delete_personal_role(client, _, event_data):
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    await guild.fetch_roles()
    role: Role = guild.get_role(int(event_data))
    await role.delete(reason='expired personal role')


@register("upr", json_data=True)
async def update_personal_role(client, _, event_data):
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    await guild.fetch_roles()
    role: Role = guild.get_role(event_data["role_id"])
    await role.edit(
        color=event_data["color"],
        name=event_data["name"],
        reason="Changed in admin panel",
    )

@register("cpr", json_data=True)
async def create_personal_role(client, user_id, event_data):
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    role = await guild.create_role(
        reason="personal role through admin panel",
        name=event_data["name"],
        color=event_data["color"],
        permissions=Permissions(
            external_emojis=True,
            external_stickers=True,
            use_soundboard=True,
            use_external_sounds=True,
            send_voice_messages=True,
        ),
    )
    await guild.edit_role_positions({
        role: conf.PERSONAL_ROLE_POSITION,
    }, reason='default position for personal roles')
    async with RoleAPIClient() as role_api_client:
        try:
            api_role = await role_api_client.create_role(role.id, role.name, role.position)
        except APIException:
            api_roles = await role_api_client.get_roles()
            for _role in api_roles:
                if str(_role['discord_id']) == str(role.id):
                    api_role = _role

    async with PersonalRoleAPIClient() as api_client:
        await api_client.set_discord_role(event_data['id'], api_role['id'])

    await _add_user_role(client, user_id, role.id)
