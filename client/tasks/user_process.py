import logging
import sys
from typing import Coroutine

from api import EmbedAPIClient
from utils import send_dm
from .events import register

logger = logging.getLogger(sys.argv[1])


async def finish_user_process(client, user_id, event_data, embed_name, item_embed_name):
    user = client.get_user(user_id)
    if user is None:
        return
    embed_data = {
        'reward_dict': event_data['reward'],
        item_embed_name: event_data['item_id'],
        'user_id': user_id
    }

    dm_msg_data = await EmbedAPIClient.class_render_embed(embed_name, embed_data)
    await send_dm(user, dm_msg_data)


@register('transform')
def finish_transformation(client, user_id, event_data) -> Coroutine:
    return finish_user_process(client, user_id, event_data, 'transform', 'transformation_id')


@register('journey')
def finish_journey(client, user_id, event_data) -> Coroutine:
    return finish_user_process(client, user_id, event_data, 'journey', 'journey_id')
