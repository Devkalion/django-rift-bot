import functools
from datetime import datetime, time
from typing import Type

from redis import Redis

from conf import conf, REDIS_CONNECTION_SETTINGS


def redis_wrapper(func):
    @functools.wraps(func)
    def f(_class, key, *args, **kwargs):
        redis_key = _class.generate_key(key)
        if 'redis' in kwargs:
            return func(_class, redis_key, *args, **kwargs)
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            return func(_class, redis_key, *args, redis=redis, **kwargs)

    return f


class BaseTimeout:
    redis_prefix = None
    expiration_seconds = None

    @classmethod
    def generate_key(cls, key):
        return f'{cls.redis_prefix}{key}'

    @classmethod
    @redis_wrapper
    def ttl(cls, key, redis=None):
        return redis.ttl(key)

    @classmethod
    @redis_wrapper
    def set(cls, key, value, timeout=None, redis=None):
        if not timeout:
            timeout = cls.expiration_seconds
        redis.set(key, value, ex=timeout)

    @classmethod
    @redis_wrapper
    def get(cls, key, redis=None, default=None):
        val = redis.get(key)
        if val is not None:
            return val
        else:
            return default


class DailyMessages(BaseTimeout):
    redis_prefix = 'daily-msg-user-'
    expiration_seconds = 24 * 60 * 60

    @classmethod
    def set(cls, key, value, **kwargs):
        today = datetime.combine(datetime.today(), time.min)
        timeout = cls.expiration_seconds - (datetime.now() - today).seconds
        super().set(key, value, timeout=timeout, **kwargs)


class MessageExpCD(BaseTimeout):
    redis_prefix = 'msg-cd-user-'
    expiration_seconds = conf.MESSAGE_COOLDOWN


class MessageStrictExpCD(BaseTimeout):
    redis_prefix = 'msg-strict-cd-user-'
    expiration_seconds = conf.STRICT_MESSAGE_COOLDOWN


def get_msg_cd(messages) -> Type[BaseTimeout]:
    if messages and messages > conf.MESSAGES_BEFORE_COOLDOWN:
        return MessageStrictExpCD
    return MessageExpCD
