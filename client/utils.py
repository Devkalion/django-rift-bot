import asyncio
import logging
import sys
import traceback
from functools import wraps
from typing import Callable, Coroutine, Optional, Union

from discord import Client, Member, Guild, errors, Role
from discord.abc import Messageable

from api import RoleAPIClient
from conf import conf

logger = logging.getLogger(sys.argv[1])


async def send_dm(user: Messageable, msg_data):
    for _ in range(60):
        try:
            await user.send(**msg_data)
            return
        except errors.Forbidden as e:
            if e.code == 50007:  # Cannot send messages to this users
                return
            logger.error('Cannot send dm message, retrying', exc_info=e)
            await asyncio.sleep(5)


async def get_role(role_id: int, guild: Guild) -> Role:
    role = guild.get_role(role_id)
    if role is None:
        roles = await guild.fetch_roles()
        for role in roles:
            if int(role.id) == role_id:
                return role
    return role


async def get_member(guild: Guild, user_id: int) -> Optional[Member]:
    member = guild.get_member(user_id)
    if member is not None:
        return member
    try:
        return await guild.fetch_member(user_id)
    except errors.NotFound:
        return


def is_role_granted(user: Member, role_id: int):
    for role in user.roles:
        if role_id == role.id:
            return True
    return False


def report_error(client: Client, e: BaseException, func, *args, **kwargs):
    if conf.ERROR_CHANNEL_ID:
        exc_type, exc_value, exc_tb = sys.exc_info()
        channel: Messageable = client.get_channel(conf.ERROR_CHANNEL_ID)
        msg = '\n'.join([
            '@here',
            f'Файл: `{" ".join(sys.argv)}`',
            f'Функция: `{func.__name__}`',
            f'Аргументы: `{args}, {kwargs}`',
            f'Ошибка: `{e}`',
            'Стектрейс: ```',
            *(traceback.format_exception(exc_type, exc_value, exc_tb)),
        ])
        msg = msg[:1950] + '\n...\n```'
        asyncio.create_task(channel.send(msg))


def error_handling(f):
    @wraps(f)
    async def func(client: Client, *args, **kwargs):
        try:
            await f(client, *args, **kwargs)
        except Exception as e:
            logger.exception("Exception occurred")
            report_error(client, e, f, *args, **kwargs)

    return func


async def _revoke_role(api_role: dict, user_roles: list, user: Member, client: Client):
    roles = [api_role['discord_id']]
    await asyncio.gather(*(
        user._state.http.remove_role(conf.MAIN_GUILD_ID, user.id, role_id)
        for role_id in roles
    ))
    return
    # TODO Enable after optimizations
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    granted_role_ids = set()
    max_role_position = -1
    delete_this_sep = api_role['separator'] is not None
    operated_sep_ids = set()

    user = await guild.fetch_member(user.id)

    for discord_role in user.roles:
        granted_role_ids.add(discord_role.id)
        if discord_role.id != api_role['discord_id']:
            max_role_position = max(max_role_position, discord_role.position)

    granted_roles_with_sep = (
        role
        for role in user_roles
        if role['separator'] and role['discord_id'] in granted_role_ids
    )

    for role in granted_roles_with_sep:
        if role['separator'] == api_role['separator'] and role['discord_id'] != api_role['discord_id']:
            delete_this_sep = False
        elif role['separator'] not in operated_sep_ids:
            operated_sep_ids.add(role['separator'])
            separator = guild.get_role(role['separator'])
            if max_role_position < separator.position:
                roles.append(separator.id)

    if delete_this_sep:
        roles.append(api_role['separator'])

    await asyncio.gather(*(
        user._state.http.remove_role(conf.MAIN_GUILD_ID, user.id, role_id)
        for role_id in roles
    ))


async def _grant_role(api_role: dict, user_roles: list, user: Member, client: Client):
    await asyncio.gather(*(
        user._state.http.add_role(conf.MAIN_GUILD_ID, user.id, role_id)
        for role_id in (api_role['discord_id'],)
    ))
    return
    # TODO Enable after optimizations
    max_role_position = api_role['position']
    guild: Guild = client.get_guild(conf.MAIN_GUILD_ID)
    for role in user_roles:
        if role['discord_id'] == api_role['discord_id']:
            break
    else:
        user_roles.append(api_role)

    user = await guild.fetch_member(user.id)

    granted_roles = {role.id for role in user.roles}
    granted_roles.add(api_role['discord_id'])
    unsetted_separator_ids = {
        role['separator']
        for role in user_roles
        if role['separator'] and role['discord_id'] in granted_roles and role['separator'] not in granted_roles
    }
    unsetted_separators = (guild.get_role(role_id) for role_id in unsetted_separator_ids)

    for discord_role in user.roles:
        max_role_position = max(max_role_position, discord_role.position)

    separators_for_set = (
        separator.id
        for separator in unsetted_separators
        if max_role_position > separator.position
    )

    await asyncio.gather(*(
        user._state.http.add_role(conf.MAIN_GUILD_ID, user.id, role_id)
        for role_id in (api_role['discord_id'], *separators_for_set)
    ))


async def operate_role(func: Callable, role: Union[int, dict], user: Member, client: Client):
    if isinstance(role, int):
        async with RoleAPIClient() as api_client:
            user_roles, api_role = await asyncio.gather(
                api_client.get_user_roles(user.id),
                api_client.get_role(role)
            )
        return await func(api_role, user_roles, user, client)
    else:
        async with RoleAPIClient() as api_client:
            user_roles: list = await api_client.get_user_roles(user.id)
        return await func(role, user_roles, user, client)


def grant_role(role: Union[int, dict], user: Member, client: Client) -> Coroutine:
    return operate_role(_grant_role, role, user, client)


def revoke_role(role: Union[int, dict], user: Member, client: Client) -> Coroutine:
    return operate_role(_revoke_role, role, user, client)
