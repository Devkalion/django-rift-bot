from django.contrib.admin import ModelAdmin, StackedInline, register

from .models import *


class CommandRoleInline(StackedInline):
    model = CommandRole
    extra = 1


@register(CommandTemplate)
class CommandTemplateAdmin(ModelAdmin):
    search_fields = ('name',)
    inlines = [CommandRoleInline]


@register(GameRole)
class GameRoleAdmin(ModelAdmin):
    search_fields = ('name',)


@register(Channel)
class ChannelAdmin(ModelAdmin):
    search_fields = ('name',)


@register(ActivityTemplate)
class ActivityTemplateAdmin(ModelAdmin):
    search_fields = ('name',)
    fieldsets = (
        (None, {
            'fields': ('name', 'commands', 'channels')
        }),
        ('Embeds', {
            'fields': (
                'info_embed', 'register_embed', 'register_error_embed', 'start_embed',
                'result_embed', 'result_dm_embed', 'thanks_embed',
            )
        }),
    )
