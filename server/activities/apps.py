from django.apps import AppConfig


class AttackConfig(AppConfig):
    name = 'activities'
    verbose_name = 'События'
