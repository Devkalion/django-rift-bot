from common.enums import ExtendedEnum


class ChannelType(ExtendedEnum):
    VOICE = "voice"
    TEXT = "text"
