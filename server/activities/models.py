from django.core.exceptions import ValidationError
from django.db import models

from activities.enums import ChannelType
from common.models import EnumField


class CommandTemplate(models.Model):
    name = models.CharField(max_length=255)
    is_voice_required = models.BooleanField(default=False)
    is_full_command_required = models.BooleanField(default=False)

    class Meta:
        db_table = 'command_template'

    def __str__(self):
        return self.name


class GameRole(models.Model):
    name = models.CharField(max_length=255)
    emoji = models.CharField(max_length=50, null=True, blank=True)
    label = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = 'game_role'

    def clean(self):
        super().clean()
        if not self.emoji and not self.label:
            raise ValidationError('Emoji or Label must be set')

    def __str__(self):
        return self.name


class CommandRole(models.Model):
    command = models.ForeignKey(CommandTemplate, on_delete=models.CASCADE)
    role = models.ForeignKey(GameRole, on_delete=models.CASCADE)
    users_amount = models.IntegerField(default=1)

    class Meta:
        db_table = 'command_role'


class Channel(models.Model):
    name = models.CharField(max_length=255)
    type = EnumField(ChannelType, default=ChannelType.TEXT)
    slots = models.IntegerField(default=1)
    for_everyone = models.BooleanField(default=False)

    class Meta:
        db_table = 'activity_channel'

    def __str__(self):
        return self.name


class ActivityTemplate(models.Model):
    name = models.CharField(max_length=255)
    info_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    register_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    register_error_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    start_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    result_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    result_dm_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    thanks_embed = models.ForeignKey('embeds.Embed', models.PROTECT, '+')
    winner_reward = models.ForeignKey('reward.RewardList', models.PROTECT, '+')
    looser_reward = models.ForeignKey('reward.RewardList', models.PROTECT, '+', null=True, blank=True)
    commands = models.ManyToManyField(CommandTemplate)
    channels = models.ManyToManyField(Channel)

    class Meta:
        db_table = 'activity_template'

    def __str__(self):
        return self.name
