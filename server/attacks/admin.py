from django.contrib.admin import register, ModelAdmin, StackedInline

from .models import *


class AttackRewardInline(StackedInline):
    model = AttackReward
    extra = 1


@register(AttackResult)
class AttackResultAdmin(ModelAdmin):
    list_display = ('name', 'chance', 'enabled')
    list_filter = ('enabled',)
    inlines = [AttackRewardInline]
    fieldsets = (
        (None, {
            'fields': ('name', 'chance', 'enabled', 'attack_cashback')
        }),
        ('Messages', {
            'fields': ('attacker_dm_msg', 'victim_dm_msg', 'channel_msg')
        }),
    )


@register(AttackSpell)
class AttackSpellAdmin(ModelAdmin):
    list_display = ('name', 'enabled')
    list_filter = ('enabled',)


@register(Attack)
class AttackAdmin(ModelAdmin):
    list_display = ('attacker', 'victim', 'spell', 'result')
    list_filter = ('created_date',)

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(
            'attacker', 'victim', 'spell', 'result'
        )
