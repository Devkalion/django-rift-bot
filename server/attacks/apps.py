from django.apps import AppConfig


class AttackConfig(AppConfig):
    name = 'attacks'
    verbose_name = 'Атаки'
