from django.db import models

from common.models import CreatedDateMixin, ExtendedEnum, EnumField


class AttackRewardRecipient(ExtendedEnum):
    ATTACKER = 'attacker'
    VICTIM = 'victim'


class AttackSpell(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    image_url = models.URLField()
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class AttackResult(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    chance = models.PositiveIntegerField()
    enabled = models.BooleanField(default=True)
    attacker_dm_msg = models.TextField()
    victim_dm_msg = models.TextField()
    channel_msg = models.TextField()
    attack_cashback = models.BooleanField()

    def __str__(self):
        return self.name


class Attack(CreatedDateMixin):
    attacker = models.ForeignKey('user.User', models.CASCADE, '+')
    victim = models.ForeignKey('user.User', models.CASCADE, '+')
    spell = models.ForeignKey(AttackSpell, models.PROTECT, '+')
    result = models.ForeignKey(AttackResult, models.CASCADE, '+')


class AttackReward(models.Model):
    attack_result = models.ForeignKey(AttackResult, models.CASCADE, 'rewards')
    recipient_type = EnumField(AttackRewardRecipient)
    reward = models.ForeignKey('reward.Reward', models.CASCADE, blank=True, null=True)

    class Meta:
        unique_together = [('attack_result', 'recipient_type')]
