from django.conf import settings
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.models import Group

from .forms import CaptchaAdminAuthenticationForm

admin.site.site_header = settings.SITE_NAME
admin.site.site_title = settings.SITE_NAME
admin.site.index_title = ''
if not settings.DEBUG:
    admin.site.login_form = CaptchaAdminAuthenticationForm
# admin.site.unregister(Group)

fit_textarea_height_js = 'this.style.height = "";this.style.height = this.scrollHeight + 3 + "px"'


class CollapsedInlineMixin:
    classes = ['collapse']


class ReadOnlyModelAdmin(ModelAdmin):
    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
