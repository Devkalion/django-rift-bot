import logging
import posixpath
from typing import Union, Optional

import httpx
from httpx import Response

logger = logging.getLogger(__name__)


class ApiClientError(Exception):
    def __init__(self, message=None, response=None, *args, **kwargs):
        super().__init__()

        assert not (kwargs and args)

        self.message_args = args
        self.message_kwargs = kwargs
        self.response: Optional[Response] = response

        if message:
            self.message = message

    def __str__(self):
        if self.message_args:
            return self.message.format(*self.message_args)
        elif self.message_kwargs:
            return self.message.format(**self.message_kwargs)
        else:
            return self.message


class BaseApiClient:
    api_url = None
    name = None
    auth_scheme = 'Bearer'
    auth_header = 'Authorization'
    auth_required = False

    def __init__(self, api_url=None, ssl_verify=False, name=None):
        if api_url:
            self.api_url = api_url
        self.default_request_headers = {}
        self.ssl_verify = ssl_verify
        if name:
            self.name = name

    def _request(self, method, url, data=None, headers=None, params=None, auth_required=None, as_json=True,
                 json_response=True, **kwargs) -> Union[Response, Union[list, dict]]:
        url = posixpath.join(self.api_url, url)
        client_name = self.name or self.__class__.__name__
        method_name = method.__name__.upper()

        if auth_required or auth_required is None and self.auth_required:
            if headers is None:
                headers = {}
            headers[self.auth_header] = ' '.join(filter(
                None, [self.auth_scheme, self._get_access_token()]
            ))

        logger.debug(f"[{client_name}] Request {method_name} {url} : {data or params}")
        try:
            if method_name != 'GET':
                kwargs['json' if as_json else 'data'] = data
            resp: Response = method(url, headers=headers, params=params, verify=self.ssl_verify, **kwargs)
        except Exception as err:
            logger.warning(f"Failed request to {client_name} {url} {data}: {err}")
            raise ApiClientError(str(err))

        logger.debug(f"[{client_name}] Response {method_name} {url} : [status: {resp.status_code}] {resp.text}")

        if 200 <= resp.status_code < 300:
            try:
                return resp.json() if json_response else resp
            except (ValueError, TypeError) as err:
                logger.warning(f"Failed convert response from {client_name} to json "
                               f"{method_name.upper()} {url}: {err} [{resp.status_code}] {resp.text}")
                raise ApiClientError(f"Wrong response '{err}': '{resp.text}'", response=resp)

        raise ApiClientError(f"Wrong status code of response [{resp.status_code}]", response=resp)

    def get(self, url, headers=None, params=None, auth_required=True, json_response=True, **kwargs):
        return self._request(httpx.get, url, headers=headers, params=params, auth_required=auth_required,
                             json_response=json_response, **kwargs)

    def post(self, url, data=None, headers=None, params=None, auth_required=True, as_json=False, json_response=True,
             **kwargs):
        return self._request(
            httpx.post, url, data, headers, params, auth_required, as_json, json_response, **kwargs
        )

    def delete(self, url, data=None, headers=None, params=None, auth_required=True, as_json=False, json_response=True,
               **kwargs):
        return self._request(
            httpx.delete, url, data, headers, params, auth_required, as_json, json_response, **kwargs
        )

    def put(self, url, data=None, headers=None, params=None, auth_required=True, as_json=False, json_response=True,
            **kwargs):
        return self._request(
            httpx.put, url, data, headers, params, auth_required, as_json, json_response, **kwargs
        )

    def _get_access_token(self):
        raise NotImplementedError
