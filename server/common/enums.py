from enum import Enum


class ExtendedEnum(Enum):
    @classmethod
    def choices(cls):
        for item in cls.__members__.values():
            yield item.value, item.name

    def __str__(self):
        return self.value

    @classmethod
    def max_length(cls):
        if len(cls.__members__):
            return max(len(item.value) for item in cls.__members__.values())
        return 1
