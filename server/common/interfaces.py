import random
from datetime import datetime


class IModel:
    @property
    def pk(self):
        raise NotImplementedError


class IModelIntId(IModel):
    id: int

    @property
    def pk(self):
        return self.id


class ICreatedDateMixin:
    created_date: datetime


class IRandomAmountMixin:
    from_amount: int
    to_amount: int
    multiplicity: int

    @property
    def amount(self):
        _from = self.from_amount // self.multiplicity
        _to = self.to_amount // self.multiplicity
        return random.randint(_from, _to) * self.multiplicity

    def get_amount_for_count(self, count):
        _from, _to = self.from_amount, self.to_amount
        self.from_amount = _from * count
        self.to_amount = _to * count
        amount = self.amount
        self.from_amount, self.to_amount = _from, _to
        return amount
