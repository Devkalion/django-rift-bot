from typing import Type

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.forms import TextInput

from .enums import ExtendedEnum
from .interfaces import IRandomAmountMixin


def EnumField(enum: Type[ExtendedEnum], **kwargs):
    kwargs['blank'] = kwargs.get('blank', False)
    kwargs['null'] = kwargs.get('null', False)
    return models.CharField(max_length=enum.max_length(), choices=enum.choices(), **kwargs)


class CreatedDateMixin(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class RandomAmount(models.Model, IRandomAmountMixin):
    from_amount = models.IntegerField(default=0)
    to_amount = models.IntegerField(default=0)
    multiplicity = models.IntegerField(default=1, validators=[MinValueValidator(1)])

    class Meta:
        abstract = True

    def clean(self):
        super().clean()
        if self.to_amount < self.from_amount:
            raise ValidationError({
                'to_amount': f'Max amount must not be less then min amount'
            })
        if self.to_amount < self.multiplicity:
            raise ValidationError({
                'to_amount': f'If max amount is less then multiplicity amount will always be 0'
            })


class ColorField(models.IntegerField):
    default_color = '#202225'
    default_validators = [MaxValueValidator(0xFFFFFF)]

    def formfield(self, **kwargs):
        kwargs['widget'] = TextInput(attrs={
            'type': 'color',
        })
        kwargs['help_text'] = kwargs.get('help_text', f'If you want to set default color use {self.default_color}')
        return super(models.IntegerField, self).formfield(**kwargs)

    def value_from_object(self, obj):
        res = super().value_from_object(obj)
        if res is None:
            return self.default_color
        return '#' + hex(int(res))[2:]

    def to_python(self, value):
        if value == self.default_color:
            return None
        return int(value[1:], 16)
