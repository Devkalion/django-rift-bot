from typing import Optional

from django.conf import settings
from redis.client import Redis


class RedisRepository:
    def __init__(self):
        self._connection = Redis(**settings.REDIS_CONNECTION_SETTINGS)

    def set(self, key: str, value=1, ttl: Optional[int] = None):
        self._connection.set(name=key, value=value, ex=ttl)

    def check_ttl(self, key: str):
        return self._connection.ttl(key)
