from rest_framework import serializers

from user.services import UserService


class UserSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=True)

    def validate(self, attrs):
        attrs['user'] = UserService.get_object(discord_id=attrs['user_id'])
        return attrs
