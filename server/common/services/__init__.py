from typing import TypeVar, Generic

from ..interfaces import IModel

T = TypeVar('T', bound=IModel)


class BaseObjectQuery(Generic[T]):
    def get_object(self, **kwargs) -> T:
        raise NotImplementedError

    def create_object(self, **kwargs) -> T:
        raise NotImplementedError

    def check_if_object_exists(self, **kwargs) -> bool:
        raise NotImplementedError


class BaseObjectFabric(Generic[T]):
    def __init__(self, query: BaseObjectQuery[T]):
        self.query = query

    def get_object(self, **kwargs) -> T:
        return self.query.get_object(**kwargs)

    def create_object(self, **kwargs) -> T:
        return self.query.create_object(**kwargs)

    def check_if_object_exists(self, **kwargs) -> bool:
        return self.query.check_if_object_exists(**kwargs)
