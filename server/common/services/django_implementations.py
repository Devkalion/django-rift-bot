from typing import TypeVar, Generic, Type

from django.db.models import Model

from . import BaseObjectQuery

T = TypeVar('T', bound=Model)


class DjangoObjectQuery(BaseObjectQuery, Generic[T]):
    model: Type[T]

    def get_object(self, **kwargs) -> T:
        return self.model.objects.get(**kwargs)

    def create_object(self, **kwargs) -> T:
        return self.model.objects.create(**kwargs)

    def check_if_object_exists(self, **kwargs) -> bool:
        return self.model.objects.filter(**kwargs).exists()
