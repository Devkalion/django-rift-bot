import random
from datetime import datetime, timedelta
from functools import wraps
from urllib.parse import urljoin

from django.conf import settings
from django.template import Context, Template
from django.template.library import Library
from django.template.loader import get_template
from django.utils.timezone import localtime, now

from common.utils import convert_html_str_to_image
from embeds.models import ImageGenerator, ImageTemplate

register = Library()


def suppress(func):
    @wraps(func)
    def f(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            return None

    return f


@register.filter
def concat(prefix, postfix):
    return ''.join((str(prefix), str(postfix)))


@register.filter
@suppress
def pretty_time(time):
    if isinstance(time, timedelta):
        td = time
    else:
        td = timedelta(seconds=time)
    if td.days:
        return f"{td.days} дн."
    elif td.seconds >= 3600:
        return f"{td.seconds // 3600} ч."
    elif td.seconds >= 60:
        return f"{td.seconds // 60} мин."
    return f"{td.seconds} сек."


@register.filter
def rand_range(n):
    return random.randint(1, n)


TIME_DIMENSIONS = [
    'нед.',
    'дн.',
    'ч.',
    'мин.',
    'сек.',
]


@register.filter
@suppress
def pretty_time2(time):
    if isinstance(time, timedelta):
        td = time
    else:
        td = timedelta(seconds=time)
    values = [
        td.days // 7,
        td.days % 7,
        (td.seconds // 3600) % 24,
        (td.seconds // 60) % 60,
        td.seconds % 60
    ]
    positive_value_strings = [
        f"{v} {a}"
        for v, a in zip(values, TIME_DIMENSIONS)
        if v > 0
    ]
    if len(positive_value_strings) == 0:
        return f"0 {TIME_DIMENSIONS[-1]}"
    return ' '.join(positive_value_strings[:2])


@register.filter
@suppress
def time_delta(time, time2=None):
    if time2 is None:
        time2 = now()
    return time2 - time


@register.filter
def parse_date(value, args):
    return datetime.strptime(value, args).astimezone(localtime().tzinfo)


@register.simple_tag
def to_list(*args):
    return args


@register.filter(is_safe=True)
@suppress
def recursive_render(html, context):
    template = Template(html)
    return template.render(Context(context))


@register.simple_tag(takes_context=True)
def render_image_generator(context: Context, generator_name: str):
    generator: ImageGenerator = ImageGenerator.objects.get(codename=generator_name)
    template = get_template('image.html')
    html = template.render({
        'generator': generator,
        'data': context,
        'media_full_url': settings.MEDIA_FULL_URL,
    })
    image_path = convert_html_str_to_image(html)
    return urljoin(settings.MEDIA_FULL_URL, image_path)


@register.simple_tag(takes_context=True)
def render_image_template(context: Context, generator_name: str):
    template: ImageTemplate = ImageTemplate.objects.get(codename=generator_name)
    context.update({
        'media_full_url': settings.MEDIA_FULL_URL,
    })
    html = Template(template.html).render(context)
    image_path = convert_html_str_to_image(html)
    return urljoin(settings.MEDIA_FULL_URL, image_path)
