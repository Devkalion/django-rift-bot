import os
import uuid

import imgkit
from django.conf import settings

options = {
    'format': 'png',
    'quality': 100,
    'transparent': '',
}


def convert_html_str_to_image(html: str) -> str:
    image_path = f'temp/{uuid.uuid4()}.png'
    imgkit.from_string(html, os.path.join(settings.MEDIA_ROOT, image_path), options=options)
    return image_path
