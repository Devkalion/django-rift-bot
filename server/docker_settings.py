from settings import *

CODE_DIR = os.path.join(BASE_DIR, 'app')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/rift-bot.sqlite3',
        'OPTIONS': {
            'timeout': 20,
        }
    },
}

LOGGING['root'] = {
    'handlers': ['console'],
    'level': 'DEBUG'
}

LOGGING['handlers'].pop('file')

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = [
    os.path.join(CODE_DIR, "static"),
]
