from django.contrib.admin import register, ModelAdmin, StackedInline

from .models import *


class DonateRewardInline(StackedInline):
    model = DonateReward
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@register(Donate)
class DonateAdmin(ModelAdmin):
    list_display = ('datetime', 'sum', 'user')
    list_filter = ('datetime',)
    inlines = [DonateRewardInline]
    raw_id_fields = ['user']

    def get_readonly_fields(self, request, obj: Donate = None):
        readonly_fields = super().get_readonly_fields(request, obj=obj)
        if obj is not None:
            readonly_fields = [*readonly_fields, "sum", "datetime"]
            if obj.user_id:
                readonly_fields.append("user")
        return readonly_fields


@register(DonateTotalSumReward)
class DonateTotalSumRewardAdmin(ModelAdmin):
    list_display = ("name", "donate_sum")
    ordering = ("donate_sum",)


@register(DonateSumReward)
class DonateSumRewardAdmin(ModelAdmin):
    list_display = ("name", "donate_sum", "fixed", "one_off")
    ordering = ("donate_sum",)
