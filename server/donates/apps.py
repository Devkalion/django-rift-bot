from django.apps import AppConfig


class DonatesConfig(AppConfig):
    name = 'donates'
    verbose_name = 'Пожертвования'
