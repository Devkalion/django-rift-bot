from datetime import datetime
from typing import Optional

from common.interfaces import IModelIntId, ICreatedDateMixin
from reward import interfaces as reward_interfaces
from user import interfaces as user_interfaces


class IDonate(IModelIntId, ICreatedDateMixin):
    sum: float
    user: Optional['user_interfaces.IUser']
    user_id: Optional[int]
    user_text: str = ""
    comment: str = ""
    is_message_published: bool = False
    datetime: datetime


class IDonateReward(IModelIntId):
    donate: IDonate
    donate_id: int
    reward: Optional["reward_interfaces.IReward"]
    reward_id: Optional[int]
    reward_text: str = ""
    is_given: bool
    is_for_total: bool


class IBaseDonateSumReward(IModelIntId):
    name: str
    image_url: Optional[str]
    thumbnail_url: Optional[str]
    text: str = ""
    reward_list: Optional['reward_interfaces.IRewardList']
    reward_list_id: Optional[int]
    donate_sum: float


class IDonateTotalSumReward(IBaseDonateSumReward):
    pass


class IDonateSumReward(IBaseDonateSumReward):
    one_off: bool
