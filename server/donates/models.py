from typing import Optional

from django.db import models

from common.models import CreatedDateMixin
from donates.interfaces import IDonate, IDonateReward, IBaseDonateSumReward, IDonateTotalSumReward, IDonateSumReward


class Donate(CreatedDateMixin, IDonate):
    sum = models.FloatField()
    user = models.ForeignKey("user.User", models.SET_NULL, null=True, blank=True)
    user_text = models.TextField(blank=True)
    comment = models.TextField(blank=True)
    datetime = models.DateTimeField()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        from donates.services import DonateService

        prev_obj: Optional[Donate] = None
        if self.pk is not None:
            prev_obj = Donate.objects.get(pk=self.pk)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

        if prev_obj is None:
            if self.user_id:
                DonateService.calc_rewards(self)
            DonateService.post_donate_messages(self)
        elif prev_obj.user_id is None and self.user_id is not None:
            DonateService.calc_rewards(self)
            DonateService.post_dm_message(self)


class DonateReward(models.Model, IDonateReward):
    donate = models.ForeignKey(Donate, models.CASCADE)
    reward = models.ForeignKey("reward.Reward", models.CASCADE, null=True, blank=True)
    reward_text = models.TextField(blank=True)
    is_given = models.BooleanField(default=False)
    is_for_total = models.BooleanField(default=False)


class BaseDonateSumReward(models.Model, IBaseDonateSumReward):
    name = models.CharField(max_length=100)
    image_url = models.URLField(blank=True, null=True)
    thumbnail_url = models.URLField(blank=True, null=True)
    text = models.TextField(blank=True)
    reward_list = models.ForeignKey("reward.RewardList", models.SET_NULL, null=True, blank=True)
    donate_sum = models.FloatField(unique=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class DonateTotalSumReward(BaseDonateSumReward, IDonateTotalSumReward):
    pass


class DonateSumReward(BaseDonateSumReward, IDonateSumReward):
    one_off = models.BooleanField()
    fixed = models.BooleanField(default=True)
