import json
from typing import Optional, Iterable

from django.db import transaction
from django.db.models import Q

from donates import models
from donates.interfaces import IDonate, IBaseDonateSumReward, IDonateSumReward, IDonateTotalSumReward, IDonateReward
from embeds.services import EmbedService
from events.enums import EventType
from events.services import EventService
from live_settings.models import Setting
from reward.interfaces import IReward
from reward.utils import ResultReward
from user.services import UserService


class DonateDBO:
    @staticmethod
    def mark_rewards_as_given(rewards: Iterable[IDonateReward]):
        models.DonateReward.objects.filter(pk__in=[r.id for r in rewards]).update(is_given=True)

    @staticmethod
    def get_sum_rewards(donate_sum: float) -> Iterable[IDonateSumReward]:
        return models.DonateSumReward.objects.filter(
            Q(donate_sum=donate_sum, fixed=True) | Q(donate_sum__lte=donate_sum, fixed=False)
        )

    @staticmethod
    def get_total_rewards(previous_total, new_total) -> Iterable[IDonateTotalSumReward]:
        return list(models.DonateTotalSumReward.objects.filter(
            donate_sum__lte=new_total, donate_sum__gt=previous_total
        ))

    @staticmethod
    def create_donate_reward(donate: IDonate, reward: IReward, reward_text: str, is_for_total: bool) -> IDonateReward:
        return models.DonateReward.objects.create(
            donate=donate,
            reward=reward,
            reward_text=reward_text,
            is_for_total=is_for_total
        )


class DonateServiceFabric:
    def __init__(self, dbo: DonateDBO):
        self.dbo = dbo

    def post_donate_messages(self, donate: IDonate):
        embed = EmbedService.get_object(name="donate")
        data = {
            'channel_id': Setting.get_value("MAIN_MESSAGE_CHANNEL_ID"),
            'messages': [
                json.loads(EmbedService.render_embed(embed, {"donate_id": donate.pk})),
            ]
        }
        EventService.create_event(EventType.PUBLISH, json.dumps(data, ensure_ascii=False))
        if donate.user_id:
            self.post_dm_message(donate)

    @staticmethod
    def post_dm_message(donate: IDonate):
        embed = EmbedService.get_object(name="donate_dm")
        data = {
            'messages': [
                json.loads(EmbedService.render_embed(embed, {"donate_id": donate.pk})),
            ]
        }
        EventService.create_event(EventType.PUBLISH, json.dumps(data, ensure_ascii=False), donate.user_id)

    @staticmethod
    def post_total_sum_reward_message(donate: IDonate, reward: IDonateTotalSumReward):
        embed = EmbedService.get_object(name="donate_total_reward")
        data = {
            'messages': [
                json.loads(EmbedService.render_embed(embed, {
                    "donate_id": donate.pk,
                    "total_sum_reward_id": reward.pk,
                })),
            ]
        }
        EventService.create_event(EventType.PUBLISH, json.dumps(data, ensure_ascii=False), donate.user_id)

    def calc_rewards(self, donate: IDonate):
        rewards = []

        sum_rewards = self.dbo.get_sum_rewards(donate.sum)
        for sum_reward in sum_rewards:
            reward = self.create_donate_reward(donate, sum_reward, False)
            rewards.append(reward)

        user = UserService.get_object(discord_id=donate.user_id)
        user_donates_sum = user.donates_sum
        total_sum_rewards = self.dbo.get_total_rewards(user_donates_sum - donate.sum, user_donates_sum)
        for sum_reward in total_sum_rewards:
            reward = self.create_donate_reward(donate, sum_reward, is_for_total=True)
            rewards.append(reward)
            self.post_total_sum_reward_message(donate, sum_reward)

        reward_dict = None
        for reward in rewards:
            if reward.reward_id is None:
                continue
            reward_dict = reward.reward.calc(donate.user, result=reward_dict)

        if reward_dict is not None:
            result_reward: ResultReward = ResultReward.from_dict(reward_dict)
            with transaction.atomic():
                result_reward.give(user=donate.user, role_reason="Донат")
                self.dbo.mark_rewards_as_given(rewards)

    def create_donate_reward(self, donate: IDonate, sum_reward: IBaseDonateSumReward, is_for_total: bool) -> IDonateReward:
        reward_list = sum_reward.reward_list
        reward: Optional[IReward] = None
        if reward_list:
            reward = sum_reward.reward_list.get_reward()

        return self.dbo.create_donate_reward(
            donate=donate,
            reward=reward,
            reward_text=sum_reward.text,
            is_for_total=is_for_total
        )


DonateService = DonateServiceFabric(
    dbo=DonateDBO()
)
