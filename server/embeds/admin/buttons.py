from django.contrib.admin import ModelAdmin, StackedInline, register

from ..models.buttons import Button, ButtonArgument


class ButtonArgumentInline(StackedInline):
    model = ButtonArgument
    extra = 0


@register(Button)
class ButtonAdmin(ModelAdmin):
    inlines = [ButtonArgumentInline]
    list_display = ['label', 'emoji', 'url', 'style', 'custom_id']
    list_filter = ['style', 'action_type']
