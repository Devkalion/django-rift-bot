from django.contrib import messages
from django.contrib.admin import ModelAdmin, StackedInline, register
from redis.client import Redis

from settings import REDIS_CONNECTION_SETTINGS
from ..models import EmbedVariable
from ..models.commands import Command, CommandArgument, CommandParameter, SimpleCommand, SlashCommand


@register(SimpleCommand)
class SimpleCommandAdmin(ModelAdmin):
    search_fields = ('command', 'description')
    list_display = ('command', 'description', 'embed')

    def save_model(self, request, obj: SimpleCommand, form, change):
        super().save_model(request, obj, form, change)
        EmbedVariable.objects.get_or_create(embed=obj.embed, variable_id='user_id')
        EmbedVariable.objects.get_or_create(embed=obj.embed, variable_id='avatar')


@register(Command)
class CommandAdmin(ModelAdmin):
    search_fields = ('prefix',)
    list_display = ('prefix', 'handler', 'cooldown')
    list_filter = ('handler',)
    filter_horizontal = ('roles',)


class SlashCommandArgumentInline(StackedInline):
    model = CommandArgument
    extra = 0
    classes = ('collapse',)

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class SlashCommandParameterInline(StackedInline):
    model = CommandParameter
    extra = 0

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@register(SlashCommand)
class SlashCommandAdmin(ModelAdmin):
    inlines = [SlashCommandParameterInline, SlashCommandArgumentInline]
    list_display = ('__str__', 'handler', 'enabled')
    list_filter = ('enabled', 'handler')
    actions = ['clear_cooldown']

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj is not None:
            readonly_fields = ('handler', *readonly_fields)
        else:
            readonly_fields = ('enabled', *readonly_fields)
        return readonly_fields

    def clear_cooldown(self, request, queryset):
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            command: SlashCommand
            keys = []
            for command in queryset:
                redis_pattern = f'slash-{command.name}-*'
                keys.extend(redis.keys(redis_pattern))
            if keys:
                redis.delete(*keys)
                self.message_user(
                    request,
                    message=f'Удалено {len(keys)} записей о кд',
                    level=messages.SUCCESS,
                )
            else:
                self.message_user(
                    request,
                    message=f'Записей о кд не найдено',
                    level=messages.SUCCESS,
                )

    clear_cooldown.short_description = "Сбросить кд по выбранным командам"
    clear_cooldown.allowed_permissions = ('clear_cooldown',)

    @staticmethod
    def has_clear_cooldown_permission(request, obj: SlashCommand = None):
        return request.user.has_perm('slash.can_clear_cooldown')
