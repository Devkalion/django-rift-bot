from queue import Queue
from typing import Iterable

from django.contrib.admin import ModelAdmin, StackedInline, register
from django.forms import BaseInlineFormSet, ModelForm, Widget
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from .utils import preview
from ..models.embeds import *
from ..services import EmbedDataService, EmbedService


class EmbedVariablesInlineFormset(BaseInlineFormSet):
    def check_permissions(self):
        form: ModelForm
        for form in self.forms:
            if self._should_delete_form(form) and not form.instance.variable.calculated:
                raise ValidationError(f'Variable {form.instance.variable.name} can\'t be deleted')
            if not form.instance.id and not form.instance.variable.calculated:
                raise ValidationError(f'Variable {form.instance.variable.name} can\'t be added')
            if form.instance.id and 'variable' in form.changed_data:
                raise ValidationError(f'You can\'t change used variable')

    def check_missing_variables(self):
        variables = {form.instance.variable.name for form in self.forms}
        errs = []
        for form in self.forms:
            instance: EmbedVariable = form.instance
            for required_variable_name in instance.variable.requires:
                if required_variable_name not in variables:
                    errs.append(
                        f"Variable {instance.variable.name} requires {required_variable_name} but it's not found"
                    )
        if errs:
            raise ValidationError(errs)

    @staticmethod
    def set_orders(instances: Iterable[EmbedVariable]):
        looked_variables = set()
        queue = Queue()
        for instance in instances:
            queue.put(instance)
        order = 1
        while not queue.empty():
            instance: EmbedVariable = queue.get()
            for required_variable_name in instance.variable.requires:
                if required_variable_name not in looked_variables:
                    queue.put(instance)
                    break
            else:
                instance.order = order
                instance.save(update_fields=['order'])
                looked_variables.add(instance.variable.name)
                order += 1

    def clean(self):
        super().clean()
        self.check_permissions()
        self.check_missing_variables()

    def save(self, commit=True):
        result = super().save(commit)
        self.set_orders(self.instance.vars.all())
        return result


class EmbedButtonInline(StackedInline):
    model = EmbedButton
    extra = 0
    ordering = ['index']
    classes = ('collapse',)


class EmbedVariablesInline(StackedInline):
    model = EmbedVariable
    extra = 0
    ordering = ['order']
    fields = ('variable', 'optional', 'example', 'order')
    readonly_fields = ('order',)
    formset = EmbedVariablesInlineFormset
    classes = ('collapse',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'variable':
            kwargs["queryset"] = Variable.objects.order_by('name')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@register(Variable)
class VariableAdmin(ModelAdmin):
    readonly_fields = ('calculated', 'requires')
    list_display = ('name', 'description')
    ordering = ['name']
    search_fields = ('name', 'description')


class Code(Widget):
    template_name = 'code.html'


@register(Embed)
class EmbedAdmin(ModelAdmin):
    inlines = [EmbedVariablesInline, EmbedButtonInline]
    readonly_fields = ('input_data', 'example', 'preview')
    list_display = ('name', 'category', 'description', 'command')
    search_fields = ('name', 'description', 'template', 'command')
    list_filter = ('category', 'command')
    ordering = ('name',)

    formfield_overrides = {
        models.TextField: {
            'widget': Code(attrs={
                'languages': [
                    ('django', 'Django'),
                    ('json', 'JSON'),
                    ('markdown', 'Markdown'),
                ],
                'default_language': 'json',
                'default': '{}',
                'with_select': True,
            }),
        },
    }

    def example(self, instance: Embed):
        example_data = EmbedService.get_example_data(instance)
        return mark_safe('<br>'.join([
            f'<b>{variable}</b> - [{type(value).__name__}]: {escape(value)}'
            for variable, value in example_data.items()
        ]))

    def input_data(self, instance: Embed):
        return mark_safe('<br>'.join([
            x.variable.name
            for x in instance.vars.all().select_related('variable').order_by('order')
        ]))

    def preview(self, instance: Embed):
        try:
            rendered_template = EmbedService.render_example(instance)
        except Exception:
            rendered_template = ''
        return preview(rendered_template)

    input_data.short_description = _("Input data")
    example.short_description = _("Input example")
    preview.short_description = _("Preview")


@register(UserMessage)
class UserMessageAdmin(ModelAdmin):
    list_display = ('user', 'recipient', 'status', 'created_date')
    list_filter = ('status',)
    readonly_fields = ('moderator', 'user', 'recipient')
    ordering = ('created_date',)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        if not super().has_change_permission(request, obj):
            return False

        return obj is None or str(obj.status) == str(UserMessageStatus.CREATED)

    def save_model(self, request, obj: UserMessage, form, change):
        obj.moderator = request.user
        return super().save_model(request, obj, form, change)


class EmbedFieldsInline(StackedInline):
    model = EmbedField
    extra = 0
    max_num = 25
    ordering = ['index']


@register(EmbedData)
class EmbedDataAdmin(ModelAdmin):
    readonly_fields = ('preview',)
    inlines = [EmbedFieldsInline]
    list_filter = ('category',)
    list_display = ('id', 'name', 'category')
    search_fields = ['name', 'category', 'title', 'description', 'content']

    @staticmethod
    def preview(embed_data: EmbedData):
        data = EmbedDataService.to_dict(embed_data)
        return preview(json.dumps(data, ensure_ascii=False))
