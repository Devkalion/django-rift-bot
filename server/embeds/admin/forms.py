from django.contrib.admin import ModelAdmin, StackedInline, register

from common.admin import CollapsedInlineMixin
from ..models.forms import *


class ModalStatusInline(StackedInline, CollapsedInlineMixin):
    model = ModalStatus
    extra = 0
    min_num = 1


class ModalFieldsInline(StackedInline, CollapsedInlineMixin):
    model = ModalFields
    extra = 0
    min_num = 1
    max_num = 5


@register(DiscordModal)
class DiscordModalAdmin(ModelAdmin):
    inlines = [ModalFieldsInline, ModalStatusInline]
    list_display = ['name', 'category', 'enabled', 'one_off']
    list_filter = ['category']


class UserFeedbackFieldsInline(StackedInline):
    model = UserFeedbackFields
    extra = 0

    def has_add_permission(self, request, obj):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@register(UserFeedback)
class UserFeedbackAdmin(ModelAdmin):
    inlines = [UserFeedbackFieldsInline]
    list_display = ['user', 'modal', 'datetime', 'status']
    list_filter = ['modal']
    readonly_fields = ['id', 'datetime', 'user', 'modal']

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj: UserFeedback = None):
        return all((
            obj is None or obj.status_id is None,
            super().has_change_permission(request, obj)
        ))
