import json

from django.contrib.admin import ModelAdmin, StackedInline, register
from django.forms import Widget
from django.template.loader import get_template
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from ..models.image_generators import *


class Code(Widget):
    template_name = 'code.html'


class ImageGeneratorFieldInline(StackedInline):
    model = ImageGeneratorField
    extra = 0
    ordering = ['-zindex']

    formfield_overrides = {
        models.TextField: {
            'widget': Code(attrs={
                'languages': [
                    ('django', 'Django'),
                    ('html', 'HTML'),
                ],
                'default': '',
                'default_language': 'html',
                'with_select': True,
            }),
        },
    }


class ImageGeneratorImageInline(StackedInline):
    model = ImageGeneratorImage
    extra = 0
    ordering = ['-zindex']


class ImageBaseAdmin(ModelAdmin):
    prepopulated_fields = {"codename": ("name",)}
    readonly_fields = ('preview',)
    list_display = ['name', 'codename', 'show_preview_url']

    def get_preview_url(self, obj: ImageBase) -> str:
        raise NotImplementedError

    def show_preview_url(self, obj: ImageBase):
        url = self.get_preview_url(obj)
        return format_html(
            """
            <a href="{url}" target="_blank" rel="noopener noreferrer" style="color: var(--color-primary-base);">
                preview
            </a>
            """,
            url=url,
        )

    def formfield_for_dbfield(self, db_field: models.Field, request, **kwargs):
        if db_field.name == 'example_data':
            return db_field.formfield(
                widget=Code(attrs={
                    'languages': [
                        ('json', 'json'),
                    ],
                    'default_language': 'json',
                    'default': '{}',
                    'with_select': False,
                }),
                **kwargs
            )
        return super().formfield_for_dbfield(db_field, request, **kwargs)

    def preview(self, instance: ImageBase):
        if instance.pk == '':
            return ''
        url = self.get_preview_url(instance)
        return mark_safe(f'''
            <iframe src="{url}"
              style="background-color: #313338;"
              onload="this.width=this.contentDocument.body.offsetWidth;this.height=this.contentDocument.body.offsetHeight;"
            >
            </iframe>
            <div>
                <a target="_blank" rel="noopener noreferrer" href="{url}">
                    Открыть в новой вкладке
                </a>
            </div>
        ''')

    preview.short_description = _("Preview")


@register(ImageGenerator)
class ImageGeneratorAdmin(ImageBaseAdmin):
    inlines = [ImageGeneratorFieldInline, ImageGeneratorImageInline]
    actions = ['transform_to_template']

    def transform_to_template(self, request, queryset):
        instance: ImageGenerator
        for instance in queryset:
            try:
                example_data = json.loads(instance.example_data)
            except json.JSONDecodeError:
                example_data = {}

            template = get_template('image.html')
            text = template.render({
                'generator': instance,
                'data': example_data,
            })

            ImageTemplate.objects.create(
                name=instance.name,
                codename=instance.codename,
                example_data=instance.example_data,
                html=text,
            )

    transform_to_template.short_description = "Преобразовать в шаблон"

    def get_preview_url(self, obj: ImageGenerator) -> str:
        return reverse('image_generator_preview', kwargs={'pk': obj.pk})

    def formfield_for_dbfield(self, db_field: models.Field, request, **kwargs):
        if db_field.name == 'css':
            return db_field.formfield(
                widget=Code(attrs={
                    'languages': [
                        ('css', 'css'),
                    ],
                    'default_language': 'css',
                    'default': '',
                    'with_select': False,
                }),
                **kwargs
            )
        if db_field.name == 'example_data':
            return db_field.formfield(
                widget=Code(attrs={
                    'languages': [
                        ('json', 'json'),
                    ],
                    'default_language': 'json',
                    'default': '{}',
                    'with_select': False,
                }),
                **kwargs
            )
        return super().formfield_for_dbfield(db_field, request, **kwargs)


@register(ImageTemplate)
class ImageGeneratorAdmin(ImageBaseAdmin):
    def get_preview_url(self, obj: ImageGenerator) -> str:
        return reverse('image_template_preview', kwargs={'pk': obj.pk})

    def formfield_for_dbfield(self, db_field: models.Field, request, **kwargs):
        if db_field.name == 'html':
            return db_field.formfield(
                widget=Code(attrs={
                    'languages': [
                        ('django', 'Django'),
                        ('html', 'HTML'),
                    ],
                    'default': '',
                    'default_language': 'html',
                    'with_select': True,
                }),
                **kwargs
            )
        return super().formfield_for_dbfield(db_field, request, **kwargs)


@register(FileUploads)
class FileUploadsAdmin(ModelAdmin):
    readonly_fields = ['url', 'pk']
    list_display = ['name', 'url']

    def url(self, instance: FileUploads):
        if not instance.pk:
            return ''
        return instance.file.url

    url.short_description = _("Ссылка")
