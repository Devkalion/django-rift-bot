import json

from django.contrib.admin import ModelAdmin, StackedInline, register

from .utils import preview
from ..models.posts import *
from ..services import PostService


class PostFieldsInline(StackedInline):
    model = PostField
    extra = 0
    max_num = 25
    ordering = ['index']


class PostButtonsInline(StackedInline):
    model = PostButton
    extra = 0
    ordering = ['index']


@register(Post)
class PostAdmin(ModelAdmin):
    readonly_fields = ('author', 'preview')
    list_filter = ('status', 'category')
    inlines = [PostFieldsInline, PostButtonsInline]
    list_display = ('name', 'author', 'category', 'status', 'preposting_embed')
    actions = ['confirm', 'duplicate']
    search_fields = ['name', 'title', 'description', 'content']

    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name == 'status':
            kwargs['choices'] = (
                (PostStatus.DRAFT.value, 'Черновик'),
                (PostStatus.TEST.value, 'Тестирование'),
            )
            if self.has_approve_permission(request, None):
                kwargs['choices'] += ((PostStatus.CONFIRMED.value, 'Подтверждено'),)
        return super().formfield_for_choice_field(db_field, request, **kwargs)

    def save_model(self, request, obj: Post, form, change):
        if obj.id is None:
            obj.author = request.user
        return super().save_model(request, obj, form, change)

    def has_change_permission(self, request, obj: Post = None):
        return all((
            super().has_change_permission(request, obj),
            obj is None or obj.editable,
            obj is None or obj.author == request.user or request.user.is_superuser
        ))

    def confirm(self, request, queryset):
        queryset = queryset.exclude(status__in=(
            PostStatus.CONFIRMED,
            PostStatus.POSTED,
        ))
        instance: Post
        for instance in queryset:
            instance.status = PostStatus.CONFIRMED
            instance.save(update_fields=('status',))

    confirm.short_description = "Подтвердить выбранные публикации"
    confirm.allowed_permissions = ('approve',)

    def duplicate(self, request, queryset):
        instance: Post
        for instance in queryset:
            PostService.duplicate(instance, request.user)

    duplicate.short_description = "Продублировать выбранные публикации"

    @staticmethod
    def has_approve_permission(request, obj: Post = None):
        return all((
            request.user.has_perm('embeds.can_approve_post'),
            obj is None or obj.editable
        ))

    @staticmethod
    def preview(post: Post):
        data = PostService.to_dict(post)
        return preview(json.dumps(data, ensure_ascii=False))
