from django.utils.html import escapejs
from django.utils.safestring import mark_safe

from settings import STATIC_URL


def preview(data):
    return mark_safe(''.join((
        '<iframe id="visualizer-frame" width="750px" height="650px"></iframe>',
        '<script>',
        'const frame_document = document.querySelector("iframe").contentDocument;'
        'frame_document.write("',
        escapejs(
            f'<script>window.initialData = JSON.parse("{escapejs(data)}")</script>'
            '<div id="root"></div>'
            f'<script type="text/javascript" src="{STATIC_URL}embed-visualizer/index.js"></script>'
            f'<link href="{STATIC_URL}embed-visualizer/index.css" rel="stylesheet">'
        ),
        '");',
        'setInterval(() => {',
        '  var el = frame_document.getElementsByClassName("flex-vertical whitney")[0];',
        '  if (el) {'
        '    document.querySelector("iframe").setAttribute("height", el.offsetHeight + 50);',
        '    frame_document.close()',
        '}}, 100)',
        '</script>',
    )))
