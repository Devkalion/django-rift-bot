from django.apps import AppConfig


class EmbedConfig(AppConfig):
    name = 'embeds'
    verbose_name = 'Эмбеды'
