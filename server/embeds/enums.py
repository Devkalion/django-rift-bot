from common.enums import ExtendedEnum


class ModalFieldType(ExtendedEnum):
    SINGLE_LINE = 'single_line'
    MULTI_LINE = 'multi_line'


class ComponentType(ExtendedEnum):
    ACTION_ROW = "1"
    BUTTON = "2"


class ButtonActionType(ExtendedEnum):
    EDIT_PERSONAL_ROLE = 'edit_role'
    EDIT_PERSONAL_CHANNEL = 'edit_personal_channel'
    RENDER_EMBED = 'render_embed'
    RENDER_ROLES = 'render_roles'
    TOGGLE_CHANNEL_VISIBILITY = 'toggle_channel_visibility'
    TOGGLE_CHANNEL_ACCESS = 'toggle_channel_access'


class ButtonStyle(ExtendedEnum):
    PRIMARY = "1"
    SECONDARY = "2"
    SUCCESS = "3"
    DANGER = "4"
    LINK = "5"


class ButtonArgumentKey(ExtendedEnum):
    EMBED_NAME = 'embed_name'
    OVERRIDES = 'overrided'
    PRIVATE_CHANNEL_ID = 'private_channel_id'


class ButtonResponseType(ExtendedEnum):
    EDIT = 'edit'
    REPLY = 'reply'


class Handler(ExtendedEnum):
    ACTIVE_USERS = 'ActiveUsersCommand'
    APPEAL_COMMAND = 'AppealCommand'
    BACKGROUNDS = 'BackgroundsCommand'
    BAN = 'BanCommand'
    BUY = 'BuyCommand'
    CLAIM_GIVEAWAY = 'ClaimCommand'
    CLEAR_BACKGROUND = 'ClearBackgroundCommand'
    CLEAR_ROLE = 'ClearRoleCommand'
    CLEAR_TITLE = 'ClearTitleCommand'
    EDIT_PERSONAL_ROLE = 'EditPersonalRoleCommand'
    EDIT_PERSONAL_ROLE_COLOR = 'EditPersonalRoleColorCommand'
    EDIT_PERSONAL_ROLE_NAME = 'EditPersonalRoleNameCommand'
    EMOTIONS = 'EmotionsCommand'
    EXCHANGE = 'ExchangeCommand'
    GIFTS = 'GiftsCommand'
    GIVEAWAYS = 'GiveawaysCommand'
    INVENTORY = 'InventoryCommand'
    JOURNEYS = 'JourneysCommand'
    KICK = 'KickCommand'
    MAKE_GIFT = 'GiftCommand'
    MARKET = 'MarketCommand'
    MUTE = 'MuteCommand'
    OFFENSES = 'OffensesCommand'
    OPEN_CONTAINER = 'OpenCommand'
    PROFILE = 'ProfileCommand'
    ROLES = 'RoleCommand'
    SELL = 'SaleCommand'
    SET_BACKGROUND = 'SetBackgroundCommand'
    SET_EMOTION = 'EmotionCommand'
    SET_ROLE = 'SetRoleCommand'
    SET_TITLE = 'SetTitleCommand'
    SHOP = 'ShopCommand'
    START_JOURNEY = 'JourneyCommand'
    START_TRANSFORM = 'TransformCommand'
    TITLES = 'TitlesCommand'
    TRANSFORMATION = 'TransformationsCommand'
    UNBAN = 'UnbanCommand'
    UNMUTE = 'UnmuteCommand'
    VOICE_KICK = 'VoiceKickCommand'
    WARN = 'WarnCommand'


class PostStatus(ExtendedEnum):
    DRAFT = 'draft'
    TEST = 'test'
    CONFIRMED = 'confirmed'
    POSTED = 'posted'


class SlashCommandHandler(ExtendedEnum):
    APPEAL = 'appeal'
    BAN = 'ban'
    BUY = 'buy'
    CLAIM = 'claim'
    EXCHANGE = 'exchange'
    KICK = 'kick'
    MESSAGE = 'message'
    MUTE = 'mute'
    OPEN_CONTAINER = 'open_container'
    PERSONAL_CHANNEL_ADD = 'channel_add'
    PERSONAL_CHANNEL_KICK = 'channel_kick'
    REFRESH_COMMANDS = 'refresh_commands'
    SALE = 'sale'
    SHOP = 'shop'
    SIMPLE = 'simple'
    START_ACTIVITY = 'start_activity'
    UNBAN = 'unban'
    UNMUTE = 'unmute'
    USER_INFO = 'user_info'
    VOICE_KICK = 'voice_kick'
    WARN = 'warn'


class SlashCommandParameterType(ExtendedEnum):
    CONTAINER_ID = 'container_id'
    ACTIVITY_CODENAME = 'activity_codename'
    SHOP_ID = 'shop_id'


handler_arguments = {
    str(SlashCommandHandler.APPEAL): [
        'user',
    ],
    str(SlashCommandHandler.BAN): [
        'user',
        'reason',
        'duration',
        'delete_message_seconds',
    ],
    str(SlashCommandHandler.BUY): [
        'item_id',
    ],
    str(SlashCommandHandler.EXCHANGE): [
        'item_id',
    ],
    str(SlashCommandHandler.KICK): [
        'user',
        'reason',
    ],
    str(SlashCommandHandler.MESSAGE): [
        'user',
    ],
    str(SlashCommandHandler.MUTE): [
        'user',
        'reason',
        'duration',
    ],
    str(SlashCommandHandler.OPEN_CONTAINER): [
        'amount',
    ],
    str(SlashCommandHandler.PERSONAL_CHANNEL_ADD): [
        'user',
    ],
    str(SlashCommandHandler.PERSONAL_CHANNEL_KICK): [
        'user',
    ],
    str(SlashCommandHandler.SALE): [
        'item_id',
    ],
    str(SlashCommandHandler.UNBAN): [
        'user',
        'reason',
    ],
    str(SlashCommandHandler.UNMUTE): [
        'user',
        'reason',
    ],
    str(SlashCommandHandler.USER_INFO): [
        'user',
    ],
    str(SlashCommandHandler.VOICE_KICK): [
        'user',
    ],
    str(SlashCommandHandler.WARN): [
        'user',
        'reason',
    ],
}

handler_variables = {
    str(SlashCommandHandler.APPEAL): {
        'appeal_id': {'optional': False},
    },
    str(SlashCommandHandler.BAN): {
        'action_id': {'optional': False},
    },
    str(SlashCommandHandler.BUY): {
        'user_id': {'optional': False},
        'reward_dict': {'optional': False},
        'shop_id': {'optional': False},
        'item_id': {'optional': False},
    },
    str(SlashCommandHandler.CLAIM): {
        'user_id': {'optional': False},
        'giveaway_id': {'optional': False},
        'reward_dict': {'optional': False},
    },
    str(SlashCommandHandler.EXCHANGE): {
        'user_id': {'optional': False},
        'reward_dict': {'optional': False},
        'item_id': {'optional': False},
    },
    str(SlashCommandHandler.KICK): {
        'action_id': {'optional': False},
    },
    str(SlashCommandHandler.MESSAGE): {
        'user_message_id': {'optional': False},
    },
    str(SlashCommandHandler.MUTE): {
        'action_id': {'optional': False},
    },
    str(SlashCommandHandler.OPEN_CONTAINER): {
        'user_id': {'optional': False},
        'container_name': {'optional': False},
        'container_id': {'optional': False},
        'count': {'optional': False},
        'reward_dict': {'optional': False},
    },
    str(SlashCommandHandler.PERSONAL_CHANNEL_ADD): {
        'user_id': {'optional': False},
        'admin_id': {'optional': False},
    },
    str(SlashCommandHandler.PERSONAL_CHANNEL_KICK): {
        'user_id': {'optional': False},
        'admin_id': {'optional': False},
    },
    str(SlashCommandHandler.REFRESH_COMMANDS): {
        'user_id': {'optional': False},
        'avatar': {'optional': False},
    },
    str(SlashCommandHandler.SHOP): {
        'user_id': {'optional': False},
        'shop_id': {'optional': False},
    },
    str(SlashCommandHandler.SIMPLE): {
        'user_id': {'optional': False},
        'avatar': {'optional': False},
    },
    str(SlashCommandHandler.START_ACTIVITY): {
        'user_id': {'optional': False},
        'avatar': {'optional': False},
        'reward_dict': {'optional': True},
        'activity_result_id': {'optional': False},
    },
    str(SlashCommandHandler.UNBAN): {
        'action_id': {'optional': False},
    },
    str(SlashCommandHandler.UNMUTE): {
        'action_id': {'optional': False},
    },
    str(SlashCommandHandler.USER_INFO): {
        'user_id': {'optional': False},
        'avatar': {'optional': False},
    },
    str(SlashCommandHandler.VOICE_KICK): {
        'user_id': {'optional': False},
        'admin_id': {'optional': False},
    },
    str(SlashCommandHandler.WARN): {
        'action_id': {'optional': False},
    },
}

handler_parameters = {
    str(SlashCommandHandler.BUY): [
        {'type': SlashCommandParameterType.SHOP_ID, 'name': 'shop_id'}
    ],
    str(SlashCommandHandler.OPEN_CONTAINER): [
        {'type': SlashCommandParameterType.CONTAINER_ID, 'name': 'container_id'}
    ],
    str(SlashCommandHandler.SHOP): [
        {'type': SlashCommandParameterType.SHOP_ID, 'name': 'shop_id'}
    ],
    str(SlashCommandHandler.START_ACTIVITY): [
        {'type': SlashCommandParameterType.ACTIVITY_CODENAME, 'name': 'activity_codename'}
    ],
}


class UserMessageStatus(ExtendedEnum):
    CREATED = 'created'
    APPROVED = 'approved'
    REJECTED = 'rejected'
