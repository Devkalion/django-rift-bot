from datetime import datetime
from typing import Type

from donates.models import Donate, DonateTotalSumReward
from giveaways.models import Draw, DrawReward, Encouragement, Generator, Giveaway
from inventory.models import *
from moder_actions.models import Appeal, ModerAction
from reward.models import Reward
from transformations.models import ActivityResult, GiftItem, JourneyItem, Shop, TransformItem
from user.models import User


def _instance_id(model: Type[models.Model], id_attr='id'):
    def f(**kwargs):
        return getattr(model.objects.first(), id_attr)

    return f


user_id = _instance_id(User, 'discord_id')
sender_id = user_id
recipient_id = user_id
admin_id = user_id

container_id = _instance_id(Container)
role_id = _instance_id(Role)
title_id = _instance_id(Title)
background_id = _instance_id(Background)
emotion_id = _instance_id(Emotion)
gift_id = _instance_id(GiftItem)
shop_id = _instance_id(Shop)
transformation_id = _instance_id(TransformItem, 'index_number')
journey_id = _instance_id(JourneyItem, 'index_number')
action_id = _instance_id(ModerAction)
giveaway_id = _instance_id(Giveaway)
draw_id = _instance_id(Draw)
draw_reward_id = _instance_id(DrawReward)
encouragement_id = _instance_id(Encouragement)
generator_id = _instance_id(Generator)
appeal_id = _instance_id(Appeal)
personal_role_id = _instance_id(PersonalRole)
donate_id = _instance_id(Donate)
user_message_id = 1
total_sum_reward_id = _instance_id(DonateTotalSumReward)
activity_result_id = _instance_id(ActivityResult)
personal_channel_id = _instance_id(PersonalChannel, 'channel_id')

def missing_inventory(**kwargs):
    return {}


def reason(**kwargs):
    return 'unknown'


def active_role_ids(**kwargs):
    role = Role.objects.first()
    if role:
        return [role.discord_id]
    return []


def to_date(**kwargs):
    return datetime.now().timestamp()


def item_id(**kwargs):
    return 1


def reward_dict(**kwargs):
    reward = Reward.get_empty_result()
    role = Role.objects.first()
    if role:
        reward['roles'][role.id] = 0
    return reward


def cooldown(**kwargs):
    return 90


def avatar(**kwargs):
    return 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'


def message_url(**kwargs):
    return 'https://www.google.com'


def level(**kwargs):
    return 5


def container_name(**kwargs):
    return 'контейнера'


def count(**kwargs):
    return 3


def text(**kwargs):
    return "some text"


def old_text(**kwargs):
    return "some old text"


def channel_id(**kwargs):
    return 384049283592355840


def everyone_access(**kwargs):
    return False


def everyone_visibility(**kwargs):
    return False
