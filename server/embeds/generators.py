from typing import Dict

from django.apps import apps
from django.db.models import QuerySet
from django.template import Template, Context
from django.utils.timezone import get_current_timezone

from donates.models import Donate, DonateTotalSumReward
from giveaways.models import Giveaway, Generator, Draw, DrawReward, Encouragement
from inventory.models import *
from moder_actions.models import *
from transformations.models import *
from user.models import User

tz = get_current_timezone()


def activity_result(activity_result_id, **kwargs):
    return ActivityResult.objects.get(pk=activity_result_id)


def user(user_id, **kwargs):
    from user.services import UserService
    return UserService.get_object(discord_id=user_id)


def cur_level_exp(user, **kwargs):
    return User.get_exp_for_level(user.level)


def next_level_exp(user, **kwargs):
    return User.get_exp_for_level(user.level + 1)


def level_percent_progress(user, cur_level_exp, next_level_exp, **kwargs):
    return (user.experience - cur_level_exp) * 100 // (next_level_exp - cur_level_exp)


def rating(user, **kwargs):
    return User.objects.filter(experience__gt=user.experience, left=False).count() + 1


def total_users(**kwargs):
    return User.objects.filter(left=False).count()


def currency(user_id, **kwargs):
    return _get_user_items(user_id, Currency.objects.all(), UserCurrency)


def containers(user_id, **kwargs):
    return _get_user_items(user_id, Container.objects.all(), UserContainer)


def items(user_id, **kwargs):
    return _get_user_items(user_id, Item.objects.all(), UserItem)


def container(container_id, **kwargs):
    return Container.objects.get(id=container_id)


def draw(draw_id, **kwargs):
    return Draw.objects.get(id=draw_id)


def draw_reward(draw_reward_id, **kwargs):
    return DrawReward.objects.get(id=draw_reward_id)


def reward(reward_dict, **kwargs):
    return ResultReward.from_dict(reward_dict)


def roles(user, **kwargs):
    return user.roles.filter(is_tech=False).select_related('category')


def temp_roles(user_id, **kwargs):
    return TempRole.objects.filter(
        user_id=user_id, role__is_tech=False, to_date__gt=now()
    ).select_related('role', 'role__category')


def active_roles(active_role_ids, **kwargs):
    return Role.objects.filter(is_tech=False, discord_id__in=active_role_ids)


def role_categories(roles, temp_roles, user_id, **kwargs):
    categories = {}
    for role in roles:
        category = categories.get(role.category, {})
        arr = category.get('roles', [])
        arr.append(role)
        category['roles'] = arr
        categories[role.category] = category

    for temp_role in temp_roles:
        category = categories.get(temp_role.role.category, {})
        arr = category.get('temp_roles', [])
        arr.append(temp_role)
        category['temp_roles'] = arr
        categories[temp_role.role.category] = category

    personal_roles = PersonalRole.objects.filter(
        user_id=user_id,
        end_date__gt=now(),
    ).select_related('role', 'role__category')
    for personal_role in personal_roles:
        category = categories.get(personal_role.role.category, {})
        arr = category.get('personal_roles', [])
        arr.append(personal_role)
        category['personal_roles'] = arr
        categories[personal_role.role.category] = category

    return [
        (category, categories[category])
        for category in sorted(categories.keys(), key=lambda x: 10e6 if x is None else x.index_number)
    ]


def emotions(user, **kwargs):
    return user.emotions.order_by('category__index_number').select_related('category')


def emotion_categories(emotions, **kwargs):
    return _category_items(emotions)


def backgrounds(user, **kwargs):
    return user.backgrounds.order_by('category__index_number').select_related('category')


def background_categories(backgrounds, **kwargs):
    return _category_items(backgrounds)


def role(role_id, **kwargs):
    return Role.objects.get(id=role_id)


def title(title_id, **kwargs):
    return Title.objects.get(id=title_id)


def background(background_id, **kwargs):
    return Background.objects.get(id=background_id)


def emotion(emotion_id, **kwargs):
    return Emotion.objects.get(id=emotion_id)


def emotion_text(emotion, **kwargs):
    emotion_text = Template(emotion.text)
    context = Context(kwargs)
    return emotion_text.render(context)


def gift_items(**kwargs):
    return _transformation_items(GiftItem.objects)


def transform_items(**kwargs):
    return _transformation_items(TransformItem.objects)


def sender(sender_id, **kwargs):
    return user(sender_id)


def recipient(recipient_id, **kwargs):
    return user(recipient_id)


def gift(gift_id, **kwargs):
    return GiftItem.objects.get(index_number=gift_id)


def calculated_missing_inventory(missing_inventory, **kwargs):
    result = []
    for model, user_model in [(Currency, UserCurrency), (Item, UserItem), (Container, UserContainer)]:
        items = {
            int(miss['id']): miss['amount']
            for miss in missing_inventory
            if apps.get_model(miss['type']) == model
        }
        result += [
            user_model(item=item, amount=items[item.id])
            for item in model.objects.filter(id__in=items.keys())
        ]

    return result


def shop(shop_id, **kwargs):
    return Shop.objects.get(id=shop_id)


def shop_items(shop_id, **kwargs):
    return _transformation_items(ShopItem.objects.filter(shop_id=shop_id))


def shop_item(shop_id, item_id, **kwargs):
    return ShopItem.objects.filter(shop_id=shop_id).get(index_number=item_id)


def transformation(transformation_id, **kwargs):
    return TransformItem.objects.get(index_number=transformation_id)


def calculated_to_date(to_date, **kwargs):
    return datetime.fromtimestamp(to_date).astimezone(tz)


def journey(journey_id, **kwargs):
    return JourneyItem.objects.get(index_number=journey_id)


def exchange_items(**kwargs):
    return _transformation_items(MarketExchange.objects)


def sale_items(**kwargs):
    return _transformation_items(MarketSale.objects)


def sale_item(item_id, **kwargs):
    return MarketSale.objects.get(index_number=item_id)


def exchange_item(item_id, **kwargs):
    return MarketExchange.objects.get(index_number=item_id)


def offenses(user_id, **kwargs):
    return ModerAction.objects.filter(user_id=user_id).exclude(
        type__in=[ActionType.UNBAN, ActionType.UNMUTE]
    )


def appeals(user_id, **kwargs):
    return Appeal.objects.filter(user_id=user_id)


def appeal(appeal_id, **kwargs):
    return Appeal.objects.get(pk=appeal_id)


def warnings(offenses, **kwargs):
    return offenses.filter(type=ActionType.WARNING).count()


def mutes(offenses, **kwargs):
    return offenses.filter(type=ActionType.MUTE).count()


def moder_action(action_id, **kwargs):
    return ModerAction.objects.get(id=action_id)


def active_giveaway(**kwargs):
    return Giveaway.active_giveaway()


def giveaways(active_giveaway, **kwargs):
    if active_giveaway:
        return Giveaway.nearest()[1:9]
    return Giveaway.nearest()[:9]


def giveaway(giveaway_id, **kwargs):
    return Giveaway.objects.get(id=giveaway_id)


def generator(generator_id, **kwargs):
    return Generator.objects.get(pk=generator_id)


def datetime(**kwargs):
    return now()


def titles(user_id, **kwargs):
    return UserTitle.objects.filter(user_id=user_id).select_related(
        'title'
    ).order_by('title_id')


def admin(admin_id, **kwargs):
    return user(admin_id)


def journey_items(**kwargs):
    return _transformation_items(JourneyItem.objects)


def encouragement(encouragement_id, **kwargs):
    return Encouragement.objects.get(id=encouragement_id)


def personal_roles(user_id, **kwargs):
    return PersonalRole.objects.select_related('role').filter(user_id=user_id)


def personal_role(personal_role_id, **kwargs):
    return PersonalRole.objects.select_related('role').get(pk=personal_role_id)


def active_users(**kwargs):
    base_queryset = User.objects.all()
    return _top(
        base_queryset,
        base_queryset,
        'experience'
    )


def donate(donate_id, **kwargs):
    return Donate.objects.get(pk=donate_id)


def user_message(user_message_id, **kwargs):
    from embeds.models.embeds import UserMessage

    return UserMessage.objects.get(pk=user_message_id)


def personal_channels(user_id, **kwargs):
    from inventory.models import PersonalChannel

    return PersonalChannel.objects.filter(creator_id=user_id)


def personal_channel(personal_channel_id, **kwargs):
    return PersonalChannel.objects.filter(channel_id=personal_channel_id)


def total_sum_reward(total_sum_reward_id, **kwargs):
    return DonateTotalSumReward.objects.get(pk=total_sum_reward_id)


def _get_user_items(user_id, queryset, user_model):
    result = []
    user_items = {
        user_item.item.id: user_item
        for user_item in user_model.objects.filter(user_id=user_id, item__in=list(queryset)).select_related('item')
    }
    for item in queryset:
        if item.id in user_items:
            result.append(user_items[item.id])
        else:
            result.append(user_model(item=item, amount=0))

    return sorted(result, key=lambda x: x.item.index_number)


def _category_items(items) -> Dict[BaseCategory, list]:
    categories = {}
    for item in items:
        arr = categories.get(item.category, [])
        arr.append(item)
        categories[item.category] = arr
    return categories


def _transformation_items(queryset: QuerySet):
    return queryset.prefetch_related(
        'price__pricecurrency_set', 'price__priceitem_set', 'price__pricecontainer_set',
        'price__pricecurrency_set__item', 'price__priceitem_set__item', 'price__pricecontainer_set__item',
    ).select_related(
        'price', 'reward'
    ).filter(hidden=False).order_by('index_number')


def _top(simple_queryset, queryset, field, limit=50):
    user = simple_queryset.order_by(f'-{field}').only(field)[min(limit, simple_queryset.count()) - 1]
    users = list(queryset.order_by(f'-{field}').filter(**{
        f'{field}__gte': max(getattr(user, field), 1)
    }))

    result = {
        1: [users[0]]
    }
    place = 1
    for index, user in enumerate(users[1:], 2):
        if getattr(user, field) != getattr(result[place][0], field):
            place = index
            result[place] = []
        result[place].append(user)
    return result
