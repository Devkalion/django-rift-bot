import importlib
from datetime import timedelta
from inspect import signature
from typing import Optional, Union

from common.interfaces import IModelIntId, IModel, ICreatedDateMixin
from user import interfaces as user_interfaces
from .enums import (
    ButtonActionType,
    ButtonArgumentKey,
    ButtonResponseType,
    ButtonStyle,
    ComponentType,
    Handler,
    PostStatus,
    SlashCommandHandler,
    SlashCommandParameterType, UserMessageStatus,
)

generator_module = importlib.import_module('.generators', 'embeds')
example_module = importlib.import_module('.examples', 'embeds')


class IEmbed(IModel):
    name: str
    description: str
    command: Optional[str]
    template: str
    category: Optional[str]

    @property
    def pk(self):
        return self.name


class IComponent(IModelIntId):
    type: ComponentType
    # row: Optional[int]


class IAttachedComponent(IModelIntId):
    item_id: int
    disabled: bool
    index: int

    @property
    def component(self) -> IComponent:
        raise NotImplementedError


class IButton(IComponent):
    style: ButtonStyle
    action_type: Optional[ButtonActionType]
    label: Optional[str]
    url: Optional[str]
    emoji: Optional[str]
    response_type: ButtonResponseType

    type: ComponentType = ComponentType.BUTTON


class IButtonArgument(IModelIntId):
    button_id: int
    key: ButtonArgumentKey
    value: str


class ISimpleCommand(IModel):
    command: str
    description: Optional[str]
    input_channel_ids: Optional[str]
    result_channel_id: Optional[str]
    embed: IEmbed
    embed_id: str

    @property
    def pk(self):
        return self.command


class IVariable(IModel):
    name: str
    description: Optional[str]
    example: Optional[str]

    @property
    def pk(self):
        return self.name

    @property
    def calculated(self):
        return hasattr(generator_module, self.name)

    @property
    def requires(self):
        generator = getattr(generator_module, self.name, lambda: None)
        return [
            key
            for key in signature(generator).parameters.keys()
            if key != 'kwargs'
        ]

    def get_value(self, **kwargs):
        if self.name in kwargs:
            return kwargs[self.name]
        generator = getattr(generator_module, self.name)
        return generator(**kwargs)

    def get_example(self, **kwargs):
        if self.example:
            return self.example
        try:
            example_func = getattr(example_module, self.name)
            return example_func(**kwargs)
        except Exception:
            pass
        try:
            return self.get_value(**kwargs)
        except Exception:
            return None


class IEmbedVariable(IModelIntId):
    variable: IVariable
    variable_id: str
    embed: IEmbed
    embed_id: str
    optional: bool
    example: Optional[str]
    order: int

    def get_example(self, **kwargs):
        return self.example if self.example else self.variable.get_example(**kwargs)

    def get_value(self, **kwargs):
        try:
            return self.variable.get_value(**kwargs)
        except:
            if self.optional:
                return None
            raise


class ICommand(IModelIntId):
    handler: Union[Handler, str]
    prefix: str
    cooldown: Optional[timedelta]
    disabled: bool

    # roles = models.ManyToManyField('inventory.Role', blank=True)

    @property
    def cooldown_seconds(self) -> Optional[float]:
        return self.cooldown.total_seconds() if self.cooldown else None


class IEmbedData(IModelIntId):
    name: str
    category: Optional[str]
    content: Optional[str]
    title: str
    description: str
    image_url: str
    color: int
    thumbnail_url: str
    footer_text: str
    footer_icon_url: str

    def __str__(self):
        return self.name


class IField(IModelIntId):
    title: str
    text: str
    inline: bool
    index: int


class IEmbedField(IField):
    embed_data: IEmbedData


class IPost(IEmbedData):
    author: 'user_interfaces.IAdmin'
    status: Union[PostStatus, str]
    channel_id: str
    preposting_embed: Optional[IEmbed]

    @property
    def editable(self):
        return str(self.status) not in (
            PostStatus.CONFIRMED.value,
            PostStatus.POSTED.value
        )


class IPostField(IField):
    post: IPost


class ISlashCommand(IModelIntId):
    name: str
    description: str
    handler: SlashCommandHandler
    enabled: bool
    ephemeral: bool
    embed_id: str
    error_embed_id: Optional[str]
    cooldown: Optional[timedelta]


class ICommandArgument(IModelIntId):
    code_name: str
    name: str
    description: str
    command_id: int


class ICommandParameter(IModelIntId):
    name: str
    type: SlashCommandParameterType
    command_id: int
    value: Optional[str]


class IUserMessage(IModelIntId, ICreatedDateMixin):
    user_id: int
    recipient_id: int
    moderator_id: Optional[int]
    text: str
    status: UserMessageStatus
