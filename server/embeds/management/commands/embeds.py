"""
Subcommands to execute:
    sync synchronize django settings with in-db livesettings.
"""
import json
import os
from argparse import RawTextHelpFormatter

from django.conf import settings
from django.core.management.base import BaseCommand

from embeds.models import Variable, EmbedVariable
from embeds.services import EmbedService


class Command(BaseCommand):
    @staticmethod
    def create_variables(variables):
        for name, description in variables.items():
            if not Variable.objects.filter(name=name).exists():
                Variable.objects.create(name=name, description=description)

    def create_embeds(self, embeds):
        for embed_name, params in embeds.items():
            if not EmbedService.check_if_object_exists(name=embed_name):
                EmbedService.create_object(
                    name=embed_name,
                    description=params['description'],
                    command=params['command'],
                    category=params['category'],
                    template='{}',
                )
            self.create_embed_variables(embed_name, params['variables'])

    @staticmethod
    def create_embed_variables(embed_name, variables):
        for variable_name, optional in variables.items():
            ev: EmbedVariable = EmbedVariable.objects.filter(
                variable_id=variable_name,
                embed_id=embed_name
            ).first()
            if ev and ev.optional != optional:
                ev.optional = optional
                ev.save(update_fields=('optional',))
            elif not ev:
                EmbedVariable.objects.get_or_create(
                    variable_id=variable_name,
                    embed_id=embed_name,
                    optional=optional
                )

    def sync(self):
        with open(os.path.join(settings.CODE_DIR, 'embeds', 'data.json'), encoding='utf8') as f:
            data = json.load(f)
        self.create_variables(data['variables'])
        self.create_embeds(data['embeds'])

    def handle(self, *args, **options):
        self.sync()

    def add_arguments(self, parser):
        parser.formatter_class = RawTextHelpFormatter
        parser.add_argument('command', choices=['sync'], help=__doc__)
