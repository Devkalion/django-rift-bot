# Generated by Django 3.1.7 on 2021-09-01 09:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('embeds', '0007_auto_20210820_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='command',
            name='handler',
            field=models.CharField(choices=[('ActiveUsersCommand', 'ACTIVE_USERS'), ('BackgroundsCommand', 'BACKGROUNDS'), ('BanCommand', 'BAN'), ('BuyCommand', 'BUY'), ('ClaimCommand', 'CLAIM_GIVEAWAY'), ('ClearBackgroundCommand', 'CLEAR_BACKGROUND'), ('ClearRoleCommand', 'CLEAR_ROLE'), ('ClearTitleCommand', 'CLEAR_TITLE'), ('EditPersonalRoleCommand', 'EDIT_PERSONAL_ROLE'), ('EditPersonalRoleColorCommand', 'EDIT_PERSONAL_ROLE_COLOR'), ('EditPersonalRoleNameCommand', 'EDIT_PERSONAL_ROLE_NAME'), ('EmotionsCommand', 'EMOTIONS'), ('ExchangeCommand', 'EXCHANGE'), ('GiftsCommand', 'GIFTS'), ('GiveawaysCommand', 'GIVEAWAYS'), ('InventoryCommand', 'INVENTORY'), ('JourneysCommand', 'JOURNEYS'), ('KickCommand', 'KICK'), ('GiftCommand', 'MAKE_GIFT'), ('MarketCommand', 'MARKET'), ('MuteCommand', 'MUTE'), ('OffensesCommand', 'OFFENSES'), ('OpenCommand', 'OPEN_CONTAINER'), ('ProfileCommand', 'PROFILE'), ('RoleCommand', 'ROLES'), ('SaleCommand', 'SELL'), ('SetBackgroundCommand', 'SET_BACKGROUND'), ('EmotionCommand', 'SET_EMOTION'), ('SetRoleCommand', 'SET_ROLE'), ('SetTitleCommand', 'SET_TITLE'), ('ShopCommand', 'SHOP'), ('JourneyCommand', 'START_JOURNEY'), ('TransformCommand', 'START_TRANSFORM'), ('TitlesCommand', 'TITLES'), ('TransformationsCommand', 'TRANSFORMATION'), ('UnbanCommand', 'UNBAN'), ('UnmuteCommand', 'UNMUTE'), ('VoiceKickCommand', 'VOICE_KICK'), ('WarnCommand', 'WARN')], max_length=28),
        ),
    ]
