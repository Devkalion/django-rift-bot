# Generated by Django 3.1.3 on 2022-12-27 22:05

from django.db import migrations, models
import django.db.models.deletion
import embeds.interfaces


class Migration(migrations.Migration):

    dependencies = [
        ('embeds', '0017_auto_20221227_0143'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommandParameter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(editable=False, max_length=32)),
                ('type', models.CharField(choices=[('container_id', 'CONTAINER_ID'), ('shop_id', 'SHOP_ID')], editable=False, max_length=12)),
                ('value', models.CharField(max_length=50, null=True)),
                ('command', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='embeds.slashcommand')),
            ],
            options={
                'unique_together': {('name', 'command_id')},
            },
            bases=(models.Model, embeds.interfaces.ICommandParameter),
        ),
    ]
