from django.core.exceptions import ValidationError
from django.db import models

from common.models import EnumField
from ..enums import (
    ButtonActionType,
    ButtonArgumentKey,
    ButtonResponseType,
    ButtonStyle,
)
from ..interfaces import IButton, IButtonArgument, IAttachedComponent


class Button(models.Model, IButton):
    style = EnumField(ButtonStyle, default=ButtonStyle.SECONDARY)
    action_type = EnumField(ButtonActionType, blank=True, null=True)
    label = models.CharField(max_length=50, blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    emoji = models.CharField(max_length=50, null=True, blank=True)
    response_type = EnumField(ButtonResponseType, default=ButtonResponseType.EDIT)

    def __str__(self):
        items = [
            self.emoji,
            self.label,
            self.url,
            f'[{dict(ButtonStyle.choices())[str(self.style)]}]',
            self.custom_id,
        ]
        return ' '.join(filter(bool, items))

    @property
    def custom_id(self):
        if self.url:
            return ''
        from embeds.services import ComponentService

        return ComponentService.render_button_custom_id(self, {})

    def clean(self):
        super().clean()
        if self.url and self.action_type:
            raise ValidationError('Only action_type or url must be provided')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.url:
            self.style = ButtonStyle.LINK
            ButtonArgument.objects.filter(button=self).delete()
        elif str(self.style) == str(ButtonStyle.LINK):
            self.style = ButtonStyle.SECONDARY
        super().save(force_insert, force_update, using, update_fields)


class AttachedButton(models.Model, IAttachedComponent):
    item: None
    button = models.ForeignKey('embeds.Button', models.CASCADE)
    disabled = models.BooleanField(default=False)
    index = models.PositiveSmallIntegerField(blank=True, null=True)

    def validate_unique(self, exclude=None):
        super().validate_unique(exclude)
        if self.button.url:
            return

        from embeds.services import ComponentService

        buttons = [
            btn.button
            for btn in type(self).objects.select_related('button').filter(item_id=self.pk)
            if not btn.button.url
        ]
        if not self.pk:
            buttons.append(self.button)
        custom_ids = {ComponentService.render_button_custom_id(btn, {}) for btn in buttons}
        if len(custom_ids) != len(buttons):
            raise ValidationError('Some button custom_id is duplicated')

    @property
    def component(self):
        return self.button

    class Meta:
        abstract = True


class ButtonArgument(models.Model, IButtonArgument):
    button = models.ForeignKey(Button, models.CASCADE)
    key = EnumField(ButtonArgumentKey)
    value = models.CharField(max_length=50)

    def clean_fields(self, exclude=None):
        if str(self.key) == str(ButtonArgumentKey.EMBED_NAME):
            from embeds.services import EmbedService
            embed_name = self.value
            if not EmbedService.check_if_object_exists(name=embed_name):
                raise ValidationError({'value': 'No such embed'})

