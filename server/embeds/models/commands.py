import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from common.models import EnumField
from .embeds import EmbedVariable
from ..enums import (
    Handler,
    handler_arguments,
    handler_variables,
    handler_parameters,
    SlashCommandHandler,
    SlashCommandParameterType,
)
from ..interfaces import (
    ICommand,
    ICommandArgument,
    ICommandParameter,
    ISimpleCommand,
    ISlashCommand,
)


class SimpleCommand(models.Model, ISimpleCommand):
    command = models.CharField(max_length=50, primary_key=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    input_channel_ids = models.CharField(
        max_length=200, blank=True, null=True,
        help_text=_('Comma separated allowed channel ids. To specify dm channel use blank string')
    )
    result_channel_id = models.CharField(
        max_length=50, blank=True, null=True,
        help_text=_('Channel id where embed will be sent. If blank, dm channel used')
    )
    embed = models.ForeignKey('embeds.Embed', models.CASCADE)


class Command(models.Model, ICommand):
    handler = EnumField(enum=Handler)
    prefix = models.CharField(max_length=127, unique=True)
    cooldown = models.DurationField(null=True, blank=True)
    disabled = models.BooleanField(default=False)
    roles = models.ManyToManyField('inventory.Role', blank=True)

    def clean(self, exclude=None):
        prefix = self.prefix.lower().strip()
        for command in Command.objects.filter(prefix__startswith=prefix[0]).exclude(id=self.id):
            if prefix.startswith(command.prefix) or command.prefix.startswith(prefix):
                raise ValidationError({
                    'prefix': f'Such prefix conflicts with command "{command}"'
                })
        self.prefix = prefix

    def __str__(self):
        return f'{self.prefix} ({self.handler})'


# from discord.py app_commands.commands
THAI_COMBINING = r'\u0e31-\u0e3a\u0e47-\u0e4e'
DEVANAGARI_COMBINING = r'\u0900-\u0903\u093a\u093b\u093c\u093e\u093f\u0940-\u094f\u0955\u0956\u0957\u0962\u0963'
regexp = r'[-_\w' + THAI_COMBINING + DEVANAGARI_COMBINING + r']{1,32}'
name_validator = RegexValidator(
    regex=re.compile(r'^' + regexp + '$'),
    message='Must be between 1-32 characters and contain only lower-case letters, numbers, hyphens, or underscores.'
)

VALID_SLASH_COMMAND_NAME = re.compile(r'^[-_\w' + THAI_COMBINING + DEVANAGARI_COMBINING + r']{1,32}$')
multi_name_validator = RegexValidator(
    regex=re.compile(r'^(' + regexp + ' )*(' + regexp + ')$'),
    message='Must be between 1-32 characters and contain only lower-case letters, numbers, hyphens, or underscores.'
)


class SlashCommand(models.Model, ISlashCommand):
    name = models.CharField(max_length=200, validators=[multi_name_validator], unique=True)
    description = models.CharField(max_length=100)
    handler = EnumField(SlashCommandHandler)
    enabled = models.BooleanField(default=False)
    ephemeral = models.BooleanField(default=False)
    embed = models.ForeignKey('embeds.Embed', models.CASCADE)
    error_embed = models.ForeignKey('embeds.Embed', models.SET_NULL, related_name='+', blank=True, null=True)
    cooldown = models.DurationField(null=True, blank=True)

    class Meta:
        permissions = [
            ("can_clear_cooldown", "Can clear cooldown"),
        ]

    def __str__(self):
        return self.name

    def validate_unique(self, exclude=None):
        first_word = self.name.split(' ')[0]
        queryset = SlashCommand.objects.filter(name__istartswith=first_word).only('name')
        if self.pk:
            queryset = queryset.exclude(pk=self.pk)
        for command in queryset:
            if self.name.startswith(command.name) or command.name.startswith(self.name):
                raise ValidationError({
                    'name': ValidationError(f'Conflicting name with command "{command.name}"')
                })

        return super().validate_unique(exclude)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        created = self.pk is None
        super().save(force_insert, force_update, using, update_fields)
        if not created:
            return

        arguments = handler_arguments.get(str(self.handler), [])
        if arguments:
            CommandArgument.objects.bulk_create(
                CommandArgument(code_name=code_name, name=code_name, description=code_name, command=self)
                for code_name in arguments
            )

        variables = handler_variables.get(str(self.handler), {})
        if variables:
            EmbedVariable.objects.bulk_create(
                (
                    EmbedVariable(variable_id=code_name, embed_id=self.embed_id, **params)
                    for code_name, params in variables.items()
                ),
                ignore_conflicts=True
            )

        parameters = handler_parameters.get(str(self.handler), [])
        if parameters:
            CommandParameter.objects.bulk_create(
                CommandParameter(name=parameter_dict['name'], type=parameter_dict['type'], command=self)
                for parameter_dict in parameters
            )


class CommandArgument(models.Model, ICommandArgument):
    code_name = models.CharField(max_length=50, editable=False)
    name = models.CharField(max_length=32, validators=[name_validator])
    description = models.CharField(max_length=200)
    command = models.ForeignKey(SlashCommand, models.CASCADE)

    def __str__(self):
        return self.code_name


class CommandParameter(models.Model, ICommandParameter):
    name = models.CharField(max_length=32, editable=False)
    type = EnumField(SlashCommandParameterType, editable=False)
    value = models.CharField(max_length=50, null=True, blank=False)
    command = models.ForeignKey(SlashCommand, models.CASCADE)

    def clean(self):
        super().clean()
        if self.id is None:
            return

        self.value = self.value.strip()

        if not self.value:
            raise ValidationError({
                'value': ValidationError('value must be specified')
            })

        if str(self.type) == str(SlashCommandParameterType.SHOP_ID):
            from transformations.models import Shop
            shop = Shop.objects.filter(pk=self.value).first()
            if shop is None:
                raise ValidationError({
                    'value': ValidationError('No shop with such id')
                })
        elif str(self.type) == str(SlashCommandParameterType.CONTAINER_ID):
            from inventory.models import Container
            container = Container.objects.filter(pk=self.value).first()
            if container is None:
                raise ValidationError({
                    'value': ValidationError('No container with such id')
                })
        elif str(self.type) == str(SlashCommandParameterType.ACTIVITY_CODENAME):
            from transformations.models import Activity

            if not Activity.objects.filter(codename=self.value).exists():
                raise ValidationError({
                    'value': ValidationError('No activity with such codename')
                })

    class Meta:
        unique_together = ['name', 'command_id']

    def __str__(self):
        return self.name
