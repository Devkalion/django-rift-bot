import json

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from common.models import ColorField, EnumField, CreatedDateMixin
from .buttons import AttachedButton
from ..enums import UserMessageStatus
from ..interfaces import (
    IEmbed,
    IEmbedData,
    IEmbedField,
    IEmbedVariable,
    IUserMessage,
    IVariable,
)


class Embed(models.Model, IEmbed):
    name = models.CharField(primary_key=True, max_length=50)
    description = models.CharField(max_length=100)
    command = models.CharField(max_length=100, blank=True, null=True)
    template = models.TextField()
    category = models.CharField(max_length=50, blank=True, null=True)

    def clean(self):
        from embeds.services import EmbedService
        try:
            msg_data = EmbedService.render_example(self)
        except Exception as e:
            raise ValidationError(e)

        try:
            json.loads(msg_data)
        except json.JSONDecodeError:
            raise ValidationError(_('JSON error'))

    def __str__(self):
        return self.name


class EmbedButton(AttachedButton):
    item = models.ForeignKey(Embed, models.CASCADE)


class Variable(models.Model, IVariable):
    name = models.CharField(max_length=100, primary_key=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    example = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return f'{self.name} - {self.description}'


class EmbedVariable(models.Model, IEmbedVariable):
    variable = models.ForeignKey(Variable, models.CASCADE, related_name='+')
    embed = models.ForeignKey(Embed, models.CASCADE, 'vars')
    optional = models.BooleanField(default=True)
    example = models.CharField(max_length=100, blank=True, null=True)
    order = models.PositiveSmallIntegerField(default=1)

    class Meta:
        unique_together = ['embed_id', 'variable_id']

    def clean(self):
        max_variable = EmbedVariable.objects.filter(embed_id=self.embed_id).exclude(id=self.id).order_by(
            '-order'
        ).only('order').first()
        if max_variable is None or max_variable.order <= self.order:
            self.order = max(self.order or 1, (getattr(max_variable, 'order', 0) + 1))


class UserMessage(CreatedDateMixin, IUserMessage):
    user = models.ForeignKey('user.User', models.CASCADE, related_name='+')
    recipient = models.ForeignKey('user.User', models.CASCADE, related_name='+')
    moderator = models.ForeignKey('user.Admin', models.CASCADE, related_name='+', blank=True, null=True)
    text = models.TextField()
    status = EnumField(UserMessageStatus, default=UserMessageStatus.CREATED)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)
        from embeds.services import UserMessageService

        if str(self.status) == str(UserMessageStatus.APPROVED):
            UserMessageService.approve_message(self)
        elif str(self.status) == str(UserMessageStatus.REJECTED):
            UserMessageService.reject_message(self)


class EmbedData(models.Model, IEmbedData):
    name = models.CharField(max_length=100, unique=True)
    category = models.CharField(max_length=50, blank=True, null=True)
    content = models.TextField(max_length=2000, blank=True, null=True)
    title = models.CharField(max_length=256, blank=True)
    description = models.TextField(max_length=4096, blank=True)
    image_url = models.URLField(blank=True)
    color = ColorField()
    thumbnail_url = models.URLField(blank=True)
    footer_text = models.TextField(max_length=2048, blank=True)
    footer_icon_url = models.URLField(blank=True)


class EmbedField(models.Model, IEmbedField):
    embed_data = models.ForeignKey(EmbedData, models.CASCADE, 'fields')
    title = models.CharField(max_length=256)
    text = models.TextField(max_length=1024)
    inline = models.BooleanField(default=False)
    index = models.PositiveIntegerField()

    class Meta:
        unique_together = ['embed_data', 'index']
