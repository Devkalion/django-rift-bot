from django.core.validators import MaxValueValidator
from django.db import models

from common.models import EnumField
from embeds.enums import ModalFieldType


class DiscordModal(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    success_embed = models.ForeignKey('embeds.Embed', on_delete=models.PROTECT, related_name='+')
    error_embed = models.ForeignKey('embeds.Embed', on_delete=models.PROTECT, related_name='+')
    enabled = models.BooleanField(default=True)
    one_off = models.BooleanField(default=False)

    class Meta:
        db_table = 'modals'

    def __str__(self):
        return self.name


class ModalStatus(models.Model):
    modal = models.ForeignKey(DiscordModal, on_delete=models.CASCADE, related_name='+')
    name = models.CharField(max_length=50)
    embed = models.ForeignKey('embeds.Embed', on_delete=models.PROTECT, related_name='+')
    reward_dict = models.ForeignKey(
        'reward.RewardList', on_delete=models.SET_NULL, related_name='+', null=True, blank=True
    )

    class Meta:
        db_table = 'modal_statuses'

    def __str__(self):
        return self.name


class ModalFields(models.Model):
    modal = models.ForeignKey(DiscordModal, on_delete=models.CASCADE, related_name='+')
    name = models.CharField(max_length=45)
    index = models.PositiveSmallIntegerField(default=1)
    type = EnumField(ModalFieldType, default=ModalFieldType.SINGLE_LINE)
    placeholder = models.CharField(max_length=200, blank=True, null=True)
    required = models.BooleanField(default=True)
    max_length = models.PositiveIntegerField(blank=True, null=True, validators=[MaxValueValidator(4000)])

    class Meta:
        db_table = 'modal_fields'

    def __str__(self):
        return self.name


class UserFeedback(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('user.User', on_delete=models.CASCADE, related_name='+')
    modal = models.ForeignKey(DiscordModal, on_delete=models.CASCADE, related_name='+')
    status = models.ForeignKey(
        ModalStatus, on_delete=models.CASCADE, related_name='+', null=True,
        limit_choices_to=models.Q(modal_id=models.F('modal_id')),
    )

    class Meta:
        db_table = 'modals_feedbacks'


class UserFeedbackFields(models.Model):
    feedback = models.ForeignKey(UserFeedback, on_delete=models.CASCADE, related_name='+')
    field = models.ForeignKey(ModalFields, on_delete=models.CASCADE, related_name='+')
    text = models.TextField()

    class Meta:
        db_table = 'modals_feedbacks_fields'
