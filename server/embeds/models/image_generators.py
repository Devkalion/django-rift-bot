from django.db import models


class ImageBase(models.Model):
    name = models.CharField(max_length=100)
    codename = models.SlugField(max_length=100, primary_key=True)
    example_data = models.TextField(blank=True, default='')

    def __str__(self):
        return f'{self.name} ({self.codename})'

    class Meta:
        abstract = True


class ImageTemplate(ImageBase):
    html = models.TextField(blank=False, default='')


class ImageGenerator(ImageBase):
    background_image = models.ImageField(upload_to='templates_background')
    css = models.TextField(blank=True, default='')


class ImageGeneratorField(models.Model):
    name = models.CharField(max_length=100)
    top = models.IntegerField(default=0)
    left = models.IntegerField(default=0)
    zindex = models.IntegerField(default=1)
    html = models.TextField(blank=False, default='')

    generator = models.ForeignKey(ImageGenerator, on_delete=models.CASCADE, related_name='fields')

    def __str__(self):
        return f'{self.name}'


class ImageGeneratorImage(models.Model):
    name = models.CharField(max_length=100)
    top = models.IntegerField(default=0)
    left = models.IntegerField(default=0)
    zindex = models.IntegerField(default=1)
    image = models.ImageField(upload_to='generator_images')

    generator = models.ForeignKey(ImageGenerator, on_delete=models.CASCADE, related_name='images')

    def __str__(self):
        return f'{self.name}'


class ImageHtmlTemplate(models.Model):
    name = models.CharField(max_length=100)
    codename = models.SlugField(max_length=100, primary_key=True)
    example_data = models.TextField(blank=True, default='')
    html = models.TextField(blank=False, default='')

    def __str__(self):
        return f'{self.name} ({self.codename})'


class FileUploads(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField(upload_to='uploads', blank=False)
    uploaded_at = models.DateTimeField(auto_now_add=True)
