from django.db import models

from common.models import EnumField, ColorField
from .buttons import AttachedButton
from ..enums import PostStatus
from ..interfaces import IPost, IPostField


class Post(models.Model, IPost):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50, blank=True, null=True)
    author = models.ForeignKey('user.Admin', models.CASCADE)
    status = EnumField(PostStatus, default=PostStatus.DRAFT)

    channel_id = models.CharField(max_length=50)
    preposting_embed = models.ForeignKey('embeds.Embed', models.SET_NULL, blank=True, null=True)

    content = models.TextField(max_length=2000, blank=True, null=True)

    title = models.CharField(max_length=256, blank=True)
    description = models.TextField(max_length=4096, blank=True)
    image_url = models.URLField(blank=True)
    color = ColorField()
    thumbnail_url = models.URLField(blank=True)
    footer_text = models.TextField(max_length=2048, blank=True)
    footer_icon_url = models.URLField(blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        from ..tasks import publish_post, test_post_publish
        if str(self.status) == PostStatus.CONFIRMED.value:
            publish_post.apply_async(args=[self.pk], countdown=5)
        elif str(self.status) == PostStatus.TEST.value:
            test_post_publish.apply_async(args=[self.pk], countdown=5)

    class Meta:
        permissions = [
            ("can_approve_post", "Can approve post"),
        ]


class PostField(models.Model, IPostField):
    post = models.ForeignKey(Post, models.CASCADE, 'fields')
    title = models.CharField(max_length=256)
    text = models.TextField(max_length=1024)
    inline = models.BooleanField(default=False)
    index = models.PositiveIntegerField()

    class Meta:
        unique_together = ['post', 'index']


class PostButton(AttachedButton):
    item = models.ForeignKey(Post, models.CASCADE)
