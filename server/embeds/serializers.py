from rest_framework import serializers

from user.serializers import RoleIdsSerializer
from .models import SimpleCommand, Command
from .models.embeds import UserMessage


class EmbedDataSerializer(serializers.Serializer):
    user_data = serializers.DictField(required=True)


class CommandSerializer(serializers.ModelSerializer):
    cooldown = serializers.IntegerField(source='cooldown_seconds')
    roles = RoleIdsSerializer(read_only=True)

    class Meta:
        model = Command
        fields = ('handler', 'prefix', 'cooldown', 'disabled', 'roles')


class SimpleCommandSerializer(serializers.ModelSerializer):
    class Meta:
        model = SimpleCommand
        fields = ('input_channel_ids', 'result_channel_id', 'embed')


class UserMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMessage
        fields = ('id', 'user', 'recipient', 'text')
