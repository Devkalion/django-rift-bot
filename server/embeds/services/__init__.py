from .django_implementations import (
    DjangoCommandsQuery,
    DjangoComponentsQuery,
    DjangoEmbedFieldsQuery,
    DjangoEmbedQuery,
    DjangoPostFieldsQuery,
    DjangoPostQuery,
    DjangoRenderer,
)
from .fabrics import (
    CommandServiceFabric,
    ComponentServiceFabric,
    EmbedDataServiceFabric,
    EmbedServiceFabric,
    PostServiceFabric,
    UserMessageServiceFabric,
)

renderer = DjangoRenderer()

ComponentService = ComponentServiceFabric(
    renderer=renderer,
    dbo=DjangoComponentsQuery(),
)

EmbedService = EmbedServiceFabric(
    component_service=ComponentService,
    renderer=renderer,
    query=DjangoEmbedQuery(),
)

EmbedDataService = EmbedDataServiceFabric(
    fields_query=DjangoEmbedFieldsQuery(),
)

PostService = PostServiceFabric(
    embed_service=EmbedService,
    fields_query=DjangoPostFieldsQuery(),
    post_query=DjangoPostQuery(),
    component_service=ComponentService,
)

CommandService = CommandServiceFabric(
    dbo=DjangoCommandsQuery(),
)

UserMessageService = UserMessageServiceFabric()
