from abc import ABC
from typing import List

from common.services import BaseObjectQuery
from ..enums import PostStatus
from ..interfaces import (
    IAttachedComponent,
    IButton,
    IButtonArgument,
    ICommandArgument,
    ICommandParameter,
    IEmbed,
    IEmbedData,
    IEmbedField,
    IEmbedVariable,
    IField, IPost,
    IPostField,
    ISlashCommand,
)


class BaseRenderer:  # pragma nocover
    @staticmethod
    def render_template(template: str, data: dict) -> str:
        raise NotImplementedError


class BaseEmbedQuery(BaseObjectQuery[IEmbed], ABC):
    @staticmethod
    def get_embed_variables(embed: IEmbed) -> List[IEmbedVariable]:
        raise NotImplementedError

    def get_embed_components(self, embed: IEmbed) -> list[IAttachedComponent]:
        raise NotImplementedError


class BasePostQuery:  # pragma nocover
    def create_post(self, **kwargs) -> IPost:
        raise NotImplementedError

    def get_post(self, post_id: int) -> IPost:
        raise NotImplementedError

    def update_post_status(self, post_id: int, status: PostStatus):
        raise NotImplementedError

    def get_components(self, post: IPost) -> list[IAttachedComponent]:
        raise NotImplementedError

    def create_post_component(self, post: IPost, component: IAttachedComponent):
        raise NotImplementedError


class BaseEmbedFieldsQuery:  # pragma nocover
    def get_fields(self, embed_data: IEmbedData) -> List[IField]:
        raise NotImplementedError


class BasePostFieldsQuery:  # pragma nocover
    def get_fields(self, post: IPost) -> List[IField]:
        raise NotImplementedError

    def create_field(self, post: IPost, title: str, text: str, index: int, inline: bool = False) -> IPostField:
        raise NotImplementedError


class BaseCommandsQuery:
    def get_command_arguments(self, command: ISlashCommand) -> list[ICommandArgument]:
        raise NotImplementedError

    def get_enabled_commands(self) -> list[ISlashCommand]:
        raise NotImplementedError

    def get_command_parameters(self, command: ISlashCommand) -> list[ICommandParameter]:
        raise NotImplementedError


class BaseComponentsQuery:
    def get_button_arguments(self, button: IButton) -> list[IButtonArgument]:
        raise NotImplementedError
