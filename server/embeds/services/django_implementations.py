import re
from typing import List

from django.template import Template, Context

from common.services.django_implementations import DjangoObjectQuery
from .base import (
    BaseCommandsQuery,
    BaseComponentsQuery,
    BaseEmbedFieldsQuery, BaseEmbedQuery,
    BasePostFieldsQuery,
    BasePostQuery,
    BaseRenderer,
)
from ..enums import PostStatus, ComponentType
from ..interfaces import (
    IButton,
    IButtonArgument,
    ICommandArgument,
    ICommandParameter,
    IEmbed,
    IAttachedComponent,
    IEmbedField, IPost,
    ISlashCommand,
)
from ..models import (
    ButtonArgument,
    CommandArgument,
    CommandParameter,
    Embed,
    EmbedButton,
    EmbedVariable,
    Post,
    PostButton,
    PostField,
    SlashCommand,
)
from ..models.embeds import EmbedData, EmbedField


class DjangoRenderer(BaseRenderer):
    @staticmethod
    def render_template(template: str, data: dict) -> str:
        template = Template(template)
        context = Context(data, autoescape=False)
        text = template.render(context)
        return re.sub(
            r' +', ' ',
            re.sub(r'[\r\n\t]', '', text)
        )


class DjangoEmbedQuery(DjangoObjectQuery, BaseEmbedQuery):
    model = Embed

    def get_embed_components(self, embed: IEmbed) -> List[IAttachedComponent]:
        buttons = [
            attached_button
            for attached_button in EmbedButton.objects.filter(item_id=embed).order_by('index')
        ]
        return buttons

    @staticmethod
    def get_embed_variables(embed: Embed) -> List[EmbedVariable]:
        return list(EmbedVariable.objects.filter(embed_id=embed.pk).order_by(
            'order'
        ).select_related('variable'))


class DjangoPostQuery(BasePostQuery):
    def create_post(self, **kwargs) -> IPost:
        return Post.objects.create(**kwargs)

    def update_post_status(self, post_id: int, status: PostStatus):
        Post.objects.filter(pk=post_id).update(status=status)

    def get_post(self, post_id) -> Post:
        return Post.objects.get(pk=post_id)

    def get_components(self, post: IPost) -> List[IAttachedComponent]:
        buttons = [
            attached_button
            for attached_button in PostButton.objects.filter(item_id=post).order_by('index')
        ]
        return buttons

    def create_post_component(self, post: IPost, component: IAttachedComponent):
        if str(component.component.type) == str(ComponentType.BUTTON):
            return PostButton.objects.create(
                item_id=post.pk,
                button=component.component,
                disabled=component.disabled,
                index=component.index
            )
        raise NotImplementedError


class DjangoEmbedFieldsQuery(BaseEmbedFieldsQuery):
    def get_fields(self, embed_data: EmbedData) -> List[EmbedField]:
        return list(
            EmbedField.objects.filter(embed_data_id=embed_data.id).order_by('index').only(
                'title', 'text', 'inline'
            )
        )


class DjangoPostFieldsQuery(BasePostFieldsQuery):
    def create_field(self, post: Post, title: str, text: str, index: int, inline: bool = False) -> PostField:
        return PostField.objects.create(
            post=post,
            title=title,
            text=text,
            inline=inline,
            index=index
        )

    def get_fields(self, post: Post) -> List[PostField]:
        return list(
            PostField.objects.filter(post_id=post.id).order_by('index').only(
                'title', 'text', 'inline'
            )
        )


class DjangoCommandsQuery(BaseCommandsQuery):
    def get_command_arguments(self, command: ISlashCommand) -> list[ICommandArgument]:
        return CommandArgument.objects.filter(command=command)

    def get_enabled_commands(self) -> list[ISlashCommand]:
        return SlashCommand.objects.filter(enabled=True)

    def get_command_parameters(self, command: ISlashCommand) -> list[ICommandParameter]:
        return CommandParameter.objects.filter(command=command)


class DjangoComponentsQuery(BaseComponentsQuery):
    def get_button_arguments(self, button: IButton) -> list[IButtonArgument]:
        return list(ButtonArgument.objects.filter(button_id=button.pk))
