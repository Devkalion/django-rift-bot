import json
from datetime import datetime
from random import randint
from typing import Iterable, Optional

from redis import Redis

from common.services import BaseObjectFabric
from events.enums import EventType
from events.services import EventService
from settings import REDIS_CONNECTION_SETTINGS
from user.interfaces import IAdmin
from .base import (
    BaseCommandsQuery,
    BaseComponentsQuery,
    BaseEmbedFieldsQuery,
    BaseEmbedQuery,
    BasePostFieldsQuery,
    BasePostQuery,
    BaseRenderer,
)
from ..enums import ComponentType, PostStatus
from ..interfaces import (
    IButton,
    IButtonArgument,
    IComponent,
    IEmbed,
    IEmbedData,
    IField,
    IPost,
    ISlashCommand,
    IUserMessage,
)


class EmbedServiceFabric(BaseObjectFabric[IEmbed]):
    query: BaseEmbedQuery

    def __init__(self, component_service: 'ComponentServiceFabric', renderer: BaseRenderer, query: BaseEmbedQuery):
        super().__init__(query=query)
        self.componentService = component_service
        self.renderer = renderer

    def normalize_data(self, embed: IEmbed, data: dict):
        variables = self.query.get_embed_variables(embed)
        for ev in variables:
            data[ev.variable.name] = ev.get_value(**data)
        return data

    def render_embed(self, embed: IEmbed, data: dict) -> str:
        data = self.normalize_data(embed, data)
        text = self.renderer.render_template(embed.template, data)
        result = json.loads(text)
        if 'components' not in result:
            components = self.query.get_embed_components(embed)
            if components:
                result["components"] = [{
                    "type": int(ComponentType.ACTION_ROW.value),
                    "components": [
                        self.componentService.render_component(c.component, data, c.disabled)
                        for c in components
                    ],
                }]
        return json.dumps(result, ensure_ascii=False)

    def get_example_data(self, embed: IEmbed):
        example_data = {}
        variables = self.query.get_embed_variables(embed)
        for ev in variables:
            example_data[ev.variable.name] = ev.get_example(**example_data)
        return example_data

    def render_example(self, embed: IEmbed):
        data = self.get_example_data(embed)
        return self.render_embed(embed, data)


class EmbedDataServiceFabric:
    def __init__(self, fields_query: BaseEmbedFieldsQuery) -> None:
        self.fields_getter = fields_query

    def to_dict(self, embed_data: IEmbedData) -> dict:
        result = self.serialize_body(embed_data)
        fields = self.fields_getter.get_fields(embed_data)
        if fields:
            result['embed']['fields'] = self.serialize_fields(fields)
        if len(result['embed']) < 2:
            result.pop('embed')
        return result

    @staticmethod
    def serialize_body(embed_data: IEmbedData) -> dict:
        embed = {'color': embed_data.color}
        result = {
            'content': embed_data.content if embed_data.content else None,
            'embed': embed,
        }
        if embed_data.title:
            embed['title'] = embed_data.title
        if embed_data.description:
            embed['description'] = embed_data.description
        if embed_data.footer_text:
            embed['footer'] = {
                'text': embed_data.footer_text
            }
            if embed_data.footer_icon_url:
                embed['footer']['icon_url'] = embed_data.footer_icon_url
        if embed_data.image_url:
            embed['image'] = {
                'url': embed_data.image_url
            }
        if embed_data.thumbnail_url:
            embed['thumbnail'] = {
                'url': embed_data.thumbnail_url
            }

        return result

    @staticmethod
    def serialize_fields(fields: Iterable[IField]) -> list[dict]:
        return [
            {
                'name': field.title,
                'value': field.text,
                'inline': field.inline
            }
            for field in fields
        ]


class PostServiceFabric:
    def __init__(
        self,
        embed_service: EmbedServiceFabric,
        fields_query: BasePostFieldsQuery,
        post_query: BasePostQuery,
        component_service: 'ComponentServiceFabric',
    ) -> None:
        self.fields_getter = fields_query
        self.embed_service = embed_service
        self.post_query = post_query
        self.component_service = component_service

    def to_dict(self, post: IPost) -> dict:
        result = EmbedDataServiceFabric.serialize_body(post)
        embed = result['embed']
        fields = self.fields_getter.get_fields(post)
        if fields:
            embed['fields'] = EmbedDataServiceFabric.serialize_fields(fields)

        components = self.post_query.get_components(post)
        if components:
            result["components"] = [{
                "type": int(ComponentType.ACTION_ROW.value),
                "components": [
                    self.component_service.render_component(c.component, {}, c.disabled)
                    for c in components
                ],
            }]

        if len(embed) < 2:
            result.pop('embed')
        return result

    def publish(self, post_id: int, channel_id=None):
        post = self.post_query.get_post(post_id)
        if channel_id is None:
            channel_id = post.channel_id
        data = {
            'channel_id': int(channel_id),
            'messages': []
        }
        if post.preposting_embed:
            data['messages'].append(json.loads(
                self.embed_service.render_embed(post.preposting_embed, {}))
            )
        data['messages'].append(self.to_dict(post))
        EventService.create_event(
            EventType.PUBLISH,
            json.dumps(data, ensure_ascii=False)
        )

    def duplicate(self, post: IPost, author: IAdmin) -> IPost:
        new_post = self.post_query.create_post(
            name=post.name,
            category=post.category,
            author=author,
            channel_id=post.channel_id,
            preposting_embed=post.preposting_embed,
            content=post.content,
            title=post.title,
            description=post.description,
            image_url=post.image_url,
            color=post.color,
            thumbnail_url=post.thumbnail_url,
            footer_text=post.footer_text,
            footer_icon_url=post.footer_icon_url,
        )

        for field in self.fields_getter.get_fields(post):
            self.fields_getter.create_field(
                post=new_post,
                title=field.title,
                text=field.text,
                index=field.index,
                inline=field.inline,
            )

        components = self.post_query.get_components(post)
        for component in components:
            self.post_query.create_post_component(post=new_post, component=component)

        return new_post

    def update_post_status(self, post_id: int, status: PostStatus):
        return self.post_query.update_post_status(post_id, status)


class CommandServiceFabric:
    def __init__(self, dbo: BaseCommandsQuery):
        self.dbo = dbo

    def serialize_command_arguments(self, command: ISlashCommand) -> dict:
        arguments = self.dbo.get_command_arguments(command)
        return {
            arg.code_name: {"name": arg.name, "description": arg.description}
            for arg in arguments
        }

    def serialize_command(self, command: ISlashCommand) -> dict:
        return {
            "type": str(command.handler),
            "name": command.name.split(' ')[-1],
            "description": command.description,
            "parameters": self.serialize_command_arguments(command),
            "extras": self.get_command_extras(command),
        }

    def get_command_extras(self, command: ISlashCommand) -> dict:
        return {
            'ephemeral': command.ephemeral,
            'embed_name': command.embed_id,
            'error_embed_name': command.error_embed_id,
            'cooldown': int(command.cooldown.total_seconds()) if command.cooldown else None,
            'parameters': {
                parameter.name: parameter.value
                for parameter in self.dbo.get_command_parameters(command)
            },
        }

    @staticmethod
    def serialize_group(group_name: str, inner_commands: list) -> dict:
        return {
            "type": "group",
            "name": group_name,
            "description": group_name,
            "commands": inner_commands,
        }

    def serialize_all_commands(self) -> list[dict]:
        commands = self.dbo.get_enabled_commands()
        result = []
        groups = {}
        for command in commands:
            serialized_command = self.serialize_command(command)
            if serialized_command["name"] == command.name:
                result.append(serialized_command)
            else:
                group_name = ' '.join(command.name.split(' ')[:-1])
                if group_name not in groups:
                    groups[group_name] = []
                groups[group_name].append(serialized_command)

        while len(groups):
            parent_groups = {
                group_name.split(' ')[:-1]
                for group_name in groups
                if ' ' in group_name
            }

            for group_name in list(groups.keys()):
                if group_name in parent_groups:
                    continue
                commands = groups.pop(group_name)
                serialized_group = self.serialize_group(group_name, commands)
                if ' ' in group_name:
                    parent_group_name = ' '.join(group_name.split(' ')[:-1])
                    if parent_group_name not in groups:
                        groups[parent_group_name] = []
                    groups[parent_group_name].append(serialized_group)
                else:
                    result.append(serialized_group)

        return result


class ComponentServiceFabric:
    def __init__(self, renderer: BaseRenderer, dbo: BaseComponentsQuery) -> None:
        self.renderer = renderer
        self.dbo = dbo

    def render_component(self, component: IComponent, data: dict, disabled: bool) -> dict:
        if isinstance(component, IButton):
            return self.render_button(component, data, disabled)
        raise NotImplementedError

    def render_button_argument(self, button_argument: IButtonArgument, data: dict) -> str:
        value = self.renderer.render_template(button_argument.value, data)
        return f'{button_argument.key}:{value}'

    def render_button_custom_id(self, button: IButton, data: dict) -> Optional[str]:
        if not button.action_type or button.url:
            return None

        button_arguments = self.dbo.get_button_arguments(button)
        result = [
            self.render_button_argument(argument, data)
            for argument in button_arguments
        ]

        custom_id = "~".join((
            str(button.action_type),
            f'response_type:{button.response_type}',
            *result
        ))
        if len(custom_id) < 100:
            return custom_id

        redis_key = f'btn-{int(datetime.now().timestamp() * 1_000_000)}{randint(1000, 10_000 - 1)}'
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            redis.set(redis_key, "~".join(result), ex=24 * 60 * 60)
        return f"{button.action_type}~response_type:{button.response_type}~key:{redis_key}"

    def render_button(self, button: IButton, data: dict, disabled: bool) -> dict:
        return {
            "type": int(str(button.type)),
            "style": int(str(button.style)),
            "label": button.label,
            "disabled": disabled,
            "custom_id": self.render_button_custom_id(button, data),
            "url": button.url,
            "emoji": button.emoji,
        }


class UserMessageServiceFabric:
    @staticmethod
    def approve_message(message: IUserMessage) -> None:
        EventService.create_event(
            EventType.APPROVE_MESSAGE,
            data=json.dumps({
                "user_message_id": message.pk,
                "user_id": message.user_id,
                "recipient_id": message.recipient_id
            })
        )

    @staticmethod
    def reject_message(message: IUserMessage) -> None:
        from . import EmbedService

        embed = EmbedService.get_object(name='user_message_reject')
        EventService.create_event(
            EventType.PUBLISH,
            data=json.dumps({
                'messages': [
                    json.loads(EmbedService.render_embed(embed, {
                        "user_message_id": message.pk
                    }))
                ]
            }, ensure_ascii=False),
            user_id=message.user_id
        )
