from live_settings.models import Setting
from schedule.celery import app
from .enums import PostStatus
from .services import PostService


@app.task
def publish_post(pk):
    PostService.publish(pk)
    PostService.update_post_status(pk, PostStatus.POSTED)


@app.task
def test_post_publish(pk):
    channel_id = Setting.get_value('POST_TEST_CHANNEL_ID')
    PostService.publish(pk, channel_id)
