import json
from datetime import datetime, timedelta, timezone
from typing import Type
from unittest import TestCase, mock

from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.urls import reverse
from rest_framework.response import Response
from rest_framework.test import APIClient

from events.models import Event
from giveaways.models import Draw, DrawReward, Encouragement, EncouragementTemplate, Generator, Giveaway
from inventory.models import Container, Emotion, PersonalRole, Role, TempRole, Title, Background
from moder_actions.models import Appeal, ModerAction
from reward.models import Reward, RewardList
from schedule.celery import CustomTask
from transformations.models import GiftItem, JourneyItem, Shop, TransformItem
from user.models import Admin, User
from user.services import UserService
from . import examples
from .enums import Handler, ButtonActionType
from .models import EmbedVariable, Variable, Embed, Command, Post, PostField, SimpleCommand, PostButton, Button, \
    SlashCommand
from .services import EmbedService, PostService

api_client = APIClient()


class VariablesTestCase(TestCase):
    @classmethod
    def create_entities(cls):
        now = datetime.now(timezone.utc)
        user: User = cls.get_or_create_entity(User, discord_id=1, discord_registration_date=now, registration_date=now)
        reward_list: RewardList = cls.get_or_create_entity(RewardList)
        reward: Reward = cls.get_or_create_entity(Reward)
        role: Role = cls.get_or_create_entity(Role, discord_id=1, is_tech=False)
        draw: Draw = cls.get_or_create_entity(Draw, duration=timedelta(hours=5))
        admin: Admin = cls.get_or_create_entity(Admin)
        encouragement_template: EncouragementTemplate = cls.get_or_create_entity(EncouragementTemplate, reward=reward)

        if not user.roles.filter(is_tech=False).exists():
            UserService.add_roles(user, role)
            cls.addClassCleanup(user.roles.filter(pk=role.pk).delete)

        cls.get_or_create_entity(TempRole, to_date=now + timedelta(hours=5), role=role, user=user)
        cls.get_or_create_entity(Container, index_number=1)
        cls.get_or_create_entity(Emotion)
        cls.get_or_create_entity(Background)
        cls.get_or_create_entity(GiftItem, reward=reward_list)
        cls.get_or_create_entity(Shop)
        cls.get_or_create_entity(Title)
        cls.get_or_create_entity(TransformItem, reward=reward_list)
        cls.get_or_create_entity(JourneyItem, reward=reward)
        cls.get_or_create_entity(ModerAction, moderator=user, user=user)
        cls.get_or_create_entity(Giveaway, reward=reward)
        cls.get_or_create_entity(DrawReward, draw=draw)
        cls.get_or_create_entity(Encouragement, author=admin, template=encouragement_template)
        cls.get_or_create_entity(Generator, reward_list=reward_list, launch_time=now)
        cls.get_or_create_entity(Appeal, author=user, user=user)
        cls.get_or_create_entity(PersonalRole, start_date=now, end_date=now, color=0, role=role, user=user)

    @classmethod
    def setUpClass(cls):
        CustomTask.apply_async = mock.Mock()

        with transaction.atomic():
            cls.create_entities()

        cls.data = {
            "user_id": examples.user_id(),
            "sender_id": examples.sender_id(),
            "recipient_id": examples.recipient_id(),
            "admin_id": examples.admin_id(),
            "container_id": examples.container_id(),
            "role_id": examples.role_id(),
            "title_id": examples.title_id(),
            "background_id": examples.background_id(),
            "emotion_id": examples.emotion_id(),
            "gift_id": examples.gift_id(),
            "shop_id": examples.shop_id(),
            "transformation_id": examples.transformation_id(),
            "journey_id": examples.journey_id(),
            "action_id": examples.action_id(),
            "giveaway_id": examples.giveaway_id(),
            "draw_id": examples.draw_id(),
            "draw_reward_id": examples.draw_reward_id(),
            "encouragement_id": examples.encouragement_id(),
            "generator_id": examples.generator_id(),
            "appeal_id": examples.appeal_id(),
            "personal_role_id": examples.personal_role_id(),
            "missing_inventory": examples.missing_inventory(),
            "reason": examples.reason(),
            "active_role_ids": examples.active_role_ids(),
            "to_date": examples.to_date(),
            "item_id": examples.item_id(),
            "reward_dict": examples.reward_dict(),
            "cooldown": examples.cooldown(),
            "avatar": examples.avatar(),
            "message_url": examples.message_url(),
            "level": examples.level(),
            "container_name": examples.container_name(),
            "count": examples.count(),
            "text": examples.text(),
            "old_text": examples.old_text(),
            "channel_id": examples.channel_id(),
        }

    @classmethod
    def get_or_create_entity(cls, model: Type[models.Model], **kwargs):
        entity = model.objects.first()
        if entity is None:
            entity = model.objects.create(**kwargs)
            cls.addClassCleanup(entity.delete)
        return entity

    def test_generators(self):
        variable: Variable
        # max level of variables ordering
        for _ in range(3):
            for variable in Variable.objects.all():
                value = variable.get_example(**self.data)
                if value is not None:
                    self.data[variable.name] = value

    def test_example_embed(self):
        embed = EmbedService.create_object(template='{"message": "{{ user_id }}"}', name="123", description="123")
        self.addCleanup(embed.delete)

        result = EmbedService.render_example(embed)
        self.assertEquals(result, '{"message": ""}')

        EmbedVariable.objects.create(embed=embed, variable_id="user_id")

        result = EmbedService.render_example(embed)
        self.assertEquals(result, '{"message": "1"}')

    def test_embed_render(self):
        embed = EmbedService.create_object(template='{"message": "user - {{ user_id }}"}', name="123",
                                           description="123")
        self.addCleanup(embed.delete)
        url = reverse("embed-render", kwargs={"pk": embed.pk})

        response: Response = api_client.post(url, data={"user_data": self.data}, format="json")
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["message"], f"user - {self.data['user_id']}")


class CommandTestCase(TestCase):

    def test_validator(self):
        command = Command.objects.create(prefix="some", handler=Handler.APPEAL_COMMAND.value)
        self.addCleanup(command.delete)
        new_command = Command(prefix="some command", handler=Handler.APPEAL_COMMAND.value)
        self.assertRaises(ValidationError, new_command.clean)

    def test_guess_command(self):
        command = Command.objects.create(handler=Handler.APPEAL_COMMAND, prefix="some test prefix")
        self.addCleanup(command.delete)
        url = reverse("commands-detail")

        response: Response = api_client.get(url, data={"command": "some test prefix"})
        self.assertEquals(response.status_code, 200)
        self.assertIn("handler", response.data)

        response: Response = api_client.get(url, data={"command": "some test prefix with extra words"})
        self.assertEquals(response.status_code, 200)

        response: Response = api_client.get(url, data={"command": "some wrong prefix"})
        self.assertEquals(response.status_code, 404)

    def test_simple_command(self):
        command = SimpleCommand.objects.create(command="some test prefix", embed=Embed.objects.order_by("name").first())
        command2 = SimpleCommand.objects.create(
            command="some test prefix long", embed=Embed.objects.order_by("-name").first()
        )
        self.addCleanup(command.delete)
        self.addCleanup(command2.delete)
        url = reverse("commands-detail")

        response: Response = api_client.get(url, data={"command": "some test prefix"})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["embed"], command.embed_id)

        response: Response = api_client.get(url, data={"command": "some test prefix with extra words"})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["embed"], command.embed_id)

        response: Response = api_client.get(url, data={"command": "some wrong prefix"})
        self.assertEquals(response.status_code, 404)


class PostTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        with transaction.atomic():
            cls.admin = Admin.objects.create(username="test admin")
            cls.embed = EmbedService.create_object(
                name="test_post_prepost", template='{"message": "prepost"}'
            )
            cls.button = Button.objects.create(
                label="",
                action_type=ButtonActionType.RENDER_EMBED,
            )
            cls.post = Post.objects.create(
                name="post",
                author=cls.admin,
                channel_id="1234567890",
                preposting_embed=cls.embed,
                content="content",
                title="title",
                description="description",
                image_url="http://google.com/image",
                color=0xFAFAFA,
                thumbnail_url="http://google.com/thumbnail",
                footer_text="footer",
                footer_icon_url="http://google.com/thumbnail",
            )
            PostField.objects.create(
                post=cls.post,
                title="field title",
                text="field text",
                inline=False,
                index=1
            )
            PostButton.objects.create(
                button=cls.button,
                item=cls.post,
            )

    def test_render_post(self):
        result = PostService.to_dict(self.post)
        self.assertDictEqual(result, {
            'content': 'content',
            'components': [{
                'type': 1,
                'components': [{
                    'type': 2,
                    'style': 2,
                    'label': '',
                    'disabled': False,
                    'custom_id': 'render_embed~response_type:edit',
                    'url': None,
                    'emoji': None
                }]
            }],
            'embed': {
                'color': 16448250,
                'description': 'description',
                'footer': {
                    'icon_url': 'http://google.com/thumbnail',
                    'text': 'footer'
                },
                'image': {
                    'url': 'http://google.com/image'
                },
                'thumbnail': {'url': 'http://google.com/thumbnail'},
                'title': 'title',
                'fields': [{
                    'inline': False,
                    'name': 'field title',
                    'value': 'field text'
                }],
            }
        })

    def test_publish_post(self):
        published_events = []

        def add_published_events(*args, **kwargs):
            published_events.append({"args": args, "kwargs": kwargs})

        CustomTask.apply_async = mock.Mock(side_effect=add_published_events)

        PostService.publish(self.post.pk)
        self.assertEquals(len(published_events), 1)
        event_id = published_events[0]["kwargs"]["args"][0]
        event = Event.objects.get(pk=event_id)
        event_data = json.loads(event.data)
        self.assertEquals(len(event_data["messages"]), 2)
        self.assertEquals(event_data["messages"][0], {'message': 'prepost'})

    def test_duplicate(self):
        new_post = PostService.duplicate(self.post, self.admin)
        self.addCleanup(new_post.delete)
        self.maxDiff = 1000

        self.assertDictEqual(PostService.to_dict(self.post), PostService.to_dict(new_post))

    @classmethod
    def tearDownClass(cls) -> None:
        cls.admin.delete()
        cls.embed.delete()
        cls.button.delete()
        cls.post.delete()


class SlashCommandTestCase(TestCase):
    def test_list_slash_commands(self):
        embed = EmbedService.create_object(
            name="test_post_prepost", template='{"message": "prepost"}'
        )
        self.addCleanup(embed.delete)

        url = reverse("slash_commands-list")
        response: Response = api_client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 0)

        disabled_command = SlashCommand.objects.create(name='test_disabled', enabled=False, embed=embed)
        self.addCleanup(disabled_command.delete)
        response: Response = api_client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 0)

        enabled_command = SlashCommand.objects.create(name='test_enabled', enabled=True, embed=embed)
        self.addCleanup(enabled_command.delete)
        response: Response = api_client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 1)
