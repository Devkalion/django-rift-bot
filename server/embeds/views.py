import logging

from django.conf import settings
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.template import Context
from django.template.base import Template
from django.template.loader import get_template
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.viewsets import GenericViewSet, ViewSet

from .models import Command, Embed, ImageGenerator, ImageTemplate, SimpleCommand
from .serializers import CommandSerializer, EmbedDataSerializer, SimpleCommandSerializer, UserMessageSerializer
from .services import CommandService, EmbedService


class EmbedViewSet(GenericViewSet):
    queryset = Embed.objects.only('name', 'template').all()
    serializer_class = EmbedDataSerializer

    @action(detail=True, methods=['post'])
    def render(self, request, pk=None):
        embed: Embed = self.get_object()
        serializer = self.get_serializer(instance=embed, data=request.data)
        serializer.is_valid(raise_exception=True)
        user_data = serializer.validated_data['user_data']
        try:
            embed_data = EmbedService.normalize_data(embed, user_data)
        except Exception as e:
            raise ValidationError(e)
        embed_text = EmbedService.render_embed(embed, embed_data)
        return Response(json.loads(embed_text))


class GuessCommandViewSet(GenericViewSet, mixins.RetrieveModelMixin):
    detail = True

    def get_object(self):
        prefix: str = self.request.query_params.get('command', None)
        first_word = prefix.split(' ')[0]
        variants = Command.objects.filter(prefix__startswith=first_word)
        for command in variants:
            if prefix.startswith(command.prefix):
                return command

        variants = SimpleCommand.objects.filter(command__startswith=first_word)
        options = [
            command
            for command in variants
            if prefix.startswith(command.command)
        ]
        if len(options):
            return sorted(options, key=lambda x: -len(x.command))[0]

        raise Http404

    def get_serializer(self, instance):
        if isinstance(instance, Command):
            serializer_class = CommandSerializer
        else:
            serializer_class = SimpleCommandSerializer
        return serializer_class(instance, context=self.get_serializer_context())


class SlashCommandsViewSet(ViewSet):
    @staticmethod
    def list(request, *args, **kwargs):
        commands = CommandService.serialize_all_commands()
        return Response(commands)


class UserMessageViewSet(GenericViewSet, mixins.CreateModelMixin):
    serializer_class = UserMessageSerializer


def generator_preview(request, pk):
    generator: ImageGenerator = get_object_or_404(ImageGenerator, pk=pk)

    try:
        example_data = json.loads(generator.example_data)
    except json.json.JSONDecodeError as e:
        logging.error('JSON parse error', exc_info=e)
        example_data = {}

    template = get_template('image.html')
    text = template.render({
        'generator': generator,
        'data': example_data,
        'media_full_url': settings.MEDIA_URL,
    })
    return HttpResponse(text)


def template_preview(request, pk):
    generator: ImageTemplate = get_object_or_404(ImageTemplate, pk=pk)

    try:
        example_data = json.loads(generator.example_data)
    except json.json.JSONDecodeError as e:
        logging.error('JSON parse error', exc_info=e)
        example_data = {}

    template = Template(generator.html)
    text = template.render(Context({
        'media_full_url': settings.MEDIA_URL,
        **example_data
    }))
    return HttpResponse(text)
