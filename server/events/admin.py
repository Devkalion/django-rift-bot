from django.contrib import messages
from django.contrib.admin import register

from common.admin import ReadOnlyModelAdmin
from .models import *
from .tasks import give_transformation_reward, give_journey_reward


@register(Event)
class EventAdmin(ReadOnlyModelAdmin):
    list_display = ('user', 'type', 'status', 'data', 'created_date')
    list_filter = ('type', 'status')
    ordering = ('-created_date',)
    readonly_fields = ('created_date',)
    search_fields = ('user__discord_id',)


class UserProcessAdmin(ReadOnlyModelAdmin):
    ordering = ('-created_date',)
    list_display = ('user', 'item', 'ready', 'to_date')
    readonly_fields = ('ready', 'created_date')
    actions = ['finish']
    finish_func = None

    def finish(self, request, queryset):
        queryset = queryset.filter(to_date__lte=now())
        ready_processes = queryset.count()
        instance: UserProcess
        for instance in queryset:
            self.finish_func(instance.id)
        self.message_user(
            request,
            f'{ready_processes} processes were finished',
            messages.SUCCESS
        )

    finish.short_description = 'Give reward if process finished'

    def has_delete_permission(self, request, obj=None):
        return super(ReadOnlyModelAdmin, self).has_delete_permission(request, obj)


@register(ActiveTransformation)
class ActiveTransformationAdmin(UserProcessAdmin):
    finish_func = give_transformation_reward


@register(ActiveJourney)
class ActiveJourneyAdmin(UserProcessAdmin):
    finish_func = give_journey_reward
