from common.enums import ExtendedEnum


class EventType(ExtendedEnum):
    APPEAL_REVIEWED = 'appeal_reviewed'
    CREATE_PERSONAL_ROLE = 'cpr'
    LEVEL_UP = 'lvl_up'
    TRANSFORM_END = 'transform'
    JOURNEY_END = 'journey'
    GRANT_USER_ROLE = 'grant_user_role'
    REVOKE_USER_ROLE = 'revoke_user_role'
    GIVEAWAY = 'giveaway'
    DRAW = 'draw'
    DRAW_FINISH = 'draw_end'
    EXPIRED_TEMP_ROLE = 'etr'
    EXPIRED_PERSONAL_ROLE = 'epr'
    DELETE_PERSONAL_ROLE = 'dpr'
    UPDATE_PERSONAL_ROLE = 'upr'
    UNMUTE = 'unmute'
    UNBAN = 'unban'
    PUBLISH = 'publish'
    APPROVE_MESSAGE = 'approve_message'
    DELETE_PERSONAL_CHANNEL = 'dpc'
    CREATE_PERSONAL_CHANNEL = 'cpc'
    ENCOURAGEMENT_FINISH = 'encouragement_end'


class EventStatus(ExtendedEnum):
    SCHEDULED = 'plan'
    PROCESSING = 'work'
    READY = 'ready'
