from datetime import datetime, timezone
from typing import Optional, Union

from common.interfaces import IModelIntId, ICreatedDateMixin
from transformations import interfaces as transformation_interfaces
from user import interfaces as user_interfaces
from .enums import EventStatus, EventType


class IEvent(IModelIntId, ICreatedDateMixin):
    user: Optional['user_interfaces.IUser']
    user_id: Optional[int]
    type: Union[EventType, str]
    status: Union[EventStatus, str]
    data: Optional[str]


class IUserProcess(IModelIntId, ICreatedDateMixin):
    user: 'user_interfaces.IUser'
    user_id: int = NotImplemented
    to_date: datetime
    item: 'transformation_interfaces.IProcessItem'
    item_id: int

    @property
    def ready(self):
        return datetime.now(timezone.utc) >= self.to_date


class IActiveTransformation(IUserProcess):
    item: 'transformation_interfaces.ITransformItem'


class IActiveJourney(IUserProcess):
    item: 'transformation_interfaces.IJourneyItem'
