from django.db import models
from django.utils.timezone import now

from common.models import EnumField, CreatedDateMixin
from user.models import User
from .enums import EventStatus, EventType
from .interfaces import IEvent, IUserProcess, IActiveTransformation, IActiveJourney


class Event(CreatedDateMixin, IEvent):
    user = models.ForeignKey(User, models.CASCADE, 'events', null=True)
    type = EnumField(enum=EventType)
    status = EnumField(enum=EventStatus, default=EventStatus.SCHEDULED, db_index=True)
    data = models.TextField(blank=True, null=True)


class UserProcess(CreatedDateMixin, IUserProcess):
    user = models.ForeignKey(User, models.CASCADE, '+')
    to_date = models.DateTimeField(default=now)
    item = None

    class Meta:
        abstract = True


class ActiveTransformation(UserProcess, IActiveTransformation):
    item = models.ForeignKey('transformations.TransformItem', models.CASCADE, '+')

    def save(self, *args, **kwargs):
        from .tasks import give_transformation_reward
        super().save(*args, **kwargs)
        give_transformation_reward.apply_async(args=[self.pk], eta=self.to_date)


class ActiveJourney(UserProcess, IActiveJourney):
    item = models.ForeignKey('transformations.JourneyItem', models.CASCADE, '+')

    def save(self, *args, **kwargs):
        from .tasks import give_journey_reward
        super().save(*args, **kwargs)
        give_journey_reward.apply_async(args=[self.pk], eta=self.to_date)
