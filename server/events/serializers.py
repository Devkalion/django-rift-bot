from rest_framework import serializers

from .models import Event, ActiveTransformation


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('user_id', 'type', 'status', 'data', 'id')


class ActiveTransformationSerializer(serializers.ModelSerializer):
    transformation_id = serializers.IntegerField(source='transform_item.index_number')

    class Meta:
        model = ActiveTransformation
        fields = ('id', 'user_id', 'transformation_id')
