from .enums import EventType


class BaseEventCreator:
    def create(self, event_type: EventType, data: str = None, user_id: int = None):
        """
        :return: Event id
        """
        raise NotImplementedError


class DjangoEventCreator(BaseEventCreator):
    def create(self, event_type: EventType, data: str = None, user_id: int = None):
        from .models import Event

        return Event.objects.create(type=event_type, data=data, user_id=user_id).pk


class BaseEventService:
    def __init__(self, event_creator: BaseEventCreator):
        self.event_creator = event_creator

    def create_event(self, event_type: EventType, data: str = None, user_id: int = None, countdown: int = 5):
        from .tasks import publish_event

        event_id = self.event_creator.create(event_type, data, user_id)
        # Give time for db object saved
        publish_event.apply_async(args=[event_id], countdown=countdown)


EventService = BaseEventService(
    DjangoEventCreator()
)
