import json
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction, DatabaseError
from django.utils.timezone import now
from pika.adapters.blocking_connection import BlockingChannel, BlockingConnection

import settings
from schedule.celery import app
from .enums import EventType, EventStatus
from .models import Event, ActiveJourney, UserProcess, ActiveTransformation
from .services import EventService


def _give_process_reward(process: UserProcess, event_type: EventType):
    reward = process.item.give(process.user)
    EventService.create_event(event_type, user_id=process.user_id, data=json.dumps({
        'item_id': process.item.index_number,
        'reward': reward
    }))
    process.delete()


def get_process(pk, model) -> UserProcess:
    return model.objects.select_for_update(
        nowait=True
    ).select_related(
        'item', 'user', 'item__reward'
    ).get(pk=pk)


@app.task
def give_journey_reward(journey_id: int):
    try:
        with transaction.atomic():
            process = get_process(journey_id, ActiveJourney)
            _give_process_reward(process, EventType.JOURNEY_END)
    except ObjectDoesNotExist:
        return
    except DatabaseError:
        give_journey_reward.apply_async(args=[journey_id], countdown=5)


@app.task
def give_transformation_reward(transform_id: int):
    try:
        with transaction.atomic():
            process = get_process(transform_id, ActiveTransformation)
            _give_process_reward(process, EventType.TRANSFORM_END)
    except ObjectDoesNotExist:
        return
    except DatabaseError:
        give_transformation_reward.apply_async(args=[transform_id], countdown=5)


@app.task
def publish_event(event_id: int):
    if not Event.objects.filter(pk=event_id).exists():
        return
    with BlockingConnection(settings.RABBIT_MQ_CONNECTION_PARAMS) as connection:
        channel: BlockingChannel = connection.channel()
        channel.queue_declare(queue='events')
        channel.basic_publish(exchange='', routing_key='events', body=str(event_id).encode())


@app.task
def clear_old_events():
    date = now() - timedelta(days=7)
    Event.objects.filter(created_date__lte=date, status=EventStatus.READY).delete()
