from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from .enums import EventStatus
from .models import Event
from .serializers import EventSerializer


class EventViewSet(mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.exclude(status=EventStatus.READY).only('user_id', 'type', 'status', 'data', 'id')
