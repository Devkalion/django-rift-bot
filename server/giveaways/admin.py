from django.contrib.admin import register, ModelAdmin, StackedInline

from .models import *
from .services import DrawService
from .tasks import generate_giveaway


@register(Giveaway)
class GiveawayAdmin(ModelAdmin):
    list_display = ('name', 'start_datetime', 'end_datetime')
    autocomplete_fields = ('reward',)


@register(Generator)
class GiveawayGeneratorAdmin(ModelAdmin):
    list_display = ('name', 'active', 'last_launch')
    list_filter = ('active',)
    actions = ['generate_giveaway']

    def generate_giveaway(self, request, queryset):
        generator: Generator
        for generator in queryset:
            generate_giveaway(generator.pk)


class DrawRewardInline(StackedInline):
    model = DrawReward
    extra = 0
    readonly_fields = ('winners',)
    autocomplete_fields = ('reward',)
    fieldsets = (
        (None, {
            'fields': ('index', 'winners_count', 'dm_reward_text', 'channel_name', 'name', 'reward')
        }),
        ('Winners', {
            'classes': ['collapse'],
            'fields': ('winners',)
        }),
    )


@register(Draw)
class DrawAdmin(ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'name', 'description', 'category', 'duration', 'emoji', 'channel_id', 'image_url', 'dm_image_url',
                'thumbnail_url', 'reward_description', 'footer_text', 'quotes_title', 'quotes',
            )
        }),
        (None, {
            'fields': ('msg_id', 'last_launch')
        }),
        ('Participants', {
            'classes': ['collapse'],
            'fields': ('participants',)
        }),
    )
    readonly_fields = ('msg_id', 'last_launch', 'participants')
    list_display = ('id', 'name')
    list_filter = ('category',)
    inlines = (DrawRewardInline,)
    # autocomplete_fields = ('reward', )
    actions = ['start']

    def start(self, request, queryset):
        for draw in queryset:
            DrawService.start(draw)


@register(EncouragementTemplate)
class EncouragementTemplateAdmin(ModelAdmin):
    autocomplete_fields = ('reward',)


@register(Encouragement)
class EncouragementAdmin(ModelAdmin):
    readonly_fields = ('status_update', 'author')
    filter_horizontal = ('users', 'roles')
    list_filter = ('status',)
    list_display = ('name', 'template', 'status', 'author')

    actions = ['confirm']

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = list(super().get_readonly_fields(request, obj))
        if obj is None:
            readonly_fields.append('status')
        return readonly_fields

    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name == 'status':
            kwargs['choices'] = (
                (EncouragementStatus.DRAFT.value, 'Черновик'),
                (EncouragementStatus.CONFIRMATION.value, 'Ждет подтверждения'),
            )
            if self.has_approve_encouragement_permission(request, None):
                kwargs['choices'] += ((EncouragementStatus.CONFIRMED.value, 'Подтверждено'),)
        return super().formfield_for_choice_field(db_field, request, **kwargs)

    def save_model(self, request, obj: Encouragement, form, change):
        if obj.id is None:
            obj.author = request.user
        else:
            old_obj = Encouragement.objects.get(id=obj.id)
            if str(old_obj.status) != str(obj.status):
                obj.status_update = now()
        return super().save_model(request, obj, form, change)

    def has_change_permission(self, request, obj: Encouragement = None):
        return all((
            super().has_change_permission(request, obj),
            obj is None or obj.editable,
            obj is None or obj.author == request.user or request.user.is_superuser
        ))

    def confirm(self, request, queryset):
        queryset = queryset.exclude(status__in=(
            EncouragementStatus.CONFIRMED,
            EncouragementStatus.READY,
        ))
        instance: Encouragement
        for instance in queryset:
            instance.status = EncouragementStatus.CONFIRMED
            instance.status_update = now()
            instance.save(update_fields=('status', 'status_update'))

    confirm.short_description = "Подтвердить выбранные выдачи"
    confirm.allowed_permissions = ('approve_encouragement',)

    def has_approve_encouragement_permission(self, request, obj: Encouragement = None):
        return all((
            request.user.has_perm('giveaways.can_approve_encouragement'),
            obj is None or obj.editable
        ))
