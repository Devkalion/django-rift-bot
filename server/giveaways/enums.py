from common.enums import ExtendedEnum


class EncouragementStatus(ExtendedEnum):
    DRAFT = 'draft'
    CONFIRMATION = 'confirmation'
    CONFIRMED = 'confirmed'
    READY = 'ready'
