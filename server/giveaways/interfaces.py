from datetime import timedelta, datetime
from typing import Optional, Union

from common.interfaces import IModelIntId
from reward import interfaces as reward_interfaces
from settings import local_tz
from user import interfaces as user_interfaces
from .enums import EncouragementStatus


class IGiveaway(IModelIntId):
    name: str
    description: Optional[str]
    reward: 'reward_interfaces.IReward'
    reward_id: int
    amount: Optional[int]
    start_datetime: datetime
    end_datetime: datetime
    hidden: bool

    @property
    def active(self):
        return self.start_datetime <= datetime.now(tz=local_tz) < self.end_datetime

    @classmethod
    def active_giveaway(cls) -> Optional['IGiveaway']:
        raise NotImplementedError


class IGenerator(IModelIntId):
    name: str
    active: bool
    reward_list: reward_interfaces.IRewardList
    reward_list_id: int
    description: str
    launch_time: datetime
    giveaway_duration: timedelta
    last_launch: Optional[datetime]


class IDraw(IModelIntId):
    name: str
    description: str
    category: str
    duration: timedelta
    emoji: str
    channel_id: str
    msg_id: Optional[str]
    last_launch: Optional[datetime]
    image_url: Optional[str]
    thumbnail_url: Optional[str]
    reward_description: Optional[str]
    footer_text: Optional[str]
    quotes_title: Optional[str]
    quotes: Optional[str]

    # participants = models.ManyToManyField('user.User', related_name='+')

    @property
    def end_datetime(self):
        return self.last_launch + self.duration


class IDrawReward(IModelIntId):
    draw: IDraw
    draw_id: int
    index: int
    winners_count: int
    dm_reward_text: str
    name: str
    channel_name: str
    reward: Optional['reward_interfaces.IReward']
    reward_id: Optional[int]

    # winners = models.ManyToManyField('user.User', related_name='+')


class IEncouragementTemplate(IModelIntId):
    name: str
    description: str
    dm_msg_text: str
    reward: 'reward_interfaces.IReward'
    reward_id: int
    thumbnail_url: Optional[str]
    image_url: Optional[str]
    channel_description: Optional[str]


class IEncouragement(IModelIntId):
    name: str
    template: IEncouragementTemplate
    template_id: int
    status_update: datetime
    status: Union[EncouragementStatus, str]
    author: 'user_interfaces.IAdmin'
    author_id: int
    # users = models.ManyToManyField('user.User', blank=True)
    # roles = models.ManyToManyField('inventory.Role', blank=True)
    comment: Optional[str]

    @property
    def editable(self):
        return str(self.status) not in (
            EncouragementStatus.CONFIRMED.value,
            EncouragementStatus.READY.value
        )
