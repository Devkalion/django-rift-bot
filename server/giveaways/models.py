from datetime import timedelta, datetime

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.datetime_safe import time
from django.utils.timezone import now, localdate
from pytz import timezone

import settings
from common.models import EnumField
from user.models import User
from .enums import EncouragementStatus
from .interfaces import IGiveaway, IGenerator, IDraw, IDrawReward, IEncouragementTemplate, IEncouragement


def tomorrow():
    return now() + timedelta(days=1)


class Giveaway(models.Model, IGiveaway):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, null=True, blank=True)
    reward = models.ForeignKey('reward.Reward', models.CASCADE)
    amount = models.PositiveIntegerField(null=True, blank=True)
    start_datetime = models.DateTimeField(default=now)
    end_datetime = models.DateTimeField(default=tomorrow)
    hidden = models.BooleanField(default=True)

    def clean(self):
        super().clean()
        giveaways = Giveaway.objects.filter(
            start_datetime__lte=self.end_datetime,
            end_datetime__gte=self.start_datetime
        )
        if self.id:
            giveaways = giveaways.exclude(id=self.id)

        if len(giveaways):
            raise ValidationError(f'This giveaway overlap with {", ".join(map(str, giveaways))}')

    @classmethod
    def nearest(cls):
        return cls.objects.filter(
            end_datetime__gt=now()
        ).exclude(
            hidden=True, start_datetime__gt=now()
        ).order_by('start_datetime')

    @classmethod
    def active_giveaway(cls):
        self = Giveaway.nearest().first()
        if not self or not self.active:
            return None
        return self


class Generator(models.Model, IGenerator):
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=False)
    reward_list = models.ForeignKey('reward.RewardList', models.CASCADE)
    description = models.CharField(max_length=255)
    launch_time = models.TimeField(default=time())
    giveaway_duration = models.DurationField(default=timedelta(hours=23, minutes=59, seconds=59))
    last_launch = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        old_object: Generator = Generator.objects.filter(pk=self.pk).first()
        super().save(*args, **kwargs)
        if old_object is not None and old_object.launch_time != self.launch_time:
            from .tasks import generate_giveaway
            today = localdate()
            generate_giveaway.apply_async(
                args=[self.pk],
                eta=datetime.combine(today, self.launch_time).astimezone(timezone(settings.TIME_ZONE))
            )


class Draw(models.Model, IDraw):
    name = models.CharField(max_length=50)
    description = models.TextField()
    category = models.CharField(max_length=100)
    duration = models.DurationField()
    emoji = models.CharField(max_length=50)
    channel_id = models.CharField(max_length=20)
    msg_id = models.CharField(max_length=20, blank=True, null=True)
    last_launch = models.DateTimeField(null=True, blank=True)
    image_url = models.URLField(blank=True, null=True)
    dm_image_url = models.URLField(blank=True, null=True)
    thumbnail_url = models.URLField(blank=True, null=True)
    reward_description = models.TextField(blank=True, null=True)
    footer_text = models.TextField(blank=True, null=True)
    quotes_title = models.CharField(max_length=128, blank=True, null=True)
    quotes = models.TextField(blank=True, null=True)

    participants = models.ManyToManyField('user.User', related_name='+')

    class Meta:
        db_table = 'draw'
        unique_together = [['channel_id', 'msg_id']]


class DrawReward(models.Model, IDrawReward):
    draw = models.ForeignKey(Draw, models.CASCADE, 'rewards')
    index = models.PositiveSmallIntegerField(default=1)
    winners_count = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1)])
    dm_reward_text = models.TextField()
    name = models.TextField()
    channel_name = models.TextField()
    reward = models.ForeignKey('reward.Reward', models.CASCADE, blank=True, null=True)

    winners = models.ManyToManyField('user.User', related_name='+')

    class Meta:
        db_table = 'draw_reward'


class EncouragementTemplate(models.Model, IEncouragementTemplate):
    name = models.CharField(max_length=50)
    description = models.TextField()
    dm_msg_text = models.TextField()
    reward = models.ForeignKey('reward.Reward', models.CASCADE)
    thumbnail_url = models.URLField(blank=True, null=True)
    image_url = models.URLField(blank=True, null=True)
    channel_description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Encouragement(models.Model, IEncouragement):
    name = models.CharField(max_length=50)
    template = models.ForeignKey(EncouragementTemplate, models.CASCADE)
    status_update = models.DateTimeField(default=now)
    status = EnumField(EncouragementStatus, default=EncouragementStatus.DRAFT)
    author = models.ForeignKey('user.Admin', models.CASCADE)
    users = models.ManyToManyField('user.User', blank=True)
    roles = models.ManyToManyField('inventory.Role', blank=True)
    comment = models.TextField(blank=True, null=True)

    class Meta:
        permissions = [
            ("can_approve_encouragement", "Can approve encouragement"),
        ]

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)
        if str(self.status) == EncouragementStatus.CONFIRMED.value:
            from .tasks import finish_encouragement
            finish_encouragement.apply_async(args=[self.pk])
