from rest_framework.serializers import ModelSerializer, SerializerMethodField

from giveaways.models import Draw, Encouragement


class DrawSerializer(ModelSerializer):
    class Meta:
        model = Draw
        fields = ('channel_id', 'msg_id', 'emoji')


class EncouragementSerializer(ModelSerializer):
    roles = SerializerMethodField()

    class Meta:
        model = Encouragement
        fields = ('id', 'users', 'roles')

    def get_roles(self, encouragement: Encouragement):
        return [
            role.discord_id
            for role in encouragement.roles.all()
        ]
