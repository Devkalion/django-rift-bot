import json
import random
from typing import Iterable, List, Optional

from django.db import transaction
from django.utils.timezone import localtime

from events.enums import EventType
from events.services import EventService
from reward.models import Reward
from reward.utils import ResultReward
from user.interfaces import IUser
from .models import Draw, DrawReward
from .tasks import finish_draw


class BaseDrawServiceHelper:
    @staticmethod
    def get_draw_rewards(draw: Draw) -> Iterable[DrawReward]:
        raise NotImplementedError

    @staticmethod
    def get_winners(draw: Draw, exclude_user_ids: Iterable[int], limit: int) -> List[IUser]:
        raise NotImplementedError

    @staticmethod
    def clear_draw(draw: Draw) -> Draw:
        raise NotImplementedError

    @staticmethod
    def give_reward(draw_reward: DrawReward, user: IUser) -> None:
        raise NotImplementedError

    @staticmethod
    def get_draw(draw_id: int) -> Draw:
        raise NotImplementedError


class DjangoDrawServiceHelper(BaseDrawServiceHelper):
    @staticmethod
    def get_draw_rewards(draw: Draw) -> Iterable[DrawReward]:
        return DrawReward.objects.filter(draw=draw).order_by("index").select_related("reward").only(
            "id", "winners_count", "reward", "winners"
        )

    @staticmethod
    def get_winners(draw: Draw, exclude_user_ids: Iterable[int], limit: int) -> List[IUser]:
        participants = list(draw.participants.exclude(
            discord_id__in=exclude_user_ids
        ).only('discord_id'))
        random.shuffle(participants)
        return participants[:limit]

    @staticmethod
    def clear_draw(draw: Draw):
        draw.participants.clear()
        for dr in DrawReward.objects.filter(draw=draw).only("winners"):
            dr.winners.clear()
        draw.last_launch = localtime()
        draw.save(update_fields=('last_launch',))
        return draw

    @staticmethod
    def give_reward(draw_reward: DrawReward, user: IUser):
        reward: Optional[Reward] = draw_reward.reward
        if reward:
            reward_dict = reward.calc(user)
            result_reward: ResultReward = ResultReward.from_dict(reward_dict)
            with transaction.atomic():
                draw_reward.winners.add(user)
                result_reward.give(user)
        else:
            draw_reward.winners.add(user)

    @staticmethod
    def get_draw(draw_id: int) -> Draw:
        return Draw.objects.only(
            "id", "channel_id", "participants"
        ).get(pk=draw_id)


class DrawServiceFabric:
    def __init__(self, helper: BaseDrawServiceHelper):
        self.helper = helper

    def start(self, draw: Draw):
        draw = self.helper.clear_draw(draw)
        EventService.create_event(
            EventType.DRAW,
            data=json.dumps({
                'draw_id': draw.id,
                'channel_id': draw.channel_id,
                'emoji': draw.emoji,
            })
        )
        finish_draw.apply_async(args=[draw.pk], eta=draw.last_launch + draw.duration)

    def finish(self, draw_id: int):
        draw = self.helper.get_draw(draw_id)
        rewards = {}
        winners = []
        for draw_reward in self.helper.get_draw_rewards(draw):
            participants = self.helper.get_winners(draw, winners, draw_reward.winners_count)
            rewards[draw_reward.id] = []
            for user in participants:
                rewards[draw_reward.id].append(user.discord_id)
                winners.append(user.discord_id)
                self.helper.give_reward(draw_reward, user)

        EventService.create_event(
            EventType.DRAW_FINISH,
            data=json.dumps({
                'draw_id': draw.id,
                'channel_id': draw.channel_id,
                'rewards': rewards,
            })
        )


DrawService = DrawServiceFabric(
    helper=DjangoDrawServiceHelper()
)
