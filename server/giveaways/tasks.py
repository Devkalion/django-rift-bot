import itertools
import json
from datetime import timedelta

from django.conf import settings
from django.db import transaction
from django.utils.timezone import datetime, localdate, localtime, now
from pytz import timezone

from events.enums import EventType
from events.services import EventService
from reward.models import Reward
from reward.utils import ResultReward
from schedule.celery import app
from user.models import User
from .enums import EncouragementStatus
from .models import Giveaway, Generator, Draw, Encouragement


@app.task
def delete_old_giveaway(pk: int):
    Giveaway.objects.filter(pk=pk, end_datetime__lte=now()).delete()


@app.task
def generate_giveaway(pk: int):
    _now = localtime()
    queryset = Generator.objects.select_for_update().filter(
        active=True, pk=pk, last_launch__lt=_now
    )
    with transaction.atomic():
        generator: Generator = queryset.first()
        if Giveaway.active_giveaway():
            return
        if not generator or generator.launch_time > _now.time():
            return
        reward: Reward = generator.reward_list.get_reward()
        reward_dict = reward.calc(User.objects.first())
        start_datetime = datetime.combine(_now.date(), generator.launch_time).astimezone(timezone(settings.TIME_ZONE))
        giveaway = Giveaway.objects.create(
            name=f'auto by {generator.name}',
            reward=reward,
            start_datetime=start_datetime,
            end_datetime=start_datetime + generator.giveaway_duration,
            hidden=False,
        )
        generator.last_launch = _now
        generator.save(update_fields=('last_launch',))
    EventService.create_event(
        EventType.GIVEAWAY,
        data=json.dumps({
            'generator_id': generator.pk,
            'giveaway_id': giveaway.pk,
            'reward_dict': reward_dict
        })
    )
    delete_old_giveaway.apply_async(args=[giveaway.pk], eta=giveaway.end_datetime + timedelta(hours=1))


@app.task
def schedule_generators():
    today = localdate()
    for gen in Generator.objects.filter(active=True).exclude(last_launch__gte=today):
        generate_giveaway.apply_async(
            args=[gen.pk],
            eta=datetime.combine(today, gen.launch_time).astimezone(timezone(settings.TIME_ZONE))
        )


@app.task
def finish_draw(pk):
    from giveaways.services import DrawService
    DrawService.finish(pk)


@app.task
def finish_encouragement(pk):
    instance = Encouragement.objects.select_related('template', 'template__reward').get(pk=pk)
    reward = instance.template.reward
    users = list(set(itertools.chain(instance.users.all(), *[
        role.user_set.all()
        for role in instance.roles.all()
    ])))
    with transaction.atomic():
        for user in users:
            result = reward.calc(user)
            result_reward = ResultReward.from_dict(result)
            result_reward.give(user)
    instance.status = EncouragementStatus.READY
    instance.status_update = now()
    instance.save(update_fields=('status', 'status_update'))
    instance.users.clear()
    for user in users:
        instance.users.add(user)
    EventService.create_event(EventType.ENCOURAGEMENT_FINISH, pk)
