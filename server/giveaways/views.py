from typing import Optional

from django.db import transaction
from django.db.models import F
from django.utils.timezone import now
from rest_framework import status, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound, PermissionDenied, MethodNotAllowed
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from common.serializers import UserSerializer
from reward.utils import ResultReward
from user.models import User
from .models import Giveaway, Draw, Encouragement
from .serializers import DrawSerializer, EncouragementSerializer


class GiveawayViewSet(GenericViewSet):
    def get_object(self):
        giveaway: Optional[Giveaway] = Giveaway.active_giveaway()
        if not giveaway:
            raise NotFound
        return giveaway

    @action(detail=False, methods=['post'], serializer_class=UserSerializer)
    def claim(self, request, **kwargs):
        giveaway: Giveaway = self.get_object()
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        user: User = serializer.validated_data['user']

        if giveaway.blocks.filter(user=user).exists():
            raise PermissionDenied

        result = giveaway.reward.calc(user)
        result_reward: ResultReward = ResultReward.from_dict(result)

        with transaction.atomic():
            giveaway = Giveaway.objects.select_for_update().only('amount').get(pk=giveaway.pk)
            if giveaway.amount == 0:
                raise MethodNotAllowed(None, detail='limit extended')
            if giveaway.amount:
                giveaway.amount = F('amount') - 1
                giveaway.save(update_fields=['amount'])
            result_reward.give(user)
            giveaway.blocks.create(user=user)

        return Response({
            'reward': result,
            'giveaway_id': giveaway.id
        })


class EncouragementViewSet(mixins.RetrieveModelMixin, GenericViewSet):
    queryset = Encouragement.objects
    serializer_class = EncouragementSerializer


class DrawViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  GenericViewSet):
    queryset = Draw.objects.all()
    serializer_class = DrawSerializer

    @action(detail=False, methods=['post'], serializer_class=UserSerializer)
    def participate(self, request, **kwargs):
        user: User = User.objects.only('discord_id').get(discord_id=self.request.data['user_id'])
        draw: Draw = Draw.objects.filter(
            msg_id=self.request.query_params['msg_id'],
            channel_id=self.request.query_params['channel_id'],
        ).only('last_launch', 'duration').first()
        if not draw or draw.last_launch + draw.duration < now():
            raise NotFound
        if not draw.participants.filter(discord_id=user.discord_id).exists():
            draw.participants.add(user)
        return Response(status=status.HTTP_204_NO_CONTENT)
