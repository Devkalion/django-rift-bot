from django.contrib.admin import ModelAdmin, StackedInline, register

from .models import *


class BaseItemAdmin(ModelAdmin):
    list_filter = ('temporary',)
    search_fields = ('name', 'description')
    list_display = ('index_number', 'name', 'description', 'temporary')
    ordering = ('index_number',)


@register(Title)
class TitleAdmin(BaseItemAdmin):
    ordering = ()


@register(Currency)
class CurrencyAdmin(BaseItemAdmin):
    fields = ('index_number', 'name', 'description', 'icon', 'temporary')


@register(Container)
class ContainerAdmin(BaseItemAdmin):
    autocomplete_fields = ('reward_list',)
    fields = (
        'index_number', 'name', 'description', 'icon', 'image', 'reward_list', 'embed_color',
        'form1', 'form2', 'form3', 'form4', 'temporary'
    )


@register(Item)
class ItemAdmin(BaseItemAdmin):
    list_filter = ('temporary', 'category')
    fields = ('index_number', 'name', 'category', 'description', 'icon', 'temporary')


class PersonalChannelMemberInline(StackedInline):
    model = ChannelMembers
    raw_id_fields = ('inviter', 'user')
    readonly_fields = ('created_date', 'inviter', 'blocked_at')
    extra = 0


@register(PersonalChannel)
class PersonalChannelAdmin(ModelAdmin):
    list_display = ('name', 'creator', 'channel_id')
    readonly_fields = ['created_date']
    search_fields = ('name', 'channel_id')
    inlines = (PersonalChannelMemberInline,)
    raw_id_fields = ('creator',)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = list(super().get_readonly_fields(request, obj))
        if obj is not None:
            readonly_fields += ['creator']
        return readonly_fields


class ItemInline(StackedInline):
    model = UserItem
    extra = 0


class CurrencyInline(StackedInline):
    model = UserCurrency
    extra = 0


class TempRoleInline(StackedInline):
    model = TempRole
    extra = 0


class PersonalRoleInline(StackedInline):
    model = PersonalRoleProxy
    extra = 0
    readonly_fields = ('expired',)


class TitleInline(StackedInline):
    model = UserTitle
    extra = 0
    fields = ('title', 'created_date')
    readonly_fields = ('created_date',)


class ContainerInline(StackedInline):
    model = UserContainer
    extra = 0


@register(Role)
class RoleAdmin(ModelAdmin):
    list_filter = ('category', 'is_tech', 'temporary')
    ordering = ('-position',)
    list_display = ('name', 'discord_id', 'category', 'description', 'is_tech')
    readonly_fields = ('discord_id', 'user_count', 'users', 'name', 'position')
    search_fields = ('name', 'description')
    fields = (
        ('name', 'discord_id', 'position',), 'category', 'description', ('temporary', 'is_tech'),
        'user_count', 'users'
    )

    def user_count(self, instance: Role):
        return instance.user_set.count()

    def users(self, instance: Role):
        return '\n'.join((
            str(user)
            for user in instance.user_set.all()
        ))


class BaseCategoryItemAdmin(ModelAdmin):
    list_display = ('name', 'description', 'category', 'user_count', 'temporary')
    list_filter = ('category',)
    search_fields = ('name', 'description')
    readonly_fields = ('user_count', 'users')

    def user_count(self, instance):
        return instance.user_set.count()

    def users(self, instance):
        return '\n'.join((
            str(user)
            for user in instance.user_set.all()
        ))


@register(Background)
class BackgroundAdmin(BaseCategoryItemAdmin):
    pass
    fields = ('name', 'category', 'image_url', 'description', 'temporary', 'user_count', 'users')


@register(Emotion)
class EmotionAdmin(BaseCategoryItemAdmin):
    pass


@register(RoleCategory, BackgroundCategory, EmotionCategory)
class CategoryAdmin(ModelAdmin):
    ordering = ('index_number',)
    search_fields = ('description',)


@register(PersonalRoleProxy)
class PersonalRoleAdmin(ModelAdmin):
    list_display = ('id', 'name', 'user', 'end_date')
    raw_id_fields = ('user',)
