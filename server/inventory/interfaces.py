from datetime import datetime
from typing import Optional

from common.interfaces import IModelIntId, ICreatedDateMixin
from reward import interfaces as reward_interfaces
from settings import local_tz
from user import interfaces as user_interfaces


# Items

class IBaseItem(IModelIntId):
    description: str
    icon: str
    name: str
    temporary: bool


class IBaseIndexedItem(IBaseItem):
    index_number: int


class IItem(IBaseIndexedItem):
    category: Optional[str]


class ICurrency(IBaseIndexedItem):
    pass


class IContainer(IBaseIndexedItem):
    # user = models.ManyToManyField(User, through='UserContainer')
    reward_list: Optional['reward_interfaces.IRewardList']
    reward_list_id: Optional[int]

    embed_color: Optional[int]
    image: Optional[str]

    form1: Optional[str]
    form2: Optional[str]
    form3: Optional[str]
    form4: Optional[str]


class ITitle(IBaseItem):
    name2: str


class IPersonalChannel(IModelIntId, ICreatedDateMixin):
    name: str
    description: str
    creator: 'user_interfaces.IUser'
    creator_id: int
    channel_id: Optional[int]
    end_date: Optional[datetime]


# User - item

class IBaseItemCount(IModelIntId):
    item: IBaseIndexedItem
    item_id: int
    amount: int


class IBaseUserItem(IBaseItemCount):
    user: 'user_interfaces.IUser'
    user_id: int


class IUserItem(IBaseUserItem):
    item: IItem


class IUserCurrency(IBaseUserItem):
    item: ICurrency


class IUserContainer(IBaseUserItem):
    item: IContainer


# Categories

class IBaseCategory(IModelIntId):
    description: str
    alternative_reward: Optional['reward_interfaces.IRewardList']
    alternative_reward_id: Optional[int]
    icon: Optional[str]
    index_number: int


class IRoleCategory(IBaseCategory):
    pass


class IBackgroundCategory(IBaseCategory):
    pass


class IEmotionCategory(IBaseCategory):
    pass


# Category Item

class IBaseCategoryItem(IModelIntId):
    name: str
    description: str
    temporary: bool
    category: Optional[IBaseCategory]
    category_id: Optional[int]


class IRole(IBaseCategoryItem):
    discord_id: int
    category: Optional[IRoleCategory]
    is_tech: bool
    position: Optional[int]
    separator: Optional['IRole']
    separator_id: Optional[int]


class IBackground(IBaseCategoryItem):
    image_url: str
    category: Optional[IBackgroundCategory]


class IEmotion(IBaseCategoryItem):
    image_url: str
    text: str
    category: Optional[IEmotionCategory]


class ITempRole(IModelIntId):
    user: 'user_interfaces.IUser'
    user_id: int
    role: IRole
    role_id: int
    to_date: datetime

    @property
    def delta(self):
        return self.to_date - datetime.now(tz=local_tz)


class IPersonalRole(IModelIntId):
    user: 'user_interfaces.IUser'
    user_id: int
    role: IRole
    role_id: int
    start_date: datetime
    end_date: datetime
    name: str
    color: int

    @property
    def expired(self):
        return self.end_date and self.end_date < datetime.now(tz=local_tz)


class IUserTitle(IModelIntId, ICreatedDateMixin):
    user: 'user_interfaces.IUser'
    user_id: int
    title: ITitle
    title_id: int
