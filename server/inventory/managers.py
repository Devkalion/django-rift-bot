from django.db import models


class ItemFilteredManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=self.model._type)


class UserItemFilteredManager(models.Manager):
    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(item__type=self.model._type)
