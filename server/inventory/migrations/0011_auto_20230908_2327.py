# Generated by Django 3.1.3 on 2023-09-08 20:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0007_auto_20230518_2039'),
        ('inventory', '0010_personalroleproxy'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personalchannel',
            name='creator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='user.user'),
        ),
        migrations.CreateModel(
            name='ChannelMembers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('channel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.personalchannel')),
                ('inviter', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='user.user')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='user.user')),
            ],
            options={
                'db_table': 'personal_channel_members',
                'unique_together': {('channel', 'user')},
            },
        ),
        migrations.AddField(
            model_name='personalchannel',
            name='members',
            field=models.ManyToManyField(related_name='_personalchannel_members_+', through='inventory.ChannelMembers', to='user.User'),
        ),
    ]
