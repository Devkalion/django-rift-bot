import json
from datetime import timedelta

from django.db import models, transaction
from django.db.models import F
from django.utils.translation import gettext_lazy as _

from common.models import ColorField, CreatedDateMixin
from events.enums import EventType
from events.services import EventService
from user.models import User
from .interfaces import (
    IBackground,
    IBackgroundCategory,
    IBaseCategory,
    IBaseCategoryItem,
    IBaseItem,
    IBaseItemCount,
    IBaseUserItem,
    IContainer,
    ICurrency,
    IEmotion,
    IEmotionCategory,
    IItem,
    IPersonalChannel,
    IPersonalRole,
    IRole,
    IRoleCategory,
    ITempRole,
    ITitle,
    IUserContainer,
    IUserCurrency,
    IUserItem,
    IUserTitle,
)


# Items

class BaseItem(models.Model, IBaseItem):
    description = models.CharField(max_length=255, blank=False, null=False)
    icon = models.CharField(max_length=255)
    name = models.CharField(max_length=255, blank=False, null=False)
    temporary = models.BooleanField(default=True)
    index_number = models.IntegerField(unique=True, db_index=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Item(BaseItem, IItem):
    user = models.ManyToManyField(User, through='UserItem')
    category = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _('item')
        verbose_name_plural = _('items')


class Currency(BaseItem, ICurrency):
    user = models.ManyToManyField(User, through='UserCurrency')

    class Meta:
        verbose_name = _('currency')
        verbose_name_plural = _('currency')


class Container(BaseItem, IContainer):
    user = models.ManyToManyField(User, through='UserContainer')
    reward_list = models.ForeignKey('reward.RewardList', models.SET_NULL, blank=True, null=True)

    embed_color = ColorField(null=True, blank=True)
    image = models.URLField(null=True, blank=True)

    form1 = models.CharField(max_length=50, unique=True, blank=True, null=True,
                             help_text=_('open 1 {name}'))
    form2 = models.CharField(max_length=50, unique=True, blank=True, null=True,
                             help_text=_('open 2 {name}'))
    form3 = models.CharField(max_length=50, unique=True, blank=True, null=True,
                             help_text=_('open 5 {name}'))
    form4 = models.CharField(max_length=50, unique=True, blank=True, null=True,
                             help_text=_('open all {name}'))

    class Meta:
        verbose_name = _('container')
        verbose_name_plural = _('containers')

    def open(self, user, count=1):
        from reward.utils import ResultReward

        rewards = self.reward_list.get_rewards(count=count)
        result = None
        for reward, _count in rewards.items():
            result = reward.calc(user, _count, result)

        result_reward = ResultReward.from_dict(result)
        with transaction.atomic():
            user_container = UserContainer.objects.select_for_update().only('amount').get(
                user_id=user.pk,
                item_id=self.pk
            )
            if count > user_container.amount:
                return None
            user_container.amount = F('amount') - count
            user_container.save(update_fields=['amount'])
            result_reward.give(user)
        return result


class Title(BaseItem, ITitle):
    index_number = None
    name2 = models.CharField(max_length=255, blank=False, null=False)


class PersonalChannel(CreatedDateMixin, IPersonalChannel):
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=100)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    channel_id = models.BigIntegerField(null=True, blank=True)
    end_date = models.DateTimeField(blank=True, null=True)
    members = models.ManyToManyField(
        User, through='ChannelMembers', through_fields=('channel', 'user'), related_name='+',
    )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        from inventory.tasks import create_personal_channel, delete_personal_channel
        super().save(force_insert, force_update, using, update_fields)
        if self.channel_id is None:
            create_personal_channel.apply_async(args=[self.pk], countdown=5)
        if self.end_date:
            delete_personal_channel.apply_async(
                args=[self.pk],
                eta=self.end_date + timedelta(minutes=1)  # handle equal end_date and now
            )

    def delete(self, using=None, keep_parents=False):
        if self.channel_id:
            EventService.create_event(
                EventType.DELETE_PERSONAL_CHANNEL,
                user_id=self.creator_id,
                data=str(self.channel_id)
            )
        return super().delete(using, keep_parents)


class ChannelMembers(CreatedDateMixin):
    channel = models.ForeignKey(PersonalChannel, models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    inviter = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='+')
    blocked_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'personal_channel_members'
        unique_together = ('channel', 'user')


# User - item

class BaseItemCount(models.Model, IBaseItemCount):
    item = None
    amount = models.IntegerField(default=0)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.item)


class BaseUserItem(BaseItemCount, IBaseUserItem):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True
        unique_together = ['user', 'item']

    def __str__(self):
        return f'<{self.item.icon}> {self.amount}'


class UserItem(BaseUserItem, IUserItem):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)


class UserCurrency(BaseUserItem, IUserCurrency):
    item = models.ForeignKey(Currency, on_delete=models.CASCADE)


class UserContainer(BaseUserItem, IUserContainer):
    item = models.ForeignKey(Container, on_delete=models.CASCADE)


# Categories

class BaseCategory(models.Model, IBaseCategory):
    description = models.CharField(max_length=100)
    alternative_reward = models.ForeignKey('reward.RewardList', on_delete=models.SET_NULL, null=True, blank=True)
    icon = models.CharField(max_length=255, blank=True, null=True)
    index_number = models.IntegerField(unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.description

    @property
    def items(self):
        raise NotImplementedError


class RoleCategory(BaseCategory, IRoleCategory):
    class Meta:
        verbose_name = _('role category')
        verbose_name_plural = _('roles categories')


class BackgroundCategory(BaseCategory, IBackgroundCategory):
    class Meta:
        verbose_name = _('background category')
        verbose_name_plural = _('background categories')


class EmotionCategory(BaseCategory, IEmotionCategory):
    class Meta:
        verbose_name = _('emotion category')
        verbose_name_plural = _('emotion categories')


# Category Item

class BaseCategoryItem(models.Model, IBaseCategoryItem):
    name = models.CharField(max_length=255, blank=False, null=False)
    description = models.CharField(max_length=255, blank=False, null=False)
    temporary = models.BooleanField(default=True)
    category = None

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Role(BaseCategoryItem, IRole):
    discord_id = models.BigIntegerField(db_index=True, unique=True)
    category = models.ForeignKey(RoleCategory, models.SET_NULL, null=True, blank=True, related_name='roles')
    is_tech = models.BooleanField(default=True)
    position = models.IntegerField(blank=True, null=True)
    separator = models.ForeignKey('Role', on_delete=models.SET_NULL, related_name='separated_roles',
                                  blank=True, null=True)


class Background(BaseCategoryItem, IBackground):
    image_url = models.URLField()
    category = models.ForeignKey(BackgroundCategory, models.SET_NULL, null=True, blank=True, related_name='backgrounds')

    class Meta:
        verbose_name = _('background')
        verbose_name_plural = _('backgrounds')


class Emotion(BaseCategoryItem, IEmotion):
    image_url = models.URLField()
    text = models.TextField(blank=True)
    category = models.ForeignKey(EmotionCategory, models.SET_NULL, null=True, blank=True, related_name='emotions')

    class Meta:
        verbose_name = _('emotion')
        verbose_name_plural = _('emotions')


class TempRole(models.Model, ITempRole):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    to_date = models.DateTimeField()

    class Meta:
        unique_together = ['user', 'role']

    def save(self, *args, **kwargs):
        from .tasks import expire_temp_role
        super().save(*args, **kwargs)
        expire_temp_role.apply_async(args=[self.pk], eta=self.to_date)


class PersonalRole(models.Model, IPersonalRole):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, blank=True, null=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    name = models.CharField(max_length=40)
    color = ColorField()

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ['user', 'role']


class PersonalRoleProxy(PersonalRole):
    class Meta:
        verbose_name = 'Personal Roles'
        verbose_name_plural = 'Personal role'
        proxy = True

    def delete(self, using=None, keep_parents=False):
        role_discord_id = self.role.discord_id if self.role else None
        super().delete(using, keep_parents)
        if role_discord_id:
            EventService.create_event(
                EventType.DELETE_PERSONAL_ROLE,
                role_discord_id,
            )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        from .tasks import expire_personal_role

        created = self.pk is None
        changed = False
        if not created and self.role_id is not None:
            old_role = PersonalRole.objects.get(pk=self.pk)
            changed = old_role.color != self.color or old_role.name != self.name
        super().save(force_insert, force_update, using, update_fields)
        if self.role_id is None:
            EventService.create_event(
                EventType.CREATE_PERSONAL_ROLE,
                json.dumps({
                    'id': self.pk,
                    'color': self.color,
                    'name': self.name,
                }),
                self.user_id,
            )
        elif created:
            EventService.create_event(
                EventType.GRANT_USER_ROLE,
                json.dumps({"role_id": int(self.role.discord_id), "reason": 'personal role through admin'}),
                self.user_id,
            )
        elif changed and self.role:
            EventService.create_event(
                EventType.UPDATE_PERSONAL_ROLE,
                json.dumps({
                    'color': self.color,
                    'name': self.name,
                    'role_id': self.role.discord_id
                }),
                self.user_id
            )
        expire_personal_role.apply_async(
            args=[self.pk],
            eta=self.end_date + timedelta(minutes=1)  # handle equal end_date and now
        )


class UserTitle(CreatedDateMixin, IUserTitle):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.ForeignKey(Title, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'title']
