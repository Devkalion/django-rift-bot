from rest_framework import serializers

from .models import *


class RoleSerializer(serializers.ModelSerializer):
    separator = serializers.IntegerField(source='separator.discord_id', allow_null=True, read_only=True)

    class Meta:
        model = Role
        fields = ('id', 'discord_id', 'name', 'description', 'position', 'separator')


class ContainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Container
        fields = ('id', 'name', 'form1', 'form2', 'form3', 'form4')


class EmotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Emotion
        fields = ('id', 'name', 'text', 'image_url')


class PersonalRoleSerializer(serializers.ModelSerializer):
    discord_id = serializers.CharField(source='role.discord_id', read_only=True)

    class Meta:
        model = PersonalRole
        fields = ('id', 'role', 'discord_id', 'color', 'name')

    def validate_name(self, name):
        role_names = {
            name.lower()
            for name in Role.objects.exclude(pk=self.instance.role_id).values_list('name', flat=True)
        }
        if name.lower() in role_names:
            raise serializers.ValidationError('Duplicated role name')
        return name


class PersonalChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalChannel
        fields = ('id', 'name', 'channel_id')

    def validate_name(self, name):
        if str(name).upper() == str(name):
            raise serializers.ValidationError('Caps name')
        return name


class PersonalChannelMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChannelMembers
        fields = ('id', 'inviter', 'user', 'channel')


class PersonalChannelMemberDeleteSerializer(serializers.Serializer):
    channel_id = serializers.IntegerField()
    user_id = serializers.IntegerField()
