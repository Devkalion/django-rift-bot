import json
from datetime import timedelta

from django.db import transaction
from django.utils.timezone import now

from events.enums import EventType
from events.services import EventService
from schedule.celery import app
from .models import PersonalRoleProxy, TempRole, PersonalRole, PersonalChannel


@app.task
def expire_temp_role(temp_role_pk: int):
    with transaction.atomic():
        temp_role: TempRole = TempRole.objects.select_for_update(nowait=True).select_related(
            'user', 'role'
        ).get(pk=temp_role_pk)
        if temp_role.to_date > now():
            return
        if not temp_role.user.roles.filter(discord_id=temp_role.role.discord_id).exists():
            EventService.create_event(
                EventType.EXPIRED_TEMP_ROLE,
                temp_role.role.discord_id,
                temp_role.user_id
            )
        temp_role.delete()


@app.task
def expire_personal_role(personal_role_pk: int):
    with transaction.atomic():
        role: PersonalRole = PersonalRole.objects.select_for_update(nowait=True).select_related(
            'user', 'role'
        ).get(pk=personal_role_pk)
        if not role.expired:
            return
        role.user.roles.filter(id=role.role_id).delete()
        EventService.create_event(
            EventType.EXPIRED_PERSONAL_ROLE,
            role.role.discord_id,
            role.user_id
        )


@app.task
def delete_expired_personal_roles():
    # TODO take timedelta from settings
    roles = PersonalRoleProxy.objects.filter(end_date__lt=now() - timedelta(days=3)).select_related('role')
    for role in roles:
        role.delete()


@app.task
def check_temp_roles():
    for temp_role_id in TempRole.objects.filter(to_date__lt=now()).values_list('id', flat=True):
        expire_temp_role.apply_async(args=[temp_role_id],)


@app.task
def create_personal_channel(personal_channel_pk: int):
    channel: PersonalChannel = PersonalChannel.objects.get(pk=personal_channel_pk)
    EventService.create_event(
        EventType.CREATE_PERSONAL_CHANNEL,
        json.dumps({
            'name': channel.name,
            'channel_id': personal_channel_pk
        }),
        channel.creator_id
    )


@app.task
def delete_personal_channel(personal_channel_pk: int):
    with transaction.atomic():
        channel: PersonalChannel = PersonalChannel.objects.select_for_update(nowait=True).get(
            pk=personal_channel_pk,
            end_date__isnull=False,
            end_date__lte=now()
        )
        channel.delete()
