from datetime import datetime, timezone
from unittest import TestCase

from django.db import transaction
from django.urls import reverse
from rest_framework.response import Response
from rest_framework.test import APIClient

from reward.models import Reward, RewardList, ChancedReward
from user.models import User
from inventory.models import Emotion, Container, UserContainer

api_client = APIClient()


class EmotionsTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=123,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.emotion: Emotion = Emotion.objects.create(name="1")
            cls.user_emotion: Emotion = Emotion.objects.create(name="2")
            cls.user.emotions.add(cls.user_emotion)

    def test_listing_emotions(self):
        url = reverse("emotions-detail", kwargs={"pk": self.emotion.pk})

        response: Response = api_client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["id"], self.emotion.pk)

        response: Response = api_client.get(f"{url}?user_id={self.user.pk}")
        self.assertEquals(response.status_code, 404)

        url = reverse("emotions-detail", kwargs={"pk": self.user_emotion.pk})
        response: Response = api_client.get(f"{url}?user_id={self.user.pk}")
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["id"], self.user_emotion.pk)

        url = reverse("emotions-detail", kwargs={"pk": 0})
        response: Response = api_client.get(url)
        self.assertEquals(response.status_code, 404)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.emotion.delete()
        cls.user_emotion.delete()


class ContainerTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=123,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.reward_list: RewardList = RewardList.objects.create()
            cls.reward: Reward = Reward.objects.create(experience=2)
            cls.container: Container = Container.objects.create(
                index_number=666
            )

    def test_missing_container(self):
        url = reverse("containers-open", kwargs={"pk": self.container.pk})

        response: Response = api_client.post(url, data={"user_id": self.user.pk, "count": "все"})
        self.assertEquals(response.status_code, 404)
        self.assertEquals(response.data, {"reason": "user"})

    def test_missing_container_reward_list(self):
        url = reverse("containers-open", kwargs={"pk": self.container.pk})
        UserContainer.objects.create(user=self.user, item=self.container, amount=5)
        self.addCleanup(UserContainer.objects.filter(user=self.user, item=self.container).delete)

        response: Response = api_client.post(url, data={"user_id": self.user.pk, "count": "все"})
        self.assertEquals(response.status_code, 404)
        self.assertEquals(response.data, {"reason": "reward"})

    def test_missing_container_reward(self):
        url = reverse("containers-open", kwargs={"pk": self.container.pk})
        UserContainer.objects.create(user=self.user, item=self.container, amount=5)
        self.addCleanup(UserContainer.objects.filter(user=self.user, item=self.container).delete)

        Container.objects.filter(pk=self.container.pk).update(reward_list=self.reward_list)
        self.addCleanup(Container.objects.filter(pk=self.container.pk).update, reward_list=None)

        response: Response = api_client.post(url, data={"user_id": self.user.pk, "count": "все"})
        self.assertEquals(response.status_code, 404)
        self.assertEquals(response.data, {"reason": "reward"})

    def test_not_enough_containers(self):
        url = reverse("containers-open", kwargs={"pk": self.container.pk})
        UserContainer.objects.create(user=self.user, item=self.container, amount=5)
        self.addCleanup(UserContainer.objects.filter(user=self.user, item=self.container).delete)

        response: Response = api_client.post(url, data={"user_id": self.user.pk, "count": 6})
        self.assertEquals(response.status_code, 404)
        self.assertEquals(response.data, {"reason": "user_count"})

    def test_open_all(self):
        url = reverse("containers-open", kwargs={"pk": self.container.pk})

        UserContainer.objects.create(user=self.user, item=self.container, amount=5)
        self.addCleanup(UserContainer.objects.filter(user=self.user, item=self.container).delete)
        Container.objects.filter(pk=self.container.pk).update(reward_list=self.reward_list)
        self.addCleanup(Container.objects.filter(pk=self.container.pk).update, reward_list=None)
        cr = ChancedReward.objects.create(reward=self.reward, reward_list=self.reward_list, chance=100)
        self.addCleanup(cr.delete)

        response: Response = api_client.post(url, data={"user_id": self.user.pk, "count": "все"})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["count"], 5)
        self.assertEquals(response.data["reward"]["experience"], 10)
        self.assertEquals(UserContainer.objects.get(user=self.user, item=self.container).amount, 0)

    def test_open(self):
        url = reverse("containers-open", kwargs={"pk": self.container.pk})

        UserContainer.objects.create(user=self.user, item=self.container, amount=5)
        self.addCleanup(UserContainer.objects.filter(user=self.user, item=self.container).delete)
        Container.objects.filter(pk=self.container.pk).update(reward_list=self.reward_list)
        self.addCleanup(Container.objects.filter(pk=self.container.pk).update, reward_list=None)
        cr = ChancedReward.objects.create(reward=self.reward, reward_list=self.reward_list, chance=100)
        self.addCleanup(cr.delete)

        response: Response = api_client.post(url, data={"user_id": self.user.pk, "count": 3})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data["count"], 3)
        self.assertEquals(response.data["reward"]["experience"], 6)
        self.assertEquals(UserContainer.objects.get(user=self.user, item=self.container).amount, 2)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.reward_list.delete()
        cls.reward.delete()
        cls.container.delete()
