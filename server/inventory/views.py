from django.db.models import Q
from django.utils.timezone import now
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from common.serializers import UserSerializer
from user.models import User
from .models import ChannelMembers, Container, Emotion, PersonalChannel, PersonalRole, Role, UserContainer
from .serializers import (
    ContainerSerializer,
    EmotionSerializer,
    PersonalChannelMemberDeleteSerializer,
    PersonalChannelMemberSerializer,
    PersonalChannelSerializer,
    PersonalRoleSerializer,
    RoleSerializer,
)


class ContainerViewSet(GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    serializer_class = ContainerSerializer

    def get_queryset(self):
        queryset = Container.objects.all()
        form = self.request.query_params.get('form', None)
        value = self.request.query_params.get('value', None)
        if form and value:
            queryset = queryset.filter(**{form: value})
        return queryset.only('id', 'name', 'form1', 'form2', 'form3', 'form4')

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def open(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        user_container: UserContainer = user.usercontainer_set.select_related(
            'item', 'item__reward_list'
        ).filter(item_id=pk).first()

        if not user_container or not user_container.amount:
            return self.no_containers_response

        if request.data['count'] == 'все':
            count = user_container.amount
        else:
            count = int(request.data['count'])
            if count > user_container.amount:
                return self.not_enough_containers_response

        container = user_container.item
        if container.reward_list is None or not container.reward_list.rewards.exists():
            return self.no_reward_response

        result_reward = container.open(user, count)
        if result_reward is None:
            return self.not_enough_containers_response

        return Response({
            'reward': result_reward,
            'count': count
        })

    @property
    def no_containers_response(self):
        return Response({'reason': 'user'}, status=status.HTTP_404_NOT_FOUND)

    @property
    def not_enough_containers_response(self):
        return Response({'reason': 'user_count'}, status=status.HTTP_404_NOT_FOUND)

    @property
    def no_reward_response(self):
        return Response({'reason': 'reward'}, status=status.HTTP_404_NOT_FOUND)


class RolesViewSet(ModelViewSet):
    queryset = Role.objects.select_related('separator').only(
        'discord_id', 'name', 'description', 'position', 'separator__discord_id'
    )
    lookup_field = 'discord_id'
    serializer_class = RoleSerializer


class UserRolesViewSet(ModelViewSet):
    lookup_field = 'pk'
    serializer_class = RoleSerializer

    def get_queryset(self):
        user = User.objects.only('roles', 'temp_roles').get(
            discord_id=self.kwargs['user_id']
        )
        queryset = user.all_roles.select_related('separator').only(
            'discord_id', 'name', 'description', 'position', 'separator__discord_id'
        )
        if self.action == 'list':
            return queryset
        return queryset.filter(is_tech=False)


class PersonalRolesViewSet(mixins.UpdateModelMixin, mixins.ListModelMixin, GenericViewSet):
    serializer_class = PersonalRoleSerializer

    def get_queryset(self):
        queryset = PersonalRole.objects.select_related('role').filter(end_date__gte=now())
        if 'user_id' in self.request.query_params:
            queryset = queryset.filter(user_id=self.request.query_params['user_id'])
        return queryset


class EmotionViewSet(mixins.RetrieveModelMixin,
                     GenericViewSet):
    serializer_class = EmotionSerializer

    def get_queryset(self):
        if "user_id" in self.request.query_params:
            user_id = self.request.query_params["user_id"]
            user = (
                User.objects.prefetch_related("emotions")
                .only("emotions__id", "emotions__name", "emotions__text", "emotions__image_url")
                .get(discord_id=user_id)
            )
            return user.emotions
        return Emotion.objects.all().only("id", "name", "text", "image_url")


class PersonalChannelViewSet(mixins.UpdateModelMixin, mixins.ListModelMixin, GenericViewSet):
    serializer_class = PersonalChannelSerializer
    queryset = PersonalChannel.objects.all()

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        requested_id = self.kwargs[lookup_url_kwarg]

        return get_object_or_404(queryset, Q(pk=requested_id) | Q(channel_id=requested_id))

    def filter_queryset(self, queryset):
        user_id = self.request.query_params.get('user_id')
        if user_id:
            queryset = queryset.filter(creator_id=user_id)
        return queryset

    @action(methods=['POST'], detail=False, serializer_class=PersonalChannelMemberSerializer)
    def members(self, request):
        serializer: PersonalChannelMemberSerializer = self.get_serializer(data=request.data)
        ChannelMembers.objects.filter(
            channel_id=serializer.initial_data['channel'],
            user_id=serializer.initial_data['user'],
            blocked_at__isnull=False,
        ).delete()
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @members.mapping.delete
    def remove_members(self, request):
        serializer = PersonalChannelMemberDeleteSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        ChannelMembers.objects.filter(
            channel_id=serializer.data['channel_id'],
            user_id=serializer.data['user_id'],
            blocked_at__isnull=True,
        ).update(blocked_at=now())
        return Response(status=status.HTTP_204_NO_CONTENT)
