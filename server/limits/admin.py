from django.contrib.admin import StackedInline

from .models import *


class ShopItemBlockInline(StackedInline):
    model = ShopItemBlock
    extra = 0
    autocomplete_fields = ('shop_item',)


class GiftItemBlockInline(StackedInline):
    model = GiftItemBlock
    extra = 0


class TransformationBlockInline(StackedInline):
    model = TransformationBlock
    extra = 0


class GiveawayBlockInline(StackedInline):
    model = GiveawayBlock
    extra = 0


class MarketExchangeBlockInline(StackedInline):
    model = MarketExchangeBlock
    extra = 0


class JourneyBlockInline(StackedInline):
    model = JourneyBlock
    extra = 0


class RoleGettingMessageBlockInline(StackedInline):
    model = RoleGettingMessageBlock
    extra = 0
