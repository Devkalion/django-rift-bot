from datetime import datetime

from common.interfaces import IModelIntId
from giveaways import interfaces as giveaways_interfaces
from reactions import interfaces as reactions_interfaces
from transformations import interfaces as transformations_interfaces
from donates import interfaces as donates_interfaces
from user import interfaces as user_interfaces


class IUserBlock(IModelIntId):
    user: 'user_interfaces.IUser'
    user_id: int
    creation_date: datetime


class IShopItemBlock(IUserBlock):
    shop_item: 'transformations_interfaces.IShopItem'
    shop_item_id: int


class IGiftItemBlock(IUserBlock):
    gift_item: 'transformations_interfaces.IGiftItem'
    gift_item_id: int


class ITransformationBlock(IUserBlock):
    transform_item: 'transformations_interfaces.ITransformItem'
    transform_item_id: int


class IMarketExchangeBlock(IUserBlock):
    market_item: 'transformations_interfaces.IMarketExchange'
    market_item_id: int


class IGiveawayBlock(IUserBlock):
    item: 'giveaways_interfaces.IGiveaway'
    item_id: int


class IJourneyBlock(IUserBlock):
    journey_item: 'transformations_interfaces.IJourneyItem'
    journey_item_id: int


class IDonateRewardBlock(IUserBlock):
    donate_reward: 'donates_interfaces.IDonateSumReward'
    donate_reward_id: int


class IRoleGettingMessageBlock(IUserBlock):
    message: 'reactions_interfaces.IRoleGettingMessage'
    message_id: int
