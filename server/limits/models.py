from django.db import models

from .intefaces import (
    IUserBlock,
    IShopItemBlock,
    IGiftItemBlock,
    ITransformationBlock,
    IMarketExchangeBlock,
    IGiveawayBlock,
    IJourneyBlock,
    IRoleGettingMessageBlock,
    IDonateRewardBlock,
)


class UserBlock(models.Model, IUserBlock):
    user = models.ForeignKey('user.User', models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class ShopItemBlock(UserBlock, IShopItemBlock):
    shop_item = models.ForeignKey('transformations.ShopItem', models.CASCADE, 'blocks')

    @property
    def shop(self):
        return self.shop_item.shop.title_name

    def __str__(self):
        return f'{self.user.name}: {self.shop_item.name} ({self.shop_item.id})'


class GiftItemBlock(UserBlock, IGiftItemBlock):
    gift_item = models.ForeignKey('transformations.GiftItem', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.gift_item.name} ({self.gift_item.id})'


class TransformationBlock(UserBlock, ITransformationBlock):
    transform_item = models.ForeignKey('transformations.TransformItem', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.transform_item.name} ({self.transform_item.id})'


class MarketExchangeBlock(UserBlock, IMarketExchangeBlock):
    market_item = models.ForeignKey('transformations.MarketExchange', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.market_item.name} ({self.market_item.id})'


class GiveawayBlock(UserBlock, IGiveawayBlock):
    item = models.ForeignKey('giveaways.Giveaway', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.item.name} ({self.item.id})'


class JourneyBlock(UserBlock, IJourneyBlock):
    journey_item = models.ForeignKey('transformations.JourneyItem', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.journey_item.name} ({self.journey_item.id})'


class DonateRewardBlock(UserBlock, IDonateRewardBlock):
    donate_reward = models.ForeignKey('donates.DonateSumReward', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.donate_reward.name} ({self.donate_reward_id})'


class RoleGettingMessageBlock(UserBlock, IRoleGettingMessageBlock):
    message = models.ForeignKey('reactions.RoleGettingMessage', models.CASCADE, 'blocks')

    def __str__(self):
        return f'{self.user.name}: {self.message.name} ({self.message.id})'
