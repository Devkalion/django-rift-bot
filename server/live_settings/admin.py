from django.contrib.admin import register, ModelAdmin

from .models import Setting


@register(Setting)
class SettingAdmin(ModelAdmin):
    list_display = ('code_name', 'display_name', 'value')
    readonly_fields = ('code_name', 'type', 'default_name', 'default_value', 'last_changed', 'required')
    list_editable = ('value', )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
