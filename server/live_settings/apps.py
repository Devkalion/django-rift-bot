from django.apps import AppConfig


class LiveSettingsConfig(AppConfig):
    name = 'live_settings'
