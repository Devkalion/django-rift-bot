from common.enums import ExtendedEnum


class Type(ExtendedEnum):
    INT = 'int'
    FLOAT = 'float'
    STRING = 'str'
    BOOLEAN = 'bool'
    CHANNEL_ID = 'channel'
    CHANNEL_ID_LIST = 'channels'
    ROLE_ID = 'role_id'
    ROLE_ID_LIST = 'roles'

