from collections import defaultdict
from datetime import datetime
from typing import List, Union, Any

from common.interfaces import IModel
from .enums import Type


def parse_channel_list(value: str):
    return [
        to_python[str(Type.CHANNEL_ID)](channel.strip())
        for channel in value.split(',')
        if channel.strip() != ''
    ]


def parse_boolean(value: str):
    return value.lower() not in ('0', 'f', 'false', '')


def dump_list(items: List):
    return ','.join(map(str, items))


def not_implemented(value):
    raise NotImplementedError


def parse_roles(value: str):
    return [
        to_python[str(Type.ROLE_ID)](role_id.strip())
        for role_id in value.split(',')
        if role_id.strip()
    ]


to_python = {
    str(Type.INT): int,
    str(Type.FLOAT): float,
    str(Type.STRING): str,
    str(Type.BOOLEAN): parse_boolean,
    str(Type.CHANNEL_ID): int,
    str(Type.CHANNEL_ID_LIST): parse_channel_list,
    str(Type.ROLE_ID): not_implemented,
    str(Type.ROLE_ID_LIST): parse_roles,
}

to_str = defaultdict(lambda: str, **{
    str(Type.CHANNEL_ID_LIST): dump_list,
    str(Type.ROLE_ID_LIST): dump_list,
})


class ISetting(IModel):
    code_name: str
    type: Union[Type, str]
    default_name: str
    default_value: str
    last_changed: datetime

    display_name: str
    value: str
    required: bool

    @property
    def pk(self):
        return self.code_name

    @property
    def normalized_value(self):
        return to_python[str(self.type)](self.value)

    @classmethod
    def get_value(cls, name: str) -> Any:
        raise NotImplementedError

    def pprint(self):
        return f'{"* " if self.required else "  "}' \
               f'{self.code_name} [{self.type}] - ' \
               f'{self.normalized_value if self.value else "~~~"}' \
               f' // {self.display_name}'
