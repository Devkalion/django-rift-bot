"""
Subcommands to execute:
    sync  - synchronize django settings with in-db livesettings.
With optional [--force] argument it will not ask you for confirmation, otherwise it first prints all changes.
    list  - display all settings to stdout
    reset - delete all livesettings from database and launch sync --force
"""
from argparse import RawTextHelpFormatter
from typing import List

from django.conf import settings
from django.core.management.base import BaseCommand

from live_settings.models import Setting, to_str


class Command(BaseCommand):
    def set_to_redis(self):
        for s in Setting.objects.all():
            s.set_to_redis()

    def sync(self, force=False):
        settings_for_update = []
        for name, obj in settings.LIVE_SETTINGS.items():
            try:
                old_obj = Setting.objects.get(code_name=name)
                if force:
                    old_obj.default_name = obj['name']
                    old_obj.default_value = obj['value']
                    old_obj.display_name = obj['name']
                    old_obj.value = obj['value']
                    old_obj.type = obj['type']
                    old_obj.required = obj.get('required', False)
                    old_obj.save()
                else:
                    settings_for_update.append(old_obj)
            except Setting.DoesNotExist:
                Setting(
                    code_name=name,
                    type=obj['type'],
                    default_name=obj['name'],
                    default_value=obj['value'],
                    display_name=obj['name'],
                    value=obj['value'],
                    required=obj.get('required', False),
                ).save()

        for old_obj in settings_for_update:
            str_func = to_str[str(old_obj.type)]
            new_obj = settings.LIVE_SETTINGS[old_obj.code_name]
            mismatches = []
            if str(old_obj.type) != new_obj['type']:
                mismatches.append('type')
            if old_obj.default_name != new_obj['name']:
                mismatches.append('default_name')
            if old_obj.default_value != str_func(new_obj['value']):
                mismatches.append('default_value')
            if old_obj.required != new_obj.get('required', False):
                mismatches.append('required')
            if len(mismatches):
                print(f'{old_obj.code_name} mismatches - {mismatches}', old_obj.pprint(), new_obj, sep='\n\t')

    def list(self):
        print(*[s.pprint() for s in Setting.objects.all()], sep='\n')

    def reset(self):
        Setting.objects.all().delete()
        self.sync(True)

    def handle(self, *args, **options):
        command = options['command']
        if command == 'sync':
            self.sync(options['force'])
        elif command == 'list':
            self.list()
        elif command == 'reset':
            self.reset()
        elif command == 'set_to_redis':
            self.set_to_redis()
        else:
            print('Incorrect command')
            exit(1)

    def add_arguments(self, parser):
        parser.formatter_class = RawTextHelpFormatter
        parser.add_argument('command', choices=['sync', 'list', 'reset', 'set_to_redis'], help=__doc__)
        parser.add_argument('--force', action='store_true', default=False,
                            help='Force settings updates without confirmation')
