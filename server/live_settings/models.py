from django.core.exceptions import ValidationError
from django.db import models
from redis import Redis

from common.models import EnumField
from settings import REDIS_CONNECTION_SETTINGS
from .enums import Type
from .interfaces import ISetting, to_python, to_str


def parse_role_id(value: str):
    from inventory.models import Role
    if not Role.objects.filter(discord_id=value).exists():
        raise ValidationError({'value': 'No such role'})

    return value


to_python[str(Type.ROLE_ID)] = parse_role_id


class Setting(models.Model, ISetting):
    code_name = models.CharField(max_length=50, primary_key=True, editable=False)
    type = EnumField(Type, editable=False)
    default_name = models.CharField(max_length=128, unique=True, editable=False)
    default_value = models.CharField(max_length=128, editable=False)
    last_changed = models.DateTimeField(auto_now=True)

    display_name = models.CharField(max_length=128, unique=True)
    value = models.CharField(max_length=128, blank=True)
    required = models.BooleanField(default=True)

    def __str__(self):
        return self.display_name

    def clean(self):
        if self.required and self.value == "":
            raise ValidationError({'value': 'Value is required'})
        if not self.required and self.value == "":
            return
        try:
            self.value = to_python[str(self.type)](self.value)
        except TypeError:
            raise ValidationError({'value': 'Value is incorrect'})

    @classmethod
    def get_value(cls, name):
        instance: 'Setting' = cls.objects.only('value', 'type').get(code_name=name)
        return instance.normalized_value

    def save(self, *args, **kwargs):
        str_func = to_str[str(self.type)]
        if not isinstance(self.value, str):
            self.value = str_func(self.value)
        if not isinstance(self.default_value, str):
            self.default_value = str_func(self.default_value)
        super().save(*args, **kwargs)
        self.set_to_redis()

    def set_to_redis(self):
        redis_key = f'settings-{self.code_name}'
        with Redis(**REDIS_CONNECTION_SETTINGS) as redis:
            redis.set(redis_key, self.value)
