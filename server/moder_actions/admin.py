from django.contrib.admin import register, ModelAdmin, StackedInline
from django.core.exceptions import ValidationError

from user.models import Admin
from .enums import AppealStatus
from .models import ModerAction, Appeal
from .services import AppealService


@register(ModerAction)
class ModerActionAdmin(ModelAdmin):
    list_display = ('moderator', 'user', 'type', 'creation_date', 'active')
    list_filter = ('active', 'type')
    readonly_fields = ('creation_date',)
    raw_id_fields = ['user']
    search_fields = ('user__discord_id', 'user__name', 'moderator__discord_id', 'moderator__name')

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.select_related('user', 'moderator')

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = list(super().get_readonly_fields(request, obj))
        if obj is not None:
            readonly_fields += ['moderator', 'user']
        return readonly_fields


@register(Appeal)
class AppealAdmin(ModelAdmin):
    list_display = ('id', 'category', 'user', 'status', 'creation_date')
    list_filter = ('status', 'category')
    readonly_fields = ['creation_date', 'moderator', 'moderator_nickname']
    raw_id_fields = ['author', 'user']

    def get_queryset(self, request):
        user: Admin = request.user
        queryset = AppealService.get_user_appeals(user)
        ordering = self.get_ordering(request)
        if ordering:
            queryset = queryset.order_by(*ordering)

        return queryset

    def save_model(self, request, obj: Appeal, form, change):
        if str(obj.status) == str(AppealStatus.REVIEWED) and obj.moderator_id is None:
            moderator: Admin = request.user
            if moderator.user_id is None:
                raise ValidationError('Moderator has no linked discord account')
            obj.moderator = request.user
            obj.moderator_nickname = obj.moderator.user.name
        return super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = list(super().get_readonly_fields(request, obj))
        if obj is not None:
            readonly_fields += ['author', 'user']
        return readonly_fields


class OffenceInline(StackedInline):
    model = ModerAction
    extra = 0
    fk_name = 'user'
    readonly_fields = ('creation_date',)
    fieldsets = (
        (None, {
            'fields': ('creation_date',)
        }),
        ('Fields', {
            'classes': ['collapse'],
            'fields': ('type', 'moderator', 'reason', 'finish_time', 'active')
        }),
    )


class AppealInline(StackedInline):
    model = Appeal
    extra = 0
    fk_name = 'user'
    readonly_fields = ('creation_date',)
    fieldsets = (
        (None, {
            'fields': ('creation_date',)
        }),
        ('Fields', {
            'classes': ['collapse'],
            'fields': ('text', 'status')
        }),
    )

    def get_queryset(self, request):
        user: Admin = request.user
        queryset = AppealService.get_user_appeals(user)
        queryset = queryset.exclude(status=AppealStatus.CREATED)
        ordering = self.get_ordering(request)
        if ordering:
            queryset = queryset.order_by(*ordering)

        if not self.has_view_or_change_permission(request):
            queryset = queryset.none()

        return queryset

    def has_change_permission(self, request, obj=None):
        return False
