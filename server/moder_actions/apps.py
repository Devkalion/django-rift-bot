from django.apps import AppConfig


class ModerActionsConfig(AppConfig):
    name = 'moder_actions'
