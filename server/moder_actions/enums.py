from common.enums import ExtendedEnum


class ActionType(ExtendedEnum):
    WARNING = 'warn'
    MUTE = 'mute'
    UNMUTE = 'unmute'
    KICK = 'kick'
    BAN = 'ban'
    UNBAN = 'unban'


class AppealStatus(ExtendedEnum):
    CREATED = 'created'
    REVIEWED = 'reviewed'
