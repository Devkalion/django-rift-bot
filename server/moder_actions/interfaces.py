from datetime import datetime
from typing import Union, Optional

from common.interfaces import IModelIntId
from user import interfaces as user_interfaces
from .enums import ActionType, AppealStatus


class IModerAction(IModelIntId):
    type: Union[ActionType, str]
    user: 'user_interfaces.IUser'
    user_id: int
    moderator: 'user_interfaces.IUser'
    moderator_id: int
    reason: str
    finish_time: Optional[datetime]
    creation_date: datetime
    active: bool


class IAppeal(IModelIntId):
    author: 'user_interfaces.IUser'
    author_id: int
    user: 'user_interfaces.IUser'
    user_id: int
    creation_date: datetime
    text: str
    category: str
    status: Union[AppealStatus, str]
    moderator_nickname: Optional[str]
    comment: Optional[str]
    admin: 'user_interfaces.IAdmin'
    admin_id: Optional[int]
