from django.db import models

from common.models import EnumField
from .enums import ActionType, AppealStatus

from .interfaces import IAppeal, IModerAction


class ModerAction(models.Model, IModerAction):
    type = EnumField(enum=ActionType)
    user = models.ForeignKey('user.User', models.CASCADE)
    moderator = models.ForeignKey('user.User', models.CASCADE, related_name='+',
                                  limit_choices_to={'is_moder': True})
    reason = models.CharField(max_length=100)
    finish_time = models.DateTimeField(blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        from .tasks import expire_moder_action
        super().save(*args, **kwargs)
        if self.finish_time and self.active:
            expire_moder_action.apply_async(args=[self.pk], eta=self.finish_time)


class Appeal(models.Model, IAppeal):
    author = models.ForeignKey('user.User', models.CASCADE, '+')
    user = models.ForeignKey('user.User', models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    category = models.CharField(max_length=10, blank=True)
    status = EnumField(enum=AppealStatus, default=AppealStatus.CREATED.value)
    moderator_nickname = models.CharField(max_length=255, null=True, blank=True)
    moderator = models.ForeignKey('user.Admin', models.PROTECT, null=True, blank=True)
    comment = models.TextField(blank=True, null=True)

    class Meta:
        permissions = [
            ("can_see_all_appeals", "Can see not his appeals"),
        ]

    def save(self, *args, **kwargs):
        from .tasks import review_appeal
        old_appeal = None
        if self.pk:
            old_appeal = Appeal.objects.get(pk=self.pk)
        super().save(*args, **kwargs)
        if all((
                old_appeal and str(old_appeal.status) == str(AppealStatus.CREATED),
                str(self.status) == str(AppealStatus.REVIEWED)
        )):
            review_appeal.apply_async(args=[self.pk])
