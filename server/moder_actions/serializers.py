from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from user.models import User
from .models import *


class ModerActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModerAction
        fields = ('id', 'reason', 'moderator', 'user', 'finish_time', 'type')

    @staticmethod
    def validate_user(user: User):
        if any((user.is_moder, user.is_admin, user.is_developer)):
            raise ValidationError('user_is_moder')
        return user


class AppealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appeal
        fields = ('id', 'author', 'user', 'text')
