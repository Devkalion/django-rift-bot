from django.db.models import Q, QuerySet

from moder_actions.enums import AppealStatus
from moder_actions.models import Appeal
from user.models import Admin


class AppealService:
    @classmethod
    def get_user_appeals(cls, user: Admin) -> QuerySet:
        queryset = Appeal.objects.all()
        if not user.is_superuser and not user.has_perm('moder_actions.can_see_all_appeals'):
            queryset = queryset.filter(Q(moderator=user) | Q(status=AppealStatus.CREATED))

        return queryset
