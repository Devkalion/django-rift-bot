from django.db import transaction
from django.utils.timezone import now

from events.enums import EventType
from events.services import EventService
from schedule.celery import app
from .enums import ActionType
from .models import ModerAction, Appeal


@app.task
def expire_moder_action(action_pk: int):
    with transaction.atomic():
        action = ModerAction.objects.select_for_update(nowait=True).get(pk=action_pk)
        if not action.active or action.finish_time > now():
            return
        action.active = False
        action.save(update_fields=['active'])
    if not ModerAction.objects.filter(
            user_id=action.user_id, active=True, type=action.type, finish_time__gt=now()
    ).exists():
        if action.type == str(ActionType.MUTE):
            EventService.create_event(EventType.UNMUTE, user_id=action.user_id)
        elif action.type == str(ActionType.BAN):
            EventService.create_event(EventType.UNBAN, user_id=action.user_id)


@app.task
def review_appeal(appeal_pk: int):
    appeal = Appeal.objects.get(pk=appeal_pk)
    EventService.create_event(EventType.APPEAL_REVIEWED, user_id=appeal.author_id, data=str(appeal_pk), )
