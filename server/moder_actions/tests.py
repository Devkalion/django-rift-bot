from datetime import datetime, timezone
from unittest import TestCase

from django.urls import reverse
from rest_framework.response import Response
from rest_framework.test import APIClient

from user.models import User

api_client = APIClient()


class ModerActionTestCase(TestCase):
    url = reverse("moder_actions-list")

    @classmethod
    def setUpClass(cls) -> None:
        cls.moder: User = User.objects.create(
            discord_id=123,
            discord_registration_date=datetime.now(timezone.utc),
            registration_date=datetime.now(timezone.utc),
            last_activity_time=datetime.now(timezone.utc),
            is_moder=True,
        )

    @classmethod
    def tearDownClass(cls) -> None:
        cls.moder.delete()

    def test_ban_moder(self):
        response: Response = api_client.post(self.url, data={
            'moderator': self.moder.pk,
            'user': self.moder.pk,
            'type': 'ban',
            'reason': 'test',
        })

        self.assertIsNotNone(response)
        self.assertEquals(response.status_code, 400)
        self.assertEquals(
            response.data,
            {'user': ['user_is_moder']}
        )

    def test_ban_admin(self):
        admin: User = User.objects.create(
            discord_id=456,
            discord_registration_date=datetime.now(timezone.utc),
            registration_date=datetime.now(timezone.utc),
            last_activity_time=datetime.now(timezone.utc),
            is_admin=True,
        )
        self.addCleanup(admin.delete)

        response: Response = api_client.post(self.url, data={
            'moderator': self.moder.pk,
            'user': admin.pk,
            'type': 'ban',
            'reason': 'test',
        })

        self.assertIsNotNone(response)
        self.assertEquals(response.status_code, 400)
        self.assertEquals(
            response.data,
            {'user': ['user_is_moder']}
        )

    def test_ban_developer(self):
        developer: User = User.objects.create(
            discord_id=654,
            discord_registration_date=datetime.now(timezone.utc),
            registration_date=datetime.now(timezone.utc),
            last_activity_time=datetime.now(timezone.utc),
            is_developer=True,
        )
        self.addCleanup(developer.delete)

        response: Response = api_client.post(self.url, data={
            'moderator': self.moder.pk,
            'user': developer.pk,
            'type': 'ban',
            'reason': 'test',
        })

        self.assertIsNotNone(response)
        self.assertEquals(response.status_code, 400)
        self.assertEquals(
            response.data,
            {'user': ['user_is_moder']}
        )

    def test_ban_by_user(self):
        user: User = User.objects.create(
            discord_id=321,
            discord_registration_date=datetime.now(timezone.utc),
            registration_date=datetime.now(timezone.utc),
            last_activity_time=datetime.now(timezone.utc),
        )
        self.addCleanup(user.delete)

        response: Response = api_client.post(self.url, data={
            'moderator': user.pk,
            'user': user.pk,
            'type': 'ban',
            'reason': 'test',
        })

        self.assertIsNotNone(response)
        self.assertEquals(response.status_code, 400)
        self.assertEquals(
            response.data,
            {'moderator': [f'Недопустимый первичный ключ "{user.pk}" - объект не существует.']}
        )
