from datetime import timedelta

from django.utils.timezone import now
from django_filters import rest_framework as filters
from rest_framework import mixins
from rest_framework.exceptions import PermissionDenied
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.viewsets import GenericViewSet

from live_settings.models import Setting
from .enums import ActionType
from .models import ModerAction
from .serializers import ModerActionSerializer, AppealSerializer


class Pagination(LimitOffsetPagination):
    default_limit = 20


class ModerActionFilter(filters.FilterSet):
    user = filters.NumberFilter(field_name="user_id")
    type = filters.CharFilter(field_name='type')

    class Meta:
        model = ModerAction
        fields = ['user_id', 'type']


class ModerActionViewSet(GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin):
    serializer_class = ModerActionSerializer
    filterset_class = ModerActionFilter
    pagination_class = Pagination
    queryset = ModerAction.objects.order_by('-finish_time').filter(active=True).only(
        'id', 'reason', 'moderator', 'user', 'finish_time', 'type'
    )

    def perform_create(self, serializer: ModerActionSerializer):
        _type = serializer.validated_data['type']
        if _type in map(str, [ActionType.KICK, ActionType.BAN]):
            count = ModerAction.objects.filter(
                moderator=serializer.validated_data['moderator'],
                type=_type,
                creation_date__gte=now() - timedelta(
                    seconds=Setting.get_value('MODER_ACTIONS_LIMIT_SECONDS')
                )
            ).count()
            if count >= Setting.get_value('MODER_ACTIONS_LIMIT_COUNT'):
                raise PermissionDenied

        super().perform_create(serializer)

        if _type == str(ActionType.UNMUTE):
            ModerAction.objects.filter(
                user=serializer.validated_data['user'],
                type=str(ActionType.MUTE),
                active=True
            ).update(active=False)
        elif _type == str(ActionType.UNBAN):
            ModerAction.objects.filter(
                user=serializer.validated_data['user'],
                type=str(ActionType.BAN),
                active=True
            ).update(active=False)


class AppealViewSet(GenericViewSet, mixins.CreateModelMixin):
    serializer_class = AppealSerializer
