import pika
from django.conf import settings
from pika.adapters.blocking_connection import BlockingChannel

connection: pika.BlockingConnection = pika.BlockingConnection(settings.RABBIT_MQ_CONNECTION_PARAMS)
channel: BlockingChannel = connection.channel()
channel.queue_declare(queue='events')
