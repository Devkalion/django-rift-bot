from django.contrib.admin import register, ModelAdmin, StackedInline

from .models import *


class MessageReactionInline(StackedInline):
    model = MessageReaction
    extra = 0


@register(RoleGettingMessage)
class RoleGettingMessageAdmin(ModelAdmin):
    inlines = [MessageReactionInline]
    list_display = ('name', 'description')
