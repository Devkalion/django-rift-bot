from typing import Optional

from common.interfaces import IModelIntId
from inventory import interfaces as inventory_interfaces
from reward import interfaces as reward_interfaces


class IRoleGettingMessage(IModelIntId):
    name: str
    description: str
    channel_id: int
    message_id: int
    reward: Optional['reward_interfaces.IRewardList']
    reward_id: Optional[int]
    single_role: bool


class IMessageReaction(IModelIntId):
    message: IRoleGettingMessage
    message_id: int
    emoji: str
    role: 'inventory_interfaces.IRole'
    role_id: int
