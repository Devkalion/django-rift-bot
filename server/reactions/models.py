from django.db import models

from .interfaces import IMessageReaction, IRoleGettingMessage


class RoleGettingMessage(models.Model, IRoleGettingMessage):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=255)
    channel_id = models.BigIntegerField()
    message_id = models.BigIntegerField()
    reward = models.ForeignKey('reward.RewardList', models.SET_NULL, blank=True, null=True)
    single_role = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        index_together = ['channel_id', 'message_id']
        unique_together = ['channel_id', 'message_id']


class MessageReaction(models.Model, IMessageReaction):
    message = models.ForeignKey(RoleGettingMessage, models.CASCADE, 'reactions')
    emoji = models.CharField(max_length=10)
    role = models.ForeignKey('inventory.Role', models.CASCADE)

    class Meta:
        index_together = ['message', 'emoji']
        unique_together = ['message', 'emoji']
