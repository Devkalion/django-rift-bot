from rest_framework import serializers

from .models import *


class ReactionSerializer(serializers.ModelSerializer):
    role = serializers.IntegerField(source='role.discord_id')

    class Meta:
        model = MessageReaction
        fields = ('emoji', 'role')


class RoleGettingMessageSerializer(serializers.ModelSerializer):
    reactions = serializers.SerializerMethodField()

    class Meta:
        model = RoleGettingMessage
        fields = ('single_role', 'reactions', 'reward')

    @staticmethod
    def get_reactions(obj):
        return ReactionSerializer(MessageReaction.objects.filter(message=obj).select_related(
            'role'
        ).only(
            'emoji', 'role__discord_id'
        ), many=True).data
