from datetime import datetime, timezone
from unittest import TestCase

from django.db import transaction
from django.urls import reverse
from rest_framework.response import Response
from rest_framework.test import APIClient

from reactions.models import RoleGettingMessage
from reward.models import Reward, RewardList, ChancedReward
from user.models import User

api_client = APIClient()


class ReactionMessageTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=1,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.reward: Reward = Reward.objects.create(experience=5)
            cls.reward_list: RewardList = RewardList.objects.create()
            ChancedReward.objects.create(reward=cls.reward, reward_list=cls.reward_list, chance=100)

            cls.message: RoleGettingMessage = RoleGettingMessage.objects.create(
                name="some",
                description="some",
                channel_id=1,
                message_id=2,
                reward=cls.reward_list,
            )

    def test_get_reward(self):
        url = reverse("reactions-reward",
                      kwargs={"message_id": self.message.message_id, "channel_id": self.message.channel_id})
        payload = {"user_id": self.user.pk}

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 200)
        self.user.refresh_from_db()
        self.assertEquals(self.user.experience, self.reward.experience)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 403)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.reward.delete()
        cls.reward_list.delete()
        cls.message.delete()
