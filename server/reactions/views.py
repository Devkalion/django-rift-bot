from django.db import transaction
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from common.serializers import UserSerializer
from limits.models import RoleGettingMessageBlock
from reward.utils import ResultReward
from user.models import User
from .models import RoleGettingMessage
from .serializers import RoleGettingMessageSerializer


class RoleGettingMessageViewSet(mixins.RetrieveModelMixin,
                                GenericViewSet):
    lookup_field = 'message_id'
    serializer_class = RoleGettingMessageSerializer

    def get_queryset(self):
        queryset = RoleGettingMessage.objects.filter(
            channel_id=self.kwargs['channel_id']
        )
        if self.action == 'reward':
            return queryset.select_related('reward').only('reward', 'id')
        return queryset.only('single_role', 'reward_id')

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def reward(self, request, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        user: User = serializer.validated_data['user']
        message: RoleGettingMessage = self.get_object()
        reward: dict = message.reward.get_reward().calc(user)
        result_reward: ResultReward = ResultReward.from_dict(reward)
        with transaction.atomic():
            if RoleGettingMessageBlock.objects.filter(
                user=user, message=message
            ).exists():
                raise PermissionDenied
            result_reward.give(user)
            RoleGettingMessageBlock.objects.create(
                user=user, message=message
            )

        return Response({
            'reward': reward
        })
