from django.contrib.admin import register, ModelAdmin, StackedInline

from .models import *


class BaseInline(StackedInline):
    extra = 0


class PriceItemInline(BaseInline):
    model = PriceItem


class PriceCurrencyInline(BaseInline):
    model = PriceCurrency


class PriceContainerInline(BaseInline):
    model = PriceContainer


class ItemInline(BaseInline):
    model = RewardItem


class CurrencyInline(BaseInline):
    model = RewardCurrency


class ContainerInline(BaseInline):
    model = RewardContainer


class ChancedRewardInline(StackedInline):
    model = ChancedReward
    extra = 0


@register(Price)
class PriceAdmin(ModelAdmin):
    list_display = ('title', 'description', 'category')
    search_fields = ('title', 'description', 'category')
    list_filter = ('category',)
    inlines = [
        PriceItemInline,
        PriceCurrencyInline,
        PriceContainerInline
    ]


@register(RewardEmotionCategory, RewardRoleCategory, RewardBackgroundCategory, RewardTitle)
class RewardCategoryAdmin(ModelAdmin):
    filter_horizontal = ('items',)


@register(Reward)
class RewardAdmin(ModelAdmin):
    list_display = ('title', 'description', 'category')
    search_fields = ('title', 'description', 'category')
    list_filter = ('type', 'category')
    inlines = [
        ItemInline,
        CurrencyInline,
        ContainerInline
    ]


@register(RewardList)
class RewardListAdmin(ModelAdmin):
    inlines = [ChancedRewardInline]
    list_display = ('title', 'description', 'category')
    search_fields = ('title', 'description')
    list_filter = ('category',)


@register(LevelReward)
class LevelRewardAdmin(ModelAdmin):
    list_display = ('reward', 'level', 'exact')


@register(DurationReward)
class DurationRewardAdmin(ModelAdmin):
    list_display = ('id', 'from_timedelta', 'to_timedelta', 'multiplicity')
