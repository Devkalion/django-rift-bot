from django.apps import AppConfig


class RewardConfig(AppConfig):
    name = 'reward'
    verbose_name = 'Награды'
