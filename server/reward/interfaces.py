import random
from datetime import timedelta
from typing import Optional, Union

from common.interfaces import IModelIntId, IRandomAmountMixin
from inventory import interfaces as inventory_interfaces
from .enums import RewardType


class IBase(IModelIntId):
    title: str
    description: Optional[str]
    category: Optional[str]


class IReward(IBase):
    experience: int
    # items = models.ManyToManyField('inventory.Item', through='RewardItem', blank=True)
    # currency = models.ManyToManyField('inventory.Currency', through='RewardCurrency', blank=True)
    # containers = models.ManyToManyField('inventory.Container', through='RewardContainer', blank=True)
    title_category: Optional['IRewardTitle']
    title_category_id: Optional['int']
    role_category: Optional['IRewardRoleCategory']
    role_category_id: Optional['int']
    background_category: Optional['IRewardBackgroundCategory']
    background_category_id: Optional['int']
    emotion_category: Optional['IRewardEmotionCategory']
    emotion_category_id: Optional['int']

    type: Union[RewardType, str]

    personal_role_id: Optional[int]
    personal_role: Optional['IDurationReward']
    private_channel_id: Optional[int]
    private_channel: Optional['IDurationReward']


class IPrice(IBase):
    # items = models.ManyToManyField('inventory.Item', through='PriceItem', blank=True)
    # currency = models.ManyToManyField('inventory.Currency', through='PriceCurrency', blank=True)
    # containers = models.ManyToManyField('inventory.Container', through='PriceContainer', blank=True)
    pass


class IChancedReward(IModelIntId):
    chance: int
    reward: IReward
    reward_id: int
    reward_list: 'IRewardList'
    reward_list_id: int


class IRewardList(IModelIntId):
    # rewards = models.ManyToManyField(IReward, through=IChancedReward)
    title: str
    description: Optional[str]
    category: Optional[str]


class ILevelReward(IModelIntId):
    level: int
    exact: bool
    reward: IReward
    reward_id: int


class IBasePriceItem(IModelIntId):
    item: 'inventory_interfaces.IBaseItem'
    item_id: int
    amount: int
    price: IPrice
    price_id: int


class IBaseRewardItem(IModelIntId, IRandomAmountMixin):
    item: 'inventory_interfaces.IBaseItem'
    item_id: int
    reward: IReward
    reward_id: int


class IBaseRewardCategory(IModelIntId, IRandomAmountMixin):
    name: str
    alternative_reward: Optional[IRewardList]
    alternative_reward_id: int
    items = None


class IPriceItem(IBasePriceItem):
    item: 'inventory_interfaces.IItem'


class IPriceCurrency(IBasePriceItem):
    item: 'inventory_interfaces.ICurrency'


class IPriceContainer(IBasePriceItem):
    item: 'inventory_interfaces.IContainer'


class IRewardItem(IBaseRewardItem):
    item: 'inventory_interfaces.IItem'


class IRewardCurrency(IBaseRewardItem):
    item: 'inventory_interfaces.ICurrency'


class IRewardContainer(IBaseRewardItem):
    item: 'inventory_interfaces.IContainer'


class IRewardEmotionCategory(IBaseRewardCategory):
    # items = models.ManyToManyField('inventory.Emotion', related_name='+')
    pass


class IRewardTitle(IBaseRewardCategory):
    # items = models.ManyToManyField('inventory.Title', related_name='+')
    pass


class IRewardRoleCategory(IBaseRewardCategory):
    # items = models.ManyToManyField('inventory.Role', related_name='+')
    duration: timedelta


class IRewardBackgroundCategory(IBaseRewardCategory):
    # items = models.ManyToManyField('inventory.Background', related_name='+')
    pass


class IDurationReward(IModelIntId):
    from_timedelta: timedelta
    to_timedelta: timedelta
    multiplicity: int

    def get_duration_for_count(self, count: int = 1) -> int:
        _from = (self.from_timedelta.total_seconds() * count) // self.multiplicity
        _to = (self.to_timedelta.total_seconds() * count) // self.multiplicity
        total_seconds = random.randint(_from, _to) * self.multiplicity
        return total_seconds
