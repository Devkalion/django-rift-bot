# Generated by Django 3.1.3 on 2020-12-09 10:26

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('inventory', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChancedReward',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chance', models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(100)])),
            ],
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(blank=True, max_length=100, null=True)),
                ('category', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Reward',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(blank=True, max_length=100, null=True)),
                ('category', models.CharField(blank=True, max_length=50, null=True)),
                ('experience', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RewardList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(blank=True, max_length=100, null=True)),
                ('category', models.CharField(blank=True, max_length=50, null=True)),
                ('rewards', models.ManyToManyField(through='reward.ChancedReward', to='reward.Reward')),
            ],
        ),
        migrations.CreateModel(
            name='RewardTitle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('name', models.CharField(max_length=100)),
                ('alternative_reward', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardlist')),
                ('items', models.ManyToManyField(related_name='_rewardtitle_items_+', to='inventory.Title')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RewardRoleCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('name', models.CharField(max_length=100)),
                ('duration', models.DurationField(default=datetime.timedelta(0))),
                ('alternative_reward', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardlist')),
                ('items', models.ManyToManyField(related_name='_rewardrolecategory_items_+', to='inventory.Role')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RewardItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.item')),
                ('reward', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.reward')),
            ],
            options={
                'abstract': False,
                'unique_together': {('reward', 'item')},
            },
        ),
        migrations.CreateModel(
            name='RewardEmotionCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('name', models.CharField(max_length=100)),
                ('alternative_reward', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardlist')),
                ('items', models.ManyToManyField(related_name='_rewardemotioncategory_items_+', to='inventory.Emotion')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RewardCurrency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.currency')),
                ('reward', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.reward')),
            ],
            options={
                'abstract': False,
                'unique_together': {('reward', 'item')},
            },
        ),
        migrations.CreateModel(
            name='RewardContainer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.container')),
                ('reward', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.reward')),
            ],
            options={
                'abstract': False,
                'unique_together': {('reward', 'item')},
            },
        ),
        migrations.CreateModel(
            name='RewardBackgroundCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_amount', models.IntegerField(default=0)),
                ('to_amount', models.IntegerField(default=0)),
                ('multiplicity', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('name', models.CharField(max_length=100)),
                ('alternative_reward', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardlist')),
                ('items', models.ManyToManyField(related_name='_rewardbackgroundcategory_items_+', to='inventory.Background')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='reward',
            name='background_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardbackgroundcategory'),
        ),
        migrations.AddField(
            model_name='reward',
            name='containers',
            field=models.ManyToManyField(blank=True, through='reward.RewardContainer', to='inventory.Container'),
        ),
        migrations.AddField(
            model_name='reward',
            name='currency',
            field=models.ManyToManyField(blank=True, through='reward.RewardCurrency', to='inventory.Currency'),
        ),
        migrations.AddField(
            model_name='reward',
            name='emotion_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardemotioncategory'),
        ),
        migrations.AddField(
            model_name='reward',
            name='items',
            field=models.ManyToManyField(blank=True, through='reward.RewardItem', to='inventory.Item'),
        ),
        migrations.AddField(
            model_name='reward',
            name='role_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardrolecategory'),
        ),
        migrations.AddField(
            model_name='reward',
            name='title_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='reward.rewardtitle'),
        ),
        migrations.CreateModel(
            name='PriceItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(default=0)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.item')),
                ('price', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.price')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PriceCurrency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(default=0)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.currency')),
                ('price', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.price')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PriceContainer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(default=0)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.container')),
                ('price', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.price')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='price',
            name='containers',
            field=models.ManyToManyField(blank=True, through='reward.PriceContainer', to='inventory.Container'),
        ),
        migrations.AddField(
            model_name='price',
            name='currency',
            field=models.ManyToManyField(blank=True, through='reward.PriceCurrency', to='inventory.Currency'),
        ),
        migrations.AddField(
            model_name='price',
            name='items',
            field=models.ManyToManyField(blank=True, through='reward.PriceItem', to='inventory.Item'),
        ),
        migrations.AddField(
            model_name='chancedreward',
            name='reward',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.reward'),
        ),
        migrations.AddField(
            model_name='chancedreward',
            name='reward_list',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.rewardlist'),
        ),
        migrations.CreateModel(
            name='LevelReward',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level', models.PositiveIntegerField()),
                ('exact', models.BooleanField(default=False)),
                ('reward', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reward.reward')),
            ],
            options={
                'unique_together': {('level', 'exact')},
            },
        ),
    ]
