import random
from collections import Counter, defaultdict
from datetime import timedelta
from typing import List

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from common.models import RandomAmount, EnumField
from inventory.models import UserItem, UserCurrency, UserContainer, Emotion, BaseItem, BaseCategory, BaseCategoryItem, \
    UserTitle
from reward.enums import RewardType
from .interfaces import (
    IBase,
    IReward,
    IPrice,
    IChancedReward,
    IDurationReward,
    IRewardList,
    ILevelReward,
    IBasePriceItem,
    IBaseRewardItem,
    IBaseRewardCategory,
    IPriceItem,
    IPriceCurrency,
    IPriceContainer,
    IRewardItem,
    IRewardCurrency,
    IRewardContainer,
    IRewardEmotionCategory,
    IRewardTitle,
    IRewardRoleCategory,
    IRewardBackgroundCategory,
)


class Base(models.Model, IBase):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100, blank=True, null=True)
    category = models.CharField(max_length=50, blank=True, null=True)

    items = None
    currency = None
    containers = None

    class Meta:
        abstract = True

    @property
    def _items_set(self):
        raise NotImplementedError

    @property
    def _currency_set(self):
        raise NotImplementedError

    @property
    def _containers_set(self):
        raise NotImplementedError

    @property
    def currency_str(self):
        return ' '.join(map(str, self._currency_set.all()))

    @property
    def containers_str(self):
        return ' '.join(map(str, self._containers_set.all()))

    @property
    def items_str(self):
        return ' '.join(map(str, self._items_set.all()))

    @property
    def array(self):
        return [
            *map(str, self._currency_set.all()),
            *map(str, self._containers_set.all()),
            *map(str, self._items_set.all()),
        ]

    def __str__(self):
        return f'{self.category}: {self.title} ({self.description})'


UNIQUE_REWARD_TYPE = {}


class DurationReward(models.Model, IDurationReward):
    from_timedelta = models.DurationField(default=0)
    to_timedelta = models.DurationField(default=0)
    multiplicity = models.IntegerField(default=1, validators=[MinValueValidator(1)])

    class Meta:
        db_table = 'duration_rewards'

    def clean(self):
        super().clean()
        if self.to_timedelta < self.from_timedelta:
            raise ValidationError({
                'to_amount': 'Max amount must not be less then min amount'
            })
        if self.to_timedelta.total_seconds() < self.multiplicity:
            raise ValidationError({
                'to_amount': 'If max amount is less then multiplicity amount will always be 0'
            })


class Reward(Base, IReward):
    experience = models.IntegerField(default=0)
    items = models.ManyToManyField('inventory.Item', through='RewardItem', blank=True)
    currency = models.ManyToManyField('inventory.Currency', through='RewardCurrency', blank=True)
    containers = models.ManyToManyField('inventory.Container', through='RewardContainer', blank=True)
    title_category = models.ForeignKey('RewardTitle', models.SET_NULL, blank=True, null=True)
    role_category = models.ForeignKey('RewardRoleCategory', models.SET_NULL, blank=True, null=True)
    background_category = models.ForeignKey('RewardBackgroundCategory', models.SET_NULL, blank=True, null=True)
    emotion_category = models.ForeignKey('RewardEmotionCategory', models.SET_NULL, blank=True, null=True)

    personal_role = models.ForeignKey(DurationReward, models.SET_NULL, related_name='+', blank=True, null=True)
    private_channel = models.ForeignKey(DurationReward, models.SET_NULL, related_name='+', blank=True, null=True)

    type = EnumField(RewardType, blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if str(self.type) in UNIQUE_REWARD_TYPE:
            if Reward.objects.exclude(id=self.id).filter(type=self.type).exists():
                raise ValidationError(f'Reward with type "{self.type}" is already exists')
        super().save(force_insert, force_update, using, update_fields)

    @property
    def _items_set(self):
        return self.rewarditem_set

    @property
    def _currency_set(self):
        return self.rewardcurrency_set

    @property
    def _containers_set(self):
        return self.rewardcontainer_set

    @classmethod
    def get_empty_result(cls):
        return {
            'experience': 0,
            'items': defaultdict(lambda: 0),
            'containers': defaultdict(lambda: 0),
            'currency': defaultdict(lambda: 0),
            'roles': {},
            'emotions': [],
            'backgrounds': [],
            'titles': [],
            'personal_role_duration': 0,
            'private_channel_duration': 0,
        }

    @staticmethod
    def calc_category(category, _type, user, count, result):
        if category is None:
            return result
        amount = category.get_amount_for_count(count)
        if amount == 0:
            return result
        items: List[int] = list(category.items.exclude(
            id__in=result[_type]
        ).exclude(user=user).values_list("id", flat=True))
        items = random.sample(items, min(len(items), amount))
        result[_type] += items
        if category.alternative_reward is not None:
            for reward, _count in category.alternative_reward.get_rewards(amount - len(items)).items():
                result = reward.calc(user, count=_count, result=result)
        return result

    def calc_role_category(self, user, count, result):
        if self.role_category is None:
            return result
        amount = self.role_category.get_amount_for_count(count)
        if amount == 0:
            return result
        permanent_roles = [
            role_id
            for role_id, duration in result['roles'].items()
            if not duration
        ]
        items: List[int] = list(
            self.role_category.items.exclude(id__in=permanent_roles).exclude(user=user).values_list("id", flat=True)
        ) * count
        items = random.sample(items, min(len(items), amount))
        for role_id in items:
            result['roles'][role_id] = result['roles'].get(role_id, 0) + self.role_category.duration.total_seconds()
        if self.role_category.alternative_reward is not None:
            for reward, _count in self.role_category.alternative_reward.get_rewards(amount - len(items)).items():
                result = reward.calc(user, count=_count, result=result)
        return result

    def calc_title_category(self, user, count, result):
        if not self.title_category:
            return result
        amount = self.title_category.get_amount_for_count(count)
        if amount == 0:
            return result
        items: List[int] = [
            item.id
            for item in self.title_category.items.exclude(id__in=result['titles']).only('id')
            if not UserTitle.objects.filter(user=user, title_id=item.id).exists()
        ]
        items = random.sample(items, min(len(items), amount))
        result['titles'] += items
        alternative_reward = self.title_category.alternative_reward
        if alternative_reward is not None:
            for reward, _count in alternative_reward.get_rewards(amount - len(items)).items():
                result = reward.calc(user, count=_count, result=result)
        return result

    def calc(self, user, count=1, result=None):
        if result is None:
            result = self.get_empty_result()
        result['experience'] += self.experience * count

        reward_category: BaseRewardCategory
        for _type, reward_category in [
            ('emotions', self.emotion_category),
            ('backgrounds', self.background_category),
        ]:
            result = self.calc_category(reward_category, _type, user, count, result)

        result = self.calc_title_category(user, count, result)
        result = self.calc_role_category(user, count, result)

        for _type, queryset in [
            ('items', self.rewarditem_set),
            ('currency', self.rewardcurrency_set),
            ('containers', self.rewardcontainer_set),
        ]:
            for reward_item in queryset.all():
                amount = reward_item.get_amount_for_count(count)
                old_count = result[_type].get(reward_item.item_id, 0)
                result[_type][reward_item.item_id] = old_count + amount

        if self.personal_role_id:
            result['personal_role_duration'] += self.personal_role.get_duration_for_count(count)
        if self.private_channel_id:
            result['private_channel_duration'] += self.private_channel.get_duration_for_count(count)

        return result


class Price(Base, IPrice):
    items = models.ManyToManyField('inventory.Item', through='PriceItem', blank=True)
    currency = models.ManyToManyField('inventory.Currency', through='PriceCurrency', blank=True)
    containers = models.ManyToManyField('inventory.Container', through='PriceContainer', blank=True)

    @property
    def _items_set(self):
        return self.priceitem_set

    @property
    def _currency_set(self):
        return self.pricecurrency_set

    @property
    def _containers_set(self):
        return self.pricecontainer_set

    @staticmethod
    def _take_item(reward_queryset, user, user_queryset):
        reward_queryset = reward_queryset.select_related('item').all()
        user_items = {
            user_item.item_id: user_item
            for user_item in user_queryset.filter(
                user=user, item_id__in=[reward_item.item_id for reward_item in reward_queryset]
            ).only('amount', 'item_id').select_for_update()
        }
        for reward_item in reward_queryset:
            amount = reward_item.amount
            item = reward_item.item
            user_item = user_items.get(item.id)
            if not user_item or user_item.amount < amount:
                yield {
                    'type': type(item)._meta.label,
                    'id': item.id,
                    'amount': amount - user_item.amount if user_item else amount
                }
            else:
                user_item.amount = models.F('amount') - amount
                user_item.save(update_fields=['amount'])

    def take(self, user):
        errors = [
            *self._take_item(self._items_set, user, UserItem.objects),
            *self._take_item(self._currency_set, user, UserCurrency.objects),
            *self._take_item(self._containers_set, user, UserContainer.objects),
        ]
        if errors:
            raise ValueError(errors)


class ChancedReward(models.Model, IChancedReward):
    chance = models.PositiveSmallIntegerField(validators=[MaxValueValidator(100)])
    reward = models.ForeignKey(Reward, models.CASCADE)
    reward_list = models.ForeignKey('RewardList', models.CASCADE)


class RewardList(models.Model, IRewardList):
    rewards = models.ManyToManyField(Reward, through=ChancedReward)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100, blank=True, null=True)
    category = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return f'{self.category}: {self.title} ({self.description})'

    def _rewards(self):
        if hasattr(self, '_cached_rewards'):
            return self._cached_rewards
        self._cached_rewards = self.chancedreward_set.select_related(
            'reward',
        ).order_by('-chance')
        return self._cached_rewards

    def get_reward(self) -> Reward:
        rewards: models.QuerySet = self._rewards()
        rand = random.randint(0, 100)
        acc = 0
        for chanced_reward in rewards:
            acc += chanced_reward.chance
            if acc >= rand:
                return chanced_reward.reward

    def get_rewards(self, count) -> Counter:
        return Counter([
            self.get_reward()
            for _ in range(count)
        ])


class LevelReward(models.Model, ILevelReward):
    level = models.PositiveIntegerField()
    exact = models.BooleanField(default=False)
    reward = models.ForeignKey(Reward, models.CASCADE)

    class Meta:
        unique_together = ['level', 'exact']

    @classmethod
    def get_rewards(cls, level) -> List[Reward]:
        rewards = []
        exact_level_rewards = cls.objects.filter(exact=True, level=level).select_related("reward")
        if exact_level_rewards.exists():
            rewards.append(exact_level_rewards[0].reward)
        modules_level_rewards = cls.objects.filter(exact=False).all()
        for level_reward in modules_level_rewards:
            if level % level_reward.level == 0:
                rewards.append(level_reward.reward)
        return rewards


class BasePriceItem(models.Model, IBasePriceItem):
    item = None
    amount = models.IntegerField(default=0)
    price = models.ForeignKey(Price, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def __str__(self):
        return f'<{self.item.icon}> {self.amount}'


class BaseRewardItem(RandomAmount, IBaseRewardItem):
    item: BaseItem = None
    reward = models.ForeignKey(Reward, on_delete=models.CASCADE)

    class Meta:
        abstract = True
        unique_together = ['reward', 'item']

    def __str__(self):
        return f'<{self.item.icon}> {self.amount}'


class BaseRewardCategory(RandomAmount, IBaseRewardCategory):
    name = models.CharField(max_length=100)
    alternative_reward = models.ForeignKey(RewardList, on_delete=models.SET_NULL, null=True, blank=True)
    items = None

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self.name} {self.amount}'


class PriceItem(BasePriceItem, IPriceItem):
    item = models.ForeignKey('inventory.Item', on_delete=models.CASCADE)


class PriceCurrency(BasePriceItem, IPriceCurrency):
    item = models.ForeignKey('inventory.Currency', on_delete=models.CASCADE)


class PriceContainer(BasePriceItem, IPriceContainer):
    item = models.ForeignKey('inventory.Container', on_delete=models.CASCADE)


class RewardItem(BaseRewardItem, IRewardItem):
    item = models.ForeignKey('inventory.Item', on_delete=models.CASCADE)


class RewardCurrency(BaseRewardItem, IRewardCurrency):
    item = models.ForeignKey('inventory.Currency', on_delete=models.CASCADE)


class RewardContainer(BaseRewardItem, IRewardContainer):
    item = models.ForeignKey('inventory.Container', on_delete=models.CASCADE)


class RewardEmotionCategory(BaseRewardCategory, IRewardEmotionCategory):
    items = models.ManyToManyField('inventory.Emotion', related_name='+')


class RewardTitle(BaseRewardCategory, IRewardTitle):
    items = models.ManyToManyField('inventory.Title', related_name='+')


class RewardRoleCategory(BaseRewardCategory, IRewardRoleCategory):
    items = models.ManyToManyField('inventory.Role', related_name='+')
    duration = models.DurationField(default=timedelta(0))


class RewardBackgroundCategory(BaseRewardCategory, IRewardBackgroundCategory):
    items = models.ManyToManyField('inventory.Background', related_name='+')
