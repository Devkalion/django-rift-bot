from rest_framework import serializers

from common.serializers import UserSerializer


class LevelSerializer(UserSerializer):
    level = serializers.IntegerField(required=True)
