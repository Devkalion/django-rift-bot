from datetime import datetime, timedelta, timezone
from unittest import TestCase, mock

from django.db import transaction
from rest_framework.test import APIClient

from inventory.models import (
    Currency,
    Container,
    Item,
    Role,
    TempRole,
    Title,
    UserContainer,
    UserCurrency,
    UserItem,
    UserTitle,
    Emotion,
    PersonalRole,
)
from reward.models import (
    ChancedReward,
    LevelReward,
    PriceContainer,
    PriceCurrency,
    PriceItem,
    Reward,
    RewardList,
    RewardCurrency,
    RewardContainer,
    RewardEmotionCategory,
    RewardItem,
    RewardRoleCategory,
    RewardTitle,
    Price,
    DurationReward,
)
from reward.utils import ResultReward
from schedule.celery import CustomTask
from user.models import User
from user.services import UserService

api_client = APIClient()
CustomTask.apply_async = mock.Mock()


class RewardTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=121,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.role: Role = Role.objects.create(discord_id=123)
            cls.currency: Currency = Currency.objects.create(index_number=666)
            cls.container: Container = Container.objects.create(index_number=666)
            cls.item: Item = Item.objects.create(index_number=666)
            cls.emotion: Emotion = Emotion.objects.create()
            cls.title: Title = Title.objects.create()
            cls.title_category = RewardTitle.objects.create(from_amount=1, to_amount=1)
            cls.title_category.items.add(cls.title)
            cls.emotion_category = RewardEmotionCategory.objects.create(from_amount=1, to_amount=1)
            cls.emotion_category.items.add(cls.emotion)
            cls.role_category = RewardRoleCategory.objects.create(from_amount=1, to_amount=1)
            cls.role_category.items.add(cls.role)
            cls.temp_role_category = RewardRoleCategory.objects.create(
                from_amount=1, to_amount=1, duration=timedelta(seconds=50)
            )
            cls.temp_role_category.items.add(cls.role)

            cls.reward_list = RewardList.objects.create()
            cls.alternative_reward = Reward.objects.create(experience=7)
            ChancedReward.objects.create(reward=cls.alternative_reward, reward_list=cls.reward_list, chance=100)

            cls.reward = Reward.objects.create(experience=5)

    def test_calc_items(self):
        reward_currency = RewardCurrency.objects.create(
            reward=self.reward, item=self.currency, from_amount=100, to_amount=100
        )
        reward_container = RewardContainer.objects.create(
            reward=self.reward, item=self.container, from_amount=10, to_amount=10
        )
        reward_item = RewardItem.objects.create(reward=self.reward, item=self.item, from_amount=50, to_amount=50)

        self.addCleanup(reward_currency.delete)
        self.addCleanup(reward_container.delete)
        self.addCleanup(reward_item.delete)

        reward = self.reward.calc(self.user)
        self.assertEquals(reward["experience"], 5)
        self.assertEquals(reward["items"][self.item.pk], 50)
        self.assertEquals(reward["containers"][self.container.pk], 10)
        self.assertEquals(reward["currency"][self.currency.pk], 100)

    def test_calc_multiple_items(self):
        reward_item = RewardItem.objects.create(reward=self.reward, item=self.item, from_amount=1, to_amount=1)
        self.addCleanup(reward_item.delete)
        reward = self.reward.calc(self.user, 2)
        self.assertEquals(reward["experience"], 10)
        self.assertEquals(reward["items"][self.item.pk], 2)

    def test_calc_role(self):
        self.reward.role_category = self.role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        reward = self.reward.calc(self.user)
        self.assertIn(self.role.pk, reward["roles"])
        self.assertFalse(reward["roles"][self.role.pk])

    def test_calc_role_alternative_reward(self):
        self.role_category.alternative_reward = self.reward_list
        self.addCleanup(setattr, self.role_category, "alternative_reward", None)

        self.reward.role_category = self.role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        UserService.add_roles(self.user, self.role)
        self.addCleanup(self.user.roles.clear)

        reward = self.reward.calc(self.user)
        self.assertFalse(reward["roles"])
        self.assertEquals(reward["experience"], 12)

    def test_calc_role_without_alternative_reward(self):
        self.reward.role_category = self.role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        UserService.add_roles(self.user, self.role)
        self.addCleanup(self.user.roles.clear)

        reward = self.reward.calc(self.user)
        self.assertFalse(reward["roles"])

    def test_calc_temp_role(self):
        self.reward.role_category = self.temp_role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        reward = self.reward.calc(self.user)
        self.assertIn(self.role.pk, reward["roles"])
        self.assertEquals(reward["roles"][self.role.pk], 50)

    def test_calc_personal_role(self):
        reward_duration = DurationReward.objects.create(
            from_timedelta=timedelta(hours=3),
            to_timedelta=timedelta(hours=4),
            multiplicity=3600,
        )
        self.reward.personal_role = reward_duration
        self.reward.personal_role_id = reward_duration.pk
        self.addCleanup(reward_duration.delete)

        reward = self.reward.calc(self.user, count=2)
        self.assertIn(reward["personal_role_duration"], (6 * 3600, 7 * 3600, 8 * 3600))

    def test_calc_private_channel(self):
        reward_duration = DurationReward.objects.create(
            from_timedelta=timedelta(hours=3),
            to_timedelta=timedelta(hours=4),
            multiplicity=3600,
        )
        self.reward.private_channel = reward_duration
        self.reward.private_channel_id = reward_duration.pk
        self.addCleanup(reward_duration.delete)

        reward = self.reward.calc(self.user, count=2)
        self.assertIn(reward["private_channel_duration"], (6 * 3600, 7 * 3600, 8 * 3600))

    def test_calc_existing_temp_role(self):
        self.reward.role_category = self.temp_role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        temp_role = TempRole.objects.create(
            role=self.role, user=self.user, to_date=datetime.now(timezone.utc) + timedelta(hours=1)
        )
        self.addCleanup(temp_role.delete)

        reward = self.reward.calc(self.user)
        self.assertIn(self.role.pk, reward["roles"])

    def test_calc_temp_role_with_permanent(self):
        self.reward.role_category = self.temp_role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        UserService.add_roles(self.user, self.role)
        self.addCleanup(self.user.roles.clear)

        reward = self.reward.calc(self.user)
        self.assertFalse(reward["roles"])

    def test_calc_role_with_temp(self):
        self.reward.role_category = self.temp_role_category
        self.addCleanup(setattr, self.reward, "role_category", None)

        reward = self.reward.calc(self.user, 2)
        self.assertIn(self.role.pk, reward["roles"])
        self.assertEquals(reward["roles"][self.role.pk], 100)

    def test_calc_title(self):
        self.reward.title_category = self.title_category
        self.addCleanup(setattr, self.reward, "title_category", None)

        reward = self.reward.calc(self.user)
        self.assertEquals(reward["titles"], [self.title.pk])

    def test_calc_title_alternative_reward(self):
        self.reward.title_category = self.title_category
        self.addCleanup(setattr, self.reward, "title_category", None)

        self.title_category.alternative_reward = self.reward_list
        self.addCleanup(setattr, self.title_category, "alternative_reward", None)

        user_title = UserTitle.objects.create(user=self.user, title=self.title)
        self.addCleanup(user_title.delete)

        reward = self.reward.calc(self.user)
        self.assertEquals(reward["titles"], [])
        self.assertEquals(reward["experience"], 12)

    def test_calc_emotion_category(self):
        self.reward.emotion_category = self.emotion_category
        self.addCleanup(setattr, self.reward, "emotion_category", None)

        reward = self.reward.calc(self.user)
        self.assertEquals(reward["emotions"], [self.emotion.pk])

    def test_calc_emotion_category_alternative_reward(self):
        self.reward.emotion_category = self.emotion_category
        self.addCleanup(setattr, self.reward, "emotion_category", None)

        self.emotion_category.alternative_reward = self.reward_list
        self.addCleanup(setattr, self.emotion_category, "alternative_reward", None)

        self.user.emotions.add(self.emotion)
        self.addCleanup(self.user.emotions.clear)

        reward = self.reward.calc(self.user)
        self.assertEquals(reward["emotions"], [])

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.role.delete()
        cls.currency.delete()
        cls.container.delete()
        cls.emotion.delete()
        cls.item.delete()
        cls.title.delete()
        cls.title_category.delete()
        cls.role_category.delete()
        cls.emotion_category.delete()
        cls.reward_list.delete()
        cls.alternative_reward.delete()
        cls.reward.delete()


class PriceTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=121,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.currency: Currency = Currency.objects.create(index_number=666)
            cls.container: Container = Container.objects.create(index_number=666)
            cls.item: Item = Item.objects.create(index_number=666)
            cls.price: Price = Price.objects.create()
            UserCurrency.objects.create(user=cls.user, item=cls.currency, amount=10)
            UserItem.objects.create(user=cls.user, item=cls.item, amount=10)
            UserContainer.objects.create(user=cls.user, item=cls.container, amount=10)
            cls.price_item: PriceItem = PriceItem.objects.create(price=cls.price, item=cls.item, amount=2)
            cls.price_currency: PriceCurrency = PriceCurrency.objects.create(
                price=cls.price, item=cls.currency, amount=3
            )
            cls.price_container: PriceContainer = PriceContainer.objects.create(
                price=cls.price, item=cls.container, amount=4
            )

    def reset_user_inventory(self):
        UserCurrency.objects.filter(user=self.user, item=self.currency).update(amount=10)
        UserItem.objects.filter(user=self.user, item=self.item).update(amount=10)
        UserContainer.objects.filter(user=self.user, item=self.container).update(amount=10)

    def test_price(self):
        self.price.take(self.user)
        self.addCleanup(self.reset_user_inventory)
        user_item = UserItem.objects.get(user=self.user, item=self.item).amount
        user_currency = UserCurrency.objects.get(user=self.user, item=self.currency).amount
        user_container = UserContainer.objects.get(user=self.user, item=self.container).amount

        self.assertEquals(user_currency, 7)
        self.assertEquals(user_container, 6)
        self.assertEquals(user_item, 8)

    def test_overtake_currency(self):
        queryset = PriceCurrency.objects.filter(price=self.price, item=self.currency)
        queryset.update(amount=16)
        self.addCleanup(queryset.update, amount=3)

        with self.assertRaises(ValueError) as exc:
            self.price.take(self.user)

        self.addCleanup(self.reset_user_inventory)
        exc_detail = exc.exception.args[0]
        self.assertEquals(len(exc_detail), 1)
        self.assertEquals(exc_detail[0]["type"], "inventory.Currency")
        self.assertEquals(exc_detail[0]["id"], self.currency.pk)
        self.assertEquals(exc_detail[0]["amount"], 6)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.currency.delete()
        cls.container.delete()
        cls.item.delete()
        cls.price.delete()


class LevelRewardTestCase(TestCase):
    def test_get_level_rewards(self):
        reward = Reward.objects.create()
        self.addCleanup(reward.delete)
        with transaction.atomic():
            positive = [
                LevelReward.objects.create(reward=reward, exact=True, level=10),
                LevelReward.objects.create(reward=reward, exact=False, level=10),
                LevelReward.objects.create(reward=reward, exact=False, level=5),
            ]

            negative = [
                LevelReward.objects.create(reward=reward, exact=True, level=5),
                LevelReward.objects.create(reward=reward, exact=False, level=4),
            ]
        self.addCleanup(LevelReward.objects.filter(id__in=(lr.pk for lr in positive + negative)).delete)

        rewards = LevelReward.get_rewards(10)
        self.assertEquals(len(rewards), len(positive))


class ResultRewardTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=121,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.role: Role = Role.objects.create(discord_id=123)
            cls.role2: Role = Role.objects.create(discord_id=345)
            cls.role3: Role = Role.objects.create(discord_id=456)
            cls.currency: Currency = Currency.objects.create(index_number=666)
            cls.currency2: Currency = Currency.objects.create(index_number=777)
            UserCurrency.objects.create(user=cls.user, item=cls.currency2, amount=10)
            cls.container: Container = Container.objects.create(index_number=666)
            cls.item: Item = Item.objects.create(index_number=666)
            # cls.emotion: Emotion = Emotion.objects.create()
            # cls.title: Title = Title.objects.create()
            # cls.title_category = RewardTitle.objects.create(from_amount=1, to_amount=1)
            # cls.title_category.items.add(cls.title)
            # cls.emotion_category = RewardEmotionCategory.objects.create(from_amount=1, to_amount=1)
            # cls.emotion_category.items.add(cls.emotion)
            # cls.role_category = RewardRoleCategory.objects.create(from_amount=1, to_amount=1)
            # cls.role_category.items.add(cls.role)
            # cls.temp_role_category = RewardRoleCategory.objects.create(
            #     from_amount=1, to_amount=1, duration=timedelta(seconds=50)
            # )
            # cls.temp_role_category.items.add(cls.role)
            #
            # cls.reward_list = RewardList.objects.create()
            # cls.alternative_reward = Reward.objects.create(experience=7)
            # ChancedReward.objects.create(reward=cls.alternative_reward, reward_list=cls.reward_list, chance=100)
            #
            # cls.reward = Reward.objects.create(experience=5)

    def test_give_items(self):
        reward = Reward.get_empty_result()
        reward["items"] = {self.item.pk: 5}
        reward["currency"] = {self.currency.pk: 6, self.currency2.pk: 6}
        reward["containers"] = {self.container.pk: 7}
        ResultReward.from_dict(reward).give(self.user)
        user_item = UserItem.objects.get(user=self.user, item=self.item)
        self.assertEquals(user_item.amount, 5)
        user_currency = UserCurrency.objects.get(user=self.user, item=self.currency)
        self.assertEquals(user_currency.amount, 6)
        user_currency = UserCurrency.objects.get(user=self.user, item=self.currency2)
        self.assertEquals(user_currency.amount, 16)
        user_container = UserContainer.objects.get(user=self.user, item=self.container)
        self.assertEquals(user_container.amount, 7)

    def test_give_roles(self):
        reward = Reward.get_empty_result()
        reward['roles'] = {
            self.role.pk: None,
            self.role2.pk: 3600,
            self.role3.pk: 3600,
        }

        now = datetime.now()

        TempRole.objects.create(user=self.user, role=self.role3, to_date=now + timedelta(seconds=3600))
        TempRole.objects.create(user=self.user, role=self.role, to_date=now + timedelta(seconds=3600))

        ResultReward.from_dict(reward).give(self.user)
        self.assertTrue(self.user.roles.filter(id=self.role.pk).exists())
        self.assertFalse(TempRole.objects.filter(user=self.user, role=self.role).exists())

        self.assertFalse(self.user.roles.filter(id=self.role2.pk).exists())
        self.assertFalse(self.user.roles.filter(id=self.role2.pk).exists())
        self.assertTrue(TempRole.objects.filter(user=self.user, role=self.role2).exists())
        temp_role = TempRole.objects.get(user=self.user, role=self.role3)
        self.assertEquals(temp_role.to_date.timestamp(), (now + timedelta(seconds=7200)).timestamp())

    def test_give_new_personal_role(self):
        reward = Reward.get_empty_result()
        reward['personal_role_duration'] = 50
        now = datetime.now(timezone.utc)

        ResultReward.from_dict(reward).give(self.user)
        personal_role = PersonalRole.objects.get(user_id=self.user.pk)
        self.addCleanup(personal_role.delete)

        self.assertTrue(50 - (personal_role.end_date - now).total_seconds() < 0.1)

    def test_give_existing_personal_role(self):
        reward = Reward.get_empty_result()
        reward['personal_role_duration'] = 50
        now = datetime.now(timezone.utc)
        personal_role = PersonalRole.objects.create(
            user_id=self.user.pk,
            start_date=now,
            end_date=now + timedelta(seconds=7200),
            name='test personal role',
            color=200,
        )
        self.addCleanup(personal_role.delete)

        ResultReward.from_dict(reward).give(self.user)
        personal_role.refresh_from_db()

        self.assertTrue(50 - (personal_role.end_date - now).total_seconds() < 0.1)

    def test_give_old_personal_role(self):
        reward = Reward.get_empty_result()
        reward['personal_role_duration'] = 50
        now = datetime.now(timezone.utc)
        personal_role = PersonalRole.objects.create(
            user_id=self.user.pk,
            start_date=now - timedelta(days=3),
            end_date=now - timedelta(days=2),
            name='test personal role',
            color=200,
        )
        self.addCleanup(personal_role.delete)

        ResultReward.from_dict(reward).give(self.user)
        personal_role.refresh_from_db()

        self.assertTrue(50 - (personal_role.end_date - now).total_seconds() < 0.1)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.role.delete()
        cls.role2.delete()
        cls.role3.delete()
        cls.user.delete()
        cls.currency.delete()
        cls.currency2.delete()
        cls.container.delete()
        cls.item.delete()
