import json
from datetime import timedelta
from typing import List, Tuple, Type, Union

from django.db.models import F
from django.utils.timezone import now

from events.enums import EventType
from events.services import EventService
from inventory.interfaces import IBaseItem, IItem, ICurrency, IContainer, IRole
from inventory.models import (
    Background,
    Container,
    Currency,
    Emotion,
    Item,
    Role,
    TempRole,
    Title,
    UserTitle,
    UserItem,
    UserCurrency,
    UserContainer,
    BaseUserItem,
    PersonalRole,
    PersonalRoleProxy,
    PersonalChannel,
)
from user.interfaces import IUser
from user.models import User
from user.services import UserService


class _CountItems(list):
    def __str__(self):
        return ' '.join((
            f'<{item.icon}> {count}' for item, count in self
            if count > 0
        ))


class _CategoryItems(list):
    @staticmethod
    def item_string(item):
        if item.category and item.category.icon:
            return f'<{item.category.icon}> {item.name}'
        return item.name

    def __str__(self):
        return ' '.join(map(self.item_string, self))


class Emotions(_CategoryItems):
    @staticmethod
    def item_string(item: Emotion):
        if item.category and item.category.icon:
            return f'<{item.category.icon}> [{item.name}]({item.image_url})'
        return f'[{item.name}]({item.image_url})'


class Backgrounds(_CategoryItems):
    @staticmethod
    def item_string(item: Background):
        if item.category and item.category.icon:
            return f'<{item.category.icon}> [{item.name}]({item.image_url})'
        return f'[{item.name}]({item.image_url})'


class Titles(_CategoryItems):
    @staticmethod
    def item_string(item: Title):
        if item.icon:
            return f'<{item.icon}> Титул «{item.name}»'
        return f'Титул «{item.name}»'


class _Role(list):
    def __str__(self):
        from common.tags import pretty_time

        return ' '.join((
            ' '.join(filter(None, (
                f'<{role.category.icon}>' if role.category and role.category.icon else None,
                'Роль',
                role.name,
                pretty_time(time) if time else None
            )))
            for role, time in self
        ))


class ResultReward:
    def __init__(self):
        self.experience = 0
        self.items: List[Tuple[IItem, int]] = _CountItems()
        self.currency: List[Tuple[ICurrency, int]] = _CountItems()
        self.containers: List[Tuple[IContainer, int]] = _CountItems()
        self.roles: List[Tuple[IRole, int]] = _Role()
        self.emotions: List[Emotion] = Emotions()
        self.titles: List[Title] = Titles()
        self.backgrounds: List[Background] = Backgrounds()
        self.personal_role_duration: int = 0
        self.private_channel_duration: int = 0

    @classmethod
    def from_dict(cls, reward: dict):
        self = cls()
        self.experience = reward['experience']
        self.personal_role_duration = reward['personal_role_duration']
        self.private_channel_duration = reward['private_channel_duration']

        for attr in ['items', 'currency', 'containers']:
            reward[attr].pop('default_factory', None)

        self.items = _CountItems(
            (item, reward['items'][item.id if item.id in reward['items'] else str(item.id)])
            for item in Item.objects.filter(id__in=list(reward['items'].keys()))
        )
        self.currency = _CountItems(
            (item, reward['currency'][item.id if item.id in reward['currency'] else str(item.id)])
            for item in Currency.objects.filter(id__in=list(reward['currency'].keys()))
        )
        self.containers = _CountItems(
            (item, reward['containers'][item.id if item.id in reward['containers'] else str(item.id)])
            for item in Container.objects.filter(id__in=list(reward['containers'].keys()))
        )
        if reward['roles']:
            self.roles = _Role([
                (role, reward['roles'].get(role.id, reward['roles'].get(str(role.id))))
                for role in Role.objects.filter(id__in=reward['roles'])
            ])
        if reward['emotions']:
            self.emotions = Emotions(Emotion.objects.filter(id__in=reward['emotions']))
        if reward['titles']:
            self.titles = Titles(Title.objects.filter(id__in=reward['titles']))
        if reward['backgrounds']:
            self.backgrounds = Backgrounds(Background.objects.filter(id__in=reward['backgrounds']))

        return self

    def without_exp(self):
        return ' '.join(filter(None, [
            str(self.currency),
            str(self.containers),
            str(self.items),
            str(self.roles),
            str(self.backgrounds),
            str(self.emotions),
            str(self.titles),
        ]))

    @staticmethod
    def give_items(user: IUser, user_item_class: Type[BaseUserItem], items: List[Tuple[IBaseItem, int]]):
        if not items:
            return
        queryset = user_item_class.objects.filter(
            user_id=user.pk, item_id__in=[i.pk for i, _ in items]
        ).only('item_id')
        user_items = {
            user_item.item_id: user_item
            for user_item in queryset
        }
        missing_items = []

        for item, amount in items:
            user_item = user_items.get(item.pk)
            if user_item:
                user_item.amount = F('amount') + amount
            else:
                missing_items.append(user_item_class(item=item, amount=amount, user=user))

        if user_items:
            user_item_class.objects.bulk_update(user_items.values(), ['amount'])

        if missing_items:
            user_item_class.objects.bulk_create(missing_items)

    def give_temp_roles(self, user: IUser, role_reason: str):
        temp_roles = [(role, time) for role, time in self.roles if time]
        queryset = TempRole.objects.filter(
            user=user, role_id__in=[role.pk for role, _ in temp_roles]
        ).select_for_update().only('role_id', 'to_date')
        existing_temp_roles = {
            tr.role_id: tr
            for tr in queryset
        }
        missing_temp_roles = []

        for role, time in temp_roles:
            temp_role = existing_temp_roles.get(role.pk)
            if temp_role is None:
                missing_temp_roles.append(TempRole(user=user, role=role, to_date=now() + timedelta(seconds=time)))
                EventService.create_event(
                    EventType.GRANT_USER_ROLE,
                    json.dumps({"role_id": int(role.discord_id), "reason": role_reason}),
                    user.pk,
                )
            else:
                temp_role.to_date += timedelta(seconds=time)

        if existing_temp_roles:
            TempRole.objects.bulk_update(existing_temp_roles.values(), ['to_date'])

        if missing_temp_roles:
            TempRole.objects.bulk_create(missing_temp_roles)

    def give_roles(self, user: IUser, role_reason: str):
        permanent_roles = [role for role, time in self.roles if not time]
        UserService.add_roles(user, *permanent_roles)
        for role in permanent_roles:
            EventService.create_event(
                EventType.GRANT_USER_ROLE,
                json.dumps({"role_id": int(role.discord_id), "reason": role_reason}),
                user.pk,
            )
        if len(self.roles) != len(permanent_roles):
            self.give_temp_roles(user, role_reason)

    def extend_personal_item(self, item: Union[PersonalRoleProxy, PersonalChannel]):
        if item.end_date is None:
            return
        item.end_date = max(item.end_date, now()) + timedelta(seconds=self.personal_role_duration)
        item.save(update_fields=('end_date',))

    def give_or_extend_personal_role(self, user: IUser) -> None:
        personal_role = PersonalRoleProxy.objects.filter(user_id=user.pk).first()
        if personal_role:
            self.extend_personal_item(personal_role)
        else:
            start_date = now()
            PersonalRoleProxy.objects.create(
                user_id=user.pk,
                start_date=start_date,
                end_date=start_date + timedelta(seconds=self.personal_role_duration),
                name=f'Личная роль {user.name}',
                color=200,
            )

    def give_or_extend_private_channel(self, user: IUser) -> None:
        personal_channel = PersonalChannel.objects.filter(user_id=user.pk).first()
        if personal_channel:
            self.extend_personal_item(personal_channel)
        else:
            start_date = now()
            PersonalChannel.objects.create(
                name=f'Приватный канал {user.name}',
                description=f'Приватный канал {user.name}',
                creator=user.pk,
                end_date=start_date + timedelta(seconds=self.private_channel_duration),
            )

    def give(self, user: IUser, role_reason: str = None):
        user = User.objects.only(
            'experience'
        ).select_for_update().get(pk=user.pk)

        if self.experience:
            UserService.add_experience(user, self.experience)

        self.give_items(user, UserItem, self.items)
        self.give_items(user, UserCurrency, self.currency)
        self.give_items(user, UserContainer, self.containers)

        if self.titles:
            UserTitle.objects.bulk_create((
                UserTitle(user=user, title=title)
                for title in self.titles
            ))

        if self.emotions:
            user.emotions.add(*self.emotions)

        if self.backgrounds:
            user.backgrounds.add(*self.backgrounds)

        if self.personal_role_duration:
            self.give_or_extend_personal_role(user)

        self.give_roles(user, role_reason)

    def __str__(self):
        if self.experience:
            return ' '.join((
                str(self.experience),
                self.without_exp()
            ))
        return self.without_exp()
