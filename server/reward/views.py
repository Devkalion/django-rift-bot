from typing import List, Optional

from django.db import transaction
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import Reward, LevelReward
from .serializers import UserSerializer, LevelSerializer
from .utils import ResultReward


class RewardViewSet(GenericViewSet):
    queryset = Reward.objects.all()

    @action(methods=['post'], detail=False, serializer_class=LevelSerializer)
    def give_level(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        rewards: List[Reward] = LevelReward.get_rewards(serializer.data['level'])
        result: Optional[dict] = None
        for reward in rewards:
            result = reward.calc(user, result=result)

        result_reward: ResultReward = ResultReward.from_dict(result)

        with transaction.atomic():
            result_reward.give(user)

        return Response(result)
