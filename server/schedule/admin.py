from django.contrib.admin import register

from common.admin import ReadOnlyModelAdmin
from .models import Task


@register(Task)
class TaskAdmin(ReadOnlyModelAdmin):
    list_filter = ['module', 'name', 'launch_time']
    list_display = ['module', 'name', 'launch_time']
    ordering = ['launch_time']
