import json
import os
from datetime import datetime, timedelta
from uuid import uuid4

import celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')


class CustomTask(celery.Task):
    def create_db_task(self, launch_time, args, kwargs):
        from .models import Task
        params = None
        if args or kwargs:
            params = {
                'args': args,
                'kwargs': kwargs,
            }
        Task.objects.create(
            name=self.name.split('.')[-1],
            module='.'.join(self.name.split('.')[:-1]),
            kwargs=json.dumps(params, ensure_ascii=False) if params else None,
            launch_time=launch_time,
        )

    def apply_async(self, args=None, kwargs=None, task_id=None, producer=None,
                    link=None, link_error=None, shadow=None, **options):
        countdown: float = options.get('countdown', 0)
        launch_time: datetime = options.get('eta', datetime.now() + timedelta(seconds=countdown))
        if launch_time - datetime.now(tz=launch_time.tzinfo) >= timedelta(minutes=5):
            self.create_db_task(launch_time, args, kwargs)
            if not task_id:
                task_id = str(uuid4())
            return options.get('result_cls', self.AsyncResult)(task_id)
        else:
            return super().apply_async(args, kwargs, task_id, producer, link, link_error, shadow, **options)


app = celery.Celery('rift-bot', task_cls=CustomTask)
app.conf.task_ignore_result = True
app.conf.task_store_errors_even_if_ignored = True

# Bug for long tasks https://docs.celeryproject.org/en/stable/getting-started/backends-and-brokers/redis.html#id1
# app.conf.broker_transport_options = {
#     'visibility_timeout': 7776000  # 90 days
# }

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
