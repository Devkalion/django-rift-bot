from datetime import datetime
from typing import Optional

from common.interfaces import IModelIntId


class ITask(IModelIntId):
    module: str
    name: str
    kwargs: Optional[str]  # JSON
    launch_time: datetime
