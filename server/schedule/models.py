from django.db import models

from .interfaces import ITask


class Task(models.Model, ITask):
    module = models.CharField(max_length=60)
    name = models.CharField(max_length=60)
    kwargs = models.TextField(blank=True, null=True)  # JSON
    launch_time = models.DateTimeField()
