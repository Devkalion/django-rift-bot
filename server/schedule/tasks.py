import importlib
import json
from datetime import timedelta

from celery.signals import worker_ready
from django.db import transaction
from django.utils.timezone import now

from .celery import app
from .models import Task


def launch_task(task_id):
    queryset = Task.objects.select_for_update(skip_locked=True)
    with transaction.atomic():
        task = queryset.get(pk=task_id)
        task.delete()
    kwargs = {}
    if task.kwargs:
        kwargs = json.loads(task.kwargs)
    module = importlib.import_module(task.module)
    function = getattr(module, task.name)
    function.apply_async(eta=task.launch_time, **kwargs)


@app.task
def schedule_tasks():
    limit_time = now() + timedelta(minutes=45)
    for task_id in Task.objects.filter(launch_time__lte=limit_time).values_list('id', flat=True):
        launch_task(task_id)


@worker_ready.connect
def at_start(*args, **kwargs):
    schedule_tasks()
