try:
    from local_settings import *
except ImportError:
    from settings import *

DEBUG = True
LOGGING = None

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': f'{BASE_DIR}/rift-bot-test.sqlite3',
        'OPTIONS': {
            'timeout': 20,
        }
    },
}
