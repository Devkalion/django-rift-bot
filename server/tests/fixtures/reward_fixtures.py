import pytest

from reward.models import Reward


@pytest.fixture()
def reward():
    return Reward.objects.create(experience=5)
