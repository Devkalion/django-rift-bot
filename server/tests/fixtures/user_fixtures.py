from datetime import datetime, timezone

import pytest

from user.models import User


@pytest.fixture()
def user():
    return User.objects.create(
        discord_id=1,
        discord_registration_date=datetime.now(timezone.utc),
        registration_date=datetime.now(timezone.utc),
        last_activity_time=datetime.now(timezone.utc),
    )
