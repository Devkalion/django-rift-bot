import pytest
from django.core.exceptions import ValidationError

from embeds.models import Embed
from embeds.services import EmbedService


@pytest.mark.django_db
def test_embed_validation():
    # arrange
    embed = Embed(name="wrong_template", description="some", template='{"}"')

    # act
    with pytest.raises(ValidationError) as excinfo:
        embed.clean()

    # assert
    assert str(excinfo.value) != ""


@pytest.mark.django_db
def test_embed_render_example():
    # arrange
    embed = Embed(name="some", description="some", template='{"some":  "value"}')

    # act
    result = EmbedService.render_example(embed)

    # assert
    assert result == '{"some": "value"}'


@pytest.mark.django_db
def test_embed_render():
    # arrange
    embed = Embed(name="some", description="some", template='{"some":  "value"}')

    # act
    result = EmbedService.render_embed(embed, {})

    # assert
    assert result == '{"some": "value"}'


@pytest.mark.django_db
def test_embed_render_with_variable():
    # arrange
    embed = Embed(name="some", description="some", template='{"say" : "Hi {{ username }}!"}')

    # act
    result = EmbedService.render_embed(embed, {'username': 'John'})

    # assert
    assert result == '{"say": "Hi John!"}'
