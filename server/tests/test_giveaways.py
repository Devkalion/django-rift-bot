from datetime import timedelta
from unittest import mock

import pytest

from giveaways.models import Draw, DrawReward
from giveaways.services import DrawService
from reward.models import Reward
from schedule.celery import CustomTask
from user.models import User


@pytest.mark.django_db
def test_draw_start():
    # arrange
    draw = Draw.objects.create(duration=timedelta(hours=1))
    published_events = []

    # act
    with mock.patch.object(
            CustomTask,
            'apply_async',
            side_effect=lambda *args, **kwargs: published_events.append({"args": args, "kwargs": kwargs})
    ):
        DrawService.start(draw)

    # assert
    assert len(published_events) == 2
    assert published_events[1]['kwargs']['args'] == [draw.pk]


@pytest.mark.django_db
@mock.patch.object(CustomTask, 'apply_async')
def test_draw_clear_on_start(mock_method, user: User):
    # arrange
    draw = Draw.objects.create(duration=timedelta(hours=1))
    draw_reward = DrawReward.objects.create(draw=draw)
    draw_reward.winners.add(user)
    draw.participants.add(user)

    # act
    DrawService.start(draw)
    draw.refresh_from_db()
    draw_reward.refresh_from_db()

    # assert
    assert draw_reward.winners.count() == 0
    assert draw.participants.count() == 0


@pytest.mark.django_db
@mock.patch.object(CustomTask, 'apply_async')
def test_draw_finish(_, user: User, reward: Reward):
    # arrange
    draw = Draw.objects.create(duration=timedelta(hours=1))
    draw_reward = DrawReward.objects.create(draw=draw, reward=reward)
    draw.participants.add(user)

    # act
    with mock.patch.object(Reward, 'calc', mock.MagicMock(return_value=Reward.get_empty_result())) as f:
        DrawService.finish(draw.pk)

    # assert
    assert list(draw_reward.winners.values_list('discord_id', flat=True)) == [user.pk]
    f.assert_called_once_with(user)
