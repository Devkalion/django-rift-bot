from unittest import mock

import pytest

from events.models import ActiveJourney
from events.tasks import give_journey_reward
from reward.models import Reward
from schedule.celery import CustomTask
from transformations.models import JourneyItem
from user.models import User


@pytest.mark.django_db
def test_give_reward(user: User, reward: Reward):
    # arrange
    journey: JourneyItem = JourneyItem.objects.create(reward=reward)
    with mock.patch.object(CustomTask, 'apply_async'):
        process: ActiveJourney = ActiveJourney.objects.create(item=journey, user=user)
    published_events = []

    # act
    with mock.patch.object(
            CustomTask,
            'apply_async',
            side_effect=lambda *args, **kwargs: published_events.append({"args": args, "kwargs": kwargs})
    ):
        give_journey_reward(journey_id=process.id)
        user.refresh_from_db()

    # assert
    assert user.experience == reward.experience
    assert not ActiveJourney.objects.filter(id=process.id).exists()
    assert len(published_events) == 1
