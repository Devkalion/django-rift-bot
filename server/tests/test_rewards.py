import pytest

from inventory.models import BaseItem, Container, Currency, Item
from reward.models import BaseRewardItem, Reward, RewardContainer, RewardCurrency, RewardItem
from user.interfaces import IUser


@pytest.mark.django_db
@pytest.mark.parametrize('reward_key, ItemModel, RewardItemModel', [
    pytest.param('currency', Currency, RewardCurrency, id='currency'),
    pytest.param('items', Item, RewardItem, id='items'),
    pytest.param('containers', Container, RewardContainer, id='containers'),
])
def test_calc_reward_items(
        user: IUser,
        reward_key: str,
        ItemModel: type[BaseItem],  # noqa
        RewardItemModel: type[BaseRewardItem],  # noqa
):
    # arrange
    item = ItemModel.objects.create(name='currency', description='currency', index_number=777)
    reward = Reward.objects.create()
    RewardItemModel.objects.create(reward=reward, item=item, from_amount=100, to_amount=100)

    # act
    result_reward = reward.calc(user)

    # assert
    assert result_reward == {
        'backgrounds': [],
        'containers': {},
        'currency': {},
        'emotions': [],
        'experience': 0,
        'items': {},
        'roles': {},
        'titles': [],
        'personal_role_duration': 0,
        'private_channel_duration': 0,
        reward_key: {item.pk: 100},
    }


@pytest.mark.django_db
@pytest.mark.parametrize('reward_key, ItemModel, RewardItemModel', [
    pytest.param('currency', Currency, RewardCurrency, id='currency'),
    pytest.param('items', Item, RewardItem, id='items'),
    pytest.param('containers', Container, RewardContainer, id='containers'),
])
def test_calc_multiple_items(
        user: IUser,
        reward_key: str,
        ItemModel: type[BaseItem],  # noqa
        RewardItemModel: type[BaseRewardItem],  # noqa
):
    # arrange
    item = ItemModel.objects.create(name='currency', description='currency', index_number=777)
    reward = Reward.objects.create()
    RewardItemModel.objects.create(reward=reward, item=item, from_amount=100, to_amount=100)

    # act
    result_reward = reward.calc(user, count=2)

    # assert
    assert result_reward == {
        'backgrounds': [],
        'containers': {},
        'currency': {},
        'emotions': [],
        'experience': 0,
        'items': {},
        'roles': {},
        'titles': [],
        'personal_role_duration': 0,
        'private_channel_duration': 0,
        reward_key: {item.pk: 200},
    }
