from datetime import timedelta
from unittest import mock

import celery
import pytest
from django.utils.timezone import now

from schedule.models import Task
from schedule.tasks import schedule_tasks


@pytest.mark.django_db
def test_task_scheduling():
    # arrange
    launch_time = now() + timedelta(hours=1)

    # act
    schedule_tasks.apply_async(eta=launch_time)
    task = Task.objects.first()

    # assert
    assert task is not None
    assert task.launch_time == launch_time
    assert task.name == 'schedule_tasks'
    assert task.kwargs is None


@pytest.mark.django_db
@mock.patch.object(celery.Task, 'apply_async')
def test_task_not_scheduling(mock_method: mock.MagicMock):
    # arrange
    launch_time = now() + timedelta(minutes=4)

    # act
    schedule_tasks.apply_async(eta=launch_time)

    # assert
    assert Task.objects.count() == 0
    mock_method.assert_called_once()
