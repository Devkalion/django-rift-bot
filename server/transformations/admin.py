from django.contrib.admin import register, ModelAdmin, StackedInline

from .models import *


class ShopItemInline(StackedInline):
    model = ShopItem
    extra = 0
    ordering = ('index_number',)
    autocomplete_fields = ('price', 'reward',)


@register(ShopItem)
class ShopItemAdmin(ModelAdmin):
    list_display = ('index_number', 'display_name', 'shop', 'hidden')
    fields = (
        'index_number', 'hidden', 'closed', 'one_off', 'display_name', 'name', 'shop', 'icon', 'image_url',
        'description', 'description2', 'reward', 'price', 'cooldown_seconds'
    )
    search_fields = ('name', 'display_name', 'description')
    list_filter = ('shop', 'hidden', 'one_off', 'closed')
    ordering = ('index_number',)
    autocomplete_fields = ('price', 'reward')
    list_select_related = ('shop',)


@register(TransformItem)
class TransformItemAdmin(ModelAdmin):
    list_display = ('index_number', 'name', 'description', 'hidden', 'duration')
    search_fields = ('name', 'description')
    ordering = ('index_number',)


@register(JourneyItem)
class JourneyItemAdmin(ModelAdmin):
    list_display = ('index_number', 'hidden', 'name', 'duration')
    search_fields = ('name',)
    ordering = ('index_number',)
    autocomplete_fields = ('reward', 'price')
    fields = (
        'index_number', 'hidden', 'closed', 'name', 'icon', 'thumbnail_url', 'image_url', 'duration', 'category',
        'start_title_description', 'location_description', 'start_description', 'finish_description',
        'reward', 'price',
    )


@register(GiftItem)
class GiftItemAdmin(ModelAdmin):
    list_display = ('index_number', 'display_name', 'hidden')
    search_fields = ('name', 'display_name', 'description')
    ordering = ('index_number',)
    list_filter = ('hidden', 'closed', 'one_off')
    autocomplete_fields = ('reward', 'price')
    fields = (
        'index_number', 'hidden', 'closed', 'one_off', 'display_name', 'name', 'icon',
        'image_url', 'description', 'description2', 'reward', 'price', 'cooldown_seconds'
    )


@register(MarketExchange)
@register(MarketSale)
class ItemAdmin(ModelAdmin):
    list_display = ('index_number', 'display_name', 'hidden')
    search_fields = ('name', 'description')
    ordering = ('index_number',)
    autocomplete_fields = ('reward', 'price')


@register(Shop)
class ShopAdmin(ModelAdmin):
    inlines = (ShopItemInline,)
    list_display = ('title_name', 'command_name', 'buy_name', 'active')
    search_fields = ('title_name', 'description')
    fields = (
        'title_name', 'active', 'command_name', 'buy_name', 'image_url', 'description', 'footer', 'cooldown_seconds'
    )


@register(Activity)
class ActivityAdmin(ModelAdmin):
    list_display = ['codename', 'cooldown_seconds']


@register(ActivityResult)
class ActivityResultAdmin(ModelAdmin):
    list_filter = ['activity__codename']
    list_display = ['name', 'activity', 'weight', 'chance']
    readonly_fields = ('chance', )

