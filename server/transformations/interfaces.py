from datetime import timedelta
from typing import Optional

from common.interfaces import IModelIntId
from reward import interfaces as reward_interfaces


class IBase(IModelIntId):
    index_number: int
    name: str
    icon: Optional[str]
    price: Optional['reward_interfaces.IPrice']
    price_id: Optional[int]
    reward: 'reward_interfaces.IReward'
    reward_id: int
    image_url: str
    hidden: bool
    closed: bool


class IOneOffItem(IBase):
    one_off: bool


class IProcessItem(IBase):
    duration: timedelta


class IGiftItem(IOneOffItem):
    description: Optional[str]
    display_name: str
    reward: 'reward_interfaces.IRewardList'
    reward_id: int
    description2: str
    cooldown_seconds: int


class IShop(IModelIntId):
    title_name: str
    command_name: str
    buy_name: str
    description: str
    active: bool
    cooldown_seconds: int
    footer: Optional[str]
    image_url: str


class IShopItem(IOneOffItem):
    description: Optional[str]
    display_name: str
    shop: IShop
    shop_id: int
    cooldown_seconds: Optional[int]
    index_number: int
    description2: Optional[str]


class ITransformItem(IProcessItem):
    temporary: bool
    reward: 'reward_interfaces.IRewardList'
    reward_id: int
    description: Optional[str]
    description2: Optional[str]
    one_off: bool


class IMarketExchange(IOneOffItem):
    description: Optional[str]
    display_name: str


class IMarketSale(IBase):
    description: Optional[str]
    display_name: str


class IJourneyItem(IProcessItem):
    start_title_description: str
    location_description: str
    start_description: str
    finish_description: str
    category: Optional[str]
    thumbnail_url: str


class IActivity(IModelIntId):
    price: Optional[Optional['reward_interfaces.IPrice']]
    price_id: Optional[Optional[int]]
    codename: str
    cooldown_seconds: Optional[int]


class IActivityResult(IModelIntId):
    name: str
    activity: IActivity
    activity_id: int
    reward_list: Optional['reward_interfaces.IRewardList']
    reward_list_id: Optional[int]
    weight: int
    content: Optional[str]
    text: Optional[str]
    embed_title: Optional[str]
    footer_text: Optional[str]
    color: Optional[str]
    thumbnail_url: Optional[str]
    footer_url: Optional[str]
    image_url: Optional[str]
