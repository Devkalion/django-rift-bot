from datetime import timedelta, datetime

from django.db import models, transaction
from django.utils.timezone import now
from rest_framework.exceptions import ValidationError

from common.models import ColorField
from reward.models import Reward
from reward.utils import ResultReward
from .interfaces import (
    IActivity,
    IOneOffItem,
    IBase,
    IProcessItem,
    IJourneyItem,
    IMarketSale,
    IMarketExchange,
    ITransformItem,
    IShopItem,
    IShop,
    IGiftItem, IActivityResult,
)


class Base(models.Model, IBase):
    index_number = models.IntegerField(default=1, unique=True, db_index=True)
    name = models.CharField(max_length=255)
    icon = models.CharField(max_length=50, blank=True, null=True)
    price = models.ForeignKey('reward.Price', models.CASCADE, related_name='+', blank=True, null=True)
    reward = models.ForeignKey('reward.Reward', models.CASCADE, related_name='+')
    image_url = models.URLField(blank=True)
    hidden = models.BooleanField(default=True)
    closed = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name} #{self.index_number}'

    class Meta:
        abstract = True

    def give(self, user):
        reward: Reward = self.reward
        result: dict = reward.calc(user)
        result_reward: ResultReward = ResultReward.from_dict(result)

        with transaction.atomic():
            if self.price:
                try:
                    self.price.take(user)
                except ValueError as e:
                    raise ValidationError(e.args[0])
            result_reward.give(user)

            self._post_give(user)

        return result

    def _post_give(self, user):
        pass


class OneOffItem(Base, IOneOffItem):
    one_off = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def _post_give(self, user):
        if self.one_off:
            self.blocks.create(user=user)


class ProcessItem(OneOffItem, IProcessItem):
    duration = models.DurationField(default=timedelta())

    @property
    def process_model(self):
        raise NotImplementedError

    def give(self, user):
        reward: Reward = self.reward
        result: dict = reward.calc(user)
        result_reward: ResultReward = ResultReward.from_dict(result)

        with transaction.atomic():
            result_reward.give(user)
            self._post_give(user)

        return result

    def start(self, user):
        to_date: datetime = now() + self.duration
        with transaction.atomic():
            if self.price:
                try:
                    self.price.take(user)
                except ValueError as e:
                    raise ValidationError(e.args[0])
            self.process_model.objects.create(
                user=user,
                item=self,
                to_date=to_date
            )

            if self.one_off:
                self.blocks.create(user=user)

        return to_date.timestamp()

    class Meta:
        abstract = True


class GiftItem(OneOffItem, IGiftItem):
    description = models.TextField(max_length=255, blank=True, null=True)
    display_name = models.CharField(max_length=255)
    reward = models.ForeignKey('reward.RewardList', models.CASCADE, related_name='+', verbose_name='reward_list')
    description2 = models.TextField(max_length=255)
    cooldown_seconds = models.PositiveIntegerField(default=0)

    def give(self, from_user, to_user):
        reward: Reward = self.reward.get_reward()
        result: dict = reward.calc(to_user)
        result_reward: ResultReward = ResultReward.from_dict(result)

        with transaction.atomic():
            if self.price is not None:
                try:
                    self.price.take(from_user)
                except ValueError as e:
                    raise ValidationError(e.args[0])
            result_reward.give(to_user)
            self._post_give(to_user)

        return result


class Shop(models.Model, IShop):
    title_name = models.CharField(max_length=50)
    command_name = models.CharField(max_length=50, blank=True, unique=True, db_index=True)
    buy_name = models.CharField(max_length=50, blank=True, unique=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=False)
    cooldown_seconds = models.PositiveIntegerField(default=0)
    footer = models.TextField(blank=True, null=True)
    image_url = models.URLField(blank=True)

    def __str__(self):
        return self.title_name


class ShopItem(OneOffItem, IShopItem):
    description = models.TextField(max_length=255, blank=True, null=True)
    display_name = models.CharField(max_length=255)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name='items')
    cooldown_seconds = models.PositiveIntegerField(blank=True, null=True)
    index_number = models.IntegerField(default=1)
    description2 = models.TextField(max_length=255, blank=True, null=True)

    class Meta:
        unique_together = [('shop', 'index_number')]

    def __str__(self):
        return f'{self.display_name} #{self.index_number}'


class TransformItem(ProcessItem, ITransformItem):
    temporary = models.BooleanField(default=True)
    reward = models.ForeignKey('reward.RewardList', models.CASCADE, related_name='+', verbose_name='reward_list')
    description = models.TextField(max_length=255, blank=True, null=True)
    description2 = models.TextField(max_length=255, blank=True, null=True)

    @property
    def process_model(self):
        from events.models import ActiveTransformation
        return ActiveTransformation

    def give(self, user):
        reward: Reward = self.reward.get_reward()
        result: dict = reward.calc(user)
        result_reward: ResultReward = ResultReward.from_dict(result)

        with transaction.atomic():
            result_reward.give(user)

        return result


class MarketExchange(OneOffItem, IMarketExchange):
    display_name = models.CharField(max_length=255)
    description = models.TextField(max_length=255, blank=True, null=True)


class MarketSale(Base, IMarketSale):
    display_name = models.CharField(max_length=255)
    description = models.TextField(max_length=255, blank=True, null=True)


class JourneyItem(ProcessItem, IJourneyItem):
    start_title_description = models.TextField(max_length=511)
    location_description = models.TextField(max_length=511)
    start_description = models.TextField(max_length=511)
    finish_description = models.TextField(max_length=511)
    category = models.CharField(max_length=200, blank=True, null=True)
    thumbnail_url = models.URLField(blank=True)

    one_off = None

    @property
    def process_model(self):
        from events.models import ActiveJourney
        return ActiveJourney


class Activity(models.Model, IActivity):
    price = models.ForeignKey('reward.Price', models.CASCADE, related_name='+', blank=True, null=True)
    codename = models.CharField(max_length=20, unique=True)
    cooldown_seconds = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.codename


class ActivityResult(IActivityResult, models.Model):
    name = models.CharField(max_length=50, unique=True)
    activity = models.ForeignKey(Activity, models.CASCADE)
    reward_list = models.ForeignKey('reward.RewardList', models.CASCADE, related_name='+', blank=True, null=True)
    weight = models.PositiveSmallIntegerField(default=1)

    # https://discord.com/developers/docs/resources/channel#embed-object-embed-limits
    content = models.TextField(blank=True, null=True)
    text = models.TextField(blank=True, null=True, max_length=4096)
    embed_title = models.TextField(blank=True, null=True, max_length=256)
    footer_text = models.TextField(blank=True, null=True, max_length=2048)
    color = ColorField(blank=True, null=True)
    thumbnail_url = models.URLField(blank=True, null=True)
    footer_url = models.URLField(blank=True, null=True)
    image_url = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def chance(self):
        total_weight = sum(ActivityResult.objects.filter(activity_id=self.activity_id).values_list('weight', flat=True))
        if total_weight == 0:
            return '-'
        percentage = self.weight / total_weight
        return f'{percentage:.2%}'
