from rest_framework import serializers

from .models import *


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ('id', 'cooldown_seconds')


class ShopItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopItem
        fields = ('index_number', 'display_name', 'name', 'hidden', 'closed')


class SaleItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketSale
        fields = ('index_number', 'display_name', 'name', 'hidden', 'closed')


class ExchangeItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketExchange
        fields = ('index_number', 'display_name', 'name', 'hidden', 'closed')
