from random import randint
from typing import Optional, Tuple

from django.db import transaction

from common.repos import RedisRepository
from reward.models import Price, RewardList
from reward.utils import ResultReward
from transformations.interfaces import IActivity, IActivityResult
from transformations.models import ActivityResult
from user.interfaces import IUser


class ActivityPriceError(Exception):
    def __init__(self, price_errors):
        self.price_errors = price_errors
        super().__init__()


class ActivityCooldownError(Exception):
    def __init__(self, remaining_seconds):
        self.remaining_seconds = remaining_seconds
        super().__init__()


class ActivityNoResultError(Exception):
    pass


class ActivityService:
    def __init__(self):
        self._redis = RedisRepository()

    @staticmethod
    def get_redis_key(activity: IActivity, user_id: int) -> str:
        return f'activity::{activity.codename}::{user_id}'

    def check_activity_user_cooldown(self, activity: IActivity, user_id: int) -> int:
        key = self.get_redis_key(activity, user_id)
        return self._redis.check_ttl(key)

    def set_activity_user_cooldown(self, activity: IActivity, user_id: int) -> None:
        if activity.cooldown_seconds:
            key = self.get_redis_key(activity, user_id)
            self._redis.set(key, ttl=activity.cooldown_seconds)

    def start_activity(self, activity: IActivity, user: IUser) -> Tuple[IActivityResult, Optional[dict]]:
        remaining_seconds = self.check_activity_user_cooldown(activity, user.pk)
        if remaining_seconds > 0:
            raise ActivityCooldownError(remaining_seconds)

        result = self.get_activity_result(activity)
        if not result:
            raise ActivityNoResultError

        price = self.get_activity_price(activity)
        reward_dict: Optional[dict] = self.get_reward_dict(result, user)

        result_reward: Optional[ResultReward] = None
        if reward_dict is not None:
            result_reward: ResultReward = ResultReward.from_dict(reward_dict)

        if price or result_reward:
            with transaction.atomic():
                if price:
                    try:
                        price.take(user)
                    except ValueError as e:
                        print(e)
                        raise ActivityPriceError(e.args[0])
                if result_reward:
                    result_reward.give(user, role_reason='activity')

        self.set_activity_user_cooldown(activity, user.pk)

        return result, reward_dict

    @staticmethod
    def get_activity_price(activity: IActivity) -> Optional[Price]:
        if activity.price_id is not None:
            return activity.price

    @staticmethod
    def get_activity_result(activity: IActivity) -> Optional[IActivityResult]:
        results = list(ActivityResult.objects.filter(
            activity_id=activity.pk
        ).select_related('reward_list').only(
            'id', 'reward_list', 'weight'
        ).order_by('weight'))
        if not results:
            return None

        total_weight = sum((result.weight for result in results))
        chance = randint(1, total_weight)
        for result in results:
            if chance <= result.weight:
                return result
            chance -= result.weight

        return None

    @staticmethod
    def get_reward_dict(activity_result: IActivityResult, user: IUser) -> Optional[dict]:
        if activity_result.reward_list_id is not None:
            reward_list: RewardList = activity_result.reward_list
            reward = reward_list.get_reward()
            return reward.calc(user)


activity_service = ActivityService()
