from contextlib import suppress
from datetime import datetime, timedelta, timezone
from unittest import TestCase, mock

from django.db import transaction
from django.urls import reverse
from rest_framework.response import Response
from rest_framework.test import APIClient

from events.models import ActiveJourney
from reward.models import ChancedReward, Price, Reward, RewardList
from schedule.celery import CustomTask
from transformations.models import JourneyItem, GiftItem, Shop, ShopItem
from user.models import User

api_client = APIClient()


class ShopTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.user: User = User.objects.create(
            discord_id=121,
            discord_registration_date=datetime.now(timezone.utc),
            registration_date=datetime.now(timezone.utc),
            last_activity_time=datetime.now(timezone.utc),
        )
        cls.reward = Reward.objects.create()
        cls.shop1 = Shop.objects.create(command_name="dabacaba", buy_name="dabacaba", active=True)
        cls.shop2 = Shop.objects.create(command_name="bacadaba", buy_name="bacadaba", active=True)
        cls.shop_item = ShopItem.objects.create(shop=cls.shop1, reward=cls.reward, hidden=False)

    def test_get_shop_by_command_name(self):
        url = reverse("shops-list")
        response: Response = api_client.get(url, data={'command_name': self.shop1.command_name})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 1)
        self.assertEquals(response.data[0]["id"], self.shop1.pk)

    def test_get_shop_by_buy_name(self):
        url = reverse("shops-list")
        response: Response = api_client.get(url, data={'buy_name': self.shop1.command_name})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 1)
        self.assertEquals(response.data[0]["id"], self.shop1.pk)

    def test_get_shop_by_invalid_param(self):
        url = reverse("shops-list")
        response: Response = api_client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 0)

    def test_buy_without_shop_param(self):
        url = reverse("shop_items-buy", kwargs={"index_number": self.shop_item.index_number})
        response: Response = api_client.post(url)
        self.assertEquals(response.status_code, 400)
        self.assertEquals(response.data["shop"], "Unable to identify shop")

    def test_buy_from_inactive_shop(self):
        url = reverse("shop_items-buy", kwargs={"index_number": self.shop_item.index_number})
        Shop.objects.filter(pk=self.shop1.pk).update(active=False)
        self.addCleanup(Shop.objects.filter(pk=self.shop1.pk).update, active=True)
        payload = {"user_id": self.user.pk}
        response: Response = api_client.post(f"{url}?shop_id={self.shop1.pk}", payload, format="json")
        self.assertEquals(response.status_code, 403)
        self.assertEquals(response.data["detail"], "not active")

    def test_buy_closed_shop_item(self):
        url = reverse("shop_items-buy", kwargs={"index_number": self.shop_item.index_number})
        ShopItem.objects.filter(pk=self.shop_item.pk).update(closed=True)
        self.addCleanup(ShopItem.objects.filter(pk=self.shop_item.pk).update, closed=False)
        payload = {"user_id": self.user.pk}
        response: Response = api_client.post(f"{url}?shop_id={self.shop1.pk}", payload, format="json")
        self.assertEquals(response.status_code, 403)
        self.assertEquals(response.data["detail"], "closed")

    def test_one_off(self):
        url = reverse("shop_items-buy", kwargs={"index_number": self.shop_item.index_number})
        ShopItem.objects.filter(pk=self.shop_item.pk).update(one_off=True)
        self.addCleanup(ShopItem.objects.filter(pk=self.shop_item.pk).update, one_off=False)
        payload = {"user_id": self.user.pk}

        response: Response = api_client.post(f"{url}?shop_id={self.shop1.pk}", payload, format="json")
        self.assertEquals(response.status_code, 200)

        response: Response = api_client.post(f"{url}?shop_id={self.shop1.pk}", payload, format="json")
        self.assertEquals(response.status_code, 403)
        self.assertEquals(response.data["detail"], "one-off")

    def test_buy(self):
        url = reverse("shop_items-buy", kwargs={"index_number": self.shop_item.index_number})
        payload = {"user_id": self.user.pk}
        response: Response = api_client.post(f"{url}?shop_id={self.shop1.pk}", payload, format="json")
        self.assertEquals(response.status_code, 200)

    def test_buy_wrong_shop_item_shop_id(self):
        url = reverse("shop_items-buy", kwargs={"index_number": self.shop_item.index_number})
        payload = {"user_id": self.user.pk}
        response: Response = api_client.post(f"{url}?shop_id={self.shop2.pk}", payload, format="json")
        self.assertEquals(response.status_code, 404)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.reward.delete()
        cls.shop1.delete()
        cls.shop2.delete()
        cls.shop_item.delete()


class GiftTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with transaction.atomic():
            cls.user: User = User.objects.create(
                discord_id=123,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            cls.reward: Reward = Reward.objects.create()
            cls.price: Price = Price.objects.create()
            cls.reward_list: RewardList = RewardList.objects.create()
            ChancedReward.objects.create(reward=cls.reward, reward_list=cls.reward_list, chance=100)
            cls.gift: GiftItem = GiftItem.objects.create(
                reward=cls.reward_list,
                price=cls.price,
                index_number=666,
                closed=False,
                hidden=False,
            )

    def test_invalid_user(self):
        url = reverse("gifts-gift", kwargs={"index_number": self.gift.index_number})
        payload = {"to_user_id": -1, "user_id": -1}
        with self.assertRaises(User.DoesNotExist):
            api_client.post(url, data=payload, format="json")

        payload["user_id"] = self.user.pk
        with self.assertRaises(User.DoesNotExist):
            api_client.post(url, data=payload, format="json")

    def test_closed(self):
        url = reverse("gifts-gift", kwargs={"index_number": self.gift.index_number})
        payload = {"user_id": self.user.pk, "to_user_id": self.user.pk}
        GiftItem.objects.filter(pk=self.gift.pk).update(closed=True)
        self.addCleanup(GiftItem.objects.filter(pk=self.gift.pk).update, closed=False)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 403)
        self.assertEquals(response.data["detail"], "closed")

    def test_hidden(self):
        url = reverse("gifts-gift", kwargs={"index_number": self.gift.index_number})
        GiftItem.objects.filter(pk=self.gift.pk).update(hidden=True)
        self.addCleanup(GiftItem.objects.filter(pk=self.gift.pk).update, hidden=False)
        payload = {"user_id": self.user.pk, "to_user_id": self.user.pk}

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 404)

    def test_one_off(self):
        url = reverse("gifts-gift", kwargs={"index_number": self.gift.index_number})
        payload = {"user_id": self.user.pk, "to_user_id": self.user.pk}

        GiftItem.objects.filter(pk=self.gift.pk).update(one_off=True)
        self.addCleanup(GiftItem.objects.filter(pk=self.gift.pk).update, one_off=False)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 200)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 403)
        self.assertEquals(response.data["detail"], "one time block")

    def test_no_reward(self):
        url = reverse("gifts-gift", kwargs={"index_number": self.gift.index_number})
        payload = {"user_id": self.user.pk, "to_user_id": self.user.pk}

        ChancedReward.objects.filter(reward=self.reward, reward_list=self.reward_list).delete()

        reward = ChancedReward(reward=self.reward, reward_list=self.reward_list, chance=100)
        self.addCleanup(reward.save)

        with self.assertRaises(AttributeError):
            api_client.post(url, data=payload, format="json")

    def test_positive(self):
        url = reverse("gifts-gift", kwargs={"index_number": self.gift.index_number})
        payload = {"user_id": self.user.pk, "to_user_id": self.user.pk}

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 200)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.reward.delete()
        cls.price.delete()
        cls.reward_list.delete()
        cls.gift.delete()


class JourneyTestCase(TestCase):
    @classmethod
    def setUpClass(self):
        with transaction.atomic():
            self.user: User = User.objects.create(
                discord_id=1,
                discord_registration_date=datetime.now(timezone.utc),
                registration_date=datetime.now(timezone.utc),
                last_activity_time=datetime.now(timezone.utc),
            )
            self.reward: Reward = Reward.objects.create()
            self.journey: JourneyItem = JourneyItem.objects.create(
                reward=self.reward,
                index_number=666,
                closed=False,
                hidden=False,
                duration=timedelta(hours=1),
            )

    def test_closed(self):
        url = reverse("journeys-start", kwargs={"index_number": self.journey.index_number})
        payload = {"user_id": self.user.pk}
        JourneyItem.objects.filter(pk=self.journey.pk).update(closed=True)
        self.addCleanup(JourneyItem.objects.filter(pk=self.journey.pk).update, closed=False)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 403)
        self.assertEquals(response.data["detail"], "closed")

    def test_hidden(self):
        url = reverse("journeys-start", kwargs={"index_number": self.journey.index_number})
        payload = {"user_id": -1}

        JourneyItem.objects.filter(pk=self.journey.pk).update(hidden=True)
        self.addCleanup(JourneyItem.objects.filter(pk=self.journey.pk).update, hidden=False)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertEquals(response.status_code, 404)

    def test_invalid_user(self):
        url = reverse("journeys-start", kwargs={"index_number": self.journey.index_number})
        payload = {"user_id": -1}
        with self.assertRaises(User.DoesNotExist):
            api_client.post(url, data=payload, format="json")

    def test_start(self):
        url = reverse("journeys-start", kwargs={"index_number": self.journey.index_number})
        payload = {"user_id": self.user.pk}

        published_events = []

        def add_published_events(*args, **kwargs):
            published_events.append({"args": args, "kwargs": kwargs})

        CustomTask.apply_async = mock.Mock(side_effect=add_published_events)

        with mock.patch(
            "transformations.views.JourneyViewSet.limit", new_callable=mock.PropertyMock
        ) as mock_journey_limit:
            mock_journey_limit.return_value = 1
            response: Response = api_client.post(url, data=payload, format="json")
            self.assertEquals(response.status_code, 200)

            active_journey = ActiveJourney.objects.filter(user=self.user, item=self.journey).first()
            self.assertIsNotNone(active_journey)

            self.assertEquals(len(published_events), 1)
            self.assertEquals(published_events[0]["kwargs"]["args"], [active_journey.pk])

            response: Response = api_client.post(url, data=payload, format="json")
            self.assertEquals(response.status_code, 405)
            self.assertIn("cooldown", response.data)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.user.delete()
        cls.journey.delete()
        cls.reward.delete()
