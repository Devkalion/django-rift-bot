from typing import Tuple

from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from common.serializers import UserSerializer
from live_settings.models import Setting
from user.models import User
from user.services import UserService
from .models import *
from .serializers import ExchangeItemSerializer, SaleItemSerializer, ShopItemSerializer, ShopSerializer
from .services import ActivityCooldownError, ActivityPriceError, activity_service, ActivityNoResultError


class BaseItemViewSet(GenericViewSet):
    lookup_field = 'index_number'

    def get_params(self) -> Tuple[Base, User]:
        item: Base = self.get_object()
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        user: User = serializer.validated_data['user']
        return item, user

    def _action(self):
        item, user = self.get_params()

        if item.closed:
            return self.closed_response

        return Response({'reward': item.give(user)})

    @property
    def closed_response(self):
        raise PermissionDenied(detail='closed')


class OneOffItemViewSet(BaseItemViewSet):
    def get_params(self) -> Tuple[OneOffItem, User]:
        return super().get_params()

    def _action(self):
        item, user = self.get_params()
        if item.closed:
            return self.closed_response
        if item.one_off and item.blocks.filter(user=user).exists():
            return self.one_off_response
        return Response({'reward': item.give(user)})

    @property
    def one_off_response(self):
        raise PermissionDenied(detail='one-off')


class GiftViewSet(OneOffItemViewSet):
    queryset = GiftItem.objects.select_related(
        'reward', 'price'
    ).filter(hidden=False)

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def gift(self, request, **kwargs):
        gift: GiftItem
        gift, user = self.get_params()
        to_user = UserService.get_object(discord_id=request.data['to_user_id'])

        if gift.closed:
            return self.closed_response

        if gift.one_off and gift.blocks.filter(user=to_user).exists():
            return self.one_off_response

        return Response({'reward': gift.give(user, to_user)})

    @property
    def one_off_response(self):
        raise PermissionDenied(detail='one time block')


class ShopItemViewSet(OneOffItemViewSet, mixins.ListModelMixin):
    serializer_class = ShopItemSerializer

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def buy(self, request, **kwargs):
        shop = self.get_shop()
        if not shop.active:
            return self.inactive_response
        return self._action()

    def get_shop(self) -> Shop:
        if 'shop_id' in self.request.query_params:
            return get_object_or_404(Shop.objects.all(), id=self.request.query_params['shop_id'])
        else:
            return self.missing_shop_response

    def get_queryset(self):
        shop = self.get_shop()
        return shop.items.select_related(
            'reward', 'price'
        ).filter(hidden=False)

    @property
    def inactive_response(self):
        raise PermissionDenied(detail='not active')

    @property
    def missing_shop_response(self):
        raise ValidationError({'shop': 'Unable to identify shop'})


class ShopViewSet(GenericViewSet,
                  mixins.RetrieveModelMixin,
                  mixins.ListModelMixin):
    serializer_class = ShopSerializer

    def get_queryset(self):
        if self.action == 'list':
            if 'command_name' in self.request.query_params:
                return Shop.objects.filter(
                    command_name=self.request.query_params['command_name']
                ).only('id', 'cooldown_seconds')
            if 'buy_name' in self.request.query_params:
                return Shop.objects.filter(
                    buy_name=self.request.query_params['buy_name']
                ).only('id', 'cooldown_seconds')
            return Shop.objects.none()
        return Shop.objects.all()


class MarketSaleViewSet(BaseItemViewSet, mixins.ListModelMixin):
    queryset = MarketSale.objects.filter(hidden=False).select_related('price', 'reward')
    serializer_class = SaleItemSerializer

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def sale(self, request, **kwargs):
        return self._action()


class MarketExchangeViewSet(OneOffItemViewSet, mixins.ListModelMixin):
    queryset = MarketExchange.objects.filter(hidden=False).select_related('price', 'reward')
    serializer_class = ExchangeItemSerializer

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def exchange(self, request, **kwargs):
        return self._action()


class ProcessItemViewSet(OneOffItemViewSet):
    @property
    def limit(self):
        return 1

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def start(self, request, **kwargs):
        item: ProcessItem
        item, user = self.get_params()

        if item.closed:
            return self.closed_response

        if item.one_off and item.blocks.filter(user=user).exists():
            return self.one_off_response

        if item.process_model.objects.filter(
                user=user,
                to_date__gte=now()
        ).count() >= self.limit:
            nearest = item.process_model.objects.filter(
                user=user,
                to_date__gte=now()
            ).order_by('to_date').only('to_date').first()
            return Response({
                'cooldown': (nearest.to_date - now()).total_seconds()
            }, status=405)

        return Response({
            'to_date': item.start(user)
        })


class TransformViewSet(ProcessItemViewSet):
    queryset = TransformItem.objects.filter(hidden=False).select_related('price')

    @property
    def limit(self):
        return Setting.get_value('PARALLEL_TRANSFORMS_COUNT')


class JourneyViewSet(ProcessItemViewSet):
    queryset = JourneyItem.objects.filter(hidden=False).select_related('price')

    @property
    def limit(self):
        return Setting.get_value('PARALLEL_JOURNEYS_COUNT')


class ActivityViewSet(GenericViewSet):
    queryset = Activity.objects.select_related('price').all()

    lookup_url_kwarg = 'activity_codename'
    lookup_field = 'codename'

    @action(detail=True, methods=['post'], serializer_class=UserSerializer)
    def start(self, request, **kwargs):
        activity = self.get_object()
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        user: User = serializer.validated_data['user']

        try:
            result, reward = activity_service.start_activity(activity, user)
        except ActivityPriceError as e:
            raise ValidationError(detail=e.price_errors, code='price')
        except ActivityNoResultError as e:
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except ActivityCooldownError as e:
            return Response({
                'cooldown': e.remaining_seconds
            }, status=status.HTTP_429_TOO_MANY_REQUESTS)

        return Response({'reward': reward, 'activity_result_id': result.id})
