"""rift_bot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

import settings
from embeds.views import (
    EmbedViewSet,
    GuessCommandViewSet,
    SlashCommandsViewSet,
    UserMessageViewSet,
    generator_preview,
    template_preview,
)
from events.views import EventViewSet
from giveaways.views import GiveawayViewSet, DrawViewSet, EncouragementViewSet
from inventory.views import ContainerViewSet, RolesViewSet, EmotionViewSet, UserRolesViewSet, PersonalRolesViewSet, \
    PersonalChannelViewSet
from moder_actions.views import ModerActionViewSet, AppealViewSet
from reactions.views import RoleGettingMessageViewSet
from reward.views import RewardViewSet
from transformations.views import *
from user.views import UserViewSet

router = DefaultRouter()
router.register(r'user/(?P<user_id>\d+)/roles', UserRolesViewSet, basename='user_roles')
router.register(r'personal_roles', PersonalRolesViewSet, basename='personal_roles')
router.register(r'channel/(?P<channel_id>\d+)/message', RoleGettingMessageViewSet, basename='reactions')
router.register(r'users', UserViewSet, basename='user')
router.register(r'events', EventViewSet, basename='event')
router.register(r'embeds', EmbedViewSet, basename='embed')
router.register(r'user_messages', UserMessageViewSet, basename='user_message')
router.register(r'reward', RewardViewSet, basename='rewards')
router.register(r'slash_commands', SlashCommandsViewSet, basename='slash_commands')
router.register(r'containers', ContainerViewSet, basename='containers')
router.register(r'roles', RolesViewSet, basename='roles')
router.register(r'emotions', EmotionViewSet, basename='emotions')
router.register(r'gifts', GiftViewSet, basename='gifts')
router.register(r'shops', ShopViewSet, basename='shops')
router.register(r'shop_items', ShopItemViewSet, basename='shop_items')
router.register(r'transformations', TransformViewSet, basename='transformations')
router.register(r'activities', ActivityViewSet, basename='activities')
router.register(r'journeys', JourneyViewSet, basename='journeys')
router.register(r'market/sales', MarketSaleViewSet, basename='market_sales')
router.register(r'market/exchanges', MarketExchangeViewSet, basename='market_exchanges')
router.register(r'moder_actions', ModerActionViewSet, basename='moder_actions')
router.register(r'appeals', AppealViewSet, basename='appeals')
router.register(r'giveaways', GiveawayViewSet, basename='giveaways')
router.register(r'encouragement', EncouragementViewSet, basename='encouragement')
router.register(r'personal_channels', PersonalChannelViewSet, basename='personal_channels')
router.register(r'draws', DrawViewSet, basename='draws')

urlpatterns = [
    path('admin/embeds/imagegenerator/<str:pk>/preview/', generator_preview, name='image_generator_preview'),
    path('admin/embeds/imagetemplate/<str:pk>/preview/', template_preview, name='image_template_preview'),
    path('admin/', admin.site.urls),
    path('api/commands/', GuessCommandViewSet.as_view({'get': 'retrieve'}), name="commands-detail"),
    path('api/', include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static('/media/', document_root=settings.MEDIA_ROOT)
