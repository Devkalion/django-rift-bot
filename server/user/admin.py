import json
from datetime import datetime

from django.contrib.admin import ModelAdmin, TabularInline, register
from django.contrib.auth import admin
from django.forms import BaseInlineFormSet, ModelForm
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from events.enums import EventType
from events.services import EventService
from inventory.admin import ContainerInline, CurrencyInline, ItemInline, PersonalRoleInline, TempRoleInline, TitleInline
from inventory.models import PersonalRole
from limits.admin import (
    GiftItemBlockInline,
    GiveawayBlockInline,
    JourneyBlockInline,
    MarketExchangeBlockInline,
    RoleGettingMessageBlockInline,
    ShopItemBlockInline,
    TransformationBlockInline,
)
from moder_actions.admin import AppealInline, OffenceInline

from .models import *
from .services import UserService


@register(Admin)
class AdminAdmin(admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'user')}),
        (_('Important dates'), {'fields': ('last_login',)}),
        (_('Privileges'), {'fields': ('role', 'is_active', 'is_superuser')})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'user', 'password1', 'password2'),
        }),
    )
    list_display = ('username', 'role', 'is_active', 'user')
    list_filter = ('role',)
    filter_horizontal = ()
    search_fields = ('username',)
    raw_id_fields = ('user',)

    def get_readonly_fields(self, request, obj=None):
        if obj is None:
            return 'last_login', 'is_superuser'
        return 'last_login', 'is_superuser', 'user'

    def has_change_permission(self, request, obj: Admin = None):
        return all((
            super().has_change_permission(request, obj),
            obj is None or not obj.is_superuser or request.user == obj
        ))

    def has_delete_permission(self, request, obj=None):
        return False


class BaseUserAdmin(ModelAdmin):
    readonly_fields = (
        'discord_id', 'name', 'registration_date', 'date_joined', 'discord_registration_date', 'time_on_server',
        'left', 'active', 'last_activity_time', 'level', 'donates_sum'
    )
    search_fields = ('discord_id', 'name')
    ordering = ('name',)
    list_filter = ('is_admin', 'is_developer', 'is_moder', 'is_streamer', 'is_partner', 'left')
    list_display = ('name', 'discord_id', 'level', 'experience', 'registration_date')

    editable_model_fields = []

    def has_add_permission(self, request):
        return False

    def save_model(self, request, obj, form, change):
        if self.editable_model_fields:
            obj.save(update_fields=self.editable_model_fields)


@register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (_('Discord fields'), {'fields': ('discord_id', 'name', 'left')}),
        (_('Permissions'), {'fields': ('is_admin', 'is_developer', 'is_moder')}),
        (_('Partnership'), {'fields': ('is_streamer', 'is_partner', 'channel_url')}),
        (_('Important dates'),
         {'fields': (
             'discord_registration_date', 'registration_date', 'date_joined', 'time_on_server', 'last_activity_time'
         )}),
        (None, {'fields': ('messages', 'daily_messages', 'donates_sum')}),
    )

    editable_model_fields = ['is_admin', 'is_developer', 'is_moder',
                             'is_streamer', 'is_partner', 'channel_url']


@register(Inventory)
class InventoryAdmin(BaseUserAdmin):
    fieldsets = (
        (_('Discord fields'), {'fields': ('discord_id', 'name')}),
        (_('Self fields'), {'fields': (
            'roles', 'active_background', 'backgrounds', 'emotions', 'active_title',
        )}),
    )
    editable_model_fields = ['active_background', 'active_title']
    filter_horizontal = ('roles', 'backgrounds', 'emotions')
    inlines = (TempRoleInline, PersonalRoleInline, TitleInline)
    list_display = ('name', 'discord_id')

    def save_related(self, request: HttpRequest, form: ModelForm, formsets, change: bool) -> None:
        user: User = form.instance
        if change:
            _user: User = UserService.get_object(discord_id=user.pk)
            self.old_user_roles: set[int] = {
                *list(_user.all_roles.values_list("discord_id", flat=True)),
                *list(
                    PersonalRole.objects.filter(user=_user, end_date__gt=datetime.now())
                    .select_related("role")
                    .values_list("role__discord_id", flat=True)
                ),
            }
        else:
            self.old_user_roles: set[int] = set()

        super().save_related(request, form, formsets, change)

        _user: User = UserService.get_object(discord_id=user.pk)
        new_user_roles: set[int] = {
            *list(_user.all_roles.values_list("discord_id", flat=True)),
            *list(
                PersonalRole.objects.filter(user=_user, end_date__gt=datetime.now())
                .select_related("role")
                .values_list("role__discord_id", flat=True)
            ),
        }

        for role_discord_id in self.old_user_roles.difference(new_user_roles):
            if role_discord_id:
                EventService.create_event(
                    EventType.REVOKE_USER_ROLE,
                    json.dumps({"role_id": int(role_discord_id), "reason": "Changed in admin"}),
                    user.pk,
                )

        for role_discord_id in new_user_roles.difference(self.old_user_roles):
            if role_discord_id:
                EventService.create_event(
                    EventType.GRANT_USER_ROLE,
                    json.dumps({"role_id": int(role_discord_id), "reason": "Changed in admin"}),
                    user.pk,
                )


@register(Storage)
class StorageAdmin(BaseUserAdmin):
    fieldsets = (
        (_('Discord fields'), {'fields': ('discord_id', 'name')}),
    )
    inlines = (CurrencyInline, ContainerInline, ItemInline)
    list_display = ('name', 'discord_id')


class AttachmentsInlineFormset(BaseInlineFormSet):
    request = None

    def save_new(self, form, commit=True):
        result = super().save_new(form, False)
        result.moder = self.request.user
        if commit:
            result.save()
        return result


class AttachmentsInline(TabularInline):
    model = ModerAttachment
    extra = 1
    # classes = ['collapse']
    fields = ('file', 'created_date')
    readonly_fields = ('created_date',)

    formset = AttachmentsInlineFormset

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        formset.request = request
        return formset

    def has_change_permission(self, *args, **kwargs):
        return False


@register(Offences)
class OffencesAdmin(BaseUserAdmin):
    fields = ('discord_id', 'name', 'comment', 'claim_count')
    inlines = (AttachmentsInline, OffenceInline, AppealInline)
    list_display = ('name', 'discord_id')

    editable_model_fields = ['comment', 'claim_count']


@register(Experience)
class ExperienceAdmin(BaseUserAdmin):
    fieldsets = (
        (_('Discord fields'), {'fields': ('discord_id', 'name')}),
        (_('Self fields'), {'fields': ('experience', 'level')}),
    )
    editable_model_fields = ['experience']


@register(Block)
class BlockAdmin(BaseUserAdmin):
    fieldsets = (
        (_('Discord fields'), {'fields': ('discord_id', 'name')}),
    )
    inlines = [
        GiftItemBlockInline, GiveawayBlockInline, JourneyBlockInline, MarketExchangeBlockInline,
        ShopItemBlockInline, TransformationBlockInline, RoleGettingMessageBlockInline
    ]
    editable_model_fields = []
