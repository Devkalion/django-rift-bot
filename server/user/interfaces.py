from datetime import timedelta, datetime, timezone
from functools import lru_cache
from typing import Optional, IO

from common.interfaces import IModelIntId, IModel
from inventory import interfaces as inventory_interfaces
from utils import word_for_digit


class IPermission(IModelIntId):
    name: str
    codename: str


class IAdminRole(IModelIntId):
    name: str
    # permissions = models.ManyToManyField(Permission)


class IAdmin(IModelIntId):
    username: str
    role: Optional[IAdminRole]
    role_id: Optional[int]

    is_superuser: bool
    is_active: bool


class IUser(IModel):
    discord_id: int
    name: str
    experience: int

    is_admin: bool
    is_developer: bool
    is_moder: bool

    is_streamer: bool
    is_partner: bool
    channel_url: str

    last_activity_time: datetime
    left: bool

    discord_registration_date: datetime
    registration_date: datetime
    date_joined: datetime

    # roles = models.ManyToManyField('inventory.Role', blank=True)
    # temp_roles = models.ManyToManyField('inventory.Role', through='inventory.TempRole', blank=True, related_name='+')
    # emotions = models.ManyToManyField('inventory.Emotion', blank=True)
    # backgrounds = models.ManyToManyField('inventory.Background', blank=True)

    active_background: Optional['inventory_interfaces.IBackground']
    active_background_id: Optional[int]

    messages: int
    daily_messages: int

    # titles = models.ManyToManyField('inventory.Title', through='inventory.UserTitle', blank=True, related_name='+')
    active_title: Optional['inventory_interfaces.ITitle']
    active_title_id: Optional[int]

    comment: str
    claim_count: int

    @property
    def pk(self):
        return self.discord_id

    @property
    def active(self):
        if self.last_activity_time.tzinfo is None:
            now = datetime.now()
        else:
            now = datetime.now(timezone.utc)
        return not self.left and self.last_activity_time > now - timedelta(days=7)

    @classmethod
    @lru_cache(50)
    def get_exp_for_level(cls, level):
        if level <= 30:
            return int(40 * (level ** 1.5)) - 40
        return int((level ** 2) / 2 + 296.5 * level - 2813)

    @property
    def level(self):
        if self.experience > 6858:
            return int((2 * self.experience + 93538.25) ** 0.5 - 296.5)
        return int(((self.experience + 41) / 40) ** (2 / 3))

    @property
    def time_on_server(self):
        if self.registration_date.tzinfo is None:
            now = datetime.now()
        else:
            now = datetime.now(timezone.utc)
        delta: timedelta = now - self.registration_date
        if delta.days > 365:
            years = now.year - self.registration_date.year
            return f"{years} {word_for_digit(years, 'год', 'года', 'лет')}"
        else:
            return f"{delta.days} {word_for_digit(delta.days, 'день', 'дня', 'дней')}"

    @property
    def donates_sum(self) -> float:
        raise NotImplementedError


class IModerAttachment(IModelIntId):
    file: Optional[IO]
    user: IUser
    user_id: int
    moder: Optional[IAdmin]
    moder_id: Optional[int]
