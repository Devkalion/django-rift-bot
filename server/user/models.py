from django.contrib import auth
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from common.models import CreatedDateMixin
from .interfaces import IAdmin, IUser, IModerAttachment
from .managers import CustomUserManager


class Admin(AbstractBaseUser, IAdmin):
    username_validator = UnicodeUsernameValidator()

    user = models.OneToOneField('user.User', models.PROTECT, null=True)
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )

    role = models.ForeignKey('auth.Group', models.CASCADE, blank=True, null=True)

    is_superuser = models.BooleanField(default=False)
    is_staff = True
    is_active = models.BooleanField(default=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def get_all_permissions(self, obj=None):
        permissions = set()
        name = 'get_all_permissions'
        for backend in auth.get_backends():
            if hasattr(backend, name):
                permissions.update(getattr(backend, name)(self, obj))
        return permissions

    class Meta:
        verbose_name = _('admin')
        verbose_name_plural = _('admins')

    @staticmethod
    def has_module_perms(module):
        return True

    def has_perm(self, permission):
        app_label, permission = permission.split('.')
        return self.is_superuser or self.is_active and self.role.permissions.filter(
            content_type__app_label=app_label, codename=permission
        ).exists()


class User(models.Model, IUser):
    discord_id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    experience = models.IntegerField(default=0)

    is_admin = models.BooleanField(default=False)
    is_developer = models.BooleanField(default=False)
    is_moder = models.BooleanField(default=False)

    is_streamer = models.BooleanField(default=False, verbose_name='Стример')
    is_partner = models.BooleanField(default=False, verbose_name='Партнер')
    channel_url = models.URLField(
        name='channel_url',
        verbose_name='Ссылка на стрим-канал',
        help_text='Опционально. Ссылка на твич или ютуб стрим-канал пользователя',
        default='',
        blank=True,
        null=True,
    )

    last_activity_time = models.DateTimeField(default=now)
    left = models.BooleanField(default=False)

    discord_registration_date = models.DateTimeField()
    registration_date = models.DateTimeField()
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)

    roles = models.ManyToManyField('inventory.Role', blank=True)
    temp_roles = models.ManyToManyField('inventory.Role', through='inventory.TempRole', blank=True, related_name='+')
    emotions = models.ManyToManyField('inventory.Emotion', blank=True)
    backgrounds = models.ManyToManyField('inventory.Background', blank=True)

    active_background = models.ForeignKey('inventory.Background', models.SET_NULL, 'active_users', null=True,
                                          blank=True)

    messages = models.IntegerField(default=0)
    daily_messages = models.IntegerField(default=0)

    titles = models.ManyToManyField('inventory.Title', through='inventory.UserTitle', blank=True, related_name='+')
    active_title = models.ForeignKey('inventory.Title', models.SET_NULL, '+', null=True, blank=True)

    comment = models.TextField(blank=True)
    claim_count = models.PositiveIntegerField(default=0)

    def clean(self):
        if self.is_partner and not self.channel_url:
            raise ValidationError('Channel url for partner is required')

    @property
    def all_roles(self):
        from inventory.models import Role
        query = models.Q(user=self) | models.Q(temprole__user=self, temprole__to_date__gt=now())
        return Role.objects.filter(query).distinct()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    @property
    def donates_sum(self) -> float:
        from donates.models import Donate
        result = Donate.objects.filter(user_id=self.pk).aggregate(models.Sum("sum"))
        return result["sum__sum"]

    def __str__(self):
        return f'{self.name} ({self.discord_id})'


class ModerAttachment(CreatedDateMixin, IModerAttachment):
    file = models.FileField(blank=True, null=True, help_text='File will be available after uploading')
    user = models.ForeignKey(User, models.CASCADE)
    moder = models.ForeignKey(Admin, models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.moder}'


class Inventory(User):
    class Meta:
        verbose_name = _('inventory')
        verbose_name_plural = _('inventories')
        proxy = True


class Storage(User):
    class Meta:
        verbose_name = _('storage')
        verbose_name_plural = _('storages')
        proxy = True


class Experience(User):
    class Meta:
        verbose_name = _('experience')
        verbose_name_plural = _('experiences')
        proxy = True


class Block(User):
    class Meta:
        verbose_name = _('user block')
        verbose_name_plural = _('user blocks')
        proxy = True


class Offences(User):
    class Meta:
        verbose_name = _('user offences')
        verbose_name_plural = _('users offences')
        proxy = True
