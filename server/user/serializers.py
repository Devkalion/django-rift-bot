from django.db.models import QuerySet
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from inventory.models import Background, Role, Title, UserTitle
from .models import User


class RoleIdField(serializers.Field):
    @staticmethod
    def to_internal_value(data):
        return Role.objects.get(discord_id=data)

    @staticmethod
    def to_representation(role):
        return role.discord_id


class RoleIdsSerializer(serializers.ListSerializer):
    child = RoleIdField()

    def to_representation(self, data):
        if isinstance(data, QuerySet):
            data = data.only('discord_id')
        return super().to_representation(data)


class SmallUserSerializer(serializers.ModelSerializer):
    roles = RoleIdsSerializer(read_only=True, source='all_roles')

    class Meta:
        model = User
        fields = (
            'discord_id', 'name', 'roles', 'left', 'is_streamer', 'is_partner',
        )


class UserSerializer(SmallUserSerializer):
    class Meta:
        model = User
        fields = (
            'discord_id', 'name', 'experience', 'is_admin', 'is_developer', 'is_moder', 'is_streamer', 'is_partner',
            'level', 'active_background', 'active_title', 'discord_registration_date',
            'registration_date', 'roles', 'left',
        )
        extra_kwargs = {
            'registration_date': {'write_only': True},
            'discord_registration_date': {'write_only': True},
        }

    def validate_active_background(self, value: Background):
        if self.instance is None or not value:
            return None
        queryset = self.instance.backgrounds.filter(id=value.id)
        if queryset.exists():
            return value
        else:
            raise ValidationError

    def validate_active_title(self, value: Title):
        if self.instance is None or not value:
            return None
        if UserTitle.objects.filter(user=self.instance, title=value).exists():
            return value
        else:
            raise ValidationError


class AmountSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=1, required=True)
