from user.services.django_implementations import UserQuery
from user.services.fabrics import UserServiceFabric

UserService = UserServiceFabric(
    query=UserQuery()
)
