from abc import ABC
from typing import Iterable

from common.services import BaseObjectQuery
from inventory.interfaces import IRole
from ..interfaces import IUser


class BaseUserQuery(BaseObjectQuery[IUser], ABC):  # pragma nocover
    def add_experience(self, user: IUser, experience: int) -> IUser:
        raise NotImplementedError

    def increase_messages(self, user: IUser) -> None:
        raise NotImplementedError

    def delete_temp_roles(self, user: IUser, *roles: Iterable[IRole]) -> None:
        raise NotImplementedError

    def add_roles(self, user: IUser, *roles: Iterable[IRole]) -> None:
        raise NotImplementedError

    def update_last_activity_time(self, user: IUser) -> None:
        raise NotImplementedError
