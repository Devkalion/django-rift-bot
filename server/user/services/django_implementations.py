from typing import Iterable

from django.db.models import F
from django.utils.timezone import now

from common.services.django_implementations import DjangoObjectQuery
from inventory.models import Role, TempRole
from .base import BaseUserQuery
from ..interfaces import IUser
from ..models import User


class UserQuery(BaseUserQuery, DjangoObjectQuery[IUser]):
    model = User

    def update_last_activity_time(self, user: IUser) -> None:
        User.objects.filter(pk=user.pk).update(
            last_activity_time=now()
        )

    def delete_temp_roles(self, user: IUser, *roles: Iterable[Role]) -> None:
        TempRole.objects.filter(user_id=user.pk, role__in=roles).delete()

    def increase_messages(self, user: IUser) -> None:
        User.objects.filter(pk=user.pk).update(
            messages=F('messages') + 1,
            daily_messages=F('daily_messages') + 1
        )

    def add_roles(self, user: User, *roles: Iterable[Role]) -> None:
        user.roles.add(*roles)

    def add_experience(self, user: IUser, experience: int) -> IUser:
        User.objects.filter(pk=user.pk).update(
            experience=F('experience') + experience
        )
        return User.objects.get(pk=user.pk)
