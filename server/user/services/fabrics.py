from typing import Iterable

from common.services import BaseObjectFabric
from events.enums import EventType
from events.services import EventService
from inventory.interfaces import IRole
from .base import BaseUserQuery
from ..interfaces import IUser


class UserServiceFabric(BaseObjectFabric[IUser]):
    query: BaseUserQuery

    def add_experience(self, user: IUser, experience: int):
        old_lvl = user.level
        user = self.query.add_experience(user, experience)

        for level in range(old_lvl, user.level):
            EventService.create_event(EventType.LEVEL_UP, data=str(level + 1), user_id=user.pk)

    def add_roles(self, user: IUser, *roles: Iterable[IRole]):
        if roles:
            self.query.add_roles(user, *roles)
            self.query.delete_temp_roles(user, *roles)

    def add_message(self, user: IUser):
        self.query.increase_messages(user)

    def touch(self, user: IUser):
        self.query.update_last_activity_time(user)
