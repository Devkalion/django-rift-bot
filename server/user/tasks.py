from schedule.celery import app
from .models import User


@app.task
def clear_daily_messages():
    User.objects.update(daily_messages=0)
