from datetime import datetime, timedelta, timezone
from unittest import TestCase, mock

from django.urls import reverse
from rest_framework.response import Response
from rest_framework.test import APIClient

from events.models import Event
from inventory.models import Role, Title, Background
from schedule.celery import CustomTask
from user.models import User
from user.services import UserService

api_client = APIClient()


class UserTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.role: Role = Role.objects.create(
            discord_id=321,
        )
        cls.user: User = User.objects.create(
            discord_id=123,
            discord_registration_date=datetime.now(timezone.utc),
            registration_date=datetime.now(timezone.utc),
            last_activity_time=datetime.now(timezone.utc),
        )

    def test_properties(self):
        self.assertEqual(self.user.level, 1)
        self.assertEqual(User.get_exp_for_level(1), 0)
        self.assertEqual(User.get_exp_for_level(20), 3537)
        self.assertEqual(User.get_exp_for_level(50), 13262)
        self.user.experience = User.get_exp_for_level(50)
        self.assertEqual(self.user.level, 50)
        self.user.experience = User.get_exp_for_level(50) - 1
        self.assertEqual(self.user.level, 49)
        self.assertIsNotNone(self.user.all_roles)

        # Check timezoned datetimes
        self.assertEqual(self.user.time_on_server, "0 дней")
        self.assertTrue(self.user.active)

        # Check non timezone datetimes
        self.user.discord_registration_date = datetime.now()
        self.user.registration_date = datetime.now()
        self.user.last_activity_time = datetime.now()
        self.assertEqual(self.user.time_on_server, "0 дней")
        self.assertTrue(self.user.active)

        self.user.registration_date = datetime.now() - timedelta(days=730)
        self.assertEqual(self.user.time_on_server, "2 года")

    def test_list(self):
        url = reverse("user-list")
        UserService.add_roles(self.user, self.role)

        response: Response = api_client.get(url)
        self.assertIsNotNone(response)
        self.assertEquals(response.data["count"], 1)
        user_dict = response.data["results"][0]
        self.assertEquals(user_dict["discord_id"], self.user.discord_id)
        self.assertEquals(user_dict["roles"][0], self.role.discord_id)

    def test_detail(self):
        url = reverse("user-detail", kwargs={"pk": self.user.pk})
        UserService.add_roles(self.user, self.role)
        response: Response = api_client.get(url)
        self.assertIsNotNone(response)
        user_dict = response.data
        self.assertEquals(user_dict["discord_id"], self.user.discord_id)
        self.assertEquals(user_dict["roles"][0], self.role.discord_id)

    def test_update(self):
        url = reverse("user-detail", kwargs={"pk": self.user.pk})

        payload = {"active_title": -1, "active_background": -1}
        response: Response = api_client.patch(url, data=payload, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertIn("active_title", response.data)
        self.assertIn("active_background", response.data)

        title = Title.objects.create()
        background = Background.objects.create()

        payload = {"active_title": title.pk, "active_background": background.pk}
        response: Response = api_client.patch(url, data=payload, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertIn("active_title", response.data)
        self.assertIn("active_background", response.data)

        self.user.backgrounds.add(background)
        self.user.titles.add(title)
        response: Response = api_client.patch(url, data=payload, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertIn("active_title", response.data)
        self.assertIn("active_background", response.data)

        self.user.refresh_from_db()

        self.assertEquals(self.user.active_background, background)
        self.assertEquals(self.user.active_title, title)

        payload = {"active_title": None, "active_background": None}
        response: Response = api_client.patch(url, data=payload, format="json")
        self.assertEqual(response.status_code, 200)
        self.user.refresh_from_db()

        self.assertIsNone(self.user.active_background)
        self.assertIsNone(self.user.active_title)

    def test_touch_user(self):
        url = reverse("user-touch", kwargs={"pk": self.user.pk})

        self.user.last_activity_time = datetime.now(timezone.utc) - timedelta(days=10)
        self.user.save(update_fields=["last_activity_time"])

        self.assertFalse(self.user.active)

        response: Response = api_client.post(url)
        self.assertIsNotNone(response)
        self.user.refresh_from_db()

        self.assertTrue(self.user.active)

    def test_message(self):
        url = reverse("user-add-message", kwargs={"pk": self.user.pk})

        self.assertEqual(self.user.messages, 0)
        self.assertEqual(self.user.daily_messages, 0)

        response: Response = api_client.post(url)
        self.assertIsNotNone(response)
        self.user.refresh_from_db()

        self.assertEqual(self.user.messages, 1)
        self.assertEqual(self.user.daily_messages, 1)

    def test_adding_roles(self):
        url = reverse("user-add-roles", kwargs={"pk": self.user.pk})
        payload = [self.role.discord_id]

        self.assertEquals(len(list(self.user.all_roles)), 0)
        self.addCleanup(self.user.roles.clear)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertIsNotNone(response)

        user_roles = list(self.user.all_roles)

        self.assertEquals(len(user_roles), 1)
        self.assertEquals(user_roles[0], self.role)

    def test_adding_experience(self):
        url = reverse("user-add-experience", kwargs={"pk": self.user.pk})
        payload = {"amount": 100}
        published_events = []

        def add_published_events(*args, **kwargs):
            published_events.append({"args": args, "kwargs": kwargs})

        CustomTask.apply_async = mock.Mock(side_effect=add_published_events)

        self.assertEquals(self.user.experience, 0)
        self.addCleanup(User.objects.filter(pk=self.user.pk).update, experience=0)

        response: Response = api_client.post(url, data=payload, format="json")
        self.assertIsNotNone(response)

        self.user.refresh_from_db()

        self.assertEquals(self.user.level, 2)
        self.assertEquals(self.user.experience, payload["amount"])

        event: Event = Event.objects.filter(user_id=self.user.pk).first()
        self.assertIsNotNone(event)
        self.assertEquals(event.data, "2")
        self.assertEquals(len(published_events), 1)
        self.assertEquals(published_events[0]["kwargs"]["args"], [event.pk])

    @classmethod
    def tearDownClass(cls) -> None:
        cls.role.delete()
        cls.user.delete()
