from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response

from .models import User
from .serializers import UserSerializer, AmountSerializer, RoleIdsSerializer, SmallUserSerializer
from .services import UserService


class Pagination(LimitOffsetPagination):
    default_limit = settings.USER_PAGE_SIZE


class UserViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = User.objects.all()
    pagination_class = Pagination
    serializer_class = UserSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return SmallUserSerializer
        return super().get_serializer_class()

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action == 'list':
            queryset = queryset.only('discord_id', 'name', 'left')
        return queryset

    @action(detail=True, methods=['post'], serializer_class=AmountSerializer)
    def add_experience(self, request, pk=None):
        user: User = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserService.add_experience(user, serializer.data['amount'])

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['post'], serializer_class=RoleIdsSerializer)
    def add_roles(self, request, pk=None):
        user: User = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        roles = serializer.validated_data
        UserService.add_roles(user, *roles)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['post'])
    def add_message(self, request, pk=None):
        user: User = self.get_object()
        UserService.add_message(user)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['post'])
    def touch(self, request, pk=None):
        user: User = self.get_object()
        UserService.touch(user)
        return Response(status=status.HTTP_204_NO_CONTENT)
