def word_for_digit(number: int, word_1: str, word_2: str, word_5: str, word_0: str = None):
    if word_0 is None:
        word_0 = word_5
    if number == 0:
        return word_0
    number = number % 100
    if number // 10 != 1:
        if number % 10 == 1:
            return word_1
        if 1 < number % 10 < 5:
            return word_2
    return word_5
